subroutine Cross_section_e_Hm(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n


!initialization

      !if(iter==Iteration_Number .OR. iter==0) then  
	if(.not. allocated(f2)) then        
	  open(100,file = "e_Hm-2e_Hp.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=45
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      endif

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		fint=f2(1)
	
	else


		if(i>37198) write(6,*) "AAAAAAAA",rank,iter,cross,x2(1),dx2,n,E_e
		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_e_Hm







subroutine Cross_section_Hm_Hp(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n


!initialization

      !if(iter==Iteration_Number .OR. iter==0) then  
	if(.not. allocated(f2)) then        
	  open(100,file = "Hm_Hp-H_H.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=50
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      endif

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		fint=f2(1)
	
	else


		!if(i>37198) write(6,*) "AAAAAAAA",rank,iter,cross,x2(1),dx2,n,E_e
		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_Hm_Hp







subroutine Cross_section_Hm_H(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n


    
!initialization

     !if(iter==Iteration_Number .OR. iter==0) then  
	if(.not. allocated(f2)) then       
	   open(100,file = "Hm_H-H2_e.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=50
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      endif

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		fint=f2(1)
	
	else



		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_Hm_H















subroutine Cross_section_Hm_H2(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n


  !initialization

     !if(iter==Iteration_Number .OR. iter==0) then  
	if(.not. allocated(f2)) then       
	   open(100,file = "Hm_H-H_H_e.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=50
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      endif

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		fint=f2(1)
	
	else



		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_Hm_H2
















subroutine Cross_section_Hm_H_charge_exange(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n


  

!initialization

      !if(iter==Iteration_Number .OR. iter==0) then  
	if(.not. allocated(f2)) then        
	  open(100,file = "Hm_H_charge_exange.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=50
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      endif

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		fint=f2(1)
	
	else



		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_Hm_H_charge_exange






subroutine Cross_section_Hm_H2_elastic_collision(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n


     

!initialization

      !if(iter==Iteration_Number .OR. iter==0) then  
       if(.not. allocated(f2)) then 
	  open(100,file = "Hm_H2_elastic_collision.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=50
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      endif

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		fint=f2(1)
	
	else



		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_Hm_H2_elastic_collision


subroutine Cross_section_e_H2v_Dissociative_attachment(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n


     
!initialization

      !if(iter==Iteration_Number .OR. iter==0) then  
       if(.not. allocated(f2)) then  
	 open(100,file = "e_H2v_Dissociative_attachment.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=50
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      endif

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		if(i<1) fint=f2(1)
			if(i>n) fint=f2(n)
	
	else



		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_e_H2v_Dissociative_attachment




!interpolation of the energy and find cross section for reaction: ELASTIC COLLISION : e+H->e+H
subroutine Cross_section_e_plus_h_equal_e_plus_H_elastic(cross,fint)
	use Parallel
      IMPLICIT NONE
      REAL, INTENT(IN) :: cross
      REAL, INTENT(out) :: fint
      INTEGER i,j
      REAL,save:: dx2
      REAL x1(1000),f1(1000)
      REAL,ALLOCATABLE,SAVE :: x2(:),f2(:)
      integer,save :: nn,n
      	

!initialization
      !if(iter==Iteration_Number .OR. iter==0 ) then      
       if(.not. allocated(f2)) then 
       
	      
         open(100,file = "e_plus_h_equal_e_plus_H_elastic.dat", status = 'unknown')
!the dataset to be interpolated has nn values
!it is assumed that the first energy step in the dataset is not larger than any of the others


         nn=50
         do j=1,nn
            read(100,*) x1(j),f1(j)
         enddo
		close( unit=100 )


         dx2=(x1(2)-x1(1))*.95
	

         n=1.*(x1(nn)-x1(1))/dx2+1
         ALLOCATE(f2(n),x2(n))


         f2(1)=f1(1)
         f2(n)=f1(nn)

         do i=1,n
            x2(i)=dx2*(i-1)+x1(1)
		!write(100+rank,*) x2(i)
         enddo


	!write(6,*) iter,rank,x2(n),n
	

         j=1
         do i=2,n-1
            if(x2(i)>x1(j+1)) j=j+1
            f2(i)=(f1(j)*(x1(j+1)-x2(i))+f1(j+1)*(x2(i)-x1(j)))/(x1(j+1)-x1(j))         
         enddo
      
      endif	

!write(6,*) iter,rank,x2(n),n
	 	i=(cross-x2(1))/dx2+1
      if(i<1 .or. i>=n) then
         	!write(6,*) 'Problem with interpolation',iter, rank,cross,x2(1),x2(n)
        	 !stop
		if(i<1) fint=f2(1)
			if(i>n) fint=f2(n)
	
	else



		fint=f2(i)*(x2(i+1)-cross)/dx2+f2(i+1)*(cross-x2(i))/dx2

	endif

     

END subroutine Cross_section_e_plus_h_equal_e_plus_H_elastic




