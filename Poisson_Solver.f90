subroutine Solve_Potential_bias()

  use Parallel
  implicit none
  include 'mpif.h'
  integer  i,j,k
  real coeffx, coeffy, coeffz, coeff
  
  double precision, allocatable :: time(:)
  double precision, allocatable :: timetot(:)

  allocate(time(1:20))
  allocate(timetot(1:19))

  coeffx = 1./delta(0)/delta(0)
  coeffy = 1./delta(1)/delta(1)
  coeffz = 1./delta(2)/delta(2)
  coeff = -2.*(coeffx+coeffy+coeffz)

  error=0
  r=0.0
  d=0.0
  z=0.0
  Ad=0.0
  C=1.0
  dx=0.0
  dy=0.0
  dz=0.0
  temp=0.0
  alpha=0.0
  beta=0.0
  err_old=0.0
  !V=0.0

  rho_Sec=-rho_Sec/8.85e-12
  iteration=0

  
  !	I put maximum iteration in the Poisson Solver for first iteration and for all others
  !	Also put external extraction and bias potential for all walls
  
  if(iter==0 .OR. iter==Iteration_Number) then
    Total_Iterat=100000
    do k = k0, k1
      do j = j0, j1
        do i = i0, i1
          !if(Inzone(i,j,k) .NE. 0) V(i,j,k) = The_Object(Inzone(i,j,k))%potential
          if(Inzone(i,j,k) ==4) then
	      V(i,j,k)=V_EG(j,k)
	  else	
	      V(i,j,k) = The_Object(Inzone(i,j,k))%potential	
	  endif
	enddo
      enddo
    enddo
  else
    Total_Iterat=800
  endif

  !alphax=1.
  !alphay=1.
  !alphaz=1.
  
  do k = k0, k1
    do j = j0, j1
      do i = i0, i1
        if(Inzone(i,j,k) == 0) then
          dx= V(i+1,j,k)*coefficentLaplacianF(i,j,k)+V(i-1,j,k)*coefficentLaplacianB(i,j,k) &
            + V(i,j+1,k)*coefficentLaplacianR(i,j,k)+V(i,j-1,k)*coefficentLaplacianL(i,j,k) &
            + V(i,j,k+1)*coefficentLaplacianU(i,j,k)+V(i,j,k-1)*coefficentLaplacianD(i,j,k) &
            + V(i,j,k)*coefficentLaplacianM(i,j,k)
          r(i,j,k)=rho_Sec(i,j,k)-dx
      
          !Calculation precondition conjugate gradien matrix C
          !C(i,j,k)=((1.+1./alphax(i,j,k))/(delta(0)*delta(0))+(1.+1./alphay(i,j,k))/(delta(1)*delta(1))+(1.+1./alphaz(i,j,k))/(delta(2)*delta(2)))
          C(i,j,k)=-1.*coefficentLaplacianM(i,j,k)
          !C(i,j,k)=1
          z(i,j,k)=r(i,j,k)/C(i,j,k) !z=C^-1*r
        endif
      end do
    end do
  end do


  d=z
  error=sum(z*r)
  
  call MPI_ALLREDUCE(error, aux, 1, MPI_real, MPI_SUM, MPI_COMM_WORLD,info)
  error=aux !initial error

  !Calculation Poisson Solver
  
  do while ((error > tolerance) .AND. (iteration  < Total_Iterat))
   
    !call MPI_Barrier(MPI_COMM_WORLD, info)
    !time(1) = MPI_WTIME()

    call Send_Plane(d)
    !call Send_Plane_v2(d)

    !call MPI_Barrier(MPI_COMM_WORLD, info)
    !time(2) = MPI_WTIME()

    temp=0.0
    do k = k0, k1
    !D IR$ UNROLL(4)
      !D IR$ SIMD
      do j = j0, j1
      !D IR$ UNROLL(4)
      !D IR$ SIMD
        do i = i0, i1
          !if(Inzone(i,j,k) == 0) then
            if(Inzone2(i,j,k) == 0) then
              Ad(i,j,k) = (d(i+1,j,k)+d(i-1,j,k))*coeffx &
                        + (d(i,j+1,k)+d(i,j-1,k))*coeffy &
                        + (d(i,j,k+1)+d(i,j,k-1))*coeffz &
                        + d(i,j,k)*coeff
              temp = temp + Ad(i,j,k)*d(i,j,k)
            else if(Inzone2(i,j,k)==1) then
              Ad(i,j,k) = d(i+1,j,k)*coefficentLaplacianF(i,j,k)+d(i-1,j,k)*coefficentLaplacianB(i,j,k) &
                        + d(i,j+1,k)*coefficentLaplacianR(i,j,k)+d(i,j-1,k)*coefficentLaplacianL(i,j,k) &
                        + d(i,j,k+1)*coefficentLaplacianU(i,j,k)+d(i,j,k-1)*coefficentLaplacianD(i,j,k) &
                        + d(i,j,k)*coefficentLaplacianM(i,j,k)!)*Inzone2(i,j,k)
              temp = temp + Ad(i,j,k)*d(i,j,k)
            endif
            !temp = temp + Ad(i,j,k)*d(i,j,k)
          !endif
        end do
      end do
    end do
    
    !call MPI_Barrier(MPI_COMM_WORLD, info)
    !time(3) = MPI_WTIME()

    !Ad(i0:i1,j0:j1,k0:k1) = Ad(i0:i1,j0:j1,k0:k1)*Inzone2(i0:i1,j0:j1,k0:k1)

    aux=0
    !temp=sum(d*Ad)
    !temp=sum(d(i0:i1,j0:j1,k0:k1)*Ad(i0:i1,j0:j1,k0:k1))
    
    !call MPI_Barrier(MPI_COMM_WORLD, info)
    !time(4) = MPI_WTIME()

    call MPI_ALLREDUCE(temp, aux, 1, MPI_real, MPI_SUM, MPI_COMM_WORLD,info)
    temp=aux
    
    !call MPI_Barrier(MPI_COMM_WORLD, info)
    !time(5) = MPI_WTIME()

    alpha = error/temp !alpha=(z*r)/(d*Ad)
    temp=0.0
    

    V=V+alpha*d!Modification V and r
    !call SAXPY(nx*ny*nz, alpha, d, 1, V, 1)
    r=r-alpha*Ad

    z=r/C
    
    aux=0
    temp=sum(z*r)
    
    !call MPI_Barrier(MPI_COMM_WORLD, info)
    !time(6) = MPI_WTIME()

    !temp=sum(z(i0:i1,j0:j1,k0:k1)*r(i0:i1,j0:j1,k0:k1))
    call MPI_ALLREDUCE( temp, aux, 1, MPI_real, MPI_SUM, MPI_COMM_WORLD,info) 
    temp=aux

    err_old=error
    error=temp
    beta = error/err_old !betta=(z(i+1)*r(i+1))/(z*r)
    
    d=z+beta*d

    iteration=iteration+1
    
    !call MPI_Barrier(MPI_COMM_WORLD, info)
    !time(7) = MPI_WTIME()

    !do i=1,6
    !  timetot(i) = timetot(i) + time(i+1) - time(i)
    !end do

    !if(rank==0) write(*,*) iteration, error

  enddo

  !do i=1,6
  !  write(*,*) rank, i, timetot(i)
  !end do

  !if(rank==0) write(*,*) "poisson", iteration, error, tolerance

end subroutine Solve_Potential_bias

 subroutine Define_Laplacian_Coefficient()
 
  use Parallel
 	implicit none
 	include 'mpif.h'
 	
 	integer  i,j,k

  coefficentLaplacianF = 0.
  coefficentLaplacianB = 0.
  coefficentLaplacianR = 0.
  coefficentLaplacianL = 0.
  coefficentLaplacianU = 0.
  coefficentLaplacianD = 0.
  coefficentLaplacianM = 0.
 	
 	do k = k0, k1
 			do j = j0, j1
 				do i = i0, i1
 					if(Inzone(i,j,k) == 0) then

            if(alphax(i,j,k) .NE. 1.) then
              if(alphax(i,j,k) < 0) then
                coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - (1.+1./abs(alphax(i,j,k)))/delta(0)/delta(0) 
                coefficentLaplacianB(i, j, k) = 1./abs(alphax(i,j,k))/delta(0)/delta(0)
                coefficentLaplacianF(i, j, k) = 1./delta(0)/delta(0)
              else
                coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - (1.+1./alphax(i,j,k))/delta(0)/delta(0) 
                coefficentLaplacianF(i, j, k) = 1./alphax(i,j,k)/delta(0)/delta(0)
                coefficentLaplacianB(i, j, k) = 1./delta(0)/delta(0)
              end if
            else
              coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - 2./delta(0)/delta(0) 
              coefficentLaplacianF(i, j, k) = 1./delta(0)/delta(0)
              coefficentLaplacianB(i, j, k) = 1./delta(0)/delta(0)
            end if

            if(alphay(i,j,k) .NE. 1.) then
              if(alphay(i,j,k) < 0) then
                coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - (1.+1./abs(alphay(i,j,k)))/delta(1)/delta(1) 
                coefficentLaplacianL(i, j, k) = 1./abs(alphay(i,j,k))/delta(1)/delta(1)
                coefficentLaplacianR(i, j, k) = 1./delta(1)/delta(1)
              else
                coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - (1.+1./alphay(i,j,k))/delta(1)/delta(1) 
                coefficentLaplacianR(i, j, k) = 1./alphay(i,j,k)/delta(1)/delta(1)
                coefficentLaplacianL(i, j, k) = 1./delta(1)/delta(1)
              end if
            else
              coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - 2./delta(1)/delta(1) 
              coefficentLaplacianR(i, j, k) = 1./delta(1)/delta(1)
              coefficentLaplacianL(i, j, k) = 1./delta(1)/delta(1)
            end if

            if(alphaz(i,j,k) .NE. 1.) then
              if(alphaz(i,j,k) < 0) then
                coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - (1.+1./abs(alphaz(i,j,k)))/delta(2)/delta(2) 
                coefficentLaplacianD(i, j, k) = 1./abs(alphaz(i,j,k))/delta(2)/delta(2)
                coefficentLaplacianU(i, j, k) = 1./delta(2)/delta(2)
              else
                coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - (1.+1./alphaz(i,j,k))/delta(2)/delta(2) 
                coefficentLaplacianU(i, j, k) = 1./alphaz(i,j,k)/delta(2)/delta(2)
                coefficentLaplacianD(i, j, k) = 1./delta(2)/delta(2)
              end if
            else
              coefficentLaplacianM(i,j,k) = coefficentLaplacianM(i,j,k) - 2./delta(2)/delta(2) 
              coefficentLaplacianU(i, j, k) = 1./delta(2)/delta(2)
              coefficentLaplacianD(i, j, k) = 1./delta(2)/delta(2)
            end if

            !coefficentLaplacianF(i, j, k) = 1./delta(0)/delta(0)
            !coefficentLaplacianB(i, j, k) = 1./delta(0)/delta(0)
            !coefficentLaplacianR(i, j, k) = 1./delta(1)/delta(1)
            !coefficentLaplacianL(i, j, k) = 1./delta(1)/delta(1)
            !coefficentLaplacianU(i, j, k) = 1./delta(2)/delta(2)
            !coefficentLaplacianD(i, j, k) = 1./delta(2)/delta(2)
            
          endif
      	end do
   		end do
    end do

do k = k0, k1
 			do j = j0, j1
 				do i = i0, i1
        if(Inzone(i,j,k)==0) then
          if(coefficentLaplacianM(i,j,k) == 0) then
            write(*,*) i,j,k,coefficentLaplacianM(i,j,k)
          end if
            end if
            end do
            end do
            end do
 
 end subroutine Define_Laplacian_Coefficient

 subroutine Check_Alpha_Coefficient()

  use Parallel
 	implicit none
 	include 'mpif.h'
 	
 	integer  i,j,k

  real epsil 
  epsil = 1e-6
  
  do k = k0, k1
    do j = j0, j1
      do i = i0, i1
        if(abs(alphax(i,j,k)) > 1.) then
          write(*,*) "Alphax > 1: ", i, j, k, alphax(i,j,k)
        end if
        if(abs(alphay(i,j,k)) > 1.) then
          write(*,*) "Alphay > 1: ", i, j, k, alphay(i,j,k)
        end if
        if(abs(alphaz(i,j,k)) > 1.) then
          write(*,*) "Alphaz > 1: ", i, j, k, alphaz(i,j,k)
        end if
        if(abs(alphax(i,j,k)) < epsil) then
          write(*,*) "Alphax small: ", i, j, k, alphax(i,j,k)
        end if
        if(abs(alphay(i,j,k)) < epsil) then
          write(*,*) "Alphay small: ", i, j, k, alphay(i,j,k)
        end if
        if(abs(alphaz(i,j,k)) < epsil) then
          write(*,*) "Alphaz small: ", i, j, k, alphaz(i,j,k)
        end if
        if(alphax(i,j,k) == 0.) then
          write(*,*) "Alphax = 0: ", i, j, k
        end if
        if(alphay(i,j,k) == 0.) then
          write(*,*) "Alphay = 0: ", i, j, k
        end if
        if(alphaz(i,j,k) == 0.) then
          write(*,*) "Alphaz = 0: ", i, j, k
        end if
        if(alphax(i,j,k) /= alphax(i,j,k)) then
          write(*,*) "Alphax = nan: ", i, j, k
        end if
        if(alphay(i,j,k) /= alphay(i,j,k)) then
          write(*,*) "Alphay = nan: ", i, j, k
        end if
        if(alphaz(i,j,k) /= alphaz(i,j,k)) then
          write(*,*) "Alphaz = nan: ", i, j, k
        end if
      end do
    end do
  end do


 end subroutine Check_Alpha_Coefficient
