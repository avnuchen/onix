subroutine Collisions_test()
	use Parallel
	implicit none
	
	integer :: i=0
	integer :: j=0
	integer :: k=0
	integer :: sp_2=0
	integer :: loop_l=0
	
	integer :: Collission_appears=0 ! For performing only 1 collision
	
	
	real :: Collission_velocity1=0 ! For performing only 1 collision
	real :: Collission_velocity2=0 ! For performing only 1 collision


	real::   rand_coll=0
	real::   Dens_H_const=1e19
	real::   Dens_H2v_const=3.06e18
	real::   ener=0
	real::   Velocity_elastic_new=0
	
	real::   rand_coll1=0
	real::   rand_coll2=0
	real::   rand_coll3=0
	real::   rand_energy_division=0
	real::   Energ_1=0
	real::   Energ_2=0


	


   	  

	Coll_e=0
 	Coll_H2p=0
	Coll_Hm=0
	Coll_Hp=0
	Delete_Hm_pos_tar=0
	Delete_Hp_pos_tar=0
	Delete_e=0
	Delete_e_pos=0

	ener=0
	
	do loop_l=1,Npart

		 Collission_appears=0 


		i=P(loop_l)%X/delta(0)
		j=P(loop_l)%Y/delta(1)
		k=P(loop_l)%Z/delta(2)
    
    if(i<i0-1 .OR. i> i1+1 .OR. j<j0-1 .OR. j>j1+1 .OR. k<k0-1 .OR. k>k1+1) then
      ! write(*,*) "!!!", P(loop_l)%X, P(loop_l)%Y, P(loop_l)%Z, P(loop_l)%VX, &
      ! P(loop_l)%VY, P(loop_l)%VZ, P(loop_l)%sp, i0, j0, k0, P(loop_l)%IdentPart, &
      ! px_arr(loop_l), py_arr(loop_l), pz_arr(loop_l), &
      ! pvx_arr(loop_l), pvy_arr(loop_l), pvz_arr(loop_l), &
      ! ex_arr(loop_l), ey_arr(loop_l), ez_arr(loop_l), &
      ! bx_arr(loop_l), by_arr(loop_l), bz_arr(loop_l), loop_l 

       Delete_e=Delete_e+1
       Delete_e_pos(Delete_e)=loop_l
     else


		sp_2=P(loop_l)%sp

		Select case (sp_2)


			case(0)


			      !Elastic collision e-  plus  H-> e-   plus H for 1s state====================================================================================================


				!Kinetic energy E=1/2mv^2 transformed from joules to eV (factor *6.2415e+18 )
				E_e=(mass(P(loop_l)%sp)*(P(loop_l)%VX**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VY**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VZ**2)/2.0 )*6.2415e+18 
				Vellocity=sqrt(P(loop_l)%VX**2+P(loop_l)%VY**2+P(loop_l)%VZ**2)

!				!Finding cross-section of the reaction from the external file taken from ALLADIN database
				call Cross_section_e_plus_h_equal_e_plus_H_elastic(E_e,Cross_sect) 

				!Calculation of the collision probability
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)

						!Check the threshold energy and collision probability
! 						if(rand_coll<Probab_coll .AND. E_e>0.001 .AND. E_e<999 .AND. Collission_appears==0) then
						  if(rand_coll<Probab_coll .AND. E_e>0.001  .AND.  E_e<500 .AND. Collission_appears==0) then

							  !write(50000+rank,'(1(1x,I8) ,3(1x,E11.4) )') iter,Probab_coll,E_e,Cross_sect

							   !Recalculation new mean absolute velocity of the electron assumint that velocity of H gas is 0
							    Velocity_elastic_new=abs(Vellocity*(mass(P(loop_l)%sp)-mass(6))/(mass(P(loop_l)%sp) +mass(6)))

							 ! write(4500+rank,*) iter,E_e,Vellocity,Velocity_elastic_new

							    !First random number for distributing new velocity among 3 direction: FROM -1 to 1
							    call random_number(rand_coll1)
							    rand_coll1=rand_coll1*2-1				 
							    P(loop_l)%VX=Velocity_elastic_new*rand_coll1

							    !Second random number for distributing new velocity among 3 direction: FROM -1+rand_coll1 to 1-rand_coll1
							    call random_number(rand_coll2)

							    !rand_coll2=rand_coll2*2*(1-abs(rand_coll1))-(1-abs(rand_coll1))
							    rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
							    P(loop_l)%VY=Velocity_elastic_new*rand_coll2

							    !Third random number for distributing new velocity among 3 direction: rand_coll1+rand_coll2+rand_coll3 must be equal 1
							     call random_number(rand_coll3)

							     !Chose the velocity direction
							      if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							      else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							      endif
				    
							      P(loop_l)%VZ=Velocity_elastic_new*rand_coll3

							!Test energy of the reflected electron
							! E_e=(mass(P(loop_l).sp)*(P(loop_l).VX**2)/2.0+mass(P(loop_l).sp)*(P(loop_l).VY**2)/2.0+mass(P(loop_l).sp)*(P(loop_l).VZ**2)/2.0 )*6.2415e+18 


							Collission_appears=1

							 !Test velocity of the reflected electron
							 !Vellocity=sqrt(P(loop_l).VX**2+P(loop_l).VY**2+P(loop_l).VZ**2)

							! write(4400+rank,*) iter,E_e,Vellocity
							! write(4600+rank,*) iter,P(loop_l).VX,P(loop_l).VY,P(loop_l).VZ

						 endif

!==================================================================END =Elastic collision e-  plus  H-> e-   plus H for 1s state
							
!Electron Detachment electron + H- = H   + electron + electron ------------------------------------------------------------------------------------------

				call Cross_section_e_Hm(E_e,Cross_sect)  
				 				
				Probab_coll=Dens_Hm(i,j,k)*Vellocity*Cross_sect*dt*time_collision
			     ! Probab_coll=0.001
				call random_number(rand_coll)



				if(rand_coll<Probab_coll .AND. E_e>9.06e-1 .AND. E_e<10000  .AND. Collission_appears==0) then

							  !write(50100+rank,'(1(1x,I8) ,4(1x,E11.4) )') iter,Probab_coll,E_e,Cross_sect,Dens_Hm(i,j,k)

						!Deleting H minus
						Delete_Hm_pos_tar(i,j,k)=Delete_Hm_pos_tar(i,j,k)+1

						! Energy of the ionization						
						E_e=E_e-9.06e-1 
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						call random_number(rand_energy_division)

						rand_energy_division=1-(rand_energy_division*0.1)

						!Energy division between two new electrons- first one has energy from 90 to 100, second from 0 to 10 procent of the incident energy


						Energ_1=rand_energy_division*E_e
						Energ_2=(1-rand_energy_division)*E_e


						Collission_velocity1=sqrt(2.0*Energ_1/mass(P(loop_l)%sp))
						Collission_velocity2=sqrt(2.0*Energ_2/mass(P(loop_l)%sp))



						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3


						!Creation of the second electron

					         call Add_new_e_coll_velocity(loop_l,Collission_velocity2)
	
					        !E_e=(mass(P(loop_l).sp)*(P(loop_l).VX**2)/2.0+mass(P(loop_l).sp)*(P(loop_l).VY**2)/2.0+mass(P(loop_l).sp)*(P(loop_l).VZ**2)/2.0 )*6.2415e+18 
						!write(6000+rank,*) iter,loop_l,E_e

					         !Flag that indicate if collision appears or not. If yes- there will be no more collision with current particle on this iteration
					         Collission_appears=1
	
				endif

!=========================END =====Electron Detachment ---------------------------------------------------------------------


!Dissociative asttachement electron + H2(v)=H + H-   ------------------------------------------------------------------------------------------

				call Cross_section_e_H2v_Dissociative_attachment(E_e,Cross_sect)  
				 				
				Probab_coll=Dens_H2v_const*Vellocity*Cross_sect*dt*time_collision
				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>0.1  .AND.  E_e<6 .AND. Collission_appears==0) then

							  !write(50200+rank,*) iter,Probab_coll,E_e,Cross_sect

					Delete_e=Delete_e+1
					Delete_e_pos(Delete_e)=loop_l

					
					if(E_e<0.745) then
						E_e=E_e
					else
						E_e=E_e-0.745
					endif	
				
			

					!from Ev to Joule
					E_e=E_e*1.6e-19 
					!Calculation velocity for NI (sp=0 ->mass(8))
					Collission_velocity1=sqrt(2.0*E_e/mass(8))
					call Add_new_HmV_coll_velocity(loop_l,Collission_velocity1)

					!call Add_new_H_coll(i,j,k,ener)

					Collission_appears=1



				endif

				!Dissociative asttachement END --------------------------------------------------------------------------------------------------------------

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			    case(1)
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			    case(2)

				!Kinetic energy E=1/2mv^2 transformed from joules to eV (factor *6.2415e+18 )
				E_e=(mass(P(loop_l)%sp)*(P(loop_l)%VX**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VY**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VZ**2)/2.0 )*6.2415e+18 
				Vellocity=sqrt(P(loop_l)%VX**2+P(loop_l)%VY**2+P(loop_l)%VZ**2)


!Mutual neatralization  H-  +  H+ = H  +   H ------------------------------------------------------------------------------------------


				call Cross_section_Hm_Hp(E_e,Cross_sect)  
				 				
				Probab_coll=Dens_Hp(i,j,k)*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>2.02e-1 .AND. E_e<20000 .AND. Collission_appears==0) then

							  !write(50300+rank,*) iter,Probab_coll,E_e

	
					Delete_Hm=Delete_Hm+1
					Delete_Hm_pos(Delete_Hm)=loop_l

		
					Delete_Hp_pos_tar(i,j,k)=Delete_Hp_pos_tar(i,j,k)+1

					!call Add_new_H_coll(i,j,k,ener)
					!call Add_new_H_coll(i,j,k,ener)
					Collission_appears=1


				endif



!Mutual neatralization  END------------------------------------------------------------------------------------------


!Associative dissociation  H-  +  H = H2 + electron ------------------------------------------------------------------------------------------

				

				call Cross_section_Hm_H(E_e,Cross_sect)  				
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
				call random_number(rand_coll)

				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then

					!write(50400+rank,*) iter,Probab_coll,E_e

	
					!Delete_Hm=Delete_Hm+1
					!Delete_Hm_pos(Delete_Hm)=loop_l


						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1


				endif


!Associative dissociation  END------------------------------------------------------------------------------------------

				
	
!Associative dissociation  H-  +  H = H + H + electron ------------------------------------------------------------------------------------------

					
					call Cross_section_Hm_H2(E_e,Cross_sect)  
					Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
					call random_number(rand_coll)
					
				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then

						!	  write(50500+rank,*) iter,Probab_coll,E_e

	
						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

					endif

!Associative dissociation   END------------------------------------------------------------------------------------------
 
				
!Charge exange H-  +  H = H + H-  ------------------------------------------------------------------------------------------

				
				call Cross_section_Hm_H_charge_exange(E_e,Cross_sect)  	
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND. E_e<30000  .AND. Collission_appears==0) then

						!	  write(50600+rank,*) iter,Probab_coll,E_e


					      ! Energy 						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(P(loop_l)%sp))
						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1
	
				endif
!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		case(8)	

				!Kinetic energy E=1/2mv^2 transformed from joules to eV (factor *6.2415e+18 )
				E_e=(mass(P(loop_l)%sp)*(P(loop_l)%VX**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VY**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VZ**2)/2.0 )*6.2415e+18 
				Vellocity=sqrt(P(loop_l)%VX**2+P(loop_l)%VY**2+P(loop_l)%VZ**2)


!Mutual neatralization  H-  +  H+ = H  +   H ------------------------------------------------------------------------------------------


				call Cross_section_Hm_Hp(E_e,Cross_sect)  
				 				
				Probab_coll=Dens_Hp(i,j,k)*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>2.02e-1 .AND. E_e<20000 .AND. Collission_appears==0) then

					!		  write(50700+rank,*) iter,Probab_coll,E_e

	
					Delete_Hm=Delete_Hm+1
					Delete_Hm_pos(Delete_Hm)=loop_l

		
					Delete_Hp_pos_tar(i,j,k)=Delete_Hp_pos_tar(i,j,k)+1

					!call Add_new_H_coll(i,j,k,ener)
					!call Add_new_H_coll(i,j,k,ener)
					Collission_appears=1


				endif



!Mutual neatralization  END------------------------------------------------------------------------------------------


!Associative dissociation  H-  +  H = H2 + electron ------------------------------------------------------------------------------------------

				

				call Cross_section_Hm_H(E_e,Cross_sect)  				
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
				call random_number(rand_coll)

				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then


					!			  write(50800+rank,*) iter,Probab_coll,E_e

					!Delete_Hm=Delete_Hm+1
					!Delete_Hm_pos(Delete_Hm)=loop_l


						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

				endif


!Associative dissociation  END------------------------------------------------------------------------------------------

				
	
!Associative dissociation  H-  +  H = H + H + electron ------------------------------------------------------------------------------------------

					
					call Cross_section_Hm_H2(E_e,Cross_sect)  
					Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
					call random_number(rand_coll)
					
				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then

					!		  write(50900+rank,*) iter,Probab_coll,E_e

	
						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

					endif

!Associative dissociation   END------------------------------------------------------------------------------------------
 
				
!Charge exange H-  +  H = H + H-  ------------------------------------------------------------------------------------------

				
				call Cross_section_Hm_H_charge_exange(E_e,Cross_sect)  	
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND. E_e<30000  .AND. Collission_appears==0) then
					!		  write(51000+rank,*) iter,Probab_coll,E_e

					      ! Energy 						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(P(loop_l)%sp))
						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1
	
				endif

!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		case(4)	

				!Kinetic energy E=1/2mv^2 transformed from joules to eV (factor *6.2415e+18 )
				E_e=(mass(P(loop_l)%sp)*(P(loop_l)%VX**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VY**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VZ**2)/2.0 )*6.2415e+18 
				Vellocity=sqrt(P(loop_l)%VX**2+P(loop_l)%VY**2+P(loop_l)%VZ**2)


!Mutual neatralization  H-  +  H+ = H  +   H ------------------------------------------------------------------------------------------


				call Cross_section_Hm_Hp(E_e,Cross_sect)  
				 				
				Probab_coll=Dens_Hp(i,j,k)*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>2.02e-1 .AND. E_e<20000 .AND. Collission_appears==0) then

							  !write(51100+rank,*) iter,Probab_coll,E_e,Dens_Hp(i,j,k)

	
					Delete_Hm=Delete_Hm+1
					Delete_Hm_pos(Delete_Hm)=loop_l

		
					Delete_Hp_pos_tar(i,j,k)=Delete_Hp_pos_tar(i,j,k)+1

					!call Add_new_H_coll(i,j,k,ener)
					!call Add_new_H_coll(i,j,k,ener)
					Collission_appears=1


				endif



!Mutual neatralization  END------------------------------------------------------------------------------------------


!Associative dissociation  H-  +  H = H2 + electron ------------------------------------------------------------------------------------------

				

				call Cross_section_Hm_H(E_e,Cross_sect)  				
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
				call random_number(rand_coll)

				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then


							!	  write(51200+rank,*) iter,Probab_coll,E_e

					!Delete_Hm=Delete_Hm+1
					!Delete_Hm_pos(Delete_Hm)=loop_l


						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

				endif


!Associative dissociation  END------------------------------------------------------------------------------------------

				
	
!Associative dissociation  H-  +  H = H + H + electron ------------------------------------------------------------------------------------------

					
					call Cross_section_Hm_H2(E_e,Cross_sect)  
					Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
					call random_number(rand_coll)
					
				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then

							 ! write(51300+rank,*) iter,Probab_coll,E_e

	
						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

					endif

!Associative dissociation   END------------------------------------------------------------------------------------------
 
				
!Charge exange H-  +  H = H + H-  ------------------------------------------------------------------------------------------

				
				call Cross_section_Hm_H_charge_exange(E_e,Cross_sect)  	
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND. E_e<30000  .AND. Collission_appears==0) then
						!	  write(51400+rank,*) iter,Probab_coll,E_e

					      ! Energy 						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(P(loop_l)%sp))
						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1
	
				endif
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		case(5)	

				!Kinetic energy E=1/2mv^2 transformed from joules to eV (factor *6.2415e+18 )
				E_e=(mass(P(loop_l)%sp)*(P(loop_l)%VX**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VY**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VZ**2)/2.0 )*6.2415e+18 
				Vellocity=sqrt(P(loop_l)%VX**2+P(loop_l)%VY**2+P(loop_l)%VZ**2)


!Mutual neatralization  H-  +  H+ = H  +   H ------------------------------------------------------------------------------------------


				call Cross_section_Hm_Hp(E_e,Cross_sect)  
				 				
				Probab_coll=Dens_Hp(i,j,k)*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>2.02e-1 .AND. E_e<20000 .AND. Collission_appears==0) then

					!		  write(51500+rank,*) iter,Probab_coll,E_e

	
					Delete_Hm=Delete_Hm+1
					Delete_Hm_pos(Delete_Hm)=loop_l

		
					Delete_Hp_pos_tar(i,j,k)=Delete_Hp_pos_tar(i,j,k)+1

					!call Add_new_H_coll(i,j,k,ener)
					!call Add_new_H_coll(i,j,k,ener)
					Collission_appears=1


				endif



!Mutual neatralization  END------------------------------------------------------------------------------------------


!Associative dissociation  H-  +  H = H2 + electron ------------------------------------------------------------------------------------------

				

				call Cross_section_Hm_H(E_e,Cross_sect)  				
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
				call random_number(rand_coll)

				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then

					!		  write(51600+rank,*) iter,Probab_coll,E_e

	
					!Delete_Hm=Delete_Hm+1
					!Delete_Hm_pos(Delete_Hm)=loop_l


						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

				endif


!Associative dissociation  END------------------------------------------------------------------------------------------

				
	
!Associative dissociation  H-  +  H = H + H + electron ------------------------------------------------------------------------------------------

					
					call Cross_section_Hm_H2(E_e,Cross_sect)  
					Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
					call random_number(rand_coll)
					
				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then


					!			  write(51700+rank,*) iter,Probab_coll,E_e

						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

					endif

!Associative dissociation   END------------------------------------------------------------------------------------------
 
				
!Charge exange H-  +  H = H + H-  ------------------------------------------------------------------------------------------

				
				call Cross_section_Hm_H_charge_exange(E_e,Cross_sect)  	
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND. E_e<30000  .AND. Collission_appears==0) then
					!		  write(51800+rank,*) iter,Probab_coll,E_e

					      ! Energy 						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(P(loop_l)%sp))
						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1
	
				endif

!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		

		case(9)	

				!Kinetic energy E=1/2mv^2 transformed from joules to eV (factor *6.2415e+18 )
				E_e=(mass(P(loop_l)%sp)*(P(loop_l)%VX**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VY**2)/2.0+mass(P(loop_l)%sp)*(P(loop_l)%VZ**2)/2.0 )*6.2415e+18 
				Vellocity=sqrt(P(loop_l)%VX**2+P(loop_l)%VY**2+P(loop_l)%VZ**2)


!Mutual neatralization  H-  +  H+ = H  +   H ------------------------------------------------------------------------------------------


				call Cross_section_Hm_Hp(E_e,Cross_sect)  
				 				
				Probab_coll=Dens_Hp(i,j,k)*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>2.02e-1 .AND. E_e<20000 .AND. Collission_appears==0) then

					!		  write(52000+rank,*) iter,Probab_coll,E_e

	
					Delete_Hm=Delete_Hm+1
					Delete_Hm_pos(Delete_Hm)=loop_l

		
					Delete_Hp_pos_tar(i,j,k)=Delete_Hp_pos_tar(i,j,k)+1

					!call Add_new_H_coll(i,j,k,ener)
					!call Add_new_H_coll(i,j,k,ener)
					Collission_appears=1


				endif



!Mutual neatralization  END------------------------------------------------------------------------------------------


!Associative dissociation  H-  +  H = H2 + electron ------------------------------------------------------------------------------------------

				

				call Cross_section_Hm_H(E_e,Cross_sect)  				
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
				call random_number(rand_coll)

				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then

					!		  write(52100+rank,*) iter,Probab_coll,E_e

	
					!Delete_Hm=Delete_Hm+1
					!Delete_Hm_pos(Delete_Hm)=loop_l


						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

				endif


!Associative dissociation  END------------------------------------------------------------------------------------------

				
	
!Associative dissociation  H-  +  H = H + H + electron ------------------------------------------------------------------------------------------

					
					call Cross_section_Hm_H2(E_e,Cross_sect)  
					Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision
					call random_number(rand_coll)
					
				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND.  E_e<30000 .AND. Collission_appears==0) then


					!		  write(52200+rank,*) iter,Probab_coll,E_e

						! Energy of the ionization						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(0))
						
						 ! Transform Hm in electron sp=2 into sp=0
						  P(loop_l)%sp=0

						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1

					endif

!Associative dissociation   END------------------------------------------------------------------------------------------
 
				
!Charge exange H-  +  H = H + H-  ------------------------------------------------------------------------------------------

				
				call Cross_section_Hm_H_charge_exange(E_e,Cross_sect)  	
				Probab_coll=Dens_H_const*Vellocity*Cross_sect*dt*time_collision

				call random_number(rand_coll)


				if(rand_coll<Probab_coll .AND. E_e>0.1 .AND. E_e<30000  .AND. Collission_appears==0) then
					!		  write(52300+rank,*) iter,Probab_coll,E_e

					      ! Energy 						
						E_e=E_e-0.1
						
						!from Ev to Joule
						E_e=E_e*1.6e-19 

						Collission_velocity1=sqrt(2.0*E_e/mass(P(loop_l)%sp))
						!Redistribution of the velocity for first electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						P(loop_l)%VX=Collission_velocity1*rand_coll1

						call random_number(rand_coll2)
						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						P(loop_l)%VY=Collission_velocity1*rand_coll2

						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					        P(loop_l)%VZ=Collission_velocity1*rand_coll3
						Collission_appears=1
	
				endif


!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++





			case DEFAULT

		end select
    
    end if
	
	enddo



end subroutine Collisions_test



subroutine Delete_partic_coll()
	
	use Parallel
	implicit none

	integer countDelPart,loop_l
	Integer i
      	Integer j
     	Integer k

	
	countDelPart=0
	do i=Delete_e,1,-1

		P(Delete_e_pos(i))=P(Npart-countDelPart)
		countDelPart=countDelPart+1

	enddo


	Npart=Npart-Delete_e
	Delete_e=0


	countDelPart=0
	do i=Delete_Hm,1,-1

		P(Delete_Hm_pos(i))=P(Npart-countDelPart)
		countDelPart=countDelPart+1

	enddo


	Npart=Npart-Delete_Hm
	Delete_Hm=0


	countDelPart=0
	do i=Delete_Hp,1,-1

		P(Delete_Hp_pos(i))=P(Npart-countDelPart)
		countDelPart=countDelPart+1

	enddo


	Npart=Npart-Delete_Hp
	Delete_Hp=0



	do loop_l=Npart,1,-1


		i=P(loop_l)%X/delta(0)
		j=P(loop_l)%Y/delta(1)
		k=P(loop_l)%Z/delta(2)

		if(Delete_Hm_pos_tar(i,j,k) .NE. 0  .AND.  (P(loop_l)%sp==2  .OR.  P(loop_l)%sp==4   .OR.  P(loop_l)%sp==5 .OR.  P(loop_l)%sp==8 .OR.  P(loop_l)%sp==9) ) then

			P(loop_l)=P(Npart)
			Npart=Npart-1
			Delete_Hm_pos_tar(i,j,k)=Delete_Hm_pos_tar(i,j,k)-1			
		endif

		if(Delete_Hp_pos_tar(i,j,k) .NE. 0 .AND.  P(loop_l)%sp==3) then

			P(loop_l)=P(Npart)
			Npart=Npart-1
			Delete_Hp_pos_tar(i,j,k)=Delete_Hp_pos_tar(i,j,k)-1			
		endif



	enddo





end subroutine Delete_partic_coll



subroutine Add_new_HmV_coll_velocity(i,velocity)

	use Parallel
	implicit none
	include 'mpif.h'

     	Integer, INTENT(IN) :: i
     	Real, INTENT(IN) :: velocity

	TYPE(Particle) :: current_Par  
	real auxx(3)
	real::   rand_coll1=0
	real::   rand_coll2=0
	real::   rand_coll3=0

	current_Par%sp=8

	call random_number(auxx)
			
			! Position of new Hm
			current_Par%X=P(i)%X
                        current_Par%Y=P(i)%Y
                      	current_Par%Z=P(i)%Z


			!Redistribution of the velocity for new Hm
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						current_Par%VX=velocity*rand_coll1


						call random_number(rand_coll2)

						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						current_Par%VY=velocity*rand_coll2


						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					      current_Par%VZ=velocity*rand_coll3


			
					    current_Par%IdentPart=0.0*IdentPartNumber
					    !IdentPartNumber=IdentPartNumber+1

					    Npart_coll=Npart_coll+1
					    Add_partic_coll(Npart_coll)=current_Par


end subroutine Add_new_HmV_coll_velocity





subroutine Add_new_e_coll_velocity(i,velocity)

	use Parallel
	implicit none
	include 'mpif.h'

      integer, intent(in) :: i
     	Real, INTENT(IN) :: velocity

	TYPE(Particle) :: current_Par  
	real auxx(3)
	real::   rand_coll1=0
	real::   rand_coll2=0
	real::   rand_coll3=0

	current_Par%sp=0


			call random_number(auxx)
			
			! Position of new electron
			current_Par%X=P(i)%X
                        current_Par%Y=P(i)%Y
                      	current_Par%Z=P(i)%Z



			!Redistribution of the velocity for new electron
						call random_number(rand_coll1)
						rand_coll1=rand_coll1*2-1				 
						current_Par%VX=velocity*rand_coll1


						call random_number(rand_coll2)

						rand_coll2=(rand_coll2*2-1)*(1-abs(rand_coll1))
						current_Par%VY=velocity*rand_coll2


						call random_number(rand_coll3)

							  if(rand_coll3>0.5) then
									! Rest of the velocity from previous direction
									rand_coll3=sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	
							  else
									! Rest of the velocity from previous direction
									rand_coll3=-sqrt(1-abs(rand_coll1)**2-abs(rand_coll2)**2)	    
							  endif

					      current_Par%VZ=velocity*rand_coll3


			
					    current_Par%IdentPart=0.0*IdentPartNumber
					    !IdentPartNumber=IdentPartNumber+1

					    Npart_coll=Npart_coll+1
					    Add_partic_coll(Npart_coll)=current_Par
			

end subroutine Add_new_e_coll_velocity

subroutine Add_new_partic_coll2()
	use Parallel
	implicit none

	integer :: i=0

	do i=1,Npart_coll

		P(Npart+i)=Add_partic_coll(i)
  				E_e=(mass(P(Npart+i)%sp)*(P(Npart+i)%VX**2)/2.0+mass(P(Npart+i)%sp)*(P(Npart+i)%VY**2)/2.0+mass(P(Npart+i)%sp)*(P(Npart+i)%VZ**2)/2.0 )*6.2415e+18 

      !write(60000+rank,'(2(1x,i8) ,9(1x,E11.4) )') iter,i,P(Npart+i)%X,P(Npart+i)%Y,P(Npart+i)%Z,P(Npart+i)%VX,P(Npart+i)%VY,P(Npart+i)%VZ,P(Npart+i)%sp,P(Npart+i)%IdentPart,E_e

	enddo

	Npart=Npart+Npart_coll
	Npart_coll=0

end subroutine Add_new_partic_coll2





subroutine Coulomb_coll()
	use Parallel
	implicit none
	
	integer :: i=0
	integer :: j=0
	integer :: k=0
	integer :: loop_l=0
	integer :: loop_ll=0
	
	real :: relatv_vel(1:3)
	real :: delta_vel(1:3)
	real :: u_perp=0.0	
	real :: mod_rel_vel=0.0

	real :: dt_coul=0.0	

	real::   rand_coll=0
	integer :: rand_coll_int=0	
		
	real::   rand_normal=0
	real(kind = SELECTED_REAL_KIND(10,34)) ::   rand_norm_variance=0	
	real::   rand_uniform=0
	

	integer :: list_H_p(1:50000)
	integer :: list_H_n(1:50000)
	integer :: positive_counter   	  
	integer :: negative_counter	
	integer :: i_max=0
	integer :: i_rest=0

	integer :: i_shuffle=0
	integer :: aux_shuffle=0

	integer :: first_grp_max=0
	integer :: first_grp_min=0
	integer :: second_grp_max=0
	integer :: second_grp_min=0

	real :: red_mass=0.0
	real :: coulomb_log=10.0
	real :: ep_permi=8.85e-12
	real :: pi=3.14159265359
	real :: min_den = 0.0
	integer :: l1=5
	integer :: l2=5
	real auxx,erfi
	real :: sin_t=0.0
	real :: l_cos_t=0.0
	integer :: ii1=0
	integer :: ii2=0
	
	integer :: part_index=0

	integer :: collision_count=0

	
	real :: my_coll_const 

	
	!my_coll_const = ((charge(l1)**2)*(charge(l2)**2)*coulomb_log/(8.0*pi*(ep_permi**2)*(red_mass**2))	
	my_coll_const=5.697351

	
	
	dt_coul= 10.0*dt

	positive_counter=0
	negative_counter=0

	call Density_projection()
	call random_number(rand_coll)


	collision_count=0	
	do k = k0, k1
		do j = j0, j1
			do i = i0, i1
				positive_counter=0
				negative_counter=0
!!Separate the list into one for positive species and another for negative species
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++					
				do loop_l = 1, Num_part_Coul_cell(i,j,k)
					part_index = Coulomb_cell_list( i, j, k, loop_l ) 
				
					if( P(part_index)%sp ==3  )then
						positive_counter=positive_counter+1
						if( positive_counter > 50000 ) then
							write(*,*) "positive_counter: ", positive_counter, " limit of array is : ", 50000
						endif
						list_H_p(positive_counter)=part_index						
					endif
					if( P(part_index)%sp ==5 .OR. P(part_index)%sp ==9  )then
						negative_counter = negative_counter+1
						if( negative_counter > 50000 ) then
							write(*,*) "negative_counter: ", negative_counter, " limit of array is : ", 50000
						endif
						list_H_n(negative_counter)=part_index					
					endif	
				enddo
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		


				if(Dens_Hp(i,j,k) < Dens_HnPG(i,j,k)  )	then
					min_den=Dens_Hp(i,j,k)
				else
					min_den=Dens_HnPG(i,j,k)
				endif
				
					  	
				!!!Start doing the pairing
				if ( positive_counter  > 0 .AND. negative_counter > 0 )then
										
					!!!Random shuffle starts	
					do loop_l = positive_counter, 1, -1
						call random_number(auxx)

						i_shuffle = int(auxx*loop_l)		!!gives a number between 0 and loop_l -1 					
						i_shuffle = i_shuffle + 1	!!gives a number between 1 and loop_l
						aux_shuffle = list_H_p(loop_l)
						list_H_p(loop_l) = list_H_p( i_shuffle )						
						list_H_p( i_shuffle ) = aux_shuffle
	
					enddo
					
					
					do loop_l = negative_counter, 1, -1
						call random_number(auxx)

						i_shuffle = int(auxx*loop_l) !!gives a number between 0 and loop_l -1 					
						i_shuffle = i_shuffle + 1    !!gives a number between 1 and loop_l	
						aux_shuffle = list_H_n(loop_l)
						list_H_n(loop_l) = list_H_n( i_shuffle )						
						list_H_n( i_shuffle ) = aux_shuffle
	
					enddo	
					!!!Random shuffle finished


					l1=P(list_H_p(1))%sp
					l2=P(list_H_n(1))%sp						
					red_mass= mass(3)/2.0



!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					!Begin Collisions between the same specie pairs
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					if ( mod(positive_counter,2) .EQ. 0) then
					


						!!! Collisions for the positive ions between themselves

						do loop_l = 1,positive_counter,2
							
						!!!Pair loop_l with loop_l+1							
							
													
							!!!Collision occurs between particle loop_l and particle loop_l +1 
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_p(loop_l))%VX - P(list_H_p(loop_l + 1))%VX 
							relatv_vel(2)=P(list_H_p(loop_l))%VY - P(list_H_p(loop_l + 1 ))%VY
							relatv_vel(3)=P(list_H_p(loop_l))%VZ - P(list_H_p(loop_l + 1))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_p(loop_l + 1))%VX = P(list_H_p(loop_l + 1))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_p(loop_l + 1))%VY = P(list_H_p(loop_l + 1))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l + 1))%VZ = P(list_H_p(loop_l + 1))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_p(loop_l + 1))%VX = P(list_H_p(loop_l + 1))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_p(loop_l + 1))%VY = P(list_H_p(loop_l + 1))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_p(loop_l + 1))%VZ = P(list_H_p(loop_l + 1))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif
								
							
						enddo	
						
						!!! Collisions for the negative ions between themselves


						

					else

					   if( positive_counter > 1 ) then
						!!! Collisions for the positive ions between themselves

						do loop_l = 1,3
							
						!!!Pair loop_l with loop_l+1							
							
													
							!!!Collision occurs between particle loop_l and particle loop_l +1 
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_p(loop_l))%VX - P(list_H_p(loop_l + 1))%VX 
							relatv_vel(2)=P(list_H_p(loop_l))%VY - P(list_H_p(loop_l + 1 ))%VY
							relatv_vel(3)=P(list_H_p(loop_l))%VZ - P(list_H_p(loop_l + 1))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_p(loop_l + 1))%VX = P(list_H_p(loop_l + 1))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_p(loop_l + 1))%VY = P(list_H_p(loop_l + 1))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l + 1))%VZ = P(list_H_p(loop_l + 1))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_p(loop_l + 1))%VX = P(list_H_p(loop_l + 1))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_p(loop_l + 1))%VY = P(list_H_p(loop_l + 1))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_p(loop_l + 1))%VZ = P(list_H_p(loop_l + 1))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif
								
							
						enddo	

	
						!!!Pair 1 with 3							
							
													
							!!!Collision occurs between particle 1 and particle 3
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_p(1))%VX - P(list_H_p(3))%VX 
							relatv_vel(2)=P(list_H_p(1))%VY - P(list_H_p(3))%VY
							relatv_vel(3)=P(list_H_p(1))%VZ - P(list_H_p(3))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_p(1))%VX = P(list_H_p(1))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_p(1))%VY = P(list_H_p(1))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(1))%VZ = P(list_H_p(1))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_p(3))%VX = P(list_H_p(3))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_p(3))%VY = P(list_H_p(3))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(3))%VZ = P(list_H_p(3))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_p(1))%VX = P(list_H_p(1))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_p(1))%VY = P(list_H_p(1))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_p(1))%VZ = P(list_H_p(1))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_p(3))%VX = P(list_H_p(3))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_p(3))%VY = P(list_H_p(3))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_p(3))%VZ = P(list_H_p(3))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif





						do loop_l = 4,positive_counter,2
							
						!!!Pair loop_l with loop_l+1							
							
													
							!!!Collision occurs between particle loop_l and particle loop_l +1 
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_p(loop_l))%VX - P(list_H_p(loop_l + 1))%VX 
							relatv_vel(2)=P(list_H_p(loop_l))%VY - P(list_H_p(loop_l + 1 ))%VY
							relatv_vel(3)=P(list_H_p(loop_l))%VZ - P(list_H_p(loop_l + 1))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_p(loop_l + 1))%VX = P(list_H_p(loop_l + 1))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_p(loop_l + 1))%VY = P(list_H_p(loop_l + 1))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l + 1))%VZ = P(list_H_p(loop_l + 1))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_p(loop_l + 1))%VX = P(list_H_p(loop_l + 1))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_p(loop_l + 1))%VY = P(list_H_p(loop_l + 1))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_p(loop_l + 1))%VZ = P(list_H_p(loop_l + 1))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif
								
							
						enddo	
					   endif

					 endif




					if ( mod(negative_counter,2) .EQ. 0) then
					


						!!! Collisions for the positive ions between themselves

						do loop_l = 1,negative_counter,2
							
						!!!Pair loop_l with loop_l+1							
							
													
							!!!Collision occurs between particle loop_l and particle loop_l +1 
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_n(loop_l))%VX - P(list_H_n(loop_l + 1))%VX 
							relatv_vel(2)=P(list_H_n(loop_l))%VY - P(list_H_n(loop_l + 1 ))%VY
							relatv_vel(3)=P(list_H_n(loop_l))%VZ - P(list_H_n(loop_l + 1))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_n(loop_l))%VX = P(list_H_n(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_n(loop_l))%VY = P(list_H_n(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l))%VZ = P(list_H_n(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_n(loop_l + 1))%VX = P(list_H_n(loop_l + 1))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(loop_l + 1))%VY = P(list_H_n(loop_l + 1))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l + 1))%VZ = P(list_H_n(loop_l + 1))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_n(loop_l))%VX = P(list_H_n(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_n(loop_l))%VY = P(list_H_n(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_n(loop_l))%VZ = P(list_H_n(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_n(loop_l + 1))%VX = P(list_H_n(loop_l + 1))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_n(loop_l + 1))%VY = P(list_H_n(loop_l + 1))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_n(loop_l + 1))%VZ = P(list_H_n(loop_l + 1))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif
								
							
						enddo	
						
						!!! Collisions for the negative ions between themselves


						

					else

					   if( negative_counter > 1 ) then
						!!! Collisions for the positive ions between themselves

						do loop_l = 1,3
							
						!!!Pair loop_l with loop_l+1							
							
													
							!!!Collision occurs between particle loop_l and particle loop_l +1 
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_n(loop_l))%VX - P(list_H_n(loop_l + 1))%VX 
							relatv_vel(2)=P(list_H_n(loop_l))%VY - P(list_H_n(loop_l + 1 ))%VY
							relatv_vel(3)=P(list_H_n(loop_l))%VZ - P(list_H_n(loop_l + 1))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_n(loop_l))%VX = P(list_H_n(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_n(loop_l))%VY = P(list_H_n(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l))%VZ = P(list_H_n(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_n(loop_l + 1))%VX = P(list_H_n(loop_l + 1))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(loop_l + 1))%VY = P(list_H_n(loop_l + 1))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l + 1))%VZ = P(list_H_n(loop_l + 1))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_n(loop_l))%VX = P(list_H_n(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_n(loop_l))%VY = P(list_H_n(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_n(loop_l))%VZ = P(list_H_n(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_n(loop_l + 1))%VX = P(list_H_n(loop_l + 1))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_n(loop_l + 1))%VY = P(list_H_n(loop_l + 1))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_n(loop_l + 1))%VZ = P(list_H_n(loop_l + 1))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif
								
							
						enddo	

	
						!!!Pair 1 with 3							
							
													
							!!!Collision occurs between particle 1 and particle 3
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_n(1))%VX - P(list_H_n(3))%VX 
							relatv_vel(2)=P(list_H_n(1))%VY - P(list_H_n(3))%VY
							relatv_vel(3)=P(list_H_n(1))%VZ - P(list_H_n(3))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_n(1))%VX = P(list_H_n(1))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_n(1))%VY = P(list_H_n(1))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(1))%VZ = P(list_H_n(1))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_n(3))%VX = P(list_H_n(3))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(3))%VY = P(list_H_n(3))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(3))%VZ = P(list_H_n(3))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_n(1))%VX = P(list_H_n(1))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_n(1))%VY = P(list_H_n(1))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_n(1))%VZ = P(list_H_n(1))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_n(3))%VX = P(list_H_n(3))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_n(3))%VY = P(list_H_n(3))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_n(3))%VZ = P(list_H_n(3))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif





						do loop_l = 4,negative_counter,2
							
						!!!Pair loop_l with loop_l+1							
							
													
							!!!Collision occurs between particle loop_l and particle loop_l +1 
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
															
							relatv_vel(1)=P(list_H_n(loop_l))%VX - P(list_H_n(loop_l + 1))%VX 
							relatv_vel(2)=P(list_H_n(loop_l))%VY - P(list_H_n(loop_l + 1 ))%VY
							relatv_vel(3)=P(list_H_n(loop_l))%VZ - P(list_H_n(loop_l + 1))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_n(loop_l))%VX = P(list_H_n(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_n(loop_l))%VY = P(list_H_n(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l))%VZ = P(list_H_n(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_n(loop_l + 1))%VX = P(list_H_n(loop_l + 1))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(loop_l + 1))%VY = P(list_H_n(loop_l + 1))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l + 1))%VZ = P(list_H_n(loop_l + 1))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_n(loop_l))%VX = P(list_H_n(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_n(loop_l))%VY = P(list_H_n(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_n(loop_l))%VZ = P(list_H_n(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									
									P(list_H_n(loop_l + 1))%VX = P(list_H_n(loop_l + 1))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_n(loop_l + 1))%VY = P(list_H_n(loop_l + 1))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_n(loop_l + 1))%VZ = P(list_H_n(loop_l + 1))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif
								
							
						enddo	
					   endif

					 endif


				
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				!Begin Collisions between the different specie pairs
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					!write(51500+rank,*) iter, i, j, k, positive_counter , negative_counter 
					if ( positive_counter  == negative_counter )then
						l1=P(list_H_p(1))%sp
						l2=P(list_H_n(1))%sp						
						red_mass= mass(3)/2.0
						
						do loop_l = 1,positive_counter
							
						!!!Pair loop_l with rand_coll_int							
							call random_number(rand_coll)
							!rand_coll_int = FLOOR( (1 + rand_coll*negative_counter) )						
							!!!Collision occurs between particle loop_l and particle rand_coll_int 
							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
							!relatv_vel(1)=P(list_H_p(loop_l))%VX - P(list_H_n(negative_counter ))%VX 
							!relatv_vel(2)=P(list_H_p(loop_l))%VY - P(list_H_n(negative_counter ))%VY
							!relatv_vel(3)=P(list_H_p(loop_l))%VZ - P(list_H_n(negative_counter ))%VZ								
							relatv_vel(1)=P(list_H_p(loop_l))%VX - P(list_H_n(loop_l ))%VX 
							relatv_vel(2)=P(list_H_p(loop_l))%VY - P(list_H_n(loop_l ))%VY
							relatv_vel(3)=P(list_H_p(loop_l))%VZ - P(list_H_n(loop_l ))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )

									!P(list_H_n(negative_counter ))%VX = P(list_H_n(negative_counter ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									!P(list_H_n(negative_counter ))%VY = P(list_H_n(negative_counter ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									!P(list_H_n(negative_counter ))%VZ = P(list_H_n(negative_counter ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
									P(list_H_n(loop_l ))%VX = P(list_H_n(loop_l ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(loop_l ))%VY = P(list_H_n(loop_l ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l ))%VZ = P(list_H_n(loop_l ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									!P(list_H_n(negative_counter ))%VX = P(list_H_n(negative_counter ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									!P(list_H_n(negative_counter ))%VY = P(list_H_n(negative_counter ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									!P(list_H_n(negative_counter ))%VZ = P(list_H_n(negative_counter ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
									P(list_H_n(loop_l ))%VX = P(list_H_n(loop_l ))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_n(loop_l ))%VY = P(list_H_n(loop_l ))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_n(loop_l ))%VZ = P(list_H_n(loop_l ))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif
							!!!Reorder target list
							!list_H_n(rand_coll_int) = list_H_n(negative_counter)
							!negative_counter= negative_counter - 1				
							
						enddo	
						

					elseif ( positive_counter  > negative_counter )then
							
						i_max= positive_counter/negative_counter
						i_rest=  positive_counter -  i_max*negative_counter 
						
			
						first_grp_max= ( i_max + 1 )*i_rest
						first_grp_min= i_rest 
						
						second_grp_max=i_max*negative_counter - i_max*i_rest
						second_grp_min= negative_counter - i_rest
						l1=P(list_H_p(1))%sp
						l2=P(list_H_n(1))%sp		
						red_mass= mass(3)/2.0
										

						!!!Pair loop_l with rand_coll_int							
																				
						do loop_ll = 1, (i_max + 1)
							do loop_l = 1, first_grp_min  						
								ii1 = loop_l + (loop_ll-1)*first_grp_min
								ii2 = loop_l
								if ( ii2 > negative_counter) then
									!write(71500+rank,*) "negative_counter 2: ", negative_counter, "ii2: ", ii2 
								endif	
								if ( ii1 > positive_counter) then
									!write(71500+rank,*) "positive_counter 2: ", positive_counter, "ii1: ", ii1 							
								endif									
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
							
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_p(ii1))%VX - P(list_H_n( ii2 ))%VX 
								relatv_vel(2)=P(list_H_p(ii1))%VY - P(list_H_n( ii2 ))%VY
								relatv_vel(3)=P(list_H_p(ii1))%VZ - P(list_H_n( ii2 ))%VZ								
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )
								
								if (mod_rel_vel==0.0) then
									
								else
									collision_count=collision_count+1
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)					
									
									call random_number(auxx)
									auxx=auxx*2.0-1.0
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
									
									l_cos_t=rand_normal*sin_t							
!									write(51500+rank,*) " PROCESS: 2 sin_t : ", sin_t, " l_cos_t : ", l_cos_t
									!write(51500+rank,*) " PROCESS: 2 rand_norm_variance : ", rand_norm_variance
									call random_number(auxx)
									auxx=auxx*2.0*pi
									!write(61500+rank,*) " PROCESS: 2  DX : ", (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx), " vx: ", P(list_H_n( ii2 ))%VX
									


									if (u_perp==0.0) then
										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

										!write(61500+rank,*) " PROCESS: 2  DX : ",  - (red_mass/mass(l2))*delta_vel(1), " vx: ",P(list_H_n( ii2 ))%VX

										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*delta_vel(3)

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*delta_vel(3)

						

									endif

								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
							enddo		
						enddo


						do loop_ll = 1, i_max
							do loop_l = 1,  second_grp_min  						
								ii1 = loop_l + (loop_ll-1)* second_grp_min
								ii2 = loop_l
							
								if ( ii2 > negative_counter) then
									!write(71500+rank,*) "negative_counter 2: ", negative_counter, "ii2: ", ii2 
								endif	
								if ( ii1 > positive_counter) then
									!write(71500+rank,*) "positive_counter 2: ", positive_counter, "ii1: ", ii1 							
								endif			 						
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
							
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_p(ii1))%VX - P(list_H_n( ii2 ))%VX 
								relatv_vel(2)=P(list_H_p(ii1))%VY - P(list_H_n( ii2 ))%VY
								relatv_vel(3)=P(list_H_p(ii1))%VZ - P(list_H_n( ii2 ))%VZ				
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )				
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								if (mod_rel_vel==0.0) then
									
								else
									collision_count=collision_count+1
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
									
									call random_number(auxx)
									auxx=auxx*2.0-1.0
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
									
									l_cos_t=rand_normal*sin_t							
!									write(51500+rank,*) " PROCESS: 2 sin_t : ", sin_t, " l_cos_t : ", l_cos_t
									!write(51500+rank,*) " PROCESS: 2 rand_norm_variance : ", rand_norm_variance
									call random_number(auxx)
									auxx=auxx*2.0*pi
									!write(61500+rank,*) " PROCESS: 2  DX : ", (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx), " vx: ", P(list_H_n( ii2 ))%VX
									
									if (u_perp==0.0) then
										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

										!write(61500+rank,*) " PROCESS: 2  DX : ",  - (red_mass/mass(l2))*delta_vel(1), " vx: ",P(list_H_n( ii2 ))%VX

										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*delta_vel(3)

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*delta_vel(3)

						

									endif


								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
							enddo		
						enddo											
							
									
									
					else if ( positive_counter  < negative_counter )then
						i_max= negative_counter/positive_counter
						i_rest= negative_counter  -  i_max*positive_counter 

			
						first_grp_max= ( i_max + 1 )*i_rest
						first_grp_min= i_rest 
						second_grp_max=i_max*positive_counter - i_max*i_rest
						second_grp_min= positive_counter - i_rest
						l1=P(list_H_p(1))%sp
						l2=P(list_H_n(1))%sp	
						red_mass= mass(3)/2.0
						
						!!!Pair loop_l with rand_coll_int							
																				
						do loop_ll = 1, (i_max + 1)
							do loop_l = 1, first_grp_min  						
								ii1 = loop_l + (loop_ll-1)*first_grp_min
								ii2 = loop_l	
								
								if ( ii1 > negative_counter) then
									!write(71500+rank,*) "negative_counter 3: ", negative_counter, "ii1: ", ii1 
								endif	
								if ( ii2 > positive_counter) then
									!write(71500+rank,*) "positive_counter 3: ", positive_counter, "ii2: ", ii2 							
								endif										
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
							
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_n(ii1))%VX - P(list_H_p( ii2 ))%VX 
								relatv_vel(2)=P(list_H_n(ii1))%VY - P(list_H_p( ii2 ))%VY
								relatv_vel(3)=P(list_H_n(ii1))%VZ - P(list_H_p( ii2 ))%VZ	
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )							
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								if (mod_rel_vel==0.0) then
									
								else
									collision_count=collision_count+1
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
									
									call random_number(auxx)
									auxx=auxx*2.0-1.0
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
!									write(51500+rank,*) " PROCESS: 3 sin_t : ", sin_t, " l_cos_t : ", l_cos_t
									!write(51500+rank,*) " PROCESS: 3 rand_norm_variance : ", rand_norm_variance
									call random_number(auxx)
									auxx=auxx*2.0*pi
									!write(61500+rank,*) " PROCESS: 3  DX : ", (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx), " vx: ", P(list_H_n(ii1))%VX 
									P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )
			
									P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )	


									if (u_perp==0.0) then
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )	
	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

	
										!write(61500+rank,*) " PROCESS: 3  DX : ", (red_mass/mass(l2))*delta_vel(1), " vx: ", P(list_H_n(ii1))%VX 										
	
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*delta_vel(3)
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*delta_vel(3)
						


									endif







								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
							enddo		
						enddo


						do loop_ll = 1, i_max
							do loop_l = 1,  second_grp_min  						
								ii1 = loop_l + (loop_ll-1)* second_grp_min
								ii2 = loop_l	
							 	
								if ( ii1 > negative_counter) then
									!write(71500+rank,*) "negative_counter 3: ", negative_counter, "ii1: ", ii1 
								endif	
								if ( ii2 > positive_counter) then
									!write(71500+rank,*) "positive_counter 3: ", positive_counter, "ii2: ", ii2 							
								endif
							
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
								
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_n(ii1))%VX - P(list_H_p( ii2 ))%VX 
								relatv_vel(2)=P(list_H_n(ii1))%VY - P(list_H_p( ii2 ))%VY
								relatv_vel(3)=P(list_H_n(ii1))%VZ - P(list_H_p( ii2 ))%VZ	
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )								
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								if (mod_rel_vel==0.0) then
								
								else
									collision_count=collision_count+1								
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
									 
									call random_number(auxx)
									auxx=(auxx*2.0-1.0)
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
									
									l_cos_t=rand_normal*sin_t							
									!write(51500+rank,*) " PROCESS: 3 sin_t : ", sin_t, " l_cos_t : ", l_cos_t
									!write(51500+rank,*) " PROCESS: 3 rand_norm_variance : ", rand_norm_variance
									call random_number(auxx)
									auxx=auxx*2.0*pi
									!write(61500+rank,*) " PROCESS: 3  DvX : ", (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx), " vx: ", P(list_H_n(ii1))%VX 
									
									if (u_perp==0.0) then
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )	
	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

										!write(61500+rank,*) " PROCESS: 3  DX : ", (red_mass/mass(l2))*delta_vel(1), " vx: ", P(list_H_n(ii1))%VX
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*delta_vel(3)
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*delta_vel(3)
						


									endif




								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
							enddo		
						enddo											
				
					endif
				endif
				



			enddo
		enddo
	enddo


	write(61500+rank,*) "Coulomb collision finished for iteration = ", iter , " Numer of collisions = ", collision_count 

	


end subroutine Coulomb_coll

subroutine Coulomb_coll_test()
	use Parallel
	implicit none
	
	integer :: i=0
	integer :: j=0
	integer :: k=0
	integer :: loop_l=0
	integer :: loop_ll=0
	
	real :: relatv_vel(1:3)
	real :: delta_vel(1:3)
	real :: u_perp=0.0	
	real :: mod_rel_vel=0.0

	real :: dt_coul=0.0	

	real::   rand_coll=0
	integer :: rand_coll_int=0	
		
	real::   rand_normal=0
	real(kind = SELECTED_REAL_KIND(10,34)) ::   rand_norm_variance=0	
	real::   rand_uniform=0
	

	integer :: list_H_p(1:50000)
	integer :: list_H_n(1:50000)
	integer :: positive_counter   	  
	integer :: negative_counter	
	integer :: i_max=0
	integer :: i_rest=0

	integer :: i_shuffle=0
	integer :: aux_shuffle=0

	integer :: first_grp_max=0
	integer :: first_grp_min=0
	integer :: second_grp_max=0
	integer :: second_grp_min=0

	real :: red_mass=0.0
	real :: coulomb_log=10.0
	real :: ep_permi=8.85e-12
	real :: pi=3.14159265359
	real :: min_den = 0.0
	integer :: l1=5
	integer :: l2=5
	real auxx,erfi
	real :: sin_t=0.0
	real :: l_cos_t=0.0
	integer :: ii1=0
	integer :: ii2=0
	
	integer :: part_index=0

	integer :: collision_count=0

	
	real :: my_coll_const 

	
	!my_coll_const = ((charge(l1)**2)*(charge(l2)**2)*coulomb_log/(8.0*pi*(ep_permi**2)*(red_mass**2))	
	my_coll_const=5.697351

	
	
	dt_coul= 10.0*dt

	positive_counter=0
	negative_counter=0

	call random_number(rand_coll)

	collision_count=0	
	do k = k0, k1
		do j = j0, j1
			do i = i0, i1
				positive_counter=0
				negative_counter=0
!!Separate the list into one for positive species and another for negative species
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++					
				do loop_l = 1, Num_part_Coul_cell(i,j,k)
					part_index = Coulomb_cell_list( i, j, k, loop_l ) 
				
					if( P(part_index)%sp ==3  )then
						positive_counter=positive_counter+1
						if( positive_counter > 50000 ) then
							write(*,*) "positive_counter: ", positive_counter, " limit of array is : ", 50000
						endif
						list_H_p(positive_counter)=part_index						
					endif
					if( P(part_index)%sp ==5 .OR. P(part_index)%sp ==9  )then
						negative_counter = negative_counter+1
						if( negative_counter > 50000 ) then
							write(*,*) "negative_counter: ", negative_counter, " limit of array is : ", 50000
						endif
						list_H_n(negative_counter)=part_index					
					endif	
				enddo
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		

				!!THe total H- is used
				if(Dens_Hp(i,j,k) < (Dens_HnPG(i,j,k)+ Dens_Hm(i,j,k) )  )	then
					min_den=Dens_Hp(i,j,k)
				else
					min_den= (Dens_HnPG(i,j,k)+ Dens_Hm(i,j,k) )  
				endif
				
					  	
				!!!Start doing the pairing
				if ( positive_counter  > 0 .AND. negative_counter > 0 )then		
					!!!Random shuffle of the list of positive ions starts	
					do loop_l = positive_counter, 1, -1
						call random_number(auxx)
						i_shuffle = int(auxx*loop_l)	!!gives a number between 0 and loop_l -1 					
						i_shuffle = i_shuffle + 1	!!gives a number between 1 and loop_l
						!The two random selected particles are shuffled	
						aux_shuffle = list_H_p(loop_l)
						list_H_p(loop_l) = list_H_p( i_shuffle )						
						list_H_p( i_shuffle ) = aux_shuffle
	
					enddo
					
					
					do loop_l = negative_counter, 1, -1
						call random_number(auxx)
						i_shuffle = int(auxx*loop_l) !!gives a number between 0 and loop_l -1 					
						i_shuffle = i_shuffle + 1    !!gives a number between 1 and loop_l
						!The two random selected particles are shuffled	
						aux_shuffle = list_H_n(loop_l)
						list_H_n(loop_l) = list_H_n( i_shuffle )						
						list_H_n( i_shuffle ) = aux_shuffle
	
					enddo	
					!!!Random shuffle finished

					l1=P(list_H_p(1))%sp
					l2=P(list_H_n(1))%sp						
					red_mass= mass(3)/2.0 !!mass of H+ and H- is taken as the same

!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				!Begin Collisions between the different specie pairs
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					!write(51500+rank,*) iter, i, j, k, positive_counter , negative_counter 
					if ( positive_counter  == negative_counter )then
						l1=P(list_H_p(1))%sp
						l2=P(list_H_n(1))%sp						
						red_mass= mass(3)/2.0
						
						do loop_l = 1,positive_counter
							
						!!!Pair loop_l with rand_coll_int							
							call random_number(rand_coll)								
							relatv_vel(1)=P(list_H_p(loop_l))%VX - P(list_H_n(loop_l ))%VX 
							relatv_vel(2)=P(list_H_p(loop_l))%VY - P(list_H_n(loop_l ))%VY
							relatv_vel(3)=P(list_H_p(loop_l))%VZ - P(list_H_n(loop_l ))%VZ																
							u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )

							mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
							if (mod_rel_vel==0.0) then
									
							else
									collision_count=collision_count+1														
									rand_norm_variance = SQRT((  min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
								
									call random_number(auxx)
									auxx=auxx*2.0-1.0

              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
						
									call random_number(auxx)
									auxx=auxx*2.0*pi



								if (u_perp==0.0) then
								

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*sin_t*cos(auxx)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )
	
									P(list_H_n(loop_l ))%VX = P(list_H_n(loop_l ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(loop_l ))%VY = P(list_H_n(loop_l ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(loop_l ))%VZ = P(list_H_n(loop_l ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
								else
									delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
									delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
									delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

									P(list_H_p(loop_l))%VX = P(list_H_p(loop_l))%VX + (red_mass/mass(l1))*delta_vel(1)
									P(list_H_p(loop_l))%VY = P(list_H_p(loop_l))%VY + (red_mass/mass(l1))*delta_vel(2)
									P(list_H_p(loop_l))%VZ = P(list_H_p(loop_l))%VZ + (red_mass/mass(l1))*delta_vel(3)

									P(list_H_n(loop_l ))%VX = P(list_H_n(loop_l ))%VX - (red_mass/mass(l2))*delta_vel(1)
									P(list_H_n(loop_l ))%VY = P(list_H_n(loop_l ))%VY - (red_mass/mass(l2))*delta_vel(2)
									P(list_H_n(loop_l ))%VZ = P(list_H_n(loop_l ))%VZ - (red_mass/mass(l2))*delta_vel(3)


								endif

							!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
							!!!Finish collision
							endif		
							call test_values(delta_vel,rand_norm_variance,rand_normal,sin_t,l_cos_t)		
						enddo	
						

					elseif ( positive_counter  > negative_counter )then
							
						i_max= positive_counter/negative_counter
						i_rest=  positive_counter -  i_max*negative_counter 
						
			
						first_grp_max= ( i_max + 1 )*i_rest
						first_grp_min= i_rest 
						
						second_grp_max=i_max*negative_counter - i_max*i_rest
						second_grp_min= negative_counter - i_rest
						l1=P(list_H_p(1))%sp
						l2=P(list_H_n(1))%sp		
						red_mass= mass(3)/2.0
										

						!!!Pair loop_l with rand_coll_int							
																				
						do loop_ll = 1, (i_max + 1)
							do loop_l = 1, first_grp_min  						
								ii1 = loop_l + (loop_ll-1)*first_grp_min
								ii2 = loop_l
								if ( ii2 > negative_counter) then
									!write(71500+rank,*) "negative_counter 2: ", negative_counter, "ii2: ", ii2 
								endif	
								if ( ii1 > positive_counter) then
									!write(71500+rank,*) "positive_counter 2: ", positive_counter, "ii1: ", ii1 							
								endif									
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
							
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_p(ii1))%VX - P(list_H_n( ii2 ))%VX 
								relatv_vel(2)=P(list_H_p(ii1))%VY - P(list_H_n( ii2 ))%VY
								relatv_vel(3)=P(list_H_p(ii1))%VZ - P(list_H_n( ii2 ))%VZ								
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )
								
								if (mod_rel_vel==0.0) then
									
								else
									collision_count=collision_count+1
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)					
									
									call random_number(auxx)
									auxx=auxx*2.0-1.0
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
									
									l_cos_t=rand_normal*sin_t							
									call random_number(auxx)
									auxx=auxx*2.0*pi

									if (u_perp==0.0) then
										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*delta_vel(3)

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*delta_vel(3)
									endif

								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
								call test_values(delta_vel,rand_norm_variance,rand_normal,sin_t,l_cos_t)
							enddo		
						enddo


						do loop_ll = 1, i_max
							do loop_l = 1,  second_grp_min  						
								ii1 = loop_l + (loop_ll-1)* second_grp_min
								ii2 = loop_l
							
								if ( ii2 > negative_counter) then
									!write(71500+rank,*) "negative_counter 2: ", negative_counter, "ii2: ", ii2 
								endif	
								if ( ii1 > positive_counter) then
									!write(71500+rank,*) "positive_counter 2: ", positive_counter, "ii1: ", ii1 							
								endif			 						
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
							
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_p(ii1))%VX - P(list_H_n( ii2 ))%VX 
								relatv_vel(2)=P(list_H_p(ii1))%VY - P(list_H_n( ii2 ))%VY
								relatv_vel(3)=P(list_H_p(ii1))%VZ - P(list_H_n( ii2 ))%VZ				
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )				
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								if (mod_rel_vel==0.0) then
									
								else
									collision_count=collision_count+1
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
									
									call random_number(auxx)
									auxx=auxx*2.0-1.0
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
									
									l_cos_t=rand_normal*sin_t							
!									write(51500+rank,*) " PROCESS: 2 sin_t : ", sin_t, " l_cos_t : ", l_cos_t
									!write(51500+rank,*) " PROCESS: 2 rand_norm_variance : ", rand_norm_variance
									call random_number(auxx)
									auxx=auxx*2.0*pi
									!write(61500+rank,*) " PROCESS: 2  DX : ", (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx), " vx: ", P(list_H_n( ii2 ))%VX
									
									if (u_perp==0.0) then
										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

										!write(61500+rank,*) " PROCESS: 2  DX : ",  - (red_mass/mass(l2))*delta_vel(1), " vx: ",P(list_H_n( ii2 ))%VX

										P(list_H_p(ii1))%VX = P(list_H_p(ii1))%VX + (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p(ii1))%VY = P(list_H_p(ii1))%VY + (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p(ii1))%VZ = P(list_H_p(ii1))%VZ + (red_mass/mass(l1))*delta_vel(3)

										P(list_H_n( ii2 ))%VX = P(list_H_n( ii2 ))%VX - (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n( ii2 ))%VY = P(list_H_n( ii2 ))%VY - (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n( ii2 ))%VZ = P(list_H_n( ii2 ))%VZ - (red_mass/mass(l2))*delta_vel(3)
									endif


								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
								call test_values(delta_vel,rand_norm_variance,rand_normal,sin_t,l_cos_t)
							enddo		
						enddo											
							
									
									
					else if ( positive_counter  < negative_counter )then
						i_max= negative_counter/positive_counter
						i_rest= negative_counter  -  i_max*positive_counter 

			
						first_grp_max= ( i_max + 1 )*i_rest
						first_grp_min= i_rest 
						second_grp_max=i_max*positive_counter - i_max*i_rest
						second_grp_min= positive_counter - i_rest
						l1=P(list_H_p(1))%sp
						l2=P(list_H_n(1))%sp	
						red_mass= mass(3)/2.0
						
						!!!Pair loop_l with rand_coll_int							
																				
						do loop_ll = 1, (i_max + 1)
							do loop_l = 1, first_grp_min  						
								ii1 = loop_l + (loop_ll-1)*first_grp_min
								ii2 = loop_l	
								
								if ( ii1 > negative_counter) then
									!write(71500+rank,*) "negative_counter 3: ", negative_counter, "ii1: ", ii1 
								endif	
								if ( ii2 > positive_counter) then
									!write(71500+rank,*) "positive_counter 3: ", positive_counter, "ii2: ", ii2 							
								endif										
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
							
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_n(ii1))%VX - P(list_H_p( ii2 ))%VX 
								relatv_vel(2)=P(list_H_n(ii1))%VY - P(list_H_p( ii2 ))%VY
								relatv_vel(3)=P(list_H_n(ii1))%VZ - P(list_H_p( ii2 ))%VZ	
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )							
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								if (mod_rel_vel==0.0) then
									
								else
									collision_count=collision_count+1
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
									
									call random_number(auxx)
									auxx=auxx*2.0-1.0
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
								
									l_cos_t=rand_normal*sin_t							
!									write(51500+rank,*) " PROCESS: 3 sin_t : ", sin_t, " l_cos_t : ", l_cos_t
									!write(51500+rank,*) " PROCESS: 3 rand_norm_variance : ", rand_norm_variance
									call random_number(auxx)
									auxx=auxx*2.0*pi
									!write(61500+rank,*) " PROCESS: 3  DX : ", (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx), " vx: ", P(list_H_n(ii1))%VX 
									P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )
			
									P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
									P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
									P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )	


									if (u_perp==0.0) then
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )	
	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

	
										!write(61500+rank,*) " PROCESS: 3  DX : ", (red_mass/mass(l2))*delta_vel(1), " vx: ", P(list_H_n(ii1))%VX 										
	
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*delta_vel(3)
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*delta_vel(3)
						


									endif

								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
								call test_values(delta_vel,rand_norm_variance,rand_normal,sin_t,l_cos_t)
							enddo		
						enddo


						do loop_ll = 1, i_max
							do loop_l = 1,  second_grp_min  						
								ii1 = loop_l + (loop_ll-1)* second_grp_min
								ii2 = loop_l	
							 	
								if ( ii1 > negative_counter) then
									!write(71500+rank,*) "negative_counter 3: ", negative_counter, "ii1: ", ii1 
								endif	
								if ( ii2 > positive_counter) then
									!write(71500+rank,*) "positive_counter 3: ", positive_counter, "ii2: ", ii2 							
								endif
							
								!!!Collision occurs Between loop_l particle and loopl*first_grp_min
								
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++							
								relatv_vel(1)=P(list_H_n(ii1))%VX - P(list_H_p( ii2 ))%VX 
								relatv_vel(2)=P(list_H_n(ii1))%VY - P(list_H_p( ii2 ))%VY
								relatv_vel(3)=P(list_H_n(ii1))%VZ - P(list_H_p( ii2 ))%VZ	
								u_perp = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) )								
								mod_rel_vel = SQRT( relatv_vel(1)*relatv_vel(1) + relatv_vel(2)*relatv_vel(2) + relatv_vel(3)*relatv_vel(3)) 
								if (mod_rel_vel==0.0) then
								
								else
									collision_count=collision_count+1								
									rand_norm_variance = SQRT(( min_den*my_coll_const/(mod_rel_vel**3))*dt_coul)
									 
									call random_number(auxx)
									auxx=(auxx*2.0-1.0)
	
              								call InvErrorFunction(auxx,erfi)
									rand_normal=sqrt(2.0)*rand_norm_variance*erfi
									sin_t=2.0*rand_normal/(1.0 + rand_normal*rand_normal)
									
									l_cos_t=rand_normal*sin_t							
									!write(51500+rank,*) " PROCESS: 3 sin_t : ", sin_t, " l_cos_t : ", l_cos_t
									!write(51500+rank,*) " PROCESS: 3 rand_norm_variance : ", rand_norm_variance
									call random_number(auxx)
									auxx=auxx*2.0*pi
									!write(61500+rank,*) " PROCESS: 3  DvX : ", (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx), " vx: ", P(list_H_n(ii1))%VX 
									
									if (u_perp==0.0) then
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*(-mod_rel_vel*l_cos_t )
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*mod_rel_vel*sin_t*cos(auxx)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*mod_rel_vel*sin_t*sin(auxx)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*(-mod_rel_vel*l_cos_t )	
	
															
									else
										delta_vel(1) = (relatv_vel(1)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) - (relatv_vel(2)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(1)*l_cos_t
										delta_vel(2) = (relatv_vel(2)/u_perp)*relatv_vel(3)*sin_t*cos(auxx) + (relatv_vel(1)/u_perp)*mod_rel_vel*sin_t*sin(auxx) - relatv_vel(2)*l_cos_t
										delta_vel(3) = -u_perp*sin_t*cos(auxx) 	- relatv_vel(3)*l_cos_t

										!write(61500+rank,*) " PROCESS: 3  DX : ", (red_mass/mass(l2))*delta_vel(1), " vx: ", P(list_H_n(ii1))%VX
										P(list_H_n(ii1))%VX = P(list_H_n(ii1))%VX + (red_mass/mass(l2))*delta_vel(1)
										P(list_H_n(ii1))%VY = P(list_H_n(ii1))%VY + (red_mass/mass(l2))*delta_vel(2)
										P(list_H_n(ii1))%VZ = P(list_H_n(ii1))%VZ + (red_mass/mass(l2))*delta_vel(3)
			
										P(list_H_p( ii2 ))%VX = P(list_H_p( ii2 ))%VX - (red_mass/mass(l1))*delta_vel(1)
										P(list_H_p( ii2 ))%VY = P(list_H_p( ii2 ))%VY - (red_mass/mass(l1))*delta_vel(2)
										P(list_H_p( ii2 ))%VZ = P(list_H_p( ii2 ))%VZ - (red_mass/mass(l1))*delta_vel(3)

									endif
									
								!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
								!!!Finish collision
								endif
								call test_values(delta_vel,rand_norm_variance,rand_normal,sin_t,l_cos_t)
							enddo		
						enddo											
				
					endif
				endif
				



			enddo
		enddo
	enddo


	write(61500+rank,*) "Coulomb collision finished for iteration = ", iter , " Numer of collisions = ", collision_count 


end subroutine Coulomb_coll_test

subroutine test_values(delta_vel,rand_norm_variance,rand_normal,sin_t,l_cos_t)
	use Parallel
	implicit none
	 real,intent( in ) :: delta_vel(1:3)
	 real,intent( in ) :: rand_norm_variance,rand_normal,sin_t,l_cos_t	
     if(mod(iter,10000)==0) then	
	write(str3,'(I7)') rank
	lgc=LEN_TRIM(ADJUSTL(str3))
      	str='test_Coul_'//str3(8-lgc:7)//'.dat'
      
      OPEN(UNIT=3333, FILE=str, status="unknown", position= "append" )		
			write(3333,'( 1(1x,I8), 7(1x,E14.7) )') iter,delta_vel(1),delta_vel(2),delta_vel(3),rand_norm_variance,rand_normal,sin_t,l_cos_t
      CLOSE(3333)
     endif	

end subroutine


