      subroutine reflection2()

        use Parallel
        implicit none
        include 'mpif.h'

        integer l, i, j, k, m
        logical isin, isin1, isin2
        real posx, posy, posz

        do l=1, NPART
        outPart=0
        isin = .FALSE.

        i = P(l)%X/delta(0)
        j = P(l)%Y/delta(1)
        k = P(l)%Z/delta(2)
  
        posx = P(l)%X
        posy = P(l)%Y
        posz = P(l)%Z
		
       !The subroutine 'isinboj' returns variable 'isin'
       !isin = .FALSE. means that particle is inside the object and
       !we do not do any reflaction. 
       !isin = .TRUE. means that particle outside and the reflection is
       !necessary.
       ! In the future I think it is better to change this variable 
       ! from .FALSE to .TRUE. in order to avoid confusion 												
        do m=1,Nb_Obj
          call isinobj(posx, posy, posz, m, isin)
          if(isin) then
            exit
          end if
        end do

        if(isin) then
          select case (The_Object(m)%types)
          case (1)
            if(The_Object(m)%ReflectionType == 1) then
              call Reflection_Box_newT(m, l)
	      In_flux_object(m, P(l)%sp) =  In_flux_object(m, P(l)%sp)+ 1.0	
            end if
              if(The_Object(m)%ReflectionType == 2) then
              OutParticles = OutParticles + 1
              DelParticles(OutParticles) = l
              outPart = 1
	      In_flux_object(m, P(l)%sp) =  In_flux_object(m, P(l)%sp)+ 1.0	
              if(P(l)%sp == 0 ) then 
		Out_e      = Out_e      + 1
		if(P(l)%IdentPart < 0.0  ) then	
		 	str='Followed_and_lost.dat'
	        	OPEN(UNIT=97, FILE=str, status="unknown", position="append")
	        	write(97,'(2(1x,E11.4) )')  P(l)%IdentPart, P(l)%sp
	        	close(UNIT=97)
              	endif
	      endif	
	      if(P(l)%sp == 1 ) Out_PI_H2p = Out_PI_H2p + 1
              if(P(l)%sp == 2 ) Out_NI     = Out_NI     + 1
              if(P(l)%sp == 3 ) Out_PI_Hp  = Out_PI_Hp  + 1
              if(P(l)%sp == 10) Out_PI_H3p = Out_PI_H3p + 1
              if(P(l)%sp == 11) Out_Cs     = Out_Cs     + 1

              if(P(l)%sp == 5 ) then
                if(P(l)%IdentPart < 0.0  ) then	
		 	str='Followed_and_lost.dat'
	        	OPEN(UNIT=97, FILE=str, status="unknown", position="append")
	        	write(97,'(2(1x,E11.4) )')  P(l)%IdentPart, P(l)%sp
	        	close(UNIT=97)
              	endif
	      endif
              if(P(l)%sp == 9 ) then 
		 if(P(l)%IdentPart < 0.0  ) then	
		 	str='Followed_and_lost.dat'
	        	OPEN(UNIT=97, FILE=str, status="unknown", position="append")
	        	write(97,'(2(1x,E11.4) )')  P(l)%IdentPart, P(l)%sp
	        	close(UNIT=97)
              	endif 	
	      endif	

            end if
            if(The_Object(m)%ReflectionType == 3) then
              OutParticles = OutParticles + 1
              DelParticles(OutParticles) = l
              outPart = 1
	      In_flux_object(m, P(l)%sp) =  In_flux_object(m, P(l)%sp)+ 1.0	
        !write(*,*) "species type ", P(l)%sp, In_flux_object(m, 3)
              if(P(l)%sp == 0 ) Out_e      = Out_e      + 1
              if(P(l)%sp == 1 ) Out_PI_H2p = Out_PI_H2p + 1
              if(P(l)%sp == 2 ) Out_NI     = Out_NI     + 1
              if(P(l)%sp == 3 ) Out_PI_Hp  = Out_PI_Hp  + 1
              if(P(l)%sp == 10) Out_PI_H3p = Out_PI_H3p + 1
              if(P(l)%sp == 11) Out_Cs     = Out_Cs     + 1

	     

            end if
            if(The_Object(m)%ReflectionType == 4) then
              call Reflection_Box(m, l)
	      In_flux_object(m, P(l)%sp) =  In_flux_object(m, P(l)%sp)+ 1.0	
            end if

          case (2)
          case (3)
            !call Reflection_Conus(m,l)
            !write(*,*) "Reflection conus"
            if(3==3) then!P(l)%sp /= 0)  then
              OutParticles = OutParticles + 1
              DelParticles(OutParticles) = l
              outPart = 1
	      In_flux_object(m, P(l)%sp) =  In_flux_object(m, P(l)%sp)+ 1.0		
              if(P(l)%sp == 0 ) then 
		Out_e      = Out_e      + 1
		if(P(l)%IdentPart < 0.0  ) then	
		 	str='Followed_and_lost.dat'
	        	OPEN(UNIT=97, FILE=str, status="unknown", position="append")
	        	write(97,'(2(1x,E11.4) )')  P(l)%IdentPart, P(l)%sp
	        	close(UNIT=97)
              	endif
              endif
              if(P(l)%sp == 1 ) Out_PI_H2p = Out_PI_H2p + 1
              if(P(l)%sp == 2 ) Out_NI     = Out_NI     + 1
              if(P(l)%sp == 3 ) Out_PI_Hp  = Out_PI_Hp  + 1
              if(P(l)%sp == 10) Out_PI_H3p = Out_PI_H3p + 1
              if(P(l)%sp == 11) Out_Cs     = Out_Cs     + 1
	      if(P(l)%sp == 5 ) then
		if(P(l)%IdentPart < 0.0  ) then	
		 	str='Followed_and_lost.dat'
	        	OPEN(UNIT=97, FILE=str, status="unknown", position="append")
	        	write(97,'(2(1x,E11.4) )')  P(l)%IdentPart, P(l)%sp
	        	close(UNIT=97)
              	endif 
	      endif	


	      if(P(l)%sp == 9 ) then 
		 if(P(l)%IdentPart < 0.0  ) then	
		 	str='Followed_and_lost.dat'
	        	OPEN(UNIT=97, FILE=str, status="unknown", position="append")
	        	write(97,'(2(1x,E11.4) )')  P(l)%IdentPart, P(l)%sp
	        	close(UNIT=97)
              	endif 	
	      endif	

            else
              !write(*,*) "conus 1", P(l)%X, P(l)%Y, P(l)%Z, m, P(l)%VX,&
              !P(l)%VY, P(l)%VZ, P(l)%IdentPart
              if(0.5*mass(0)*(P(l)%vx*P(l)%vx+P(l)%vy*P(l)%vy+P(l)%vz*P(l)%vz)/1.6e-19>3e1) then
                call random_number(rand_num)
                if(rand_num<0.9) then
                  call Reflection_Conus(m, l)
                  isin1 = .FALSE.
                  call isinobj(P(l)%X, P(l)%Y, P(l)%Z, 2, isin1)
                  if(isin1) write(*,*) "Error conus reflection 2", P(l)%X, P(l)%Y, P(l)%Z, P(l)%VX, P(l)%VY, P(l)%VZ, m, P(l)%sp, P(l)%IdentPart
                  isin2 = .FALSE.
                  call isinobj(P(l)%X, P(l)%Y, P(l)%Z, 3, isin2)
                  if(isin2) write(*,*) "Error conus reflection 3", P(l)%X, P(l)%Y, P(l)%Z, P(l)%VX, P(l)%VY, P(l)%VZ, m, P(l)%sp, P(l)%IdentPart
                  if(isin1 .OR. isin2) then
                    OutParticles = OutParticles + 1
                    DelParticles(OutParticles) = l
                    outPart = 1

                    if(P(l)%sp == 0 ) Out_e      = Out_e      + 1
                  end if
                else
                  OutParticles = OutParticles + 1
                  DelParticles(OutParticles) = l
                  outPart = 1

                  Out_e      = Out_e      + 1
                end if
              else
                OutParticles = OutParticles + 1
                DelParticles(OutParticles) = l
                outPart = 1

                Out_e      = Out_e      + 1
              end if
              !write(*,*) "conus 2", P(l)%X, P(l)%Y, P(l)%Z, m, P(l)%VX,&
              !P(l)%VY, P(l)%VZ, P(l)%IdentPart
              
            end if
          case default
          end select
        
          if(3==3) then
          posx = P(l)%X
          posy = P(l)%Y
          posz = P(l)%Z
          
          isin = .FALSE.
          do m=1,Nb_Obj
            call isinobj(posx, posy, posz, m, isin)
            if(isin .AND. The_Object(m)%ReflectionType /= 2 &
                    .AND. The_Object(m)%ReflectionType /= 3) then
              write(*,*) "Error detected in reflection subroutine", m,&
                l, P(l)%X, P(l)%Y, P(l)%Z, posx, posy, posz
              exit
            end if
          end do
          end if
        end if
        !end if
        if(outPart == 0 ) then
        call Particles_Loss(l)
        endif

        end do

      end subroutine reflection2
      
     subroutine reflection_trajectories()
    	use Parallel
        implicit none
        include 'mpif.h'

        integer l, i, j, k, m
        logical isin, isin1, isin2
        real posx, posy, posz

        do l=1, N_p_t
        outPart=0
        isin = .FALSE.

        i = P_trajectory(l)%X/delta(0)
        j = P_trajectory(l)%Y/delta(1)
        k = P_trajectory(l)%Z/delta(2)
  
        posx = P_trajectory(l)%X
        posy = P_trajectory(l)%Y
        posz = P_trajectory(l)%Z
        
        do m=1,Nb_Obj
          call isinobj(posx, posy, posz, m, isin)
          if(isin) then
            exit
          end if
        end do

        if(isin) then
          select case (The_Object(m)%types)
          case (1)
            if(The_Object(m)%ReflectionType == 1) then
              call Reflection_Box_newT(m, l)	
            end if
              if(The_Object(m)%ReflectionType == 2) then
              OutParticles = OutParticles + 1
              DelParticles(OutParticles) = l
              outPart = 1
             end if

            if(The_Object(m)%ReflectionType == 3) then
              OutParticles = OutParticles + 1
              DelParticles(OutParticles) = l
              outPart = 1
	    end if
            
	    if(The_Object(m)%ReflectionType == 4) then
              call Reflection_Box(m, l)	
            end if

          case (2)
          case (3)
            !call Reflection_Conus(m,l)
            !write(*,*) "Reflection conus"
            if(3==3) then!P(l)%sp /= 0)  then
              OutParticles = OutParticles + 1
              DelParticles(OutParticles) = l
              outPart = 1
            else
              if(0.5*mass(0)*(P_trajectory(l)%vx*P_trajectory(l)%vx+P_trajectory(l)%vy*P_trajectory(l)%vy+P_trajectory(l)%vz*P(l)%vz)/1.6e-19>3e1) then
                call random_number(rand_num)
                if(rand_num<0.9) then
                  call Reflection_Conus(m, l)
                  isin1 = .FALSE.
                  call isinobj(P_trajectory(l)%X, P_trajectory(l)%Y, P_trajectory(l)%Z, 2, isin1)
                  if(isin1) write(*,*) "Error conus reflection 2", P_trajectory(l)%X, P_trajectory(l)%Y, P_trajectory(l)%Z, P_trajectory(l)%VX, P_trajectory(l)%VY, P_trajectory(l)%VZ, m, P_trajectory(l)%sp, P_trajectory(l)%IdentPart
                  isin2 = .FALSE.
                  call isinobj(P_trajectory(l)%X, P_trajectory(l)%Y, P_trajectory(l)%Z, 3, isin2)
                  if(isin2) write(*,*) "Error conus reflection 3", P_trajectory(l)%X, P_trajectory(l)%Y, P_trajectory(l)%Z, P_trajectory(l)%VX, P_trajectory(l)%VY, P_trajectory(l)%VZ, m, P_trajectory(l)%sp, P_trajectory(l)%IdentPart
                  if(isin1 .OR. isin2) then
                    OutParticles = OutParticles + 1
                    DelParticles(OutParticles) = l
                    outPart = 1
                  end if
                else
                  OutParticles = OutParticles + 1
                  DelParticles(OutParticles) = l
                  outPart = 1
                end if
              else
                OutParticles = OutParticles + 1
                DelParticles(OutParticles) = l
                outPart = 1
              end if
             
              
            end if
          case default
          end select
        
          if(3==3) then
          posx = P_trajectory(l)%X
          posy = P_trajectory(l)%Y
          posz = P_trajectory(l)%Z
          
          isin = .FALSE.
          do m=1,Nb_Obj
            call isinobj(posx, posy, posz, m, isin)
            if(isin .AND. The_Object(m)%ReflectionType /= 2 &
                    .AND. The_Object(m)%ReflectionType /= 3) then
              write(*,*) "Error detected in reflection subroutine", m,&
                l, P_trajectory(l)%X, P_trajectory(l)%Y, P_trajectory(l)%Z, posx, posy, posz
              exit
            end if
          end do
          end if
        end if
        !end if

        !end if
        if(outPart == 0 ) then
        	call Particles_Loss_trajectories(l)
        endif

        end do

      end subroutine reflection_trajectories	



      subroutine NI_emission_rate2(obj)
        
        use Parallel
        implicit none
        
        integer obj

        real:: s_con
        real:: s_flat
        
        if(The_Object(obj)%types==3) then
          
          s_con = 3.14*sqrt((The_Object(obj)%width(0)*2)**2 + (The_Object(obj)%width(1)-The_Object(obj)%width(2))**2) & 
                      *(The_Object(obj)%width(1)+The_Object(obj)%width(2))
          NI_rate_from_cone = (NI_emission*s_con*dt/(abs(charge(9))*weight(9)))
          
          s_flat = (Nyp)*(Nzp)*delta(1)*delta(2)-3.14*The_Object(obj)%width(1)**2 
          NI_rate_from_flat = (NI_emission* s_flat*dt/(abs(charge(5))*weight(5)))
        
          write(*,*) "Emission from obj", obj, "=", s_flat, s_con, &
          NI_rate_from_flat, NI_rate_from_cone

        else
          
          write(str3,'(i7)') iter
          lgc=len_trim(adjustl(str3))
          str='ERROR_iteration_'//str3(8-lgc:7)//'.txt'
          open(unit=65, file=str, status="unknown")
          write(65,*) "ERROR, Problem with NI emmision rate calculation"
          close(65)
          Stop
        
        endif 
      
      end subroutine NI_emission_rate2
      
      subroutine NI_emission_rate3(obj)
        
        use Parallel
        implicit none
        
        integer obj, i, j, k

        real:: s_con
        real:: s_flat
        real :: posx, posy, posz, drrandom, rrandomstart, rrandom, start_real, end_real, theta
        
        if(The_Object(obj)%types==3) then

          j = 0
          rrandomstart = The_Object(obj)%width(2)**2
          drrandom = The_Object(obj)%width(1)**2-The_Object(obj)%width(2)**2
          start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)
          end_real=The_Object(obj)%center(0)+The_Object(obj)%width(0)

          do i=1, 1000000
        
            call random_number(rand_num)
            rrandom = sqrt(rand_num*drrandom + rrandomstart)
            
            posx = (end_real-start_real)/(The_Object(obj)%width(2)-The_Object(obj)%width(1))*(rrandom-The_Object(obj)%width(1))+start_real
                        
            rrandom = rrandom - 1e-7
            
            call random_number(rand_num)
            theta=rand_num*acos(-1.)*2.0
            
            posy=cos(theta)*rrandom+The_Object(obj)%center(1)
            posz=sin(theta)*rrandom+The_Object(obj)%center(2)

            if(posx<0 .OR. posx>Nxp*delta(0) .OR. &
               posy<0 .OR. posy>Nyp*delta(1) .OR. &
               posz<0 .OR. posz>Nzp*delta(2)) then
              continue
            else
              j=j+1
            end if

          end do

          k=j

          s_con = 3.14*sqrt((The_Object(obj)%width(0)*2)**2 + (The_Object(obj)%width(1)-The_Object(obj)%width(2))**2) & 
                      *(The_Object(obj)%width(1)+The_Object(obj)%width(2))*real(j)/1000000
          NI_rate_from_cone = (NI_emission*s_con*dt/(abs(charge(9))*weight(9)))
          
          j = 0
          rrandomstart = The_Object(obj)%width(2)**2
          drrandom = The_Object(obj)%width(1)**2-The_Object(obj)%width(2)**2
          start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)
          end_real=The_Object(obj)%center(0)+The_Object(obj)%width(0)

          do i=1, 1000000
        
            call random_number(rand_num)
            posy = Nyp*delta(1)*rand_num
            call random_number(rand_num)
            posz = Nzp*delta(2)*rand_num
           
            if(sqrt( (posy-The_Object(obj)%center(1))*(posy-The_Object(obj)%center(1)) &
                    +(posz-The_Object(obj)%center(2))*(posz-The_Object(obj)%center(2))) &
                  < The_Object(obj)%width(1)) then
              continue
            else
              j = j+1
            end if
          
          end do

          
          s_flat = Nyp*Nzp*delta(1)*delta(2)*real(j)/1000000 
          NI_rate_from_flat = (NI_emission* s_flat*dt/(abs(charge(5))*weight(5)))
        
          write(*,*) "Emission 3 from obj", obj, "=", s_flat, s_con, &
          NI_rate_from_flat, NI_rate_from_cone, j, k

        else
          
          write(str3,'(i7)') iter
          lgc=len_trim(adjustl(str3))
          str='ERROR_iteration_'//str3(8-lgc:7)//'.txt'
          open(unit=65, file=str, status="unknown")
          write(65,*) "ERROR, Problem with NI emmision rate calculation"
          close(65)
          Stop
        
        endif 
      
      end subroutine NI_emission_rate3


      subroutine Flux_NI_from_PG_wall_2(obj, NToAdd)

        use Parallel
        implicit none
        include 'mpif.h'

        integer obj, NToAdd
        
        integer i,testIn,counter_essays,integer_random_100
        real auxx(3),Vt,Vt2,erfi
        TYPE(Particle) :: current_Par  
        
        
        
        integer :: counterTest1=0
        integer :: j_start=0
        integer :: j_end=0
        integer :: k_start=0
        integer :: k_end=0
        
        
        real :: num_rand_r=0
        real :: i_start_real=0.0
        
        real xbegin, xend, ybegin, yend, zbegin, zend

        xbegin = i0*delta(0)
        xend = (i1+1)*delta(0)
        ybegin = j0*delta(1)
        yend = (j1+1)*delta(1)
        zbegin = k0*delta(2)
        zend = (k1+1)*delta(2)
        
        integer_random_100=0
        
        j_start=0
        j_end=Nyp !Could not put j1 or k1 because in the case of few procces in the same direction one will have k1= 32 another 98
        
        k_start=0
        k_end=Nzp
        
        Vt=0
        Vt2=0
        erfi=0
        
        
        i_start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)-1e-7
        
        current_Par%sp=5
        current_Par%X=i_start_real
       
        NPartInPToAdd = 0
        if(rank==0) then
          do i=1, NToAdd
            counter_essays=0
            
            
            testIn=0
            counterTest1=counterTest1+1
            
            do while (testIn==0)
              counter_essays=counter_essays+1
            
              call random_number(num_rand_r)
              current_Par%Y=j_end*delta(1)*num_rand_r
              
              call random_number(num_rand_r)
              current_Par%Z=k_end*delta(2)*num_rand_r
              
              if(sqrt(((current_Par%Y-The_Object(obj)%center(1))**2) + &
                       (current_Par%Z-The_Object(obj)%center(2))**2) > &
                       The_Object(obj)%width(1)+1e-5) then 
                     
                testIn=1
                
                !  call random_number(num_rand_r)
                Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
                Vt2=sqrt(2.0)*Vt
                
                call random_number(auxx)
                auxx=auxx*2.0-1.0
                
                call InvErrorFunction(auxx(1),erfi)
                current_Par%VX=-abs(Vt2*erfi)
                
                call InvErrorFunction(auxx(2),erfi)
                current_Par%VY=Vt2*erfi
                
                call InvErrorFunction(auxx(3),erfi)
                current_Par%VZ=Vt2*erfi
                
                current_Par%IdentPart=0*IdentPartNumber*10000000
                !IdentPartNumber=IdentPartNumber+1
                
                counter_P_wall=counter_P_wall+1
                P_wall(counter_P_wall)%X_wall=current_Par%X
                P_wall(counter_P_wall)%Y_wall=current_Par%Y
                P_wall(counter_P_wall)%Z_wall=current_Par%Z
                
                if(counter_P_wall==100) counter_P_wall=0
                  
              endif
                
              if(counter_essays==100000 .AND. testIn==0) then
                
                testIn=1
                
                call random_number(num_rand_r)
                integer_random_100=num_rand_r*100
                
                current_Par%X=i_start_real
                current_Par%Y=P_wall(integer_random_100)%Y_wall
                current_Par%Z=P_wall(integer_random_100)%Z_wall
                
                Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
                Vt2=sqrt(2.0)*Vt
                
                call random_number(auxx)
                auxx=auxx*2.0-1.0
                
                call InvErrorFunction(auxx(1),erfi)
                current_Par%VX=-abs(Vt2*erfi)
                
                call InvErrorFunction(auxx(2),erfi)
                current_Par%VY=Vt2*erfi*0
                
                call InvErrorFunction(auxx(3),erfi)
                current_Par%VZ=Vt2*erfi*0
                
                current_Par%IdentPart=0*IdentPartNumber*10000000
                !IdentPartNumber=IdentPartNumber+1
                
              endif
            enddo
 
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par

         enddo
        endif
        
        Call MPI_BCAST(NPartInPToAdd, 1,MPI_INTEGER,0,MPI_COMM_WORLD, info)
        Call MPI_BCAST(PToAdd, 8*NPartInPToAdd,MPI_REAL,0,MPI_COMM_WORLD, info)
      
        !write(*,*) "NI flat", rank, NPartInPToAdd, NToAdd, Npart, xbegin, xend, ybegin,&
        !yend, zbegin, zend
        do i=1, NPartInPToAdd
          if(PToAdd(i)%x >= xbegin .AND. PToAdd(i)%x < xend .AND. &
             PToAdd(i)%y >= ybegin .AND. PToAdd(i)%y < yend .AND. &
             PToAdd(i)%z >= zbegin .AND. PToAdd(i)%z < zend ) then
            
            Npart=Npart+1
            P(Npart)=PToAdd(i)
          
          endif
        end do
        !write(*,*) "NI flat", rank, NPartInPToAdd, Npart
        
        NPartInPToAdd = 0

      end subroutine Flux_NI_from_PG_wall_2
      
     subroutine Flux_NI_from_PG_wall_3(obj, NToAdd)

        use Parallel
        implicit none
        include 'mpif.h'

        integer obj, NToAdd
        
        integer i,testIn,counter_essays,integer_random_100
        real auxx(3),Vt,Vt2,erfi
        TYPE(Particle) :: current_Par  
         
        integer :: counterTest1=0
        integer :: j_start=0
        integer :: j_end=0
        integer :: k_start=0
        integer :: k_end=0
        
        
        real :: num_rand_r=0
        real :: i_start_real=0.0
        
        real xbegin, xend, ybegin, yend, zbegin, zend

	real sin_t_d,cos_t_d,sin_f_d,cos_f_d
	real rrandom, drrandom, rrandomstart, norm, phi, randomV, VmaxMaxwell, PmaxMaxwell, Cste1Maxwell, Cste2Maxwell


	VmaxMaxwell = 4.*sqrt(2.*K_b*temperature(9)/mass(9))
        PmaxMaxwell = 4.*sqrt(mass(9)/(2.*acos(-1.)*K_b*temperature(9)))*exp(-1.)
        Cste1Maxwell = (mass(9)/(2.*acos(-1.)*k_b*temperature(9)))**(3./2.)*4.*acos(-1.)
        Cste2Maxwell = mass(9)/(2.*k_b*temperature(9))

        xbegin = i0*delta(0)
        xend = (i1+1)*delta(0)
        ybegin = j0*delta(1)
        yend = (j1+1)*delta(1)
        zbegin = k0*delta(2)
        zend = (k1+1)*delta(2)
        
        integer_random_100=0
        
        j_start=0
        j_end=Nyp !Could not put j1 or k1 because in the case of few procces in the same direction one will have k1= 32 another 98
        
        k_start=0
        k_end=Nzp
        
        Vt=0
        Vt2=0
        erfi=0
        
	sin_t_d = 0.0 
	cos_t_d = 0.0
	sin_f_d = 0.0 
	cos_f_d = 0.0        


        i_start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)-1e-7
        
        current_Par%sp=5
        current_Par%X=i_start_real
       
        NPartInPToAdd = 0
        if(rank==0) then
          do i=1, NToAdd
            counter_essays=0
                
            testIn=0
            counterTest1=counterTest1+1
            
            do while (testIn==0)
              counter_essays=counter_essays+1
            
              call random_number(num_rand_r)
              current_Par%Y=j_end*delta(1)*num_rand_r
              
              call random_number(num_rand_r)
              current_Par%Z=k_end*delta(2)*num_rand_r
              
              if(sqrt(((current_Par%Y-The_Object(obj)%center(1))**2) + &
                       (current_Par%Z-The_Object(obj)%center(2))**2) > &
                       The_Object(obj)%width(1)+1e-5) then 
                     
                testIn=1
              endif  
       
                current_Par%IdentPart=0*IdentPartNumber*10000000
                !IdentPartNumber=IdentPartNumber+1
                
	     enddo
 

	!Velocity with maxwellian distribution
            testIn = 1
            do while(testIn==1)
              call random_number(randomV)
              call random_number(rrandom)
              
              randomV = randomV*VmaxMaxwell
              rrandom = rrandom*PmaxMaxwell

              if( rrandom < Cste1Maxwell*randomV*randomV*exp(-Cste2Maxwell*randomV*randomV) ) then
                Vt = randomV
                testIn = 0
              end if
            end do

	     call random_number(auxx)
                
		sin_t_d = sqrt(auxx(1)) 
		cos_t_d = sqrt( 1.0 - sin_t_d*sin_t_d)
		sin_f_d = sin(2.0*acos(-1.)*auxx(2)) 
		cos_f_d = cos(2.0*acos(-1.)*auxx(2))                

                current_Par%VX=-Vt*cos_t_d             
                current_Par%VY=Vt*sin_t_d*cos_f_d               
                current_Par%VZ=Vt*sin_t_d*sin_f_d	

            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par

         enddo
        endif
        
        Call MPI_BCAST(NPartInPToAdd, 1,MPI_INTEGER,0,MPI_COMM_WORLD, info)
        Call MPI_BCAST(PToAdd, 8*NPartInPToAdd,MPI_REAL,0,MPI_COMM_WORLD, info)
      
        !write(*,*) "NI flat", rank, NPartInPToAdd, NToAdd, Npart, xbegin, xend, ybegin,&
        !yend, zbegin, zend
        do i=1, NPartInPToAdd
          if(PToAdd(i)%x >= xbegin .AND. PToAdd(i)%x < xend .AND. &
             PToAdd(i)%y >= ybegin .AND. PToAdd(i)%y < yend .AND. &
             PToAdd(i)%z >= zbegin .AND. PToAdd(i)%z < zend ) then
            
            Npart=Npart+1
            P(Npart)=PToAdd(i)
          
          endif
        end do
        !write(*,*) "NI flat", rank, NPartInPToAdd, Npart
        
        NPartInPToAdd = 0

      end subroutine Flux_NI_from_PG_wall_3






      subroutine Flux_NI_from_PG_sin_2(obj, NToAdd)
        
        use Parallel
        implicit none
        include 'mpif.h'

        integer obj, NToAdd
        
        integer i,testIn
        real Vt,Vt2,erfi,j,num_rand,angle_con
        real x0,x1,y0,y1,y_pos,Conus_r_y, theta
        
        TYPE(Particle) :: current_Par  
        integer :: i_start=0
        integer :: i_end=0
        
        real :: start_real=0.0
        real :: end_real=0.0
        
        integer :: j_start=0
        integer :: j_end=0
        
        integer :: k_start=0
        integer :: k_end=0
        
        integer num_rand_int
        
        real alfa,py_n,pz_n
        
        real xbegin, xend, ybegin, yend, zbegin, zend
        real rrandom, drrandom, rrandomstart, norm, phi, randomV, VmaxMaxwell, PmaxMaxwell, Cste1Maxwell, Cste2Maxwell

        real n(3), tco(3), tc(3)

        xbegin = i0*delta(0)
        xend = (i1+1)*delta(0)
        ybegin = j0*delta(1)
        yend = (j1+1)*delta(1)
        zbegin = k0*delta(2)
        zend = (k1+1)*delta(2)

        VmaxMaxwell = 4.*sqrt(2.*K_b*temperature(9)/mass(9))
        PmaxMaxwell = 4.*sqrt(mass(9)/(2.*acos(-1.)*K_b*temperature(9)))*exp(-1.)
        Cste1Maxwell = (mass(9)/(2.*acos(-1.)*k_b*temperature(9)))**(3./2.)*4.*acos(-1.)
        Cste2Maxwell = mass(9)/(2.*k_b*temperature(9))
        
        i_start=68
        i_end=74
        
        j_start=0
        j_end=Nyp-3 !Could not put j1 or k1 becouse in the case of few procces in the same direction one will have k1= 32 another 98
        
        k_start=0
        k_end=Nzp-3
        
        Vt=0
        Vt2=0
        erfi=0
        alfa=0
        
        num_rand=0
        num_rand_int=1
        y_pos=0
        Conus_r_y=0
        py_p=0
        pz_p=0
        py_n=0
        pz_n=0
        vy_p=0
        vz_p=0
        angle_con=0
        
        !  start_real=0.019
        !  end_real=0.0226
        
        start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)
        end_real=The_Object(obj)%center(0)+The_Object(obj)%width(0)
        x1=The_Object(obj)%center(0)+The_Object(obj)%width(0)
        x0=The_Object(obj)%center(0)-The_Object(obj)%width(0)
        y1=The_Object(obj)%center(1)+The_Object(obj)%width(2)
        y0=The_Object(obj)%center(1)+The_Object(obj)%width(1)

            
        current_Par%sp=9

        rrandomstart = The_Object(obj)%width(2)**2
        drrandom = The_Object(obj)%width(1)**2-The_Object(obj)%width(2)**2
        
        NPartInPToAdd = 0

        do j=1,NToAdd
          
          if(rank==0) then

            !Getting x, y, z of the new particle
        
            call random_number(num_rand)

            rrandom = sqrt(num_rand*drrandom + rrandomstart)
            
            current_Par%X = (end_real-start_real)/(The_Object(obj)%width(2)-The_Object(obj)%width(1))*(rrandom-The_Object(obj)%width(1))+start_real
                        
            rrandom = rrandom - 1e-7
            
            call random_number(num_rand)
            theta=num_rand*acos(-1.)*2.0
            
            current_Par%Y=cos(theta)*rrandom+The_Object(obj)%center(1)
            current_Par%Z=sin(theta)*rrandom+The_Object(obj)%center(2)

            !Getting the new velocity of the new particle

            !First step: get the new repere with n normal to the conus surface

            tco(1) = x0-The_Object(obj)%width(1)*(x1-x0)/(The_Object(obj)%width(2)-The_Object(obj)%width(1)) - current_Par%X
            tco(2) = The_Object(obj)%center(1) - current_Par%Y
            tco(3) = The_Object(obj)%center(2) - current_Par%Z
            norm = sqrt(tco(1)*tco(1) + tco(2)*tco(2) + tco(3)*tco(3))
            tco(1) = tco(1)/norm
            tco(2) = tco(2)/norm
            tco(3) = tco(3)/norm

            tc(1) = 0
            tc(2) = sin(theta)
            tc(3) = -cos(theta)

            n(2) = The_Object(obj)%center(1) - current_Par%Y
            n(3) = The_Object(obj)%center(2) - current_Par%Z
            n(1) = -(n(2)*tco(2) + n(3)*tco(3))/tco(1)
            norm = sqrt(n(1)*n(1) + n(2)*n(2) + n(3)*n(3))
            n(1) = n(1)/norm
            n(2) = n(2)/norm
            n(3) = n(3)/norm

            !Second step: velocity!

            !velocity ie energy is constant here
            !Vt = sqrt(2*K_b*temperature(current_Par%sp)/mass(current_Par%sp))

            !Velocity with maxwellian distribution
            testIn = 1
            do while(testIn==1)
              call random_number(randomV)
              call random_number(rrandom)
              
              randomV = randomV*VmaxMaxwell
              rrandom = rrandom*PmaxMaxwell

              if(rrandom<Cste1Maxwell*randomV*randomV*exp(-Cste2Maxwell*randomV*randomV)) then
                Vt = randomV
                testIn = 0
              end if
            end do

            call random_number(num_rand)
            theta = asin(-num_rand)+acos(-1.)/2.

            call random_number(num_rand)
            phi = num_rand*acos(-1.)*2.

            !this is probably not isotropic
            current_Par%VX = Vt*(cos(theta)*n(1) + sin(theta)*(cos(phi)*tco(1) + sin(phi)*tc(1)))
            current_Par%VY = Vt*(cos(theta)*n(2) + sin(theta)*(cos(phi)*tco(2) + sin(phi)*tc(2)))
            current_Par%VZ = Vt*(cos(theta)*n(3) + sin(theta)*(cos(phi)*tco(3) + sin(phi)*tc(3)))
           
            !current_Par.IdentPart=0
            current_Par%IdentPart=0*IdentPartNumber*20000000
            !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par

          endif

        enddo

        Call MPI_BCAST(NPartInPToAdd, 1,MPI_INTEGER,0,MPI_COMM_WORLD, info)
        Call MPI_BCAST(PToAdd, 8*NPartInPToAdd,MPI_REAL,0,MPI_COMM_WORLD, info)
       
        do i=1, NPartInPToAdd
          if(PToAdd(i)%x >= xbegin .AND. PToAdd(i)%x < xend .AND. &
             PToAdd(i)%y >= ybegin .AND. PToAdd(i)%y < yend .AND. &
             PToAdd(i)%z >= zbegin .AND. PToAdd(i)%z < zend ) then
            
            Npart=Npart+1
            P(Npart)=PToAdd(i)
          
          endif
        end do

        NPartInPToAdd = 0

      
      End subroutine Flux_NI_from_PG_sin_2
      
      subroutine Flux_NI_from_PG_sin_3(obj, NToAdd)
        
        use Parallel
        implicit none
        include 'mpif.h'

        integer obj, NToAdd
        
        integer i,testIn
        real Vt,Vt2,erfi,j,num_rand,angle_con
        real x0,x1,y0,y1,y_pos,Conus_r_y, theta
        
        TYPE(Particle) :: current_Par  
        integer :: i_start=0
        integer :: i_end=0
        
        real :: start_real=0.0
        real :: end_real=0.0
        
        integer :: j_start=0
        integer :: j_end=0
        
        integer :: k_start=0
        integer :: k_end=0
        
        integer num_rand_int
        
        real alfa,py_n,pz_n
        
        real xbegin, xend, ybegin, yend, zbegin, zend
        real rrandom, drrandom, rrandomstart, norm, phi, randomV, VmaxMaxwell, PmaxMaxwell, Cste1Maxwell, Cste2Maxwell

        real n(3), tco(3), tc(3)

        xbegin = i0*delta(0)
        xend = (i1+1)*delta(0)
        ybegin = j0*delta(1)
        yend = (j1+1)*delta(1)
        zbegin = k0*delta(2)
        zend = (k1+1)*delta(2)

        VmaxMaxwell = 4.*sqrt(2.*K_b*temperature(9)/mass(9))
        PmaxMaxwell = 4.*sqrt(mass(9)/(2.*acos(-1.)*K_b*temperature(9)))*exp(-1.)
        Cste1Maxwell = (mass(9)/(2.*acos(-1.)*k_b*temperature(9)))**(3./2.)*4.*acos(-1.)
        Cste2Maxwell = mass(9)/(2.*k_b*temperature(9))
        
        i_start=68
        i_end=74
        
        j_start=0
        j_end=Nyp-3 !Could not put j1 or k1 becouse in the case of few procces in the same direction one will have k1= 32 another 98
        
        k_start=0
        k_end=Nzp-3
        
        Vt=0
        Vt2=0
        erfi=0
        alfa=0
        
        num_rand=0
        num_rand_int=1
        y_pos=0
        Conus_r_y=0
        py_p=0
        pz_p=0
        py_n=0
        pz_n=0
        vy_p=0
        vz_p=0
        angle_con=0
        
        !  start_real=0.019
        !  end_real=0.0226
        
        start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)
        end_real=The_Object(obj)%center(0)+The_Object(obj)%width(0)
        x1=The_Object(obj)%center(0)+The_Object(obj)%width(0)
        x0=The_Object(obj)%center(0)-The_Object(obj)%width(0)
        y1=The_Object(obj)%center(1)+The_Object(obj)%width(2)
        y0=The_Object(obj)%center(1)+The_Object(obj)%width(1)

            
        current_Par%sp=9

        rrandomstart = The_Object(obj)%width(2)**2
        drrandom = The_Object(obj)%width(1)**2-The_Object(obj)%width(2)**2
        
        NPartInPToAdd = 0

        do j=1,NToAdd
          
          if(rank==0) then

            testIn = 0

            do while(testIn==0)

              !Getting x, y, z of the new particle
          
              call random_number(num_rand)

              rrandom = sqrt(num_rand*drrandom + rrandomstart)
              
              current_Par%X = (end_real-start_real)/(The_Object(obj)%width(2)-The_Object(obj)%width(1))*(rrandom-The_Object(obj)%width(1))+start_real
                          
              rrandom = rrandom - 1e-7
              
              call random_number(num_rand)
              theta=num_rand*acos(-1.)*2.0
              
              current_Par%Y=cos(theta)*rrandom+The_Object(obj)%center(1)
              current_Par%Z=sin(theta)*rrandom+The_Object(obj)%center(2)

              if(current_Par%x>0 .AND. current_Par%x<Nxp*delta(0) .AND. &
                 current_Par%y>0 .AND. current_Par%y<Nyp*delta(1) .AND. &
                 current_Par%z>0 .AND. current_Par%z<Nzp*delta(2)) then
                testIn = 1
              end if

            end do

            !Getting the new velocity of the new particle

            !First step: get the new repere with n normal to the conus surface

            tco(1) = x0-The_Object(obj)%width(1)*(x1-x0)/(The_Object(obj)%width(2)-The_Object(obj)%width(1)) - current_Par%X
            tco(2) = The_Object(obj)%center(1) - current_Par%Y
            tco(3) = The_Object(obj)%center(2) - current_Par%Z
            norm = sqrt(tco(1)*tco(1) + tco(2)*tco(2) + tco(3)*tco(3))
            tco(1) = tco(1)/norm
            tco(2) = tco(2)/norm
            tco(3) = tco(3)/norm

            tc(1) = 0
            tc(2) = sin(theta)
            tc(3) = -cos(theta)

            n(2) = The_Object(obj)%center(1) - current_Par%Y
            n(3) = The_Object(obj)%center(2) - current_Par%Z
            n(1) = -(n(2)*tco(2) + n(3)*tco(3))/tco(1)
            norm = sqrt(n(1)*n(1) + n(2)*n(2) + n(3)*n(3))
            n(1) = n(1)/norm
            n(2) = n(2)/norm
            n(3) = n(3)/norm

            !Second step: velocity!

            !velocity ie energy is constant here
            !Vt = sqrt(2*K_b*temperature(current_Par%sp)/mass(current_Par%sp))

            !Velocity with maxwellian distribution
            testIn = 1
            do while(testIn==1)
              call random_number(randomV)
              call random_number(rrandom)
              
              randomV = randomV*VmaxMaxwell
              rrandom = rrandom*PmaxMaxwell

              if(rrandom<Cste1Maxwell*randomV*randomV*exp(-Cste2Maxwell*randomV*randomV)) then
                Vt = randomV
                testIn = 0
              end if
            end do

            call random_number(num_rand)
            theta = asin(-num_rand)+acos(-1.)/2.

            call random_number(num_rand)
            phi = num_rand*acos(-1.)*2.

            !this is probably not isotropic
            current_Par%VX = Vt*(cos(theta)*n(1) + sin(theta)*(cos(phi)*tco(1) + sin(phi)*tc(1)))
            current_Par%VY = Vt*(cos(theta)*n(2) + sin(theta)*(cos(phi)*tco(2) + sin(phi)*tc(2)))
            current_Par%VZ = Vt*(cos(theta)*n(3) + sin(theta)*(cos(phi)*tco(3) + sin(phi)*tc(3)))
           
            !current_Par.IdentPart=0
            current_Par%IdentPart=0*IdentPartNumber*20000000
            !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par

          endif

        enddo

        Call MPI_BCAST(NPartInPToAdd, 1,MPI_INTEGER,0,MPI_COMM_WORLD, info)
        Call MPI_BCAST(PToAdd, 8*NPartInPToAdd,MPI_REAL,0,MPI_COMM_WORLD, info)
       
        do i=1, NPartInPToAdd
          if(PToAdd(i)%x >= xbegin .AND. PToAdd(i)%x < xend .AND. &
             PToAdd(i)%y >= ybegin .AND. PToAdd(i)%y < yend .AND. &
             PToAdd(i)%z >= zbegin .AND. PToAdd(i)%z < zend ) then
            
            Npart=Npart+1
            P(Npart)=PToAdd(i)
          
          endif
        end do

        NPartInPToAdd = 0

      
      End subroutine Flux_NI_from_PG_sin_3

      
    
      
      subroutine AddParticleMPI3()
        
        use Parallel
        implicit none
        include 'mpif.h'
        
        integer i
        real auxx(3),Vt,Vt2,erfi
        TYPE(Particle) :: current_Par  
              
        integer :: j_start=0
        integer :: j_end=0
        
        integer :: k_start=0
        integer :: k_end=0

        integer counterAddAll(0:11)
        real xbegin, xend, ybegin, yend, zbegin, zend

        real xsave(1:Tot_Out_PI_Hp+Tot_Out_PI_H2p+Tot_Out_PI_H3p+Tot_Out_Cs)
        real ysave(1:Tot_Out_PI_Hp+Tot_Out_PI_H2p+Tot_Out_PI_H3p+Tot_Out_Cs)
        real zsave(1:Tot_Out_PI_Hp+Tot_Out_PI_H2p+Tot_Out_PI_H3p+Tot_Out_Cs)

        counterAddAll = 0
        xbegin = i0*delta(0)
        xend = (i1+1)*delta(0)
        ybegin = j0*delta(1)
        yend = (j1+1)*delta(1)
        zbegin = k0*delta(2)
        zend = (k1+1)*delta(2)
             
        j_start=0
        j_end=Nyp !Could not put j1 or k1 becouse in the case of few procces in the same direction one will have k1= 32 another 98
        
        k_start=0
        k_end=Nzp
        
        Vt=0
        Vt2=0
        erfi=0
        
        !------------------------------------------------Add New Electrons
       
       Tot_Out_e = Tot_Out_PI_Hp+Tot_Out_PI_H2p+Tot_Out_PI_H3p+Tot_Out_Cs
        do i=1,Tot_Out_e
          
          if(rank==0) then
            
            current_Par%sp=0
            Vt =sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            !current_Par.X=auxx(1)*delta(0)*(i_end-i_start)+i_start*delta(0)
            current_Par%X=auxx(1)*(plasma_x1-plasma_x0) + plasma_x0
            current_Par%Y=auxx(2)*delta(1)*(j_end-j_start)
            current_Par%Z=auxx(3)*delta(2)*(k_end-k_start)

            xsave(i) = current_Par%x
            ysave(i) = current_Par%y
            zsave(i) = current_Par%z
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart=0*IdentPartNumber
            !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par
          
          endif
                   
        enddo
        
        Tot_Out_e=0
        
        !------------------------------------------------------------------
        
        !------------------------------------------------Add New Positive ions H+
        
        do i=1,Tot_Out_PI_Hp
          
          if(rank==0) then
            
            current_Par%sp=3
            Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            !current_Par.X=auxx(1)*delta(0)*(i_end-i_start)+i_start*delta(0)
            current_Par%X=xsave(i)
            current_Par%Y=ysave(i)
            current_Par%Z=zsave(i)
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart=0*IdentPartNumber
            !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par
          
          endif
          
        enddo
        
        Tot_Out_PI_Hp=0
        
        !-------------------------------------------------------------------------------
        
        !------------------------------------------------Add New Positive ions H2+
        
        do i=1,Tot_Out_PI_H2p
        
          if(rank==0) then
            
            current_Par%sp=1
            Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            !current_Par.X=auxx(1)*delta(0)*(i_end-i_start)+i_start*delta(0)
            current_Par%X=xsave(i+Tot_Out_PI_Hp)
            current_Par%Y=ysave(i+Tot_Out_PI_Hp)
            current_Par%Z=zsave(i+Tot_Out_PI_Hp)
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart=0*IdentPartNumber
            !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par
         
          endif
          
        enddo
        
        Tot_Out_PI_H2p=0
        
        !-------------------------------------------------------------------------------
        
        !------------------------------------------------Add New Positive ions H3+
        
        do i=1,Tot_Out_PI_H3p
        
          if(rank==0) then
            
            current_Par%sp=10
            Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            !current_Par.X=auxx(1)*delta(0)*(i_end-i_start)+i_start*delta(0)
            current_Par%X=xsave(i+Tot_Out_PI_Hp+Tot_Out_PI_H2p)
            current_Par%Y=ysave(i+Tot_Out_PI_Hp+Tot_Out_PI_H2p)
            current_Par%Z=zsave(i+Tot_Out_PI_Hp+Tot_Out_PI_H2p)
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart=0*IdentPartNumber
            !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par
          
          endif
          
        enddo
        
        Tot_Out_PI_H3p=0
        
        !-------------------------------------------------------------------------------
        
        !------------------------------------------------Add New Negative ions
        
        do i=1,Tot_Out_NI_Hm
          
          if(rank==0) then
            
            current_Par%sp=2
            Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            !current_Par.X=auxx(1)*delta(0)*(i_end-i_start)+i_start*delta(0)
            current_Par%X=auxx(1)*(plasma_x1-plasma_x0) + plasma_x0
            current_Par%Y=auxx(2)*delta(1)*(j_end-j_start)
            current_Par%Z=auxx(3)*delta(2)*(k_end-k_start)
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart=0*IdentPartNumber
           !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par
          
          endif
          
        enddo
        
        Tot_Out_NI_Hm=0
        
        !-------------------------------------------------------------------------------
        
        !------------------------------------------------Add New Cs+
        
        do i=1,Tot_Out_Cs
        
          if(rank==0) then
            
            current_Par%sp=11
            Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            !current_Par.X=auxx(1)*delta(0)*(90-i_start)+i_start*delta(0)
            current_Par%X=xsave(i+Tot_Out_PI_Hp+Tot_Out_PI_H2p+Tot_Out_PI_H3p)
            current_Par%Y=ysave(i+Tot_Out_PI_Hp+Tot_Out_PI_H2p+Tot_Out_PI_H3p)
            current_Par%Z=zsave(i+Tot_Out_PI_Hp+Tot_Out_PI_H2p+Tot_Out_PI_H3p)
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart=0*IdentPartNumber
            !IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par

            
          endif
          
        enddo
        
        Tot_Out_Cs=0
            
        Call MPI_BCAST(NPartInPToAdd, 1,MPI_INTEGER,0,MPI_COMM_WORLD, info)
        Call MPI_BCAST(PToAdd, 8*NPartInPToAdd,MPI_REAL,0,MPI_COMM_WORLD, info)

        do i=1,NPartInPToAdd

          if(PToAdd(i)%x > xbegin .AND. PToAdd(i)%x <= xend .AND. &
             PToAdd(i)%y > ybegin .AND. PToAdd(i)%y <= yend .AND. &
             PToAdd(i)%z > zbegin .AND. PToAdd(i)%z <= zend ) then
           
            Npart = Npart+1
            P(Npart) = PToAdd(i)
            counterAddAll(P(Npart)%sp) = counterAddAll(P(Npart)%sp) + 1

          end if

        end do

        NPartInPToAdd = 0
        
        Counter_Add_e = counterAddAll(0)
        Counter_Add_Hp = counterAddAll(3)
        Counter_Add_H2p = counterAddAll(1)
        Counter_Add_H3p = counterAddAll(10)
        Counter_Add_NI_Hm = counterAddAll(2)
        Counter_Add_Cs = counterAddAll(11)
        
        !------------------------------------------------------------------
      
      end subroutine AddParticleMPI3


subroutine Particle_trajectory_loading(obj)
        
        use Parallel
        implicit none
        include 'mpif.h'
        
        integer i
        real auxx(3),Vt,Vt2,erfi
        TYPE(Particle) :: current_Par  
              
        integer :: j_start=0
        integer :: j_end=0
        
        integer :: k_start=0
        integer :: k_end=0

        integer counterAddAll(0:11)
        real xbegin, xend, ybegin, yend, zbegin, zend

	integer testIn,counter_essays,integer_random_100
           
        integer :: counterTest1=0 
        integer :: obj
        real :: num_rand_r=0
        real :: i_start_real=0.0

	real sin_t_d,cos_t_d,sin_f_d,cos_f_d
	real rrandom, drrandom, rrandomstart, norm, phi, randomV, VmaxMaxwell, PmaxMaxwell, Cste1Maxwell, Cste2Maxwell
	
	real num_rand
        real x0,x1,theta      
        real :: start_real=0.0
        real :: end_real=0.0
        real n(3), tco(3), tc(3)


	VmaxMaxwell = 4.*sqrt(2.*K_b*temperature(9)/mass(9))
        PmaxMaxwell = 4.*sqrt(mass(9)/(2.*acos(-1.)*K_b*temperature(9)))*exp(-1.)
        Cste1Maxwell = (mass(9)/(2.*acos(-1.)*k_b*temperature(9)))**(3./2.)*4.*acos(-1.)
        Cste2Maxwell = mass(9)/(2.*k_b*temperature(9))

    
	sin_t_d = 0.0 
	cos_t_d = 0.0
	sin_f_d = 0.0 
	cos_f_d = 0.0        

        i_start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)-1e-7

        counterAddAll = 0
        xbegin = i0*delta(0)
        xend = (i1+1)*delta(0)
        ybegin = j0*delta(1)
        yend = (j1+1)*delta(1)
        zbegin = k0*delta(2)
        zend = (k1+1)*delta(2)
             
        j_start=0
        j_end=Nyp !Could not put j1 or k1 becouse in the case of few procces in the same direction one will have k1= 32 another 98
        
        k_start=0
        k_end=Nzp
        
        Vt=0
        Vt2=0
        erfi=0
        IdentPartNumber=1
        !------------------------------------------------Add source region Electrons
        do i=1,(N_trajectory/4)
          
          if(rank==0) then
            
            current_Par%sp=0
            Vt =sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            current_Par%X=auxx(1)*(plasma_x1-plasma_x0) + plasma_x0
            current_Par%Y=auxx(2)*delta(1)*(j_end-j_start)
            current_Par%Z=auxx(3)*delta(2)*(k_end-k_start)
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart= IdentPartNumber
            IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par
          
          endif
                   
        enddo
        
        !-------------------------------------------------------------------------------
        
        !------------------------------------------------Add volume Negative ions
        
        do i=(NPartInPToAdd+1),(N_trajectory/2)
          
          if(rank==0) then
            
            current_Par%sp=2
            Vt=sqrt(K_b*temperature(current_Par%sp)/mass(current_Par%sp))
            Vt2=sqrt(2.0)*Vt
            
            call random_number(auxx)
            
            current_Par%X=auxx(1)*(plasma_x1-plasma_x0) + plasma_x0
            current_Par%Y=auxx(2)*delta(1)*(j_end-j_start)
            current_Par%Z=auxx(3)*delta(2)*(k_end-k_start)
            
            call random_number(auxx)
            auxx=auxx*2.0-1.0
            
            call InvErrorFunction(auxx(1),erfi)
            current_Par%VX=Vt2*erfi
            call InvErrorFunction(auxx(2),erfi)
            current_Par%VY=Vt2*erfi
            call InvErrorFunction(auxx(3),erfi)
            current_Par%VZ=Vt2*erfi
            
            current_Par%IdentPart=IdentPartNumber
            IdentPartNumber=IdentPartNumber+1
            
	    NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par
          
          endif
          
        enddo
        
!-------------------------------------------------------------------------------
        
        !------------------------------------------------Add Flat PG Negative ions

	if(rank==0) then
	  current_Par%sp=5
          current_Par%X=i_start_real 	        
	  do i=NPartInPToAdd+1, (3*N_trajectory/4)
            counter_essays=0
            testIn=0
            counterTest1=counterTest1+1
            
            do while (testIn==0)
              counter_essays=counter_essays+1
            
              call random_number(num_rand_r)
              current_Par%Y=j_end*delta(1)*num_rand_r
              
              call random_number(num_rand_r)
              current_Par%Z=k_end*delta(2)*num_rand_r
              
              if(sqrt(((current_Par%Y-The_Object(obj)%center(1))**2) + &
                       (current_Par%Z-The_Object(obj)%center(2))**2) > &
                       The_Object(obj)%width(1)+1e-5) then      
                	testIn=1
              endif 
	    enddo
 
            current_Par%IdentPart=IdentPartNumber
            IdentPartNumber=IdentPartNumber+1

	!Velocity with maxwellian distribution
            testIn = 1
            do while(testIn==1)
              call random_number(randomV)
              call random_number(rrandom)
              
              randomV = randomV*VmaxMaxwell
              rrandom = rrandom*PmaxMaxwell

              if( rrandom < Cste1Maxwell*randomV*randomV*exp(-Cste2Maxwell*randomV*randomV) ) then
                Vt = randomV
                testIn = 0
              end if
            end do

	     call random_number(auxx)
                
		sin_t_d = sqrt(auxx(1)) 
		cos_t_d = sqrt( 1.0 - sin_t_d*sin_t_d)
		sin_f_d = sin(2.0*acos(-1.)*auxx(2)) 
		cos_f_d = cos(2.0*acos(-1.)*auxx(2))                
                current_Par%VX=-Vt*cos_t_d             
                current_Par%VY=Vt*sin_t_d*cos_f_d               
                current_Par%VZ=Vt*sin_t_d*sin_f_d	

            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par

         enddo
        endif
        
!-------------------------------------------------------------------------------
        
        !------------------------------------------------Add Conical PG Negative ions

	Vt=0
        Vt2=0
        
        num_rand=0  
        
        start_real=The_Object(obj)%center(0)-The_Object(obj)%width(0)
        end_real=The_Object(obj)%center(0)+The_Object(obj)%width(0)
        x1=The_Object(obj)%center(0)+The_Object(obj)%width(0)
        x0=The_Object(obj)%center(0)-The_Object(obj)%width(0)

        current_Par%sp=9

        rrandomstart = The_Object(obj)%width(2)**2
        drrandom = The_Object(obj)%width(1)**2-The_Object(obj)%width(2)**2
        
	 do i=NPartInPToAdd+1,N_trajectory
          
          if(rank==0) then

            testIn = 0

            do while(testIn==0)

              !Getting x, y, z of the new particle
          
              call random_number(num_rand)

              rrandom = sqrt(num_rand*drrandom + rrandomstart)
              
              current_Par%X = (end_real-start_real)/(The_Object(obj)%width(2)-The_Object(obj)%width(1))*(rrandom-The_Object(obj)%width(1))+start_real
                          
              rrandom = rrandom - 1e-7
              
              call random_number(num_rand)
              theta=num_rand*acos(-1.)*2.0
              
              current_Par%Y=cos(theta)*rrandom+The_Object(obj)%center(1)
              current_Par%Z=sin(theta)*rrandom+The_Object(obj)%center(2)

              if(current_Par%x>0 .AND. current_Par%x<Nxp*delta(0) .AND. &
                 current_Par%y>0 .AND. current_Par%y<Nyp*delta(1) .AND. &
                 current_Par%z>0 .AND. current_Par%z<Nzp*delta(2)) then
                testIn = 1
              end if

            end do

            !Getting the new velocity of the new particle

            !First step: get the new repere with n normal to the conus surface

            tco(1) = x0-The_Object(obj)%width(1)*(x1-x0)/(The_Object(obj)%width(2)-The_Object(obj)%width(1)) - current_Par%X
            tco(2) = The_Object(obj)%center(1) - current_Par%Y
            tco(3) = The_Object(obj)%center(2) - current_Par%Z
            norm = sqrt(tco(1)*tco(1) + tco(2)*tco(2) + tco(3)*tco(3))
            tco(1) = tco(1)/norm
            tco(2) = tco(2)/norm
            tco(3) = tco(3)/norm

            tc(1) = 0
            tc(2) = sin(theta)
            tc(3) = -cos(theta)

            n(2) = The_Object(obj)%center(1) - current_Par%Y
            n(3) = The_Object(obj)%center(2) - current_Par%Z
            n(1) = -(n(2)*tco(2) + n(3)*tco(3))/tco(1)
            norm = sqrt(n(1)*n(1) + n(2)*n(2) + n(3)*n(3))
            n(1) = n(1)/norm
            n(2) = n(2)/norm
            n(3) = n(3)/norm

            !Second step: velocity!

            !velocity ie energy is constant here
            !Vt = sqrt(2*K_b*temperature(current_Par%sp)/mass(current_Par%sp))

            !Velocity with maxwellian distribution
            testIn = 1
            do while(testIn==1)
              call random_number(randomV)
              call random_number(rrandom)
              
              randomV = randomV*VmaxMaxwell
              rrandom = rrandom*PmaxMaxwell

              if(rrandom<Cste1Maxwell*randomV*randomV*exp(-Cste2Maxwell*randomV*randomV)) then
                Vt = randomV
                testIn = 0
              end if
            end do

            call random_number(num_rand)
            theta = asin(-num_rand)+acos(-1.)/2.

            call random_number(num_rand)
            phi = num_rand*acos(-1.)*2.

            current_Par%VX = Vt*(cos(theta)*n(1) + sin(theta)*(cos(phi)*tco(1) + sin(phi)*tc(1)))
            current_Par%VY = Vt*(cos(theta)*n(2) + sin(theta)*(cos(phi)*tco(2) + sin(phi)*tc(2)))
            current_Par%VZ = Vt*(cos(theta)*n(3) + sin(theta)*(cos(phi)*tco(3) + sin(phi)*tc(3)))
           
            current_Par%IdentPart=IdentPartNumber
            IdentPartNumber=IdentPartNumber+1
            
            NPartInPToAdd = NPartInPToAdd + 1
            PToAdd(NPartInPToAdd) = current_Par

          endif

        enddo

!-------------------------------------------------------------------------------------
!-------------------------------------------------------------------------------------
            
        Call MPI_BCAST(NPartInPToAdd, 1,MPI_INTEGER,0,MPI_COMM_WORLD, info)
        Call MPI_BCAST(PToAdd, 8*NPartInPToAdd,MPI_REAL,0,MPI_COMM_WORLD, info)

        do i=1,NPartInPToAdd

          if(PToAdd(i)%x > xbegin .AND. PToAdd(i)%x <= xend .AND. &
             PToAdd(i)%y > ybegin .AND. PToAdd(i)%y <= yend .AND. &
             PToAdd(i)%z > zbegin .AND. PToAdd(i)%z <= zend ) then
           
            N_p_t = N_p_t+1
            P_trajectory(N_p_t) = PToAdd(i)
           
          end if

        end do

        NPartInPToAdd = 0
        
        !------------------------------------------------------------------
      
      end subroutine Particle_trajectory_loading



