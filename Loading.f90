subroutine Particle_loading_IPP_2

  use Parallel
  implicit none
  include 'mpif.h'
  integer l,i,j,k,NpartCell, imin, imax
  real auxx(3),erfi,Vt,Vt2
  
  
  logical isin
  real, allocatable :: rest2(:)
  real vol
  integer, dimension(0:12) :: total_initial_part_temp

  total_initial_part_temp = 0
  total_initial_part = 0

  allocate(rest2(0:11))

  rest2 = 0

  vol = delta(0)*delta(1)*delta(2)

  Npart = 0
  imin = plasma_x0/delta(0)
  imax = plasma_x1/delta(0)+1
  imin = max(i0, imin)
  imax = min(i1+1, imax)
  do k = k0, k1
    do j = j0, j1
      do i = i0, i1
        if(i>=imin .AND. i<imax) then
          do l=0, 11
            NpartCell = INT(density0(l)/weight(l)*vol)
            do ll=1, NpartCell
              
              Npart=Npart+1
              P(Npart)%sp=l
              Vt=sqrt(K_b*temperature(l)/mass(l))
              Vt2=sqrt(2.0)*Vt
              
              call random_number(auxx)
              P(Npart)%X=auxx(1)*delta(0)+i*delta(0)
              P(Npart)%Y=auxx(2)*delta(1)+j*delta(1)
              P(Npart)%Z=auxx(3)*delta(2)+k*delta(2)

              call random_number(auxx)
              auxx=auxx*2.0-1.0

              call InvErrorFunction(auxx(1),erfi)
              P(Npart)%VX=Vt2*erfi
              
              call InvErrorFunction(auxx(2),erfi)
              P(Npart)%VY=Vt2*erfi

              call InvErrorFunction(auxx(3),erfi)
              P(Npart)%VZ=Vt2*erfi

              P(Npart)%IdentPart=IdentPartNumber
              IdentPartNumber=IdentPartNumber+1
              
            end do

          end do
        end if
      end do
    end do
  end do

  N_e=0
  do i=1,Npart
    if( abs(P(i)%sp)<1e-5 )   N_e=N_e+1
  end do
  rest2(:) = (density0(:)/weight(:)*vol-int(density0(:)/weight(:)*vol))*(imax-imin)*(j1-j0+1)*(k1-k0+1)

  do l=0, 11
    do ll=1, int(rest2(l))
      Npart=Npart+1
      P(Npart)%sp=l
      Vt=sqrt(K_b*temperature(l)/mass(l))
      Vt2=sqrt(2.0)*Vt
      
      call random_number(auxx)
      P(Npart)%X=auxx(1)*(imax-imin)*delta(0)+imin*delta(0)
      P(Npart)%Y=auxx(2)*(j1-j0+1)*delta(1)+j0*delta(1)
      P(Npart)%Z=auxx(3)*(k1-k0+1)*delta(2)+k0*delta(2)

      call random_number(auxx)
      auxx=auxx*2.0-1.0

      call InvErrorFunction(auxx(1),erfi)
      P(Npart)%VX=Vt2*erfi
      
      call InvErrorFunction(auxx(2),erfi)
      P(Npart)%VY=Vt2*erfi

      call InvErrorFunction(auxx(3),erfi)
      P(Npart)%VZ=Vt2*erfi

      P(Npart)%IdentPart=IdentPartNumber
      IdentPartNumber=IdentPartNumber+1
    end do
  end do
  
  if(3==3) then
  OutParticles = 0
  do l=1, Npart
    isin = .FALSE.
    do ll=1, Nb_Obj
      call isinobj(P(l)%x, P(l)%y, P(l)%z, ll, isin)
      if(isin .EQV. .TRUE.) then
        OutParticles = OutParticles + 1
        DelParticles(OutParticles) = l
        exit
      end if
    end do
  end do

  call Heapsort(DelParticles, OutParticles)
  call DeleteParticle()
  end if
  
  do i=1,Npart
    total_initial_part_temp(P(i)%sp) = total_initial_part_temp(P(i)%sp) + 1 
  end do
      
  call MPI_ALLREDUCE(total_initial_part_temp, total_initial_part, 13, MPI_int, MPI_SUM, MPI_COMM_WORLD,info)
  if(rank==0) then
  do i=0, 13
  write(*,*) total_initial_part(i)
  end do
end if

end subroutine
