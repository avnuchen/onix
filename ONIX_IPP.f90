module Parallel
    
  integer Iteration_Number ! The iteration step
  
  !integer,    parameter  :: N=100		! The number of grids 
  integer Nxp ! The number of grids
  integer Nyp ! The number of grids
  integer Nzp ! The number of grids

	real, allocatable :: V( :, :, : ) !Array of potential
	real, allocatable :: Exm( :, :, : ) !Array of Electric Field in x direction
	real, allocatable :: Eym( :, :, : ) !Array of Electric Field in y direction
	real, allocatable :: Ezm( :, :, : ) !Array of Electric Field in z direction
    
  	real, allocatable :: Bxm( :, :, : ) !Array of Magnetic Field in x direction
	real, allocatable :: Bym( :, :, : ) !Array of Magnetic Field in y direction
	real, allocatable :: Bzm( :, :, : ) !Array of Magnetic Field in z direction

  !For reading purposes, the magnetic field used in particle pusher is Bx-y-zm
  real, allocatable :: BxRead(:,:,:)
  real, allocatable :: ByRead(:,:,:)
  real, allocatable :: BzRead(:,:,:)
  
  real, allocatable :: V_EG_Read(:,:)
  real, allocatable :: V_EG(:,:)


  integer NxB, NyB, NzB
  real deltaxB, deltayB, deltazB
  character(len=20)::filenameB
    
    real, allocatable :: ReadB( :, :) !In order to read external magnetic field array
       
	real, allocatable :: d( :, :, : ) !Array for calculation Poisson equation
	real, allocatable :: Inzone( :, :, : ) !Array for estimation the points which are inside the shape of domain. 
	real, allocatable :: Inzone2( :, :, : ) !Array for estimation the points which are inside the shape of domain. 
	real, allocatable :: alphax( :, :, : ) !Array of correction in the boundaries in X direction. =1 if there isn't correction 
	real, allocatable :: alphay( :, :, : )
	real, allocatable :: alphaz( :, :, : )

  !Laplacian coefficients Forward, Backward, Right, Left, Up, Down, Mid
	real, allocatable :: coefficentLaplacianF(:,:,:)
	real, allocatable :: coefficentLaplacianB(:,:,:) 
	real, allocatable :: coefficentLaplacianR(:,:,:) 
	real, allocatable :: coefficentLaplacianL(:,:,:) 
	real, allocatable :: coefficentLaplacianU(:,:,:) 
	real, allocatable :: coefficentLaplacianD(:,:,:)
	real, allocatable :: coefficentLaplacianM(:,:,:) 
  
	real, allocatable :: ZoneNeig( :, :, : )!Array for estimation the neigbours points of shape boundaries 
	real, allocatable :: Ad( :, :, : ) !Array for conjugate gradient method (derivative of d)
	real, allocatable :: r( :, :, : ) !Array for conjugate gradient method (error)
	real, allocatable :: z( :, :, : ) !Array for precondition conjugate gradient method z=(C^-1)*z
	real, allocatable :: rho( :, :, : ) !Charge density array
	real, allocatable :: rho2( :, :, : ) !Charge density array
	real, allocatable :: rho_Sec( :, :, : ) !Charge density array for Second order interpolation routine
    
  	real, allocatable :: Mom_e_x( :, :, : ) !Charge density array for Second order interpolation routine
	real, allocatable :: Mom_e_y( :, :, : ) !Charge density array for Second order interpolation routine
	real, allocatable :: Mom_e_z( :, :, : ) !Charge density array for Second order interpolation routine

  real, allocatable :: C( :, :, : ) !Array for precondition conjugate gradient method

  real, allocatable :: output_plane_density( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_density_average( :, :, : ) !Array for printing data into the files
  real, allocatable :: density_average( :, :, :, : ) !Array for printing data into the files
  
  real, allocatable :: output_plane_momx_xy( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_momx_xy_average( :, :, : ) !Array for printing data into the files
  real, allocatable :: momx_xy( :, :, :, : ) !Array for printing data into the files

  real, allocatable :: output_plane_momy_xy( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_momy_xy_average( :, :, : ) !Array for printing data into the files
  real, allocatable :: momy_xy( :, :, :, : ) !Array for printing data into the files

  real, allocatable :: output_plane_momx_xz( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_momx_xz_average( :, :, : ) !Array for printing data into the files
  real, allocatable :: momx_xz( :, :, :, : ) !Array for printing data into the files

  real, allocatable :: output_plane_momz_xz( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_momz_xz_average( :, :, : ) !Array for printing data into the files
  real, allocatable :: momz_xz( :, :, :, : ) !Array for printing data into the files  

  real, allocatable :: output_plane_density_xz( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_density_average_xz( :, :, : ) !Array for printing data into the files
  real, allocatable :: density_average_xz( :, :, :, : ) !Array for printing data into the files

  real, allocatable :: output_plane_potential( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_potential_average( :, :, : ) !Array for printing data into the files
  real, allocatable :: potential_average( :, :, :, : ) !Array for printing data into the files
  
  real, allocatable :: output_plane_potential_xz( :, :, : ) !Array for printing data into the files
  real, allocatable :: output_plane_potential_average_xz( :, :, : ) !Array for printing data into the files


  real, allocatable :: buffer( :, : ) !Array for printing data into the files
  real, allocatable :: buffer2( :, : ) !Array for printing data into the files
  real, allocatable :: bufferyz( :, : ) !Array for printing data into the files   	  

  real, allocatable :: bufferx( :, : ) !Array for printing data into the files
  real, allocatable :: buffery( :, : ) !Array for printing data into the files  

  real, allocatable :: buffer2x( :, : ) !Array for printing data into the files
  real, allocatable :: buffer2z( :, : ) !Array for printing data into the files  

  real, allocatable :: emittance_arr(:,:,:,:)
  integer :: iN_emit
  real  :: rN_emit
  real, allocatable :: current_den(:,:,:)
  real, allocatable :: output_plane_potential_yz(:,:)		
  real, allocatable :: buffer3( :, :, :, : ) !Array for printing data into the files
  real, allocatable :: buffer4( :, :, : ) !Array for printing data into the files

  !----------------------Arrays for making collisions. Calculation number of particles in each cell
  integer, allocatable :: Coll_e( :, :, : )
  integer, allocatable :: Coll_H2p( :, :, : )
  integer, allocatable :: Coll_Hm( :, :, : )
  integer, allocatable :: Coll_Hp( :, :, : )
  !--------------------------------------------------------------------------------------------------

  real, dimension(3):: delta(0:2) !Distance between grid points in X,Y,Z directions

  integer Periodic,iT,iB,jT,jB,kT !Variables for calculation in periodic boundary conditions

  integer numtasks, rank, ierr, rc,info   !Variabels for MPI calculations
  integer, allocatable :: status(:)

  integer,allocatable :: i00(:),i11(:),j00(:),j11(:),k00(:),k11(:) !Boundaries of each process	
  integer :: indexes(0:5),neig(0:5) !Arrays of neigbours and index of each process

  !Boundary of each boxes
  integer  i0,i1
  integer  j0,j1
  integer  k0,k1
  
  !Variables for exchanging planes between boxes
  integer  indexx,indexy,indexz
  integer  xy0,xz0,yz0,nx,ny,nz,xy20,xz20,yz20,nx2,ny2,nz2,xy30,xz30,yz30,nx3,ny3,nz3

  !Number of process in each directions
  integer  NprocX 
  integer  NprocY 
  integer  NprocZ 

  !Number of boxes and cylinders in domain
  integer nbox,ncyl

  !Parameters of box and cylinder
	real,dimension(3,4) :: center(0:2,1:4)
	real,dimension(3,4) :: width(0:2,1:4)
	real,dimension(3,4)::  centerCyl(0:2,1:4)
	real,dimension(4) ::   radiusCyl(1:4),XstartCyl(1:4),XendCyl(1:4)

!!Geomtry of the PG and EG is defined by Objects
  TYPE Object
	real,dimension(3) :: center(0:2)
	real,dimension(8) :: width(0:2)
	real Potential !potential on the grid
	integer types ! 1- box, 2 - cylinder, 3 conus. Now work only box and conus- if want cylinder make conus with same radius R and r
	integer ReflectionType 
    !1 - Reflexion with new temperature ; 2 - Right end of domain (deletion and reinjection) ; 3 - Deletion and reinjection (output
    !current not updated) ; 4 - balistic reflexion
  END TYPE Object

  integer Nb_Obj !Object (or grid) number, must be read from external file

  TYPE(Object), allocatable, dimension(:) :: The_Object

	real,allocatable,dimension(:) :: mass,charge,weight,temperature, density0


!!Particle structure: note it is 8*REAL and this is used in the MPI send
  TYPE Particle
		real :: x,y,z
		real :: vx,vy,vz
		real :: sp
		real :: IdentPart
  END TYPE Particle

  TYPE(Particle),allocatable, dimension(:):: P
  
  TYPE(Particle),allocatable, dimension(:):: P_trajectory	

  real, allocatable, dimension(:) :: px_arr, py_arr, pz_arr, pvx_arr, pvy_arr, pvz_arr, ex_arr, ey_arr, ez_arr, bx_arr, by_arr, bz_arr
  integer, allocatable, dimension(:) :: sumNpart, sumNpartMPI

  integer NPartInPToAdd
  TYPE(Particle),allocatable, dimension(:):: PToAdd

  TYPE Particle_wall
		real :: x_wall,y_wall,z_wall
  END TYPE Particle_wall

  TYPE(Particle_wall),allocatable, dimension(:):: P_wall

  integer :: NPART  = 50000000 !Not more particles in one domain
  integer :: NPARTL = 50000000
  integer :: Np_Coul_cell = 1000
  integer :: N_trajectory = 400		
  integer :: N_p_t=0	 

  integer, allocatable ,dimension(:):: Flag( : )
  integer, allocatable,dimension(:) :: LLoss( :)
  TYPE(Particle), allocatable ,dimension(:):: PLoss(:)
  TYPE(Particle), allocatable,dimension(:) :: PGain(:)

  TYPE(Particle), allocatable ,dimension(:):: PLoss_followed(:)
  TYPE(Particle), allocatable,dimension(:) :: PGain_followed(:) 


  TYPE(Particle), allocatable,dimension(:) :: AddParticles(:) !Array to add new particle from the begining if some particle move out of the initial distance x=33*delta(0) 

	real t,dt
  integer iter
  integer NpartOld,NpartNew,counter,Ngain,NLoss,lost

  !Variabels for the radial field calculation 
  integer ir,iq
	real rr,q,dr,dq,pot ,testsend

  integer :: iteration=0 !Number of iterations in conjugate gradien method

  !Addition variabels for using conjugate gradient method
  real,    parameter  :: tolerance=5E7
  integer  :: Total_Iterat=0
  real,    parameter  :: K_b=1.38e-23

  real ::  dx=0.0,dy=0.0,dz=0.0,temp=0.0,x,y,zz
  real ::  error=0.0,err_old=0.0,alpha=0.0,beta=0.0,aux=0.0,xi0,xi1,yj0,yj1,zk0,zk1


  !Array for exchanging particles between proccesses
  integer, dimension(:) :: LCounter(0:5)
  integer, dimension(:) :: LCounter1(0:5)
  integer, dimension(:) :: LCounter2(0:5)
  integer, dimension(:) :: GCounter(0:5)
  integer, dimension(:) :: GCounter1(0:5)
  integer, dimension(:) :: GCounter2(0:5)

  integer :: ll,llos,TP,count1,count2,count3,count4,dd
  integer :: inside,bouder_cross ! Variables for checking movement of each species

  real ::  px_p,py_p,pz_p,vx_p,vy_p,vz_p !Priveious position of particles
  real ::  rand_num ! random number for checking reflection from the wall

  integer :: OutParticles=0,outPart=0,counterPLoss=0
  integer, allocatable :: DelParticles(:)
  integer, allocatable :: DelParticles_sort(:)
  integer, allocatable :: Arr_new_sort(:)

  !integer, allocatable :: Arr_to_sort(:)

  TYPE(Particle), allocatable,dimension(:) :: AddWallParticles(:) !Array to add particles which are produced from the wall interaction

  integer :: Niteration=9999 !amount of main time iteration loop

  integer :: NpartBefoteDelete=0 !density before delating procedure

  integer :: indexBfield=0 !index of the magnetic field array

  Real :: IdentPartNumber=0 !Spesial number in order to mark all particles

  
  !------------------------------------------------------------------------------------------------------------

  !------------------------------------------------------------------------------------------------------------

  integer :: En_Fun_Index=0

  !------------------------------------------------------------------------------------------------------------

  !------------------------------------------------------------------------------------------------------------

  Integer :: Inside_obj=0
  Integer :: Inside_obj2=0
  !------------------------------------------------------------------------------------------------------------


  !--------------------------------------  Number of particles move out from the volume (in order to reinject new one)

  Integer :: Out_e=0
  Integer :: Out_PI_Hp=0
  Integer :: Out_PI_H2p=0
  Integer :: Out_PI_H3p=0
  Integer :: Out_Cs=0

  Integer :: Out_NI=0
  Integer :: Out_NI_v=0
  Integer :: Out_NI_PG=0
  Integer :: Out_NI_PG_cone=0

  Integer :: Out_H=0
  Integer :: Out_H2=0

  Integer :: Tot_Out_e=0
  Integer :: Tot_Out_PI_Hp=0
  Integer :: Tot_Out_PI_H2p=0
  Integer :: Tot_Out_PI_H3p=0
  Integer :: Tot_Out_Cs=0

  !Integer :: Tot_Out_H=0
  !Integer :: Tot_Out_H2=0

  Integer :: Tot_Out_NI_Hm=0

  !----------------------------------------------------------------------------------------------------------------



  !--------------------------------------  Check total dencity of different species
  
  Integer :: N_e=0
  Integer :: N_H3p=0
  Integer :: N_H2p=0
  Integer :: N_Hp=0
  Integer :: N_H=0
  Integer :: N_H2=0
  Integer :: N_NI=0
  Integer :: N_HmVv=0
  Integer :: N_Hm_PG_Neutr=0
  Integer :: N_Hm_PG_Neutr_PG=0
  Integer :: N_Hm_PG_PI=0
  Integer :: N_Cs=0

  Integer :: N_e_tot=0
  Integer :: N_H3p_tot=0
  Integer :: N_H2p_tot=0
  Integer :: N_Hp_tot=0
  Integer :: N_H_tot=0
  Integer :: N_H2_tot=0
  Integer :: N_NI_tot=0
  Integer :: N_HmVv_tot=0
  Integer :: N_Hm_PG_Neutr_tot=0
  Integer :: N_Hm_PG_Neutr_PG_tot=0
  Integer :: N_Hm_PG_PI_tot=0
  Integer :: N_Cs_tot=0

  !----------------------------------------------------------------------------------------------------------------

  Real :: counter_e=0
  Real :: counter_e_tot=0
  Real :: counter_d=0
  Real :: counter_d_tot=0


  !--------------variable to keep neutral plasma plane
  integer :: typ_p=0
  integer :: dif_e=0
  integer :: dif_H2p=0
  integer :: dif_Hp=0
  integer :: dif_Hm=0
  real    :: test1count=0
  real plasma_x0, plasma_x1
  !----------------------------------------------------------------------------------------------------------------


  !----------------------Arrays for making collisions. 
	real, allocatable :: Dens_e( :, :, : )  
	real, allocatable :: Dens_H2p( :, :, : )
	real, allocatable :: Dens_Hm( :, :, : )
	real, allocatable :: Dens_Hp( :, :, : )
	real, allocatable :: Dens_HnPG( :, :, : )

  !--------------------------------------------------------------------------------------------------

  !----------------------Varibles for make prodaction NI from PG 

  Real :: E_in=0
  Real :: Yield_Hp=0
  Real :: Yield_H2p=0
  Real :: E_th_R_E_Hp=2
  Real :: Rn_n_Hp=0.3
  Real :: E_th_R_E_H2p=2
  Real :: Rn_n_H2p=0.3

  !--------------------------------------------------------------------------------------------------


  !----------------------Varibles to calculate the cross section for different reaction

  Real :: Cross_sect=0
  Real :: E_e=0
  Real :: Ener_kin_test=0
  Real :: Probab_coll=0
  Real :: Vellocity =0

  Integer :: Delete_e=0
  Integer :: Delete_Hm=0
  Integer :: Delete_Hp=0
  Integer :: Add_e=0
  Integer :: Add_H=0

  TYPE(Particle), allocatable ,dimension(:):: Add_partic_coll(:)


  real, allocatable :: Delete_e_pos( : ) 
  real, allocatable :: Delete_Hm_pos( : )
  real, allocatable :: Delete_Hp_pos( : )
  real, allocatable :: Delete_Hp_pos_tar( : ,: ,:)
  real, allocatable :: Delete_Hm_pos_tar( : ,: ,:)


  integer:: Npart_coll=0

  integer :: IdentCheck=0

  real :: Ener_e_threshold=0 ! Check in collision procedure if electron energy more than 5000V that new electron can have energy in 4 times more (that is wrong)

  integer ::counter_P_wall=0 ! For NI injected from the PG wall

  integer :: i_loop1=0 !loop varibles
  !--------------------------------------------------------------------------------------------------



  !-------------------------------------------------------------------------------------------
  !Varilables for function AddParticle() in order to check if all particles that should be reinject into domain are there, if not - call again AddParticle.
  Integer ::Tot_Counter_Add_e=0
  Integer ::Counter_Add_e=0

  Integer ::Tot_Counter_Add_Hp=0
  Integer ::Counter_Add_Hp=0

  Integer ::Tot_Counter_Add_H2p=0
  Integer ::Counter_Add_H2p=0

  Integer ::Tot_Counter_Add_H3p=0
  Integer ::Counter_Add_H3p=0

  Integer ::Tot_Counter_Add_NI_Hm=0
  Integer ::Counter_Add_NI_Hm=0

  Integer ::Tot_Counter_Add_Cs=0
  Integer ::Counter_Add_Cs=0

  !-------------------------------------------------------------------------------------------
  !NI emission rate from PG surface
  Integer ::NI_rate_from_cone=0
  Integer ::NI_rate_from_flat=0
  Integer ::NI_emission=0
  !-------------------------------------------------------------------------------------------

  !--------Time for making collision is one time per XX iterations

  real::   time_collision=0
  !-------------------------------------------------------------------------------------------

  !-------------------------------------------------------------------------------------------
  Integer :: test1count2=0
  !-------------------------------------------------------------------------------------------

  Real time1,time2,time3

  CHARACTER(LEN=27):: str
  CHARACTER(LEN=7):: str3
  integer :: lgc

  !---------------------------- Test particle

  Integer :: counter_energy_test=0

  !Velocity Distribution Function

  integer :: nPointVDF=500, nPointVDF_NI=100 , time_interval_measure_vdf=10000, nPosPlaneVDF, nPosSliceVDF, sendplaneVDF, sendsliceVDF, sendsliceVDF_NI
  real :: vmaxVDF=10, deltaVDF, deltaVDFNI !factor of mean energy

  real,allocatable :: posPlaneVDF(:), posSliceVDF(:)
  integer,allocatable :: vel_distribution_plane(:,:), vel_distribution_planeMPI(:,:)
  integer,allocatable :: vel_distribution_slice(:,:), vel_distribution_sliceMPI(:,:)
  real,allocatable :: vel_distribution_slice_NI(:,:), vel_distribution_sliceMPI_NI(:,:)  	

  real, dimension(1:6,0:12) :: In_flux_object	
  real, dimension(1:6,0:12) :: Total_In_flux_object
  integer, dimension(0:12) :: total_initial_part

  real, allocatable :: Coulomb_cell_list( :, :, :, : )
  integer, allocatable :: Num_part_Coul_cell( :, :, :)


end module Parallel


program Extraction
  
  use Parallel
  implicit none
  include 'mpif.h'
  
  integer  Rproc !Rank of process
  
  integer :: i=0,j=0,k=0
	  
  integer :: l,sp
  
  integer :: iter_threshold=0
 
  	

  !Array for time calculation
  double precision, allocatable :: time(:)
  double precision, allocatable :: timetot(:)

  allocate(time(1:16))
  allocate(timetot(1:15))

  rN_emit = 400.0
  iN_emit= int(rN_emit)


  !Read initial parameters:size of grid,number of proccess in each direction,periodic conditions
  write(*,*) "Proc number: ", rank, " Reading parameters ..."
   ! call MPI_Barrier(MPI_COMM_WORLD, info)
    call Read_Parameters()   
  write(*,*) "Proc number: ", rank, " Reading parameters done"

  !Initialization of MPI
  write(*,*) "Proc number: ", rank, " MPI Initialization ..."
    call Initialization_MPI()
  write(*,*) "Proc number: ", rank, " MPI Initialization done"
  
  !Display some info
  write(*,*) "info: ", numtasks, NprocX, NprocY, NprocZ

  !Initialization of RNG
  call init_random_seed(rank)


  !Allocation memory of each proccesses box 
  allocate( i00(0:numtasks-1 ),i11(0:numtasks-1 ) )
  allocate( j00(0:numtasks-1 ),j11(0:numtasks-1 ) )
  allocate( k00(0:numtasks-1 ),k11(0:numtasks-1 ) )

  allocate(status(MPI_STATUS_SIZE))

  !Calculation of boundaries and neighbours of each box (process)
  Rproc=numtasks-1
  if(rank == 0 ) then
    do k=NprocZ-1,0,-1
      do j=NprocY-1,0,-1
        do i=NprocX-1,0,-1
          call Set_Boundary(k,j,i,Rproc)
          indexx=i
          indexy=j
          indexz=k
          
          if( (i .NE. 0) .or. (k .NE. 0) .or. (j .NE. 0) ) then
            CALL MPI_SEND(indexes(0), 6,MPI_INTEGER, Rproc,54, MPI_COMM_WORLD, info)
            CALL MPI_SEND(neig(0), 6,MPI_INTEGER, Rproc,54, MPI_COMM_WORLD, info)
            CALL MPI_SEND(indexx, 1,MPI_INTEGER, Rproc,54, MPI_COMM_WORLD, info)
            CALL MPI_SEND(indexy, 1,MPI_INTEGER, Rproc,54, MPI_COMM_WORLD, info)
            CALL MPI_SEND(indexz, 1,MPI_INTEGER, Rproc,54, MPI_COMM_WORLD, info)
          endif
          Rproc=Rproc-1
        enddo
      enddo
    enddo
  else
    CALL MPI_RECV(indexes(0), 6,MPI_INTEGER, 0,54, MPI_COMM_WORLD, status,info)
    CALL MPI_RECV(neig(0), 6,MPI_INTEGER, 0,54, MPI_COMM_WORLD, status,info)
    CALL MPI_RECV(indexx, 1,MPI_INTEGER, 0,54, MPI_COMM_WORLD, status,info)
    CALL MPI_RECV(indexy, 1,MPI_INTEGER, 0,54, MPI_COMM_WORLD, status,info)
    CALL MPI_RECV(indexz, 1,MPI_INTEGER, 0,54, MPI_COMM_WORLD, status,info)
  end if

  !Initialization of all arrays 
  write(*,*) "Proc number: ", rank, " Initialization ..."
    call Initialization()
  write(*,*) "Proc number: ", rank, " Initialization done"

  call MPI_Barrier(MPI_COMM_WORLD, info)

  call Set_Boundary_Zero()

  call MPI_Barrier(MPI_COMM_WORLD, info)
  write(*,*) "Loading Particles"
  call MPI_Barrier(MPI_COMM_WORLD, info)
  write(*,*) "Proc number: ", rank, " Particles Loaded :)"


!!*************************************************************
!!	   Load particles or Read particle input files
!!*************************************************************

  if(Iteration_Number==0) then
    Npart=0
    IdentPartNumber=100000*rank
    call Particle_loading_IPP_2()
  else
    call Read_Particles()
    Iteration_Number=Iteration_Number+1
  endif
    
!!*************************************************************
!!*************************************************************
 
  xi0=i0*delta(0)
  xi1=i1*delta(0)
  yj0=j0*delta(1)
  yj1=j1*delta(1)
  zk0=k0*delta(2)
  zk1=k1*delta(2)

!!*************************************************************
!!		  Load Magnetic Field input
!!*************************************************************

  !call Read_Bfield_IPP3()
  call Read_Bfield_IPP3()

!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		  Load EG potential input
!!*************************************************************

  call Read_extraction_voltage()

!Check Poisson_Solver.f90 in case you do not give an input file
!for Read_extraction_voltage() in Input.f90
!!*************************************************************
!!*************************************************************


!!*************************************************************
!!		Calculation of NI emission rate
!!*************************************************************

	call NI_emission_rate3(2)

!To check the other ways to calculate NI_emission_rate_
!check in Particles.f90
!!*************************************************************
!!*************************************************************


!!**********************************************************************************************************************************
!!				Particle In Cell cycle starts a.k.a main loop
!!**********************************************************************************************************************************
  do iter=Iteration_Number,1100000
  if(rank==0) then 
    write(*,*) "Main loop ", rank, iter
  end if

	t=iter*dt	
	sp=0	    
	
	call MPI_Barrier(MPI_COMM_WORLD, info)

!!*************************************************************
!!		Print particle data
!!*************************************************************
!! ????????????  10000
	if (mod (iter,5000) .EQ. 0  .AND. (iter .NE. 0)   .AND. (iter .NE. Iteration_Number) )  then
		call Print_particle_position()
	endif
!	if (mod (iter,50) .EQ. 0  .AND. (iter .NE. 0)   .AND. (iter .NE. Iteration_Number) )  then
!		call Print_potential2()
!	endif

!This data is used to restart the simulation
!It is possible to automatize only to print when it is close
!to the end of the simulation, but the current HPC systems crush
!frequently enough not to be reliable, thus save at an adequate 
!rate or by your own risk only once before the end using MPI_WTIME() 
!!*************************************************************
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Print magnetic field
!!*************************************************************

	!call Print_magnetic_field()	

!Useful to check if ONIX is reading the input correctly
!Discomment if the magnetic field is needed to be checked
!otherwise it is not necessary
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Reset variables used in the PIC process
!!*************************************************************    	    

	call reset_variables()
   
!!	This function was added for readability of the code.
!!	Calling functions decrease performance rather
!!	than just having it inline.
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Print charge density
!!*************************************************************

	if (mod (iter,10) .EQ. 0 ) then
		call Print_charge_density2()	
	endif

!It prints charge density, thus, 
!it is required to divide by the charge to obtain the density
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Print flow map
!!*************************************************************

	if (mod (iter,10) .EQ. 0 .AND.  (iter > 100000 ) ) then
		!call Print_Momentum_density()	
	endif
  
	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(1)=MPI_WTIME()

!It prints average momentum 
!from this map it is possible to obtain also the average flow
!if you divide by the mass
!!*************************************************************
!!*************************************************************
  
!!*************************************************************
!!		Charge projection onto the mesh
!!*************************************************************
  
	call ChargeProjection_Second_orderAll()
	call ChargeExchange_3planes(rho_Sec)

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(2)=MPI_WTIME()
    
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Poisson's Solver
!!*************************************************************
	call Solve_Potential_bias()
	call Send_Plane(V)

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(3)=MPI_WTIME()
	
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Electric Field calculation
!!*************************************************************

	call E_field_calculation15points()

	call Send_Plane(Exm)
   	call Send_Plane(Eym)
	call Send_Plane(Ezm)
    
	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(4)=MPI_WTIME()	
    
!!*************************************************************
!!*************************************************************
        
!!*************************************************************
!!		Print data Electrostatic potential 
!!*************************************************************

	if (mod (iter,1)==0 ) then
		call Print_potential3()
	endif
	
	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(5)=MPI_WTIME()

!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Print data Particle trajectories 
!!*************************************************************

	if(mod(iter,50)==0) then	
		!call follow_particle_position_mpi2()  
	endif	

!!Following particles make use of the IdentPart variable to follow 
!!random particle from each core while the PIC cycle is on 
!!Check how the load particle subroutine works 
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Generate frozen Particle trajectories 
!!*************************************************************

	if(iter .eq. + ( Iteration_Number + 999) ) then
		!call generate_trajectories()
		sp=0
	endif
	
	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(7)=MPI_WTIME()

!!Generate Particle trajectories outside of the PIC cycle 
!!Gnerates e-, H- trjactories with a electrostatics Potetial
!!averaged over 1000 iterations  
!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Particle Pusher & particle surface actions
!!*************************************************************

	call movepartAll()
	call Reflection2()

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(6)=MPI_WTIME()

!!*************************************************************
!!*************************************************************
   	
!!*************************************************************
!!		Electron Termalization
!!*************************************************************

	call electron_thermalization()

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(8)=MPI_WTIME()

!!*************************************************************
!!		Print velocity distribution
!!*************************************************************
	if(mod(iter,10)==0) then
		call thermal_energy_plane()
		call thermal_energy_slice()
	end if

	call write_vdf(iter)

	call MPI_Barrier(MPI_COMM_WORLD, info)
    	time(9)=MPI_WTIME()

!!*************************************************************
!!*************************************************************     
    
!!*************************************************************
!!		Generate IBSimu input
!!*************************************************************
	
	if( (iter .GE. Iteration_Number) .and. (iter < (Iteration_Number + 100)) ) then
!		call Print_particle_position_EG()
	endif

!!*************************************************************
!!*************************************************************

!!*************************************************************
!!	Redistribute particles inbetween cores
!!*************************************************************
  
	call Particle_Exchange()
  
	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(10)=MPI_WTIME()	

!!*************************************************************
!!*************************************************************
    
!----------------------------------------------------------------------------------------------------------

!Sort the array of deleting particles from minimum index number to maximum in order to delete correct particle. 
!This we need if we delete in the begining particle in the middle of array and put in its place particle from the end 
!of array and after the second particle that we need to delete is the same particle in the end of array but now it is in the middle place. 
  
!call Sort_DeleteParticle2(DelParticles,OutParticles)
	call Heapsort(DelParticles, OutParticles)

	NpartBefoteDelete=Npart
	if(OutParticles>0)  then
		call DeleteParticle()
	endif

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(11)=MPI_WTIME()

	if(mod(iter,1) .EQ. 0) then
		call MPI_ALLREDUCE(Out_e,Tot_Out_e,   1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD,info)
		call MPI_ALLREDUCE(Out_PI_Hp,Tot_Out_PI_Hp, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD,info)
		call MPI_ALLREDUCE(Out_PI_H2p,Tot_Out_PI_H2p, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD,info)
		call MPI_ALLREDUCE(Out_PI_H3p,Tot_Out_PI_H3p, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD,info)
		call MPI_ALLREDUCE(Out_NI,Tot_Out_NI_Hm, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD,info)
		call MPI_ALLREDUCE(Out_Cs,Tot_Out_Cs, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD,info)

		Out_e=0
		Out_PI_Hp=0
		Out_PI_H2p=0
		Out_PI_H3p=0
		Out_NI=0
		Out_Cs=0      
	endif

!!*************************************************************
!!	Particle Reinjection in the source region
!!*************************************************************
!Reinjection of the deleted particles
	if( Tot_Out_PI_Hp>0 .OR. Tot_Out_PI_H2p>0 .OR. Tot_Out_PI_H3p>0 .OR. Tot_Out_NI_Hm>0 .OR. Tot_Out_Cs>0)  then

		call AddParticleMPI3()

		Tot_Out_e=0
		Tot_Counter_Add_e=0
		Counter_Add_e=0

		Tot_Out_PI_Hp=0
		Tot_Counter_Add_Hp=0
		Counter_Add_Hp=0

		Tot_Out_PI_H2p=0
		Tot_Counter_Add_H2p=0
		Counter_Add_H2p=0

		Tot_Out_PI_H3p=0
		Tot_Counter_Add_H3p=0
		Counter_Add_H3p=0

		Tot_Out_NI_Hm=0
		Tot_Counter_Add_NI_Hm=0
		Counter_Add_NI_Hm=0
    
		Tot_Out_Cs=0
		Tot_Counter_Add_Cs=0
		Counter_Add_Cs=0
	endif

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(12)=MPI_WTIME()

!!*************************************************************
!!*************************************************************
!!*************************************************************

	TP=0
	if (mod (iter,1) .EQ. 0 ) then

		call MPI_ALLREDUCE(Npart,TP , 1, MPI_Integer, MPI_SUM, MPI_COMM_WORLD,info)
		N_e=0
		N_H2p=0
		N_H3p=0
		N_NI=0
		N_Hp=0
		N_H=0
		N_H2=0
		N_HmVv=0
		N_Hm_PG_neutr=0
		N_Hm_PG_neutr_PG=0
		N_Hm_PG_PI=0
		N_Cs=0
    
		sumNpart = 0
		sumNpartMPI = 0

		do i=1,Npart
			sumNpart(P(i)%sp+1) = sumNpart(P(i)%sp+1) + 1
		enddo
  
		call MPI_ALLREDUCE(sumNpart, sumNpartMPI, 12, MPI_Integer, MPI_SUM,MPI_COMM_WORLD, info)
   
		N_e_tot = sumNpartMPI(1)
		N_H2p_tot = sumNpartMPI(2)
		N_NI_tot = sumNpartMPI(3)
		N_Hp_tot = sumNpartMPI(4)
		N_Hm_PG_PI_tot = sumNpartMPI(5)
		N_Hm_PG_neutr_tot = sumNpartMPI(6) 
		N_H_tot = sumNpartMPI(7)
		N_H2_tot = sumNpartMPI(8)
		N_HmVv_tot = sumNpartMPI(9)
		N_Hm_PG_neutr_PG_tot = sumNpartMPI(10)
		N_H3p_tot = sumNpartMPI(11)	    
		N_Cs_tot = sumNpartMPI(12)

		if(rank == 0) then
      			str='Dencity.dat'
			OPEN(UNIT=91, FILE=str, status="unknown", position="append")
			write(91,'(14(1x,I11) )')  iter, N_e_tot, N_NI_tot, N_H2p_tot,N_H3p_tot, N_Hp_tot, N_H_tot,N_H2_tot, N_Hm_PG_neutr_tot, &
			N_Hm_PG_neutr_PG_tot, N_Hm_PG_PI_tot, N_HmVv_tot,N_Cs_tot
			CLOSE(UNIT=91)
   		endif
	endif
	
	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(13)=MPI_WTIME()

!!*************************************************************
!!		Particle Collisions
!!*************************************************************

	if (mod (iter,10) .EQ. 0 ) then
	
		call Density_projection()

!---------------Monte Carlo Collisions-------------

		call Collisions_test() 
		call Delete_partic_coll() 
		call Add_new_partic_coll2() 
		Npart_coll = 0

!---------------Binary Colllisions-------------

		call Coulomb_coll()	
		!call Coulomb_coll_test()	
		Coulomb_cell_list=0
		Num_part_Coul_cell=0		
	
	endif

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(14)=MPI_WTIME()

!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Emission of negative particles
!!		from the PG surface
!!*************************************************************

	call Flux_NI_from_PG_wall_3(2, NI_rate_from_flat)
	call Flux_NI_from_PG_sin_3(2, NI_rate_from_cone)
	
	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(15)=MPI_WTIME()

!!*************************************************************
!!*************************************************************

!!*************************************************************
!!	Export files with the flux
!!	of macroparticles to the first 4 objects
!!	every it iterations: call print_flux_to_objects(it)
!!	object 1: left boundary
!!	object 2: plasma facing part of PG
!!	object 3: downstream part of PG
!!	object 4: EG
!!	If more object are added this part should be edited
!!*************************************************************
 

	call print_flux_to_objects(10)

!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Print emittance
!!*************************************************************

	if( mod(iter,10)==0  .AND. ( iter > 200000 ) ) then
		call emittance()
	end if	

!!*************************************************************
!!*************************************************************

!!*************************************************************
!!		Print charge density at the EG
!!*************************************************************

	if( mod(iter,10)==0  .AND. ( iter > 200000 ) ) then
		call Current_density() 	
	end if			

!!*************************************************************
!!*************************************************************	

	call MPI_Barrier(MPI_COMM_WORLD, info)
	time(16)=MPI_WTIME()

!!*************************************************************
!!		Print the time required for each
!!		part of the code to analyze its
!!		performance
!!*************************************************************	

	if(rank==0) then
		do i=1, 15
			timetot(i) = timetot(i) + time(i+1)-time(i)
		end do
		if(mod(iter, 100) == 0) then
			write(*,*) "time", timetot(1), timetot(2), timetot(3), timetot(4), timetot(5), timetot(6), timetot(7), timetot(8), timetot(9), timetot(10), &
			timetot(11), timetot(12), timetot(13), timetot(14), timetot(15)
			timetot = 0
		end if
	end if
!!*************************************************************
!!*************************************************************

 enddo

!!**********************************************************************************************************************************
!!			End of Particle In Cell cycle a.k.a main loop
!!**********************************************************************************************************************************

  call Deallocation()
  call MPI_FINALIZE(ierr)
  print *,'FFFFFFFFFf',error,iteration,rank

end program Extraction

 

subroutine MagneticFieldProjectionIPP (l,bx,by,bz)


     use Parallel
	implicit none

	integer, intent(in):: l
	real, intent(out):: bx
	real, intent(out):: by
	real, intent(out):: bz

	integer :: i=0,j=0,k=0
	real :: distance(0:2)

	i=P(l)%X/delta(0)
	j=P(l)%Y/delta(1)
	k=P(l)%Z/delta(2)

	distance(0)=1.*P(l)%X/delta(0)-i
 	distance(1)=1.*P(l)%Y/delta(1)-j
 	distance(2)=1.*P(l)%Z/delta(2)-k


if(i>i1 .OR. j>j1 .OR. k>k1 .OR. i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then

!if we gain particle from the other procces that is in the border of other proces - in function Particle_Loss P().X<i0 and P().X>=i1, then we need one extra plane Exm(i1+2) where is not calculation - just skip these points (less then 0.000001% per iteration)

	if(i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then

			write(3500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
			write(3500+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart
			write(3500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

	endif

else

	bx=0
  	bx=bx +Bxm(i,  j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))
	bx=bx +Bxm(i+1,j,k)      *(distance(0))    * (1.-distance(1)) * (1.-distance(2))
	bx=bx +Bxm(i,  j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2))
	bx=bx +Bxm(i+1,j+1,k)    *distance(0)      * (distance(1))    * (1.-distance(2))
	bx=bx +Bxm(i,  j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2))
	bx=bx +Bxm(i+1,j,k+1)    *(distance(0))    * (1.-distance(1)) * (distance(2))
	bx=bx +Bxm(i,  j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2))
	bx=bx +Bxm(i+1,j+1,k+1)  *(distance(0))    * (distance(1))    * (distance(2))

	by=0
	by=by +Bym(i,j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))
	by=by +Bym(i+1,j,k)    *(distance(0))    * (1.-distance(1)) * (1.-distance(2))
	by=by +Bym(i,j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2))
	by=by +Bym(i+1,j+1,k)  *distance(0)      * (distance(1))    * (1.-distance(2))
	by=by +Bym(i,j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2))
	by=by +Bym(i+1,j,k+1)  *(distance(0))    * (1.-distance(1)) * (distance(2))
	by=by +Bym(i,j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2))
	by=by +Bym(i+1,j+1,k+1)*(distance(0))    * (distance(1))    * (distance(2))
	
	bz=0
	bz=bz +Bzm(i,j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))
	bz=bz +Bzm(i+1,j,k)    *(distance(0))    * (1.-distance(1)) * (1.-distance(2))
	bz=bz +Bzm(i,j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2))
	bz=bz +Bzm(i+1,j+1,k)  *distance(0)      * (distance(1))    * (1.-distance(2))
	bz=bz +Bzm(i,j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2))
	bz=bz +Bzm(i+1,j,k+1)  *(distance(0))    * (1.-distance(1)) * (distance(2))
	bz=bz +Bzm(i,j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2))
	bz=bz +Bzm(i+1,j+1,k+1)*(distance(0))    * (distance(1))    * (distance(2))
endif



end subroutine  MagneticFieldProjectionIPP




subroutine Density_projection()
	use Parallel
	implicit none
	
	integer :: i=0
	integer :: j=0
	integer :: k=0
	integer :: sp_2=0
	integer :: loop_l=0

	real :: distance(0:2)
	real :: vol=0


	Dens_e=0
	Dens_H2p=0
	Dens_Hm=0
	Dens_Hp=0
	Dens_HnPG=0

	vol=1.0/(delta(0)*delta(1)*delta(2))

	do loop_l=1,Npart
		
		i=P(loop_l)%X/delta(0)
		j=P(loop_l)%Y/delta(1)
		k=P(loop_l)%Z/delta(2)



		distance(0)=1.*P(loop_l)%X/delta(0)-i
 		distance(1)=1.*P(loop_l)%Y/delta(1)-j
 		distance(2)=1.*P(loop_l)%Z/delta(2)-k
 	


		if(i>i1 .OR. j>j1 .OR. k>k1 .OR. i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then
			!If the particle is outside of the domain of the task then it should print which particle.
			!This was inheritated from the first versions of the code.
			!As this function is called after Particle_exchange and DelParticles no particle should satisfy the conditions
			!to enter in this block 

			!write(9000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
			!write(9000+rank,*) rank,iter,loop_l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(loop_l).X,P(loop_l).Y,P(loop_l).Z,P(loop_l).IdentPart,P(loop_l).X,P		(loop_l).Y,P(loop_l).Z
			!write(9000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"


		else	
			! A List of the negative ions emitted from the PG and is H+ done in order to 
			! make them collide in between using the Takizuka algorithm
      			if(  (P(loop_l)%sp == 3) .OR. (P(loop_l)%sp == 5) .OR.  (P(loop_l)%sp == 9)  ) then
				!Number of particles in each cell
		      		Num_part_Coul_cell( i, j, k) = Num_part_Coul_cell( i, j, k) + 1
				!Indexes of the particles at each cell 
       	 			Coulomb_cell_list( i, j, k, Num_part_Coul_cell( i, j, k) ) = loop_l
       			endif
		        
					sp_2=P(loop_l)%sp
					Select case (sp_2)

						case(0)

							Dens_e(i,j,k)      =Dens_e(i,j,k)      +(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_e(i+1,j,k)    =Dens_e(i+1,j,k)    +(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_e(i,j+1,k)    =Dens_e(i,j+1,k)    +(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_e(i+1,j+1,k)  =Dens_e(i+1,j+1,k)  +(distance(0) )   * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_e(i,j,k+1)    =Dens_e(i,j,k+1)    +(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_e(i+1,j,k+1)  =Dens_e(i+1,j,k+1)  +(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_e(i,j+1,k+1)  =Dens_e(i,j+1,k+1)  +(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_e(i+1,j+1,k+1)=Dens_e(i+1,j+1,k+1)+(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)

						case(1)
							Dens_H2p(i,j,k)      =Dens_H2p(i,j,k)      +(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_H2p(i+1,j,k)    =Dens_H2p(i+1,j,k)    +(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_H2p(i,j+1,k)    =Dens_H2p(i,j+1,k)    +(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_H2p(i+1,j+1,k)  =Dens_H2p(i+1,j+1,k)  +(distance(0))     * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)

							Dens_H2p(i,j,k+1)    =Dens_H2p(i,j,k+1)   +(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_H2p(i+1,j,k+1)  =Dens_H2p(i+1,j,k+1)  +(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_H2p(i,j+1,k+1)  =Dens_H2p(i,j+1,k+1)  +(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_H2p(i+1,j+1,k+1)=Dens_H2p(i+1,j+1,k+1)+(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
						
						case(2)
		
							Dens_Hm(i,j,k)      =Dens_Hm(i,j,k)      +(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j,k)    =Dens_Hm(i+1,j,k)    +(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i,j+1,k)    =Dens_Hm(i,j+1,k)    +(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j+1,k)  =Dens_Hm(i+1,j+1,k)  +(distance(0))    * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i,j,k+1)    =Dens_Hm(i,j,k+1)   +(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j,k+1)  =Dens_Hm(i+1,j,k+1)  +(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i,j+1,k+1)  =Dens_Hm(i,j+1,k+1)  +(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j+1,k+1)=Dens_Hm(i+1,j+1,k+1)+(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)

						case(3)
							Dens_Hp(i,j,k)      =Dens_Hp(i,j,k)      +(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hp(i+1,j,k)    =Dens_Hp(i+1,j,k)    +(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hp(i,j+1,k)    =Dens_Hp(i,j+1,k)    +(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hp(i+1,j+1,k)  =Dens_Hp(i+1,j+1,k)  +(distance(0))     * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hp(i,j,k+1)    =Dens_Hp(i,j,k+1)   +(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hp(i+1,j,k+1)  =Dens_Hp(i+1,j,k+1)  +(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hp(i,j+1,k+1)  =Dens_Hp(i,j+1,k+1)  +(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hp(i+1,j+1,k+1)=Dens_Hp(i+1,j+1,k+1)+(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)

						case(8)
		
							Dens_Hm(i,j,k)      =Dens_Hm(i,j,k)      +(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j,k)    =Dens_Hm(i+1,j,k)    +(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i,j+1,k)    =Dens_Hm(i,j+1,k)    +(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j+1,k)  =Dens_Hm(i+1,j+1,k)  +(distance(0))    * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i,j,k+1)    =Dens_Hm(i,j,k+1)   +(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j,k+1)  =Dens_Hm(i+1,j,k+1)  +(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i,j+1,k+1)  =Dens_Hm(i,j+1,k+1)  +(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_Hm(i+1,j+1,k+1)=Dens_Hm(i+1,j+1,k+1)+(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
						case(5)
		
							Dens_HnPG(i,j,k)      =Dens_HnPG(i,j,k)      +(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j,k)    =Dens_HnPG(i+1,j,k)    +(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i,j+1,k)    =Dens_HnPG(i,j+1,k)    +(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j+1,k)  =Dens_HnPG(i+1,j+1,k)  +(distance(0))    * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i,j,k+1)    =Dens_HnPG(i,j,k+1)   +(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j,k+1)  =Dens_HnPG(i+1,j,k+1)  +(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i,j+1,k+1)  =Dens_HnPG(i,j+1,k+1)  +(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j+1,k+1)=Dens_HnPG(i+1,j+1,k+1)+(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
						case(9)
		
							Dens_HnPG(i,j,k)      =Dens_HnPG(i,j,k)      +(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j,k)    =Dens_HnPG(i+1,j,k)    +(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i,j+1,k)    =Dens_HnPG(i,j+1,k)    +(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j+1,k)  =Dens_HnPG(i+1,j+1,k)  +(distance(0))    * (distance(1))    * (1.-distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i,j,k+1)    =Dens_HnPG(i,j,k+1)   +(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j,k+1)  =Dens_HnPG(i+1,j,k+1)  +(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i,j+1,k+1)  =Dens_HnPG(i,j+1,k+1)  +(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
							Dens_HnPG(i+1,j+1,k+1)=Dens_HnPG(i+1,j+1,k+1)+(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(loop_l)%sp)
					
						case DEFAULT
					end select
		endif
	enddo


	call ChargeExchange(Dens_e)
	call ChargeExchange(Dens_H2p)
	call ChargeExchange(Dens_Hm)
	call ChargeExchange(Dens_Hp)
	call ChargeExchange(Dens_HnPG)
	
end subroutine Density_projection



subroutine Set_Boundary(k,j,i,Rproc)

  use Parallel
  implicit none

  integer, intent(in)  :: k
  integer, intent(in)  :: j
  integer, intent(in)  :: i
  integer, intent(in)  :: Rproc

  !Calculation the boundaries of each process. For last process not considere last point.If we have 100 points and 2 process:First 0-49,Second from 50 to 99
  indexes(0)=Nxp/NprocX*i
  if(i == NprocX-1) then
    indexes(1)=Nxp/NprocX*(i+1)-1
  else
    indexes(1)=Nxp/NprocX*(i+1)-1
  endif
  
  indexes(2)=Nyp/NprocY*j
  if(j == NprocY-1) then
    indexes(3)=Nyp/NprocY*(j+1)-1
  else
    indexes(3)=Nyp/NprocY*(j+1)-1
  endif

  indexes(4)=Nzp/NprocZ*k
  if(k == NprocZ-1) then
    indexes(5)=Nzp/NprocZ*(k+1)-1
  else
    indexes(5)=Nzp/NprocZ*(k+1)-1
  endif

  i00(Rproc)=indexes(0)
  i11(Rproc)=indexes(1)

  j00(Rproc)=indexes(2)
  j11(Rproc)=indexes(3)

  k00(Rproc)=indexes(4)
  k11(Rproc)=indexes(5)

  !Calculation the neighbours of each process
  if(i>0) then 
    neig(0)=Rproc-1
  else
    neig(0)=-1
  endif
  
  if(i<NprocX-1) then 
    neig(1)=Rproc+1
  else
    neig(1)=-1
  endif
  
  if(j>0) then 
    neig(2)=Rproc-NprocX
  else
    if(Periodic==1) then
      neig(2)=(Rproc+NprocX*(NprocY-1))
    else
      neig(2)=-1
    endif
  endif

  if(j<NprocY-1) then 
    neig(3)=Rproc+NprocX
  else
    if(Periodic==1) then
      neig(3)=(Rproc-NprocX*(NprocY-1))
    else
      neig(3)=-1
    endif
  endif

  if(k>0) then 
    neig(4)=Rproc-(NprocX*NprocY)
  else
    if(Periodic==1) then
      neig(4)=(Rproc+(NprocZ-1)*NprocX*NprocY)
    else
      neig(4)=-1
    endif
  endif



  if(k<NprocZ-1) then 
    neig(5)=Rproc+(NprocX*NprocY)
  else
    if(Periodic==1) then
      neig(5)=(Rproc-(NprocZ-1)*NprocX*NprocY)
    else
      neig(5)=-1
    endif
  endif

end subroutine Set_Boundary

subroutine Set_Boundary_Zero()
   
  use Parallel
  implicit none

  !Put boundary equals 0
  if(neig(0)==-1) then 
    V(i0-1,:,:)=0
  endif
  if(neig(1)==-1) then
    V(i1+1,:,:)=0
  endif

  if(neig(2)==-1) then
    V(:,j0-1,:)=0
  endif
  if(neig(3)==-1) then 
    V(:,j1+1,:)=0
  endif

  if(neig(4)==-1) then 
    V(:,:,k0-1)=0
  endif
  if(neig(5)==-1) then 
    V(:,:,k1+1)=0
  endif

end subroutine Set_Boundary_Zero


subroutine Send_Plane(Arr)
	use Parallel
	implicit none
	include 'mpif.h'


        real,intent( inout ) :: Arr(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1) 

	if(mod(indexx,2)==0) then
	!---------------------------X direction-------------------------------------------------------------
		if(neig(0)>-1) CALL MPI_SEND(Arr(i0,j0-1,k0-1), 1,yz0, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+1,j0-1,k0-1), 1,yz0, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1,j0-1,k0-1), 1,yz0, neig(1),51, MPI_COMM_WORLD, info)
		
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,yz0, neig(0),51, MPI_COMM_WORLD, status, info)

		
	else
		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+1,j0-1,k0-1), 1,yz0, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(0)>-1) CALL MPI_SEND(Arr(i0,j0-1,k0-1), 1,yz0, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,yz0, neig(0),51, MPI_COMM_WORLD, status, info)
	
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1,j0-1,k0-1), 1,yz0, neig(1),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexy,2)==0) then
	!---------------------------Y direction-------------------------------------------------------------
		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-1,j0,k0-1), 1,xz0, neig(2),51, MPI_COMM_WORLD, info)
	
		if(neig(3)>-1) CALL MPI_RECV(Arr(i0-1,j1+1,k0-1), 1,xz0, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-1,j1,k0-1), 1,xz0, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xz0, neig(2),51, MPI_COMM_WORLD, status, info)
			
		
	else
		if(neig(3)>-1) CALL MPI_RECV(Arr(i0-1,j1+1,k0-1), 1,xz0, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-1,j0,k0-1), 1,xz0, neig(2),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xz0, neig(2),51, MPI_COMM_WORLD, status, info)
!	
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-1,j1,k0-1), 1,xz0, neig(3),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexz,2)==0) then
	!---------------------------Z direction-------------------------------------------------------------
		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-1,j0-1,k0), 1,xy0, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k1+1), 1,xy0, neig(5),51, MPI_COMM_WORLD, status, info)
	
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-1,j0-1,k1), 1,xy0, neig(5),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xy0, neig(4),51, MPI_COMM_WORLD, status, info)

	else
		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k1+1), 1,xy0, neig(5),51, MPI_COMM_WORLD, status, info)

		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-1,j0-1,k0), 1,xy0, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xy0, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-1,j0-1,k1), 1,xy0, neig(5),51, MPI_COMM_WORLD, info)
	endif

end subroutine Send_Plane



subroutine Send2_Plane(Arr)
	use Parallel
	implicit none
	include 'mpif.h'

        real,intent( inout ) :: Arr(i0-2:i1+2 ,j0-2:j1+2, k0-2:k1+2) 

	nx2=i1+2-(i0-2)+1
	ny2=j1+2-(j0-2)+1
	nz2=k1+2-(k0-2)+1


	CALL MPI_TYPE_VECTOR(1, nx2*ny2, 1, MPI_REAL, xy20, info)
	CALL MPI_TYPE_COMMIT(xy20, info)
	CALL MPI_TYPE_VECTOR(nz2, nx2, nx2*ny2, MPI_REAL, xz20, info)
	CALL MPI_TYPE_COMMIT(xz20, info)
	CALL MPI_TYPE_VECTOR( nz2*ny2 , 1, nx2, MPI_REAL, yz20, info)	
	CALL MPI_TYPE_COMMIT(yz20, info)



	if(mod(indexx,2)==0) then
	!---------------------------X direction-------------------------------------------------------------
		if(neig(0)>-1) CALL MPI_SEND(Arr(i0,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+1,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, info)
		
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-1,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, status, info)

		
	else
		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+1,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(0)>-1) CALL MPI_SEND(Arr(i0,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-1,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, status, info)
	
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexy,2)==0) then
	!---------------------------Y direction-------------------------------------------------------------
		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-2,j0,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, info)
	
		if(neig(3)>-1) CALL MPI_RECV(Arr(i0-2,j1+1,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-2,j1,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-2,j0-1,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, status, info)

		
	else
		if(neig(3)>-1) CALL MPI_RECV(Arr(i0-2,j1+1,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-2,j0,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-2,j0-1,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, status, info)
!	
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-2,j1,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexz,2)==0) then
	!---------------------------Z direction-------------------------------------------------------------
		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k0), 1,xy20, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k1+1), 1,xy20, neig(5),51, MPI_COMM_WORLD, status, info)
	
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k1), 1,xy20, neig(5),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-1), 1,xy20, neig(4),51, MPI_COMM_WORLD, status, info)

	else
		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k1+1), 1,xy20, neig(5),51, MPI_COMM_WORLD, status, info)

		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k0), 1,xy20, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-1), 1,xy20, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k1), 1,xy20, neig(5),51, MPI_COMM_WORLD, info)
	endif

	!For the second plane

if(mod(indexx,2)==0) then
	!---------------------------X direction-------------------------------------------------------------
		if(neig(0)>-1) CALL MPI_SEND(Arr(i0+1,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+2,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1-1,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, info)
		
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, status, info)

		
	else
		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+2,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(0)>-1) CALL MPI_SEND(Arr(i0-1,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-2), 1,yz20, neig(0),51, MPI_COMM_WORLD, status, info)
	
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1+1,j0-2,k0-2), 1,yz20, neig(1),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexy,2)==0) then
	!---------------------------Y direction-------------------------------------------------------------
		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-2,j0+1,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, info)
	
		if(neig(3)>-1) CALL MPI_RECV(Arr(i0-2,j1+2,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-2,j1-1,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, status, info)

		
	else
		if(neig(3)>-1) CALL MPI_RECV(Arr(i0-2,j1+2,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-2,j0-1,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-2), 1,xz20, neig(2),51, MPI_COMM_WORLD, status, info)
!	
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-2,j1+1,k0-2), 1,xz20, neig(3),51, MPI_COMM_WORLD, info)
	endif



	if(mod(indexz,2)==0) then
	!---------------------------Z direction-------------------------------------------------------------
		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k0+1), 1,xy20, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k1+2), 1,xy20, neig(5),51, MPI_COMM_WORLD, status, info)
	
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k1-1), 1,xy20, neig(5),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-2), 1,xy20, neig(4),51, MPI_COMM_WORLD, status, info)

	else
		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k1+2), 1,xy20, neig(5),51, MPI_COMM_WORLD, status, info)

		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k0-1), 1,xy20, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-2,j0-2,k0-2), 1,xy20, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-2,j0-2,k1+1), 1,xy20, neig(5),51, MPI_COMM_WORLD, info)
	endif

end subroutine Send2_Plane














subroutine Flux_NI_from_PG_sin()
	use Parallel
	implicit none
	include 'mpif.h'



	
	real Vt,Vt2,erfi,j,num_rand,angle_con
	real x0,x1,y0,y1,y_pos,Conus_r_y, theta
	
	TYPE(Particle) :: current_Par  
	
	integer :: i_start=0
	integer :: i_end=0



	real :: start_real=0.0
	real :: end_real=0.0
	
	integer :: j_start=0
	integer :: j_end=0

	integer :: k_start=0
	integer :: k_end=0

	integer num_rand_int

	real alfa,radius,py_n,pz_n
	
	i_start=68
	i_end=74

	j_start=0
	j_end=Nyp-3 !Could not put j1 or k1 becouse in the case of few procces in the same direction one will have k1= 32 another 98

	k_start=0
	k_end=Nzp-3

	Vt=0
	Vt2=0
	erfi=0
	alfa=0

num_rand=0
num_rand_int=1
y_pos=0
Conus_r_y=0
py_p=0
pz_p=0
py_n=0
pz_n=0
vy_p=0
vz_p=0
angle_con=0

!  start_real=0.019
!  end_real=0.0226
	
  start_real=The_Object(4)%center(0)-The_Object(4)%width(0)+0.000125
  end_real=The_Object(4)%center(0)+The_Object(4)%width(0)
 

do j=1,1
		  call random_number(num_rand)

          current_Par%X=start_real+num_rand*(end_real-start_real)
		  current_Par%sp=9

            x1=The_Object(4)%center(0)+The_Object(4)%width(0)
			x0=The_Object(4)%center(0)-The_Object(4)%width(0)
			y1=The_Object(4)%center(1)+The_Object(4)%width(2)
			y0=The_Object(4)%center(1)+The_Object(4)%width(1)
            
            angle_con = atan( (The_Object(4)%width(1)-The_Object(4)%width(2))  /(x1-x0) ) 
			
            y_pos=(y1-y0)*(current_Par%X-x0)/(x1-x0)+y0 
			Conus_r_y=abs(y_pos-The_Object(4)%center(1))-1e-5
                  
	
            
            if(rank==0) then
                
                  num_rand=0
			      call random_number(num_rand)	
                  theta=num_rand*3.14*2.0
                  current_Par%Y=cos(theta)*Conus_r_y+The_Object(4)%center(1)
			      current_Par%Z=sin(theta)*Conus_r_y+The_Object(4)%center(2) 
          
                  py_p=current_Par%Y
                  pz_p=current_Par%Z
			
		!            call random_number(num_rand)                                    
                    vx_p=sqrt(2.0*k_b*temperature(current_Par%sp)/(mass(current_Par%sp)))
                 !   vx_p=vx_p*num_rand+100
                  !  call random_number(num_rand)	
                    vy_p=sqrt(2.0*k_b*temperature(current_Par%sp)/(mass(current_Par%sp)))
                   ! vy_p=vy_p*num_rand+100
                    !call random_number(num_rand)	
                    vz_p=sqrt(2.0*k_b*temperature(current_Par%sp)/(mass(current_Par%sp))) 
                   ! vz_p=vz_p*num_rand+100
                        
                    radius=sqrt((py_p-The_Object(4)%center(1))**2+(pz_p-The_Object(4)%center(2))**2)
                    py_n=py_p-The_Object(4)%center(1)
                    pz_n=pz_p-The_Object(4)%center(2)
                            
             IF(3==3) THEN         
                    if((py_n/radius>=-1 .AND. py_n/radius<=0  .AND. pz_n/radius>=0 .AND. pz_n/radius<=1) .OR. (py_n/radius<=1 .AND. py_n/radius>=0 .AND.  pz_n/radius<=0 .AND. pz_n/radius>=-1)) then                         
                                    
                                    call random_number(num_rand)	
                                    
                                    theta=num_rand*(3.14-2.0* angle_con)+ angle_con+asin(py_n/radius)+3.14/2.0
                                    current_Par%VY=sin(theta)*vy_p
                                    
                                    call random_number(num_rand)	
                                    theta=num_rand*(3.14-2.0* angle_con)+ angle_con-acos(pz_n/radius)+3.14/2.0
                                    current_Par%VZ=cos(theta)*vz_p 
                                    
                                    call random_number(num_rand)	               
                                    theta=num_rand*(3.14-2.0*angle_con)+angle_con
                                    current_Par%VX=cos(theta)*vx_p
                                                                   
                    elseif ((py_n/radius<=0 .AND. py_n/radius>=-1  .AND. pz_n/radius<=0 .AND. pz_n/radius>=-1) .OR. (py_n/radius<=1 .AND. py_n/radius>=0 .AND.  pz_n/radius>=0 .AND. pz_n/radius<=1)) then
                            
                                    call random_number(num_rand)	               
                                   
                                    theta=num_rand*(3.14-2.0* angle_con)+ angle_con+asin(py_n/radius)+3.14/2.0
                                    current_Par%VY=sin(theta)*vy_p
                                    
                                    call random_number(num_rand)	
                        
                                    theta=num_rand*(3.14-2.0* angle_con)+ angle_con +acos(pz_n/radius)+3.14/2.0
                                    current_Par%VZ=cos(theta)*vz_p
                                                                                       
                                    call random_number(num_rand)	               
                                    theta=num_rand*(3.14-2.0*angle_con)+angle_con
                                    current_Par%VX=cos(theta)*vx_p
                                
                    else
                                
                                write(20000+rank,*) "BAG in the NI injection routine from PG inner (conical) wall"
                                write(20000+rank,*)  iter,py_n/radius,pz_n/radius
                                        current_Par%VY=-py_n*vy_p/radius
                                        current_Par%VZ=-pz_n*vz_p/radius
                                    
                    endif
                    
             ELSE
                 
                 
                     current_Par%VY=-py_n*vy_p/radius
                     current_Par%VZ=-pz_n*vz_p/radius
             
             
             ENDIF
             
             
                    
                    current_Par%IdentPart=0
                    IdentPartNumber=IdentPartNumber+1


                     Call MPI_BCAST(current_Par, 8,MPI_REAL,0,MPI_COMM_WORLD, info)
            else
                     Call MPI_BCAST(current_Par, 8,MPI_REAL,0,MPI_COMM_WORLD, info)
            endif
                    
        if(current_Par%x>i0*delta(0) .AND.  current_Par%x<=i1*delta(0)+delta(0) .AND. current_Par%y>j0*delta(1) .AND.  current_Par%y<=j1*delta(1)+delta(1)  .AND. current_Par%z>k0*delta(2) .AND.  current_Par%z<=k1*delta(2)+delta(2) ) then

				Npart=Npart+1
				P(Npart)=current_Par
             Ener_kin_test=6.24e18* ( mass(current_Par%sp)*(current_Par%VX**2)/2.+mass(current_Par%sp)*(current_Par%VY**2)/2.+mass(current_Par%sp)*(current_Par%VZ**2)/2.)
             !if(iter==0)    write(7100+rank,*) current_Par.VX,current_Par.VY,current_Par.VZ,Ener_kin_test
            !if(iter==0)    write(7200+rank,*) current_Par.X,current_Par.Y,current_Par.Z
        
        else

        endif

enddo

end subroutine Flux_NI_from_PG_sin


subroutine Test_Box_Inzone(ibox)
    use Parallel
    implicit none
    integer,intent(in) :: ibox
    integer  i,j,k
    real  i0width,i1width,j0width,j1width,k0width,k1width

    i0width=The_Object(ibox)%center(0)-The_Object(ibox)%width(0)-1e-7
    i1width=The_Object(ibox)%center(0)+The_Object(ibox)%width(0)+1e-7

    j0width=The_Object(ibox)%center(1)-The_Object(ibox)%width(1)-1e-7
    j1width=The_Object(ibox)%center(1)+The_Object(ibox)%width(1)+1e-7

    k0width=The_Object(ibox)%center(2)-The_Object(ibox)%width(2)-1e-7
    k1width=The_Object(ibox)%center(2)+The_Object(ibox)%width(2)+1e-7


    if(3==3) then !periodic==0) then
        do k=k0, k1
            do j=j0, j1
                do i=i0, i1
                    if( (1.*i*delta(0)>i0width)  .AND.  (1.*i*delta(0)<i1width) ) then           
                        if( (1.*j*delta(1)>j0width)  .AND.  (1.*j*delta(1)<j1width) ) then       
                            if( (1.*k*delta(2)>k0width)  .AND.  (1.*k*delta(2)<k1width) ) then
                                 Inzone(i,j,k)=ibox
       
                            endif
                        endif
                    endif
                enddo
            enddo   
        enddo

    else

        !If we have periodic conditions the boundaries of first box are considered the same size as the grid in y and z directions
        do i=i0,i1
            if( (1.*i*delta(0)>i0width)  .AND.  (1.*i*delta(0)<i1width)) then           
                Inzone(i,j0:j1,k0:k1)=ibox
            endif
        enddo
    endif

end subroutine Test_Box_Inzone



subroutine Test_Box_Nei(ibox)
	
	use Parallel
	implicit none
	integer,intent(in) :: ibox
	integer  i,j,k
	
	do k=k0, k1
		do j=j0, j1
			do i=i0, i1
				if( Inzone(i,j,k)==0) then
					if ( (Inzone(i+1,j,k)==ibox) .AND.  (Inzone(i-1,j,k)==0) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphax(i,j,k)=(The_Object(ibox)%center(0)-The_Object(ibox)%width(0))/delta(0)-1.*i
							if(alphax(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts box i0' 
								stop
							endif
	 					!endif
					endif

					if ( (Inzone(i+1,j,k)==0) .AND.  (Inzone(i-1,j,k)==ibox) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphax(i,j,k)=(The_Object(ibox)%center(0)+The_Object(ibox)%width(0))/delta(0)-1.*i
							if(alphax(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts box i1' 
								stop
							endif
						!endif
					endif

			
					if ( (Inzone(i,j+1,k)==ibox) .AND.  (Inzone(i,j-1,k)==0) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphay(i,j,k)=(The_Object(ibox)%center(1)-The_Object(ibox)%width(1))/delta(1)-1.*j
							if(alphay(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts box j0' 
								stop
							endif
 						!endif

					endif

					if ( (Inzone(i,j+1,k)==0) .AND.  (Inzone(i,j-1,k)==ibox) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphay(i,j,k)=(The_Object(ibox)%center(1)+The_Object(ibox)%width(1))/delta(1)-1.*j
							if(alphay(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts box j1' 
								stop
							endif
 						!endif
					endif


					if ( (Inzone(i,j,k+1)==ibox) .AND.  (Inzone(i,j,k-1)==0) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphaz(i,j,k)=(The_Object(ibox)%center(2)-The_Object(ibox)%width(2))/delta(2)-1.*k
							if(alphaz(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts box k0' 
								stop
							endif
 						!endif
					endif

					if ( (Inzone(i,j,k+1)==0) .AND.  (Inzone(i,j,k-1)==ibox) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphaz(i,j,k)=(The_Object(ibox)%center(2)+The_Object(ibox)%width(2))/delta(2)-1.*k
							if(alphaz(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts box k1' 
								stop
							endif
 						!endif
					endif
				endif
			enddo
		enddo	
	enddo

end subroutine Test_Box_Nei


subroutine Test_Cylinder_Inzone(ibox)

	use Parallel
	implicit none

 	integer,intent( in ) :: ibox
	integer  i,j,k
	real  i0width,i1width

	i0width=The_Object(ibox)%center(0)-The_Object(ibox)%width(0)+1e-5
	i1width=The_Object(ibox)%center(0)+The_Object(ibox)%width(0)-1e-5



	do i=i0, i1
		do j=j0, j1

			do k=k0, k1
				if( (i*delta(0)>i0width) .AND. (i*delta(0)<i1width) ) then

					if((sqrt (1.0*( (1.*delta(2)*k-The_Object(ibox)%center(2))*(1.*delta(2)*k-The_Object(ibox)%center(2))+(1.*delta(1)*j-The_Object(ibox)%center(1))*(1.*delta(1)*j-The_Object(ibox)%center(1)))))< The_Object(ibox)%width(1)-1e-5) then	
 							Inzone(i,j,k)=ibox	
					endif
				endif
			enddo
		enddo	
	enddo


end subroutine Test_Cylinder_Inzone

subroutine Test_Cylinder_Nei(ibox)

	use Parallel
	implicit none
	integer,intent( in ) :: ibox
	integer  i,j,k

	do k=k0, k1
		do j=j0, j1
			do i=i0, i1
				if(Inzone(i,j,k)==ibox) then
	
					if ( (Inzone(i+1,j,k)>0) .AND.  (Inzone(i-1,j,k)==0) ) then
						ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						if(ZoneNeig(i,j,k)>1) then
 							alphax(i,j,k)=1.*i-(The_Object(ibox)%center(0)-The_Object(ibox)%width(0))/delta(0)
							if(alphax(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts cyl i0' 
								stop
							endif
 						endif
					endif

					if ( (Inzone(i+1,j,k)==0) .AND.  (Inzone(i-1,j,k)>0) ) then
						ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						if(ZoneNeig(i,j,k)>1) then
 							alphax(i,j,k)=(The_Object(ibox)%center(0)+The_Object(ibox)%width(0))/delta(0)-1.*i
							if(alphax(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts cyl i1' 
								stop
							endif
 						endif
					endif

			
					if ( (Inzone(i,j+1,k)>0) .AND.  (Inzone(i,j-1,k)==0) ) then
						ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						if(ZoneNeig(i,j,k)>1) then
 							alphay(i,j,k)=1.*j-(-sqrt(The_Object(ibox)%width(1)*The_Object(ibox)%width(1)-(k*delta(2)-The_Object(ibox)%center(2))*(k*delta(2)-The_Object(ibox)%center(2)) )+The_Object(ibox)%center(1))/delta(1)
							if(alphay(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts cyl j0' 
								stop
							endif
								
 						endif
					endif

					if ( (Inzone(i,j+1,k)==0) .AND.  (Inzone(i,j-1,k)>0) ) then
						ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						if(ZoneNeig(i,j,k)>1) then
 							alphay(i,j,k)=(sqrt(The_Object(ibox)%width(1)*The_Object(ibox)%width(1)-(k*delta(2)-The_Object(ibox)%center(2))*(k*delta(2)-The_Object(ibox)%center(2)) )+The_Object(ibox)%center(1))/delta(1)-1.*j
							if(alphay(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts cyl j1' 
								stop
							endif
 						endif
					endif


					if ( (Inzone(i,j,k+1)>0) .AND.  (Inzone(i,j,k-1)==0) ) then
						ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						if(ZoneNeig(i,j,k)>1) then
 							alphaz(i,j,k)=1.*k-(-sqrt(The_Object(ibox)%width(1)*The_Object(ibox)%width(1)-(1.*j*delta(1)-The_Object(ibox)%center(1))*(1.*j*delta(1)-The_Object(ibox)%center(1)) )+The_Object(ibox)%center(2))/delta(2)
							if(alphaz(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts cyl k0' 
								stop
							endif
 						endif
					endif

					if ( (Inzone(i,j,k+1)==0) .AND.  (Inzone(i,j,k-1)>0) ) then
						ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						if(ZoneNeig(i,j,k)>1) then
 							alphaz(i,j,k)=(sqrt(The_Object(ibox)%width(1)*The_Object(ibox)%width(1)-(1.*j*delta(1)-The_Object(ibox)%center(1))*(1.*j*delta(1)-The_Object(ibox)%center(1)) )+The_Object(ibox)%center(2))/delta(2)-1.*k
							if(alphaz(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts cyl k1' 
								stop
							endif
 						endif
					endif
				endif
			enddo
		enddo	
	enddo

end subroutine Test_Cylinder_Nei



subroutine Test_Conus_Inzone(ibox)


	use Parallel
	implicit none

 	integer,intent( in ) :: ibox
	integer  i,j,k
  double precision  i0width,i1width
  double precision :: Conus_r_y=0
  double precision :: x0,x1,y0,y1,y_pos


	x1=The_Object(ibox)%center(0)+The_Object(ibox)%width(0)
	x0=The_Object(ibox)%center(0)-The_Object(ibox)%width(0)
	y1=The_Object(ibox)%center(2)+The_Object(ibox)%width(2)
	y0=The_Object(ibox)%center(1)+The_Object(ibox)%width(1)



	i0width=The_Object(ibox)%center(0)-The_Object(ibox)%width(0)
	i1width=The_Object(ibox)%center(0)+The_Object(ibox)%width(0)


	do i=i0, i1
		do j=j0, j1
			do k=k0, k1
				if( (i*delta(0)>=i0width) .AND. (i*delta(0)<=i1width) ) then	
					
					y_pos=(y1-y0)*(i*delta(0)-x0)/(x1-x0)+y0 
					Conus_r_y=y_pos-The_Object(ibox)%center(1)


					if(sqrt(1.0*( (1.*delta(2)*k-The_Object(ibox)%center(2))*(1.*delta(2)*k-The_Object(ibox)%center(2)) &
                       +(1.*delta(1)*j-The_Object(ibox)%center(1))*(1.*delta(1)*j-The_Object(ibox)%center(1)))) > Conus_r_y) then	
 							Inzone(i,j,k)=ibox	
					endif
				endif
			enddo
		enddo	
	enddo


end subroutine Test_Conus_Inzone


subroutine Test_Conus_Nei(ibox)

	use Parallel
	implicit none
	integer,intent( in ) :: ibox
	integer  i,j,k

	real :: Conus_r_y=0
	real :: x0,x1,y0,y1,y_pos,r_conus,yy0,yy1


	x1=The_Object(ibox)%center(0)+The_Object(ibox)%width(0)
	x0=The_Object(ibox)%center(0)-The_Object(ibox)%width(0)
	y1=The_Object(ibox)%center(1)+The_Object(ibox)%width(2)
	y0=The_Object(ibox)%center(1)+The_Object(ibox)%width(1)


	yy1=The_Object(ibox)%width(2)
	yy0=The_Object(ibox)%width(1)





	do k=k0, k1
		do j=j0, j1
			do i=i0, i1
				if(Inzone(i,j,k)==0) then
					
					y_pos=(y1-y0)*(i*delta(0)-x0)/(x1-x0)+y0 
					Conus_r_y=abs(y_pos-The_Object(ibox)%center(1))

					r_conus=sqrt (1.0*( (1.*delta(2)*k-The_Object(ibox)%center(2))**2+(1.*delta(1)*j-The_Object(ibox)%center(1))**2))

					

					if ( (Inzone(i+1,j,k)==ibox) .AND.  (Inzone(i-1,j,k)==0) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						
						!if(ZoneNeig(i,j,k)>1) then
!  					
							alphax(i,j,k)=((((x1-x0)/(yy1-yy0))*(r_conus-yy0)+x0))/delta(0)-1.*i


							
							if(alphax(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts con i0' 
								stop
							endif
 						!endif
					endif

					if ( (Inzone(i+1,j,k)==0) .AND.  (Inzone(i-1,j,k)==ibox) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
			

							alphax(i,j,k)=((((x1-x0)/(yy1-yy0))*(r_conus-yy0)+x0))/delta(0)-1.0*i
			
! 
							if(alphax(i,j,k)==0) then
								write(6,*) 'Correction coefitient is very small- program aborts con i1' 
								stop
							endif
 						!endif
					endif

			
					if ( (Inzone(i,j+1,k)==ibox) .AND.  (Inzone(i,j-1,k)==0) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphay(i,j,k)=(-sqrt(Conus_r_y*Conus_r_y-(k*delta(2)-The_Object(ibox)%center(2))*(k*delta(2)-The_Object(ibox)%center(2)))+The_Object(ibox)%center(1))/delta(1)-1.*j


							if(alphay(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts con j0' 
								stop
							endif
								
 						!endif
					endif

					if ( (Inzone(i,j+1,k)==0) .AND.  (Inzone(i,j-1,k)==ibox) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphay(i,j,k)=(sqrt(Conus_r_y*Conus_r_y-(k*delta(2)-The_Object(ibox)%center(2))*(k*delta(2)-The_Object(ibox)%center(2)) )+The_Object(ibox)%center(1))/delta(1)-1.*j

					
							if(alphay(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts con j1' 
								stop
							endif
 						!endif
					endif


					if ( (Inzone(i,j,k+1)==ibox) .AND.  (Inzone(i,j,k-1)==0) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphaz(i,j,k)=(-sqrt(Conus_r_y*Conus_r_y-(1.*j*delta(1)-The_Object(ibox)%center(1))*(1.*j*delta(1)-The_Object(ibox)%center(1)))+The_Object(ibox)%center(2))/delta(2)-1.*k
							if(alphaz(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts con k0' 
								stop
							endif
 						!endif
					endif

					if ( (Inzone(i,j,k+1)==0) .AND.  (Inzone(i,j,k-1)==ibox) ) then
						!ZoneNeig(i,j,k)=1+Inzone(i,j,k)
						!if(ZoneNeig(i,j,k)>1) then
 							alphaz(i,j,k)=(sqrt(Conus_r_y*Conus_r_y-(1.*j*delta(1)-The_Object(ibox)%center(1))*(1.*j*delta(1)-The_Object(ibox)%center(1)) )+The_Object(ibox)%center(2))/delta(2)-1.*k
							if(alphaz(i,j,k)==0) then
								write(6,*) 'Correction is very small and program aborts con k1' 
								stop
							endif
 						!endif
					endif
				endif
			enddo
		enddo	
	enddo

end subroutine Test_Conus_Nei


subroutine Test_Conus_Nei_2(ibox)

	use Parallel
	implicit none
	integer,intent( in ) :: ibox
	integer  i,j,k

	
  double precision :: x0,x1,y0,y1,yy0,yy1, r0, r1, posx, posy, posz, rr2


	x0=The_Object(ibox)%center(0)-The_Object(ibox)%width(0)
	x1=The_Object(ibox)%center(0)+The_Object(ibox)%width(0)
	y0=The_Object(ibox)%center(1)+The_Object(ibox)%width(1)
	y1=The_Object(ibox)%center(1)+The_Object(ibox)%width(2)
  r0=The_Object(ibox)%width(1)
  r1=The_Object(ibox)%width(2)

	yy1=The_Object(ibox)%width(2)
	yy0=The_Object(ibox)%width(1)

	do k=k0, k1
		do j=j0, j1
			do i=i0, i1
				if(Inzone(i,j,k)==0) then

					if ( (Inzone(i+1,j,k)==ibox) .AND.  (Inzone(i-1,j,k)==0) ) then
            posx = i*delta(0)
            posy = j*delta(1)
            posz = k*delta(2)
            rr2 = sqrt((posy-The_Object(ibox)%center(1))**2 + (posz-The_Object(ibox)%center(2))**2)
            if(rr2 > r0) then
              alphax(i,j,k) = (x0-posx)/delta(0)
            else if(rr2>r1) then
              alphax(i,j,k) = ((rr2-r0)/(r1-r0)*(x1-x0)+x0-posx)/delta(0)
            end if
          end if
          
          if ( (Inzone(i+1,j,k)==0) .AND.  (Inzone(i-1,j,k)==ibox) ) then
            posx = i*delta(0)
            posy = j*delta(1)
            posz = k*delta(2)
            rr2 = sqrt((posy-The_Object(ibox)%center(1))**2 + (posz-The_Object(ibox)%center(2))**2)
            if(rr2 > r1) then
              alphax(i,j,k) = (x1-posx)/delta(0)
            else if(rr2>r0) then
              alphax(i,j,k) = ((rr2-r0)/(r1-r0)*(x1-x0)+x0-posx)/delta(0)
            end if
          end if
					
          if ( (Inzone(i,j+1,k)==ibox) .AND.  (Inzone(i,j-1,k)==0) ) then
            posx = i*delta(0)
            posy = j*delta(1)
            posz = k*delta(2)
            rr2 = (posx-x0)*(r1-r0)/(x1-x0)+r0
            alphay(i,j,k) = (The_Object(ibox)%center(1) + sqrt(rr2**2-(posz-The_Object(ibox)%center(2))**2) - posy)/delta(1)
          endif

          if ( (Inzone(i,j+1,k)==0) .AND.  (Inzone(i,j-1,k)==ibox) ) then
            posx = i*delta(0)
            posy = j*delta(1)
            posz = k*delta(2)
            rr2 = (posx-x0)*(r1-r0)/(x1-x0)+r0
            alphay(i,j,k) = (The_Object(ibox)%center(1) - sqrt(rr2**2-(posz-The_Object(ibox)%center(2))**2) - posy)/delta(1)
          endif

          if ( (Inzone(i,j,k+1)==ibox) .AND.  (Inzone(i,j,k-1)==0) ) then
            posx = i*delta(0)
            posy = j*delta(1)
            posz = k*delta(2)
            rr2 = (posx-x0)*(r1-r0)/(x1-x0)+r0
            alphaz(i,j,k) = (The_Object(ibox)%center(2) + sqrt(rr2**2-(posy-The_Object(ibox)%center(1))**2) - posz)/delta(2)
          endif

          if ( (Inzone(i,j,k+1)==0) .AND.  (Inzone(i,j,k-1)==ibox) ) then
            posx = i*delta(0)
            posy = j*delta(1)
            posz = k*delta(2)
            rr2 = (posx-x0)*(r1-r0)/(x1-x0)+r0
            alphaz(i,j,k) = (The_Object(ibox)%center(2) - sqrt(rr2**2-(posy-The_Object(ibox)%center(1))**2) - posz)/delta(2)
          endif

        endif
			enddo
		enddo	
	enddo

end subroutine Test_Conus_Nei_2








subroutine Particles_Loss(l)
  use Parallel
  implicit none

  integer, intent(in):: l

  lost=0
  if(P(l)%X .LE. xi0) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=0
      LLoss(ll)=l
      LCounter(0)=LCounter(0)+1
      ! 			write(6,*) rank,l,LCounter(0),P(l).X
    endif
  endif
  
  if(P(l)%X > xi1+delta(0)) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=1
      LLoss(ll)=l
      LCounter(1)=LCounter(1)+1
    endif
  endif

  if(P(l)%Y .LE. yj0) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=2
      LLoss(ll)=l
      LCounter(2)=LCounter(2)+1
    endif
  endif

  if(P(l)%Y > yj1+delta(1)) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=3
      LLoss(ll)=l
      LCounter(3)=LCounter(3)+1
    endif
  endif

  if(P(l)%Z .LE. zk0) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=4
      LLoss(ll)=l
      LCounter(4)=LCounter(4)+1
    endif
  endif

  if(P(l)%Z > zk1+delta(2)) then
  !if(iter==29 .AND. rank==15) write(6,*) "z111",P(l).Z,P(l).IdentPart
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=5
      LLoss(ll)=l
      LCounter(5)=LCounter(5)+1
    endif
  endif
  
end subroutine Particles_Loss

subroutine Particles_Loss_trajectories(l)
  use Parallel
  implicit none

  integer, intent(in):: l

  lost=0
  if(P_trajectory(l)%X .LE. xi0) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=0
      LLoss(ll)=l
      LCounter(0)=LCounter(0)+1
      ! 			write(6,*) rank,l,LCounter(0),P(l).X
    endif
  endif
  
  if(P_trajectory(l)%X > xi1+delta(0)) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=1
      LLoss(ll)=l
      LCounter(1)=LCounter(1)+1
    endif
  endif

  if(P_trajectory(l)%Y .LE. yj0) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=2
      LLoss(ll)=l
      LCounter(2)=LCounter(2)+1
    endif
  endif

  if(P_trajectory(l)%Y > yj1+delta(1)) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=3
      LLoss(ll)=l
      LCounter(3)=LCounter(3)+1
    endif
  endif

  if(P_trajectory(l)%Z .LE. zk0) then
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=4
      LLoss(ll)=l
      LCounter(4)=LCounter(4)+1
    endif
  endif

  if(P_trajectory(l)%Z > zk1+delta(2)) then
  !if(iter==29 .AND. rank==15) write(6,*) "z111",P(l).Z,P(l).IdentPart
    lost=lost+1
    if(lost<2) then
      ll=ll+1
      Flag(ll)=5
      LLoss(ll)=l
      LCounter(5)=LCounter(5)+1
    endif
  endif
  
end subroutine Particles_Loss_trajectories


subroutine Particle_Exchange()
	use Parallel
	implicit none
	include 'mpif.h'
			

	llos=ll
	
	LCounter1=0
	LCounter2(0)=1
	LCounter2(1)=LCounter2(0)+LCounter(0)
	LCounter2(2)=LCounter2(1)+LCounter(1)
	LCounter2(3)=LCounter2(2)+LCounter(2)
	LCounter2(4)=LCounter2(3)+LCounter(3)
	LCounter2(5)=LCounter2(4)+LCounter(4)

	do ll=1,llos
	
 		PLoss(LCounter2(Flag(ll))+LCounter1(Flag(ll)))=P(LLoss(ll))
 		LCounter1(Flag(ll))=LCounter1(Flag(ll))+1
		
 	 enddo


	if(mod(indexx,2)==0) then

!if(rank==15 .AND. iter==29) write(6,*)"RRR1111"

	!---------------------------X direction-------------------------------------------------------------
		if(neig(0)>-1) CALL MPI_SEND(LCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(1)>-1) CALL MPI_RECV(GCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(1)>-1) CALL MPI_SEND(LCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, info)
		
		if(neig(0)>-1) CALL MPI_RECV(GCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, status, info)
	
	else

!if(rank==15 .AND. iter==29) write(6,*)"RRR2222"

		if(neig(1)>-1) CALL MPI_RECV(GCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(0)>-1) CALL MPI_SEND(LCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(GCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, status, info)
	
		if(neig(1)>-1) CALL MPI_SEND(LCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexy,2)==0) then

!if(rank==15 .AND. iter==29) write(6,*)"RRR33333"
	!---------------------------Y direction-------------------------------------------------------------
		if(neig(2)>-1) CALL MPI_SEND(LCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, info)
	
		if(neig(3)>-1) CALL MPI_RECV(GCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(LCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(GCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, status, info)
	
	else
!if(rank==15 .AND. iter==29) write(6,*)"RR44444"
		if(neig(3)>-1) CALL MPI_RECV(GCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(2)>-1) CALL MPI_SEND(LCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(GCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, status, info)
!	
		if(neig(3)>-1) CALL MPI_SEND(LCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexz,2)==0) then

	!---------------------------Z direction-------------------------------------------------------------
		if(neig(4)>-1) CALL MPI_SEND(LCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(5)>-1) CALL MPI_RECV(GCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, status, info)
	
		if(neig(5)>-1) CALL MPI_SEND(LCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(GCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, status, info)

	else

		if(neig(5)>-1) CALL MPI_RECV(GCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, status, info)

		if(neig(4)>-1) CALL MPI_SEND(LCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(GCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(LCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, info)
	endif

	GCounter2=0
	GCounter2(0)=1
	GCounter2(1)=GCounter2(0)+GCounter(0)
	GCounter2(2)=GCounter2(1)+GCounter(1)
	GCounter2(3)=GCounter2(2)+GCounter(2)
	GCounter2(4)=GCounter2(3)+GCounter(3)
	GCounter2(5)=GCounter2(4)+GCounter(4)

	if(mod(indexx,2)==0) then

	!---------------------------X direction-------------------------------------------------------------
		if(neig(0)>-1) CALL MPI_SEND(PLoss(LCounter2(0)), LCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(1)>-1) CALL MPI_RECV(PGain(GCounter2(1)), GCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(1)>-1) CALL MPI_SEND(PLoss(LCounter2(1)), LCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, info)
		
		if(neig(0)>-1) CALL MPI_RECV(PGain(GCounter2(0)), GCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, status, info)
	
	else


		if(neig(1)>-1) CALL MPI_RECV(PGain(GCounter2(1)), GCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(0)>-1) CALL MPI_SEND(PLoss(LCounter2(0)), LCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(PGain(GCounter2(0)), GCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, status, info)
	
		if(neig(1)>-1) CALL MPI_SEND(PLoss(LCounter2(1)), LCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexy,2)==0) then

	!---------------------------Y direction-------------------------------------------------------------
		if(neig(2)>-1) CALL MPI_SEND(PLoss(LCounter2(2)), LCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, info)
	
		if(neig(3)>-1) CALL MPI_RECV(PGain(GCounter2(3)), GCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(PLoss(LCounter2(3)), LCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(PGain(GCounter2(2)), GCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, status, info)
	
	else

		if(neig(3)>-1) CALL MPI_RECV(PGain(GCounter2(3)), GCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(2)>-1) CALL MPI_SEND(PLoss(LCounter2(2)), LCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(PGain(GCounter2(2)), GCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, status, info)
	
		if(neig(3)>-1) CALL MPI_SEND(PLoss(LCounter2(3)), LCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexz,2)==0) then

	!---------------------------Z direction-------------------------------------------------------------
		if(neig(4)>-1) CALL MPI_SEND(PLoss(LCounter2(4)), LCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(5)>-1) CALL MPI_RECV(PGain(GCounter2(5)), GCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, status, info)
	
		if(neig(5)>-1) CALL MPI_SEND(PLoss(LCounter2(5)), LCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(PGain(GCounter2(4)), GCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, status, info)

	else

		if(neig(5)>-1) CALL MPI_RECV(PGain(GCounter2(5)), GCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, status, info)

		if(neig(4)>-1) CALL MPI_SEND(PLoss(LCounter2(4)), LCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(PGain(GCounter2(4)), GCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(PLoss(LCounter2(5)), LCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, info)
	endif


    NGain=GCounter(0)+GCounter(1)+GCounter(2)+GCounter(3)+GCounter(4)+GCounter(5)
    NLoss=LCounter(0)+LCounter(1)+LCounter(2)+LCounter(3)+LCounter(4)+LCounter(5)



! 		write(6,*) "555",rank,  GCounter

! 		write(6,*) "555",rank, Npart,NpartNew
    NpartOld=Npart
    NpartNew=Npart+NGain-NLoss


    counter=0
    
    if(NpartOld<=NpartNew) then
      
      do ll=1,NGain
      !If particle crosses two boundaries in the same time - put one coordinate = baundary OR if periodic
        
        if(PGain(ll)%X<xi0)          PGain(ll)%X=xi0
        if(PGain(ll)%X>xi1+delta(0)) PGain(ll)%X=xi1+delta(0)
        
        if(PGain(ll)%Y<yj0) then 
        !if(PGain(ll)%Y<0) then 
          if((abs(PGain(ll)%Y-yj0) > 20*delta(1) ) .OR. (periodic==1 .AND. numtasks==1)  ) then
            PGain(ll)%Y=(j1-j0+1)*NprocY*delta(1)-abs(PGain(ll)%Y)
          else
            PGain(ll)%Y=yj0
          endif
        endif 
        
        if(PGain(ll)%Y>yj1+delta(1)) then
        !if(PGain(ll)%Y>Nyp*delta(1)) then
          if((abs(PGain(ll)%Y-yj1) > 20*delta(1) ) .OR. (periodic==1 .AND. numtasks==1)  ) then
            PGain(ll)%Y=PGain(ll)%Y-(j1-j0+1)*NprocY*delta(1)
          else
            PGain(ll)%Y=yj1+delta(1) 
          endif
        endif
        
        if(PGain(ll)%Z<zk0) then
        !if(PGain(ll)%Z<0) then
          if((abs(PGain(ll)%Z-zk0) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1) ) then
            PGain(ll)%Z=(k1-k0+1)*NprocZ*delta(2)-abs(PGain(ll)%Z)
          else
            PGain(ll)%Z=zk0
          endif
        endif
        
        if(PGain(ll)%Z>zk1+delta(2)) then
        !if(PGain(ll)%Z>Nzp*delta(2)) then
          if((abs(PGain(ll)%Z-zk1) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1) ) then
            PGain(ll)%Z=PGain(ll)%Z-(k1-k0+1)*NprocZ*delta(2)
          else
            PGain(ll)%Z=zk1+delta(2)
          endif
        endif
        
        if (ll<=NLoss) then
          if(LLoss(ll)>Npart) then
            !If particle which is lost is in the end of array and was put in the middle in the Deleting procedure				
            P( DelParticles (OutParticles-NpartBefoteDelete+LLoss(ll) ) )=PGain(ll)
          else
            P(LLoss(ll))=PGain(ll)
            !write(4600+RANK,*) iter,rank
          endif
        else
          counter=counter+1
          P(Npart+counter)=PGain(ll)
        endif
        
      enddo

    else

      counterPLoss=1
      
      do ll=NLoss,1,-1
      
        if (counterPLoss<=NGain) then
          
          if(PGain(counterPLoss)%X<xi0) PGain(counterPLoss)%X=xi0
          if(PGain(counterPLoss)%X>xi1+delta(0)) PGain(counterPLoss)%X=xi1+delta(0)
          
          if(PGain(counterPLoss)%Y<yj0) then 
            if(abs(PGain(counterPLoss)%Y-yj0) > 20*delta(1) .OR. (periodic==1 .AND. numtasks==1) ) then
              PGain(counterPLoss)%Y=(j1-j0+1)*NprocY*delta(1)-abs(PGain(counterPLoss)%Y)
            else
              PGain(counterPLoss)%Y=yj0
            endif
          endif
          
          if(PGain(counterPLoss)%Y>yj1+delta(1)) then
            if((abs(PGain(counterPLoss)%Y-yj1) > 20*delta(1)) .OR. (periodic==1 .AND. numtasks==1)  ) then
              PGain(counterPLoss)%Y=PGain(counterPLoss)%Y-(j1-j0+1)*NprocY*delta(1)
            else



              PGain(counterPLoss)%Y=yj1+delta(1)
            endif
          endif
          
          if(PGain(counterPLoss)%Z<zk0) then
            if((abs(PGain(counterPLoss)%Z-zk0) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1)  ) then
              PGain(counterPLoss)%Z=(k1-k0+1)*NprocZ*delta(2)-abs(PGain(counterPLoss)%Z)
            else
              PGain(counterPLoss)%Z=zk0
            endif
          endif
          
          if(PGain(counterPLoss)%Z>zk1+delta(2)) then
            if((abs(PGain(counterPLoss)%Z-zk1) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1) ) then
              PGain(counterPLoss)%Z=PGain(counterPLoss)%Z-(k1-k0+1)*NprocZ*delta(2)
            else
              PGain(counterPLoss)%Z=zk1+delta(2) 
            endif
          endif
          
          if(LLoss(ll)>Npart) then
            !In new code version- never come here, may be improved, but works OK		
            P( DelParticles (OutParticles-NpartBefoteDelete+LLoss(ll) ) )=PGain(counterPLoss)
          else
            P(LLoss(ll))=PGain(counterPLoss)
          endif
          
          counterPLoss=counterPLoss+1
        
        else
          
          if(LLoss(ll)>Npart) then
            !In new code version- never come here, may be improved, but works OK
            P( DelParticles (OutParticles-NpartBefoteDelete+LLoss(ll) ) )=PGain(Npart-counter)
          else
            P(LLoss(ll))=P(Npart-counter)
            !If particle which is lost is in the end of array and was put in the middle in the Deleting procedure	
            do i_loop1=OutParticles,1,-1
              if(DelParticles(i_loop1)==Npart-counter) then
                DelParticles(i_loop1)=LLoss(ll)
              endif
            enddo
          endif
          counter=counter+1
        
        endif
        
      enddo
    
    endif
    
    Npart=NpartNew

end subroutine Particle_Exchange

subroutine Particle_Exchange_trajectories()
	use Parallel
	implicit none
	include 'mpif.h'
			

	llos=ll
	
	LCounter1=0
	LCounter2(0)=1
	LCounter2(1)=LCounter2(0)+LCounter(0)
	LCounter2(2)=LCounter2(1)+LCounter(1)
	LCounter2(3)=LCounter2(2)+LCounter(2)
	LCounter2(4)=LCounter2(3)+LCounter(3)
	LCounter2(5)=LCounter2(4)+LCounter(4)

	do ll=1,llos
	
 		PLoss(LCounter2(Flag(ll))+LCounter1(Flag(ll)))=P_trajectory(LLoss(ll))
 		LCounter1(Flag(ll))=LCounter1(Flag(ll))+1
		
 	 enddo


	if(mod(indexx,2)==0) then

!if(rank==15 .AND. iter==29) write(6,*)"RRR1111"

	!---------------------------X direction-------------------------------------------------------------
		if(neig(0)>-1) CALL MPI_SEND(LCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(1)>-1) CALL MPI_RECV(GCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(1)>-1) CALL MPI_SEND(LCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, info)
		
		if(neig(0)>-1) CALL MPI_RECV(GCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, status, info)
	
	else

!if(rank==15 .AND. iter==29) write(6,*)"RRR2222"

		if(neig(1)>-1) CALL MPI_RECV(GCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(0)>-1) CALL MPI_SEND(LCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(GCounter(0), 1,MPI_INTEGER, neig(0),51, MPI_COMM_WORLD, status, info)
	
		if(neig(1)>-1) CALL MPI_SEND(LCounter(1), 1,MPI_INTEGER, neig(1),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexy,2)==0) then

!if(rank==15 .AND. iter==29) write(6,*)"RRR33333"
	!---------------------------Y direction-------------------------------------------------------------
		if(neig(2)>-1) CALL MPI_SEND(LCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, info)
	
		if(neig(3)>-1) CALL MPI_RECV(GCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(LCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(GCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, status, info)
	
	else
!if(rank==15 .AND. iter==29) write(6,*)"RR44444"
		if(neig(3)>-1) CALL MPI_RECV(GCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(2)>-1) CALL MPI_SEND(LCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(GCounter(2), 1,MPI_INTEGER, neig(2),51, MPI_COMM_WORLD, status, info)
!	
		if(neig(3)>-1) CALL MPI_SEND(LCounter(3), 1,MPI_INTEGER, neig(3),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexz,2)==0) then

	!---------------------------Z direction-------------------------------------------------------------
		if(neig(4)>-1) CALL MPI_SEND(LCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(5)>-1) CALL MPI_RECV(GCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, status, info)
	
		if(neig(5)>-1) CALL MPI_SEND(LCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(GCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, status, info)

	else

		if(neig(5)>-1) CALL MPI_RECV(GCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, status, info)

		if(neig(4)>-1) CALL MPI_SEND(LCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(GCounter(4), 1,MPI_INTEGER, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(LCounter(5), 1,MPI_INTEGER, neig(5),51, MPI_COMM_WORLD, info)
	endif

	GCounter2=0
	GCounter2(0)=1
	GCounter2(1)=GCounter2(0)+GCounter(0)
	GCounter2(2)=GCounter2(1)+GCounter(1)
	GCounter2(3)=GCounter2(2)+GCounter(2)
	GCounter2(4)=GCounter2(3)+GCounter(3)
	GCounter2(5)=GCounter2(4)+GCounter(4)

	if(mod(indexx,2)==0) then

	!---------------------------X direction-------------------------------------------------------------
		if(neig(0)>-1) CALL MPI_SEND(PLoss(LCounter2(0)), LCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(1)>-1) CALL MPI_RECV(PGain(GCounter2(1)), GCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(1)>-1) CALL MPI_SEND(PLoss(LCounter2(1)), LCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, info)
		
		if(neig(0)>-1) CALL MPI_RECV(PGain(GCounter2(0)), GCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, status, info)
	
	else


		if(neig(1)>-1) CALL MPI_RECV(PGain(GCounter2(1)), GCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, status, info)
		
		if(neig(0)>-1) CALL MPI_SEND(PLoss(LCounter2(0)), LCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(PGain(GCounter2(0)), GCounter(0)*8,MPI_REAL, neig(0),51, MPI_COMM_WORLD, status, info)
	
		if(neig(1)>-1) CALL MPI_SEND(PLoss(LCounter2(1)), LCounter(1)*8,MPI_REAL, neig(1),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexy,2)==0) then

	!---------------------------Y direction-------------------------------------------------------------
		if(neig(2)>-1) CALL MPI_SEND(PLoss(LCounter2(2)), LCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, info)
	
		if(neig(3)>-1) CALL MPI_RECV(PGain(GCounter2(3)), GCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(PLoss(LCounter2(3)), LCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(PGain(GCounter2(2)), GCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, status, info)
	
	else

		if(neig(3)>-1) CALL MPI_RECV(PGain(GCounter2(3)), GCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, status, info)

		if(neig(2)>-1) CALL MPI_SEND(PLoss(LCounter2(2)), LCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(PGain(GCounter2(2)), GCounter(2)*8,MPI_REAL, neig(2),51, MPI_COMM_WORLD, status, info)
	
		if(neig(3)>-1) CALL MPI_SEND(PLoss(LCounter2(3)), LCounter(3)*8,MPI_REAL, neig(3),51, MPI_COMM_WORLD, info)
	endif


	if(mod(indexz,2)==0) then

	!---------------------------Z direction-------------------------------------------------------------
		if(neig(4)>-1) CALL MPI_SEND(PLoss(LCounter2(4)), LCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(5)>-1) CALL MPI_RECV(PGain(GCounter2(5)), GCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, status, info)
	
		if(neig(5)>-1) CALL MPI_SEND(PLoss(LCounter2(5)), LCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(PGain(GCounter2(4)), GCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, status, info)

	else

		if(neig(5)>-1) CALL MPI_RECV(PGain(GCounter2(5)), GCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, status, info)

		if(neig(4)>-1) CALL MPI_SEND(PLoss(LCounter2(4)), LCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, info)

		if(neig(4)>-1) CALL MPI_RECV(PGain(GCounter2(4)), GCounter(4)*8,MPI_REAL, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(PLoss(LCounter2(5)), LCounter(5)*8,MPI_REAL, neig(5),51, MPI_COMM_WORLD, info)
	endif


    NGain=GCounter(0)+GCounter(1)+GCounter(2)+GCounter(3)+GCounter(4)+GCounter(5)
    NLoss=LCounter(0)+LCounter(1)+LCounter(2)+LCounter(3)+LCounter(4)+LCounter(5)



! 		write(6,*) "555",rank,  GCounter

! 		write(6,*) "555",rank, Npart,NpartNew
    NpartOld=N_p_t
    NpartNew=N_p_t+NGain-NLoss


    counter=0
    
    if(NpartOld<=NpartNew) then
      
      do ll=1,NGain
      !If particle crosses two boundaries in the same time - put one coordinate = baundary OR if periodic
        
        if(PGain(ll)%X<xi0)          PGain(ll)%X=xi0
        if(PGain(ll)%X>xi1+delta(0)) PGain(ll)%X=xi1+delta(0)
        
        if(PGain(ll)%Y<yj0) then 
        !if(PGain(ll)%Y<0) then 
          if((abs(PGain(ll)%Y-yj0) > 20*delta(1) ) .OR. (periodic==1 .AND. numtasks==1)  ) then
            PGain(ll)%Y=(j1-j0+1)*NprocY*delta(1)-abs(PGain(ll)%Y)
          else
            PGain(ll)%Y=yj0
          endif
        endif 
        
        if(PGain(ll)%Y>yj1+delta(1)) then
        !if(PGain(ll)%Y>Nyp*delta(1)) then
          if((abs(PGain(ll)%Y-yj1) > 20*delta(1) ) .OR. (periodic==1 .AND. numtasks==1)  ) then
            PGain(ll)%Y=PGain(ll)%Y-(j1-j0+1)*NprocY*delta(1)
          else
            PGain(ll)%Y=yj1+delta(1) 
          endif
        endif
        
        if(PGain(ll)%Z<zk0) then
        !if(PGain(ll)%Z<0) then
          if((abs(PGain(ll)%Z-zk0) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1) ) then
            PGain(ll)%Z=(k1-k0+1)*NprocZ*delta(2)-abs(PGain(ll)%Z)
          else
            PGain(ll)%Z=zk0
          endif
        endif
        
        if(PGain(ll)%Z>zk1+delta(2)) then
        !if(PGain(ll)%Z>Nzp*delta(2)) then
          if((abs(PGain(ll)%Z-zk1) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1) ) then
            PGain(ll)%Z=PGain(ll)%Z-(k1-k0+1)*NprocZ*delta(2)
          else
            PGain(ll)%Z=zk1+delta(2)
          endif
        endif
        
        if (ll<=NLoss) then
          if(LLoss(ll)>N_p_t) then
            !If particle which is lost is in the end of array and was put in the middle in the Deleting procedure				
            P_trajectory( DelParticles (OutParticles-NpartBefoteDelete+LLoss(ll) ) )=PGain(ll)
          else
            P_trajectory(LLoss(ll))=PGain(ll)
            !write(4600+RANK,*) iter,rank
          endif
        else
          counter=counter+1
          P_trajectory(N_p_t+counter)=PGain(ll)
        endif
        
      enddo

    else

      counterPLoss=1
      
      do ll=NLoss,1,-1
      
        if (counterPLoss<=NGain) then
          
          if(PGain(counterPLoss)%X<xi0) PGain(counterPLoss)%X=xi0
          if(PGain(counterPLoss)%X>xi1+delta(0)) PGain(counterPLoss)%X=xi1+delta(0)
          
          if(PGain(counterPLoss)%Y<yj0) then 
            if(abs(PGain(counterPLoss)%Y-yj0) > 20*delta(1) .OR. (periodic==1 .AND. numtasks==1) ) then
              PGain(counterPLoss)%Y=(j1-j0+1)*NprocY*delta(1)-abs(PGain(counterPLoss)%Y)
            else
              PGain(counterPLoss)%Y=yj0
            endif
          endif
          
          if(PGain(counterPLoss)%Y>yj1+delta(1)) then
            if((abs(PGain(counterPLoss)%Y-yj1) > 20*delta(1)) .OR. (periodic==1 .AND. numtasks==1)  ) then
              PGain(counterPLoss)%Y=PGain(counterPLoss)%Y-(j1-j0+1)*NprocY*delta(1)
            else



              PGain(counterPLoss)%Y=yj1+delta(1)
            endif
          endif
          
          if(PGain(counterPLoss)%Z<zk0) then
            if((abs(PGain(counterPLoss)%Z-zk0) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1)  ) then
              PGain(counterPLoss)%Z=(k1-k0+1)*NprocZ*delta(2)-abs(PGain(counterPLoss)%Z)
            else
              PGain(counterPLoss)%Z=zk0
            endif
          endif
          
          if(PGain(counterPLoss)%Z>zk1+delta(2)) then
            if((abs(PGain(counterPLoss)%Z-zk1) > 20*delta(2)) .OR. (periodic==1 .AND. numtasks==1) ) then
              PGain(counterPLoss)%Z=PGain(counterPLoss)%Z-(k1-k0+1)*NprocZ*delta(2)
            else
              PGain(counterPLoss)%Z=zk1+delta(2) 
            endif
          endif
          
          if(LLoss(ll)>N_p_t) then
            !In new code version- never come here, may be improved, but works OK		
            P_trajectory( DelParticles (OutParticles-NpartBefoteDelete+LLoss(ll) ) )=PGain(counterPLoss)
          else
            P_trajectory(LLoss(ll))=PGain(counterPLoss)
          endif
          

          counterPLoss=counterPLoss+1
        
        else
          
          if(LLoss(ll)>N_p_t) then
            !In new code version- never come here, may be improved, but works OK
            P_trajectory( DelParticles (OutParticles-NpartBefoteDelete+LLoss(ll) ) )=PGain(N_p_t-counter)
          else
            P_trajectory(LLoss(ll))=P_trajectory(N_p_t-counter)
            !If particle which is lost is in the end of array and was put in the middle in the Deleting procedure	
            do i_loop1=OutParticles,1,-1
              if(DelParticles(i_loop1)==N_p_t-counter) then
                DelParticles(i_loop1)=LLoss(ll)
              endif
            enddo
          endif
          counter=counter+1
        endif
        
      enddo
    
    endif
    
    N_p_t=NpartNew

end subroutine Particle_Exchange_trajectories


subroutine Initialization()

  use Parallel
  implicit none
  include 'mpif.h'

  integer i, j, k
  integer file_status

  !Boundaries of each box
  i0=indexes(0)
  i1=indexes(1)
  j0=indexes(2)
  j1=indexes(3)
  k0=indexes(4)
  k1=indexes(5)

  !Allocation memory of all arrays
  allocate( V(i0-1:i1+1    , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( Exm(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( Eym(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( Ezm(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
    
  allocate( Bxm(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( Bym(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( Bzm(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( ReadB(1:453690 , 1:7 ) )

  allocate( V_EG( j0-1:j1+1, k0-1:k1+1 ) )	
    
  allocate( Ad(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( r(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( z(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( d(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )

  allocate( rho(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( rho2(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( rho_Sec(i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ) )
    
  allocate( Num_part_Coul_cell(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( Coulomb_cell_list(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1, 1:Np_Coul_cell ) )

  allocate(Mom_e_x(i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ))    
  allocate(Mom_e_y(i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ))	
  allocate(Mom_e_z(i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ))

  allocate( output_plane_density(0:11, 0:Nxp, 0:Nyp) )
  allocate( output_plane_density_average(0:11, 0:Nxp, 0:Nyp) )
  allocate( density_average(0:11, i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ) )
  

  allocate( output_plane_momx_xy(0:11, 0:Nxp, 0:Nyp) )
  allocate( output_plane_momx_xy_average(0:11, 0:Nxp, 0:Nyp) )
  allocate( momx_xy(0:11, i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ) )

  allocate( output_plane_momy_xy(0:11, 0:Nxp, 0:Nyp) )
  allocate( output_plane_momy_xy_average(0:11, 0:Nxp, 0:Nyp) )
  allocate( momy_xy(0:11, i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ) )

  allocate( output_plane_momx_xz(0:11, 0:Nxp, 0:Nzp) )

  allocate( output_plane_momx_xz_average(0:11, 0:Nxp, 0:Nzp) )
  allocate( momx_xz(0:11, i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ) )

  allocate( output_plane_momz_xz(0:11, 0:Nxp, 0:Nzp) )
  allocate( output_plane_momz_xz_average(0:11, 0:Nxp, 0:Nzp) )
  allocate( momz_xz(0:11, i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3 ) )

  allocate( output_plane_density_xz(0:11, 0:Nxp, 0:Nzp) )
  allocate( output_plane_density_average_xz(0:11, 0:Nxp, 0:Nzp) )

  allocate( output_plane_potential(0:3, 0:Nxp, 0:Nyp) )
  allocate( output_plane_potential_average(0:3, 0:Nxp, 0:Nyp) )
  allocate( potential_average(0:3, i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  
  allocate( output_plane_potential_xz(0:3, 0:Nxp, 0:Nzp) )
  allocate( output_plane_potential_average_xz(0:3, 0:Nxp, 0:Nzp) )

  allocate( emittance_arr(0:11,0:1,0:iN_emit,0:iN_emit) )
  allocate( current_den(0:11,0:1000,0:1000) )
  allocate( output_plane_potential_yz(0:Nyp,0:Nzp) )

  allocate(Coll_e(  i0:i1,j0:j1,k0:k1 )) 
  allocate(Coll_H2p(i0:i1,j0:j1,k0:k1 ))
  allocate(Coll_Hm( i0:i1,j0:j1,k0:k1 ))
  allocate(Coll_Hp( i0:i1,j0:j1,k0:k1 ))

  allocate( Add_partic_coll(1:100000) )

  allocate (Dens_e(  i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1  ))
  allocate (Dens_H2p(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1  ))
  allocate (Dens_Hm( i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1  ))
  allocate (Dens_Hp( i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1  ))
  allocate (Dens_HnPG(  i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1  )) 		

  allocate (Delete_e_pos(   10000) )
  allocate (Delete_Hm_pos(  10000) )
  allocate (Delete_Hp_pos(  10000) )
  allocate (Delete_Hp_pos_tar(  i0-1:i1+1,j0-1:j1+1,k0-1:k1+1 ) )
  allocate (Delete_Hm_pos_tar( i0-1:i1+1,j0-1:j1+1,k0-1:k1+1) )
  !allocate (Add_e_pos(  10000, 4  ) )	 
  !allocate (Add_H_pos(  10000, 4  ) )	 



  !allocate (Dens_e( 1:1  , 1:+1, 1:1  ) )	 
  !allocate (Dens_H2p(1:1  , 1:+1, 1:1  )	)
  !allocate (Dens_Hm( 1:1  , 1:+1, 1:1  )	)


  allocate( Inzone(i0-2:i1+2  , j0-2:j1+2, k0-2:k1+2 ) )
  allocate( Inzone2(i0-2:i1+2  , j0-2:j1+2, k0-2:k1+2 ) )
  allocate( ZoneNeig(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( alphax(i0:i1 , j0:j1, k0:k1))
  allocate( alphay(i0:i1 , j0:j1, k0:k1))
  allocate( alphaz(i0:i1 , j0:j1, k0:k1))
  allocate( C(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1 ) )
  allocate( P(1:NPART+NPART) )
  allocate( P_trajectory(1:N_trajectory))
  allocate( P_wall(0:100))
  allocate( PLoss(1:NPARTL) )
  allocate( PGain(1:NPARTL) )


  allocate( PLoss_followed(1:2*numtasks) )
  allocate( PGain_followed(1:2*numtasks) )	

  allocate( Flag(1:NPARTL) )
  allocate( LLoss(1:NPARTL) )
  allocate( DelParticles(1:NPARTL) )
  allocate( DelParticles_sort(1:NPARTL) )
  allocate( Arr_new_sort(1:NPARTL) )
  !allocate( Arr_to_sort(1:NPARTL) )
  allocate( AddParticles(1:NPARTL) )
  allocate( AddWallParticles(1:NPARTL) )
  allocate( mass(0:11) , charge(0:11), weight(0:11) , temperature(0:12), density0(0:11))
  
  allocate(px_arr(1:NPART+NPART))
  allocate(py_arr(1:NPART+NPART))
  allocate(pz_arr(1:NPART+NPART))
  allocate(pvx_arr(1:NPART+NPART))
  allocate(pvy_arr(1:NPART+NPART))
  allocate(pvz_arr(1:NPART+NPART))
  allocate(ex_arr(1:NPART+NPART))
  allocate(ey_arr(1:NPART+NPART))
  allocate(ez_arr(1:NPART+NPART))
  allocate(bx_arr(1:NPART+NPART))
  allocate(by_arr(1:NPART+NPART))
  allocate(bz_arr(1:NPART+NPART))

  allocate(sumNpart(1:12))
  allocate(sumNpartMPI(1:12))

  !Initialization
  V=0.0
  Exm=0.0
  Eym=0.0
  Ezm=0.0
    
    
  Bxm=0.0
  Bym=0.0
  Bzm=0.0
  ReadB=0.0
 
  V_EG=0.0 
   
  d=0.0
  Ad=0.0
  r=0.0
  z=0.0
  rho=0.0
  rho2=0.0

  Inzone=0.0
  ZoneNeig=0.0
  Inzone=0
  C=1
  Ener_e_threshold=1500
  P_wall(:)%X_wall=0.0189
  P_wall(:)%Y_wall=0.0195
  P_wall(:)%Z_wall=0.0195

  i_loop1=0


  DelParticles=0

  !Parameters of the shape (box and cylinder)
  nbox=2
  ncyl=1

  !--------------------------------------  Number of particles move out from the volume (in order to reinject new one)
  Out_e=0
  Out_PI_Hp=0
  Out_PI_H2p=0
  Out_PI_H3p=0


  Out_NI=0
  !--------------------------------------------------------------------------------------------------

! !----------e Electron
!   mass(0)=9.1e-31
!   charge(0)=-1.6e-19
!   weight(0)=5e4

! !----------H3 plus
!   mass(10)=3.0*1.674e-27
!   charge(10)=1.6e-19
!   weight(10)=5e4

! !----------H2 plus
!   mass(1)=2.0*1.674e-27
!   charge(1)=1.6e-19
!   weight(1)=5e4

! !----------H plus
!   mass(3)=1.674e-27
!   charge(3)=1.6e-19
!   weight(3)=5e4

! !----------H minus
!   mass(2)=1.674e-27
!   charge(2)=-1.6e-19
!   weight(2)=5e4 !n=10^17  m-3

! !----------H minus from PG wall by PI

!   mass(4)=1.674e-27
!   charge(4)=-1.6e-19
!   weight(4)=1e3 !n=10^17  m-3

! !----------H minus from PG cone wall by neutrals
!   mass(5)=1.674e-27
!   charge(5)=-1.6e-19

!   weight(5)=1e4

! !----------H minus from PG wall by neutrals
!   mass(9)=1.674e-27
!   charge(9)=-1.6e-19
!   weight(9)=1e4

! !----------H minus from chamber
!   mass(8)=1.674e-27
!   charge(8)=-1.6e-19
!   weight(8)=5e4 !n=10^17  m-3

! !----------H  
!   mass(6)=1.674e-27
!   charge(6)=0
!   weight(6)=1e7 !n=10^19  m-3

! !----------H2 
!   mass(7)=2.0*1.674e-27
!   charge(7)=0
!   weight(7)=1e7 !n=10^19  m-3
!   
!   
!   !----------Cs+ plus
!   mass(11)=133.0*1.674e-27
!   charge(11)=1.6e-19
!   weight(11)=5e4


!   temperature(0)=11600.0*1.5
!   temperature(1)=11600.0*0.1
!   temperature(3)=11600.0*0.8
!   temperature(10)=11600.0*0.1
!   temperature(2)=11600.0*1.0
!   temperature(4)=11600.0
!   temperature(5)=11600.0*1.0
!   temperature(6)=11600.0*0.1
!   temperature(7)=11600.0*0.8
!   temperature(8)=11600.0
!   temperature(9)=11600.0*1.0
!   temperature(11)=11600.0*0.1
  
  open( unit=10001, file="Particles.in", status="old", iostat= file_status)
  
  if(file_status==0) then  
    do i=0, 11
      read( unit=10001, fmt=* ) mass(i), charge(i), weight(i), temperature(i), density0(i)
    end do
  
    close(unit=10001)
  
  else
    print *, "Unable to open file : Particles.in"
  endif
  temperature = 11600*temperature
  
  !weight(:) = weight(:)/30.

  Npart=1


  allocate(PToAdd(1:100000))
  NPartInPToAdd = 0


  !=====================================================================================Small with conus
  write(*,*) "Proc number: ", rank, " Object Initialization ..."
  alphax=1.
  alphay=1.
  alphaz=1.

  open( unit=10000, file="Geometry.in", status="old", iostat= file_status)
  
  if(file_status==0) then
    
    read( unit=10000, fmt=* ) Nb_Obj
    allocate(The_Object(Nb_Obj))

    do i=1, Nb_Obj
      read( unit=10000, fmt=* ) The_Object(i)%center(0), The_Object(i)%center(1), The_Object(i)%center(2),&
        The_Object(i)%width(0), The_Object(i)%width(1), The_Object(i)%width(2), &
        The_Object(i)%types, The_Object(i)%ReflectionType, The_Object(i)%Potential
    end do
  
    close(unit=10000)
  
  else
    print *, "Unable to open file : Geometry.in"
  endif


!          Nb_Obj = 4
!        allocate(The_Object(Nb_Obj))
!
!	The_Object(1)%center(0)=0.00044
! 	The_Object(1)%center(1)=0.007
! 	The_Object(1)%center(2)=0.007
! 	The_Object(1)%width(0)= 0.00045
! 	The_Object(1)%width(1)=0.008  !If periodic , boundaries in Y and Z directions will be automaticely set to j1 and k1
! 	The_Object(1)%width(2)=0.008
!	The_Object(1)%types=1
!  The_Object(1)%ReflectionType = 1
!	The_Object(1)%Potential=0
!  
!  The_Object(2)%center(0)=0.0201
!  The_Object(2)%center(1)=0.007
!  The_Object(2)%center(2)=0.007
!  The_Object(2)%width(0)=0.001
!  !The_Object(2)%width(1)=0.007
!  The_Object(2)%width(1)=0.006
!  The_Object(2)%width(2)=0.004
!  The_Object(2)%types=3
!  The_Object(2)%ReflectionType = 3
!  The_Object(2)%Potential=0
!  
!  The_Object(3)%center(0)=0.0221
!  The_Object(3)%center(1)=0.007
!  The_Object(3)%center(2)=0.007
!  The_Object(3)%width(0)=0.00101
!  The_Object(3)%width(1)=0.004
!  The_Object(3)%width(2)=0.006
!  The_Object(3)%types=3
!  The_Object(3)%ReflectionType = 3
!  The_Object(3)%Potential=0
!
!	The_Object(4)%center(0)=0.028
! 	The_Object(4)%center(1)=0.007
! 	The_Object(4)%center(2)=0.007
! 	The_Object(4)%width(0)= 0.001151
! 	The_Object(4)%width(1)=0.008  !If periodic , boundaries in Y and Z directions will be automaticely set to j1 and k1
! 	The_Object(4)%width(2)=0.008
!	The_Object(4)%types=1
!  The_Object(4)%ReflectionType = 2
!	The_Object(4)%Potential=10000
!  
  do i=1, Nb_Obj
    if(The_Object(i)%types==1) then
      call Test_Box_Inzone(i)
    end if
    if(The_Object(i)%types==2) then
      call Test_Cylinder_Inzone(i)
    end if
    if(The_Object(i)%types==3) then
      call Test_Conus_Inzone(i)
    end if
  end do
  
  call Send2_Plane(Inzone)

  Inzone2 = 0
  do k=k0-1, k1+1
    do j=j0-1, j1+1
      do i=i0-1, i1+1
        if(Inzone(i,j,k)==0) then 
          if(Inzone(i+1,j,k) /= 0 .OR. Inzone(i-1,j,k) /= 0 .OR. &
             Inzone(i,j+1,k) /= 0 .OR. Inzone(i,j-1,k) /= 0 .OR. &
             Inzone(i,j,k+1) /= 0 .OR. Inzone(i,j,k-1) /= 0) then
            Inzone2(i,j,k)=1
          end if
        else
          Inzone2(i,j,k)=-1
        end if
      end do
    end do
  end do
  
  do i=1, Nb_Obj
    if(The_Object(i)%types==1) then
      call Test_Box_Nei(i)
    end if
    if(The_Object(i)%types==2) then
      call Test_Cylinder_Nei(i)
    end if
    if(The_Object(i)%types==3) then
      call Test_Conus_Nei_2(i)
    end if
  end do

  !call Send2_Plane(alphax)
  !call Send2_Plane(alphay)
  !call Send2_Plane(alphaz)
 
  call Check_Alpha_Coefficient()

  allocate( coefficentLaplacianF(i0:i1 , j0:j1, k0:k1) )
  allocate( coefficentLaplacianB(i0:i1 , j0:j1, k0:k1) )
  allocate( coefficentLaplacianR(i0:i1 , j0:j1, k0:k1) )
  allocate( coefficentLaplacianL(i0:i1 , j0:j1, k0:k1) )
  allocate( coefficentLaplacianU(i0:i1 , j0:j1, k0:k1) )
  allocate( coefficentLaplacianD(i0:i1 , j0:j1, k0:k1) )
  allocate( coefficentLaplacianM(i0:i1 , j0:j1, k0:k1) )

  coefficentLaplacianF = 1.
  coefficentLaplacianB = 1.
  coefficentLaplacianR = 1.
  coefficentLaplacianL = 1.
  coefficentLaplacianU = 1.
  coefficentLaplacianD = 1.
  coefficentLaplacianM = 1.
  
  call Define_Laplacian_Coefficient()
  
  !call Send2_Plane(coefficentLaplacianF)
  !call Send2_Plane(coefficentLaplacianB)
  !call Send2_Plane(coefficentLaplacianR)
  !call Send2_Plane(coefficentLaplacianL)
  !call Send2_Plane(coefficentLaplacianU)
  !call Send2_Plane(coefficentLaplacianD)
  !call Send2_Plane(coefficentLaplacianM)
   
IF(4==3) THEN
    call Print_domain()
ENDIF
  
  call MPI_Barrier(MPI_COMM_WORLD, info)
  write(*,*) "Proc number: ", rank, " Object Initialization done"
  call MPI_Barrier(MPI_COMM_WORLD, info)

! !-----First Cone normal-------------------------------------------
! 	The_Object(4)%center(0)=0.0201
!  	The_Object(4)%center(1)=0.007
!  	The_Object(4)%center(2)=0.007
!  	The_Object(4)%width(0)=0.001
! 	!The_Object(4)%width(1)=0.007
!  	The_Object(4)%width(1)=0.006
! 	The_Object(4)%width(2)=0.004
! 	The_Object(4)%types=3
! !-----------------------------------------------------------
! 
! !-----Second Cone normal-------------------------------------------
! 	The_Object(5)%center(0)=0.0221
!  	The_Object(5)%center(1)=0.007
!  	The_Object(5)%center(2)=0.007
!  	The_Object(5)%width(0)=0.00101
!  	The_Object(5)%width(1)=0.004
! 	The_Object(5)%width(2)=0.006
! 	The_Object(5)%types=3
! !-----------------------------------------------------------
! 
! !-----------------------------------------------------------
! 
! 
!  	The_Object(3)%center(0)=0.02485
!  	The_Object(3)%center(1)=0.007




!  	The_Object(3)%center(2)=0.007
!  	The_Object(3)%width(0)=0.00175
!  	The_Object(3)%width(1)=0.007
!  	The_Object(3)%width(2)=0.007
! 	The_Object(3)%types=1

  

!=====================================================================================

  !Initial boundaries correction arrays


  inside=2
  
  !Creation of new type vector for sending neighbour plane from one box to another
 
  nx=i1+1-(i0-1)+1
  ny=j1+1-(j0-1)+1
  nz=k1+1-(k0-1)+1
    
  nx2=i1+2-(i0-2)+1
  ny2=j1+2-(j0-2)+1
  nz2=k1+2-(k0-2)+1

  nx3=i1+3-(i0-3)+1
  ny3=j1+3-(j0-3)+1
  nz3=k1+3-(k0-3)+1
    
  ! P(1).IdentPart=rank*10000000
  IdentPartNumber=rank*1500000


  CALL MPI_TYPE_VECTOR(1, nx*ny, 1, MPI_REAL, xy0, info)
  CALL MPI_TYPE_COMMIT(xy0, info)
  CALL MPI_TYPE_VECTOR(nz, nx, nx*ny, MPI_REAL, xz0, info)
  CALL MPI_TYPE_COMMIT(xz0, info)
  CALL MPI_TYPE_VECTOR( nz*ny , 1, nx, MPI_REAL, yz0, info)	
  CALL MPI_TYPE_COMMIT(yz0, info)
    
  CALL MPI_TYPE_VECTOR(1, nx2*ny2, 1, MPI_REAL, xy20, info)
  CALL MPI_TYPE_COMMIT(xy20, info)
  CALL MPI_TYPE_VECTOR(nz2, nx2, nx2*ny2, MPI_REAL, xz20, info)
  CALL MPI_TYPE_COMMIT(xz20, info)
  CALL MPI_TYPE_VECTOR( nz2*ny2 , 1, nx2, MPI_REAL, yz20, info)	
  CALL MPI_TYPE_COMMIT(yz20, info)

  CALL MPI_TYPE_VECTOR(1, nx3*ny3, 1, MPI_REAL, xy30, info)
  CALL MPI_TYPE_COMMIT(xy30, info)
  CALL MPI_TYPE_VECTOR(nz3, nx3, nx3*ny3, MPI_REAL, xz30, info)
  CALL MPI_TYPE_COMMIT(xz30, info)
  CALL MPI_TYPE_VECTOR( nz3*ny3 , 1, nx3, MPI_REAL, yz30, info)	
  CALL MPI_TYPE_COMMIT(yz30, info)

  !VDF
  nPosPlaneVDF = 20
  nPosSliceVDF = 20
  allocate(posPlaneVDF(1:nPosPlaneVDF))
  allocate(posSliceVDF(1:nPosSliceVDF))
  allocate(vel_distribution_plane(1:nPosPlaneVDF,0:nPointVDF))
  allocate(vel_distribution_planeMPI(1:nPosPlaneVDF,0:nPointVDF))
  allocate(vel_distribution_slice(1:nPosSliceVDF,0:nPointVDF))
  allocate(vel_distribution_sliceMPI(1:nPosSliceVDF,0:nPointVDF))

  allocate(vel_distribution_slice_NI(0:nPosSliceVDF,0:nPointVDF_NI))
  allocate(vel_distribution_sliceMPI_NI(0:nPosSliceVDF,0:nPointVDF_NI))


  do i=1,20
    posPlaneVDF(i) = i*1e-3
    posSliceVDF(i) = i*1e-3
  end do
  
  deltaVDF = vmaxVDF*sqrt(8.*K_b*temperature(0)/acos(-1.)/mass(0))/nPointVDF
  deltaVDFNI  = 8.0*sqrt(8.*K_b*temperature(5)/acos(-1.)/mass(5))/nPointVDF_NI
  !do i=0, nPointVDF
    !vel_distribution_sliceMPI(0, i) = i*deltaVDF
    !vel_distribution_slice(0, i) = i*deltaVDF
    !vel_distribution_sliceMPI_NI(0, i) = i*deltaVDF
    !vel_distribution_slice_NI(0, i) = i*deltaVDF	

    !vel_distribution_planeMPI(0, i) = i*deltaVDF
    !vel_distribution_plane(0, i) = i*deltaVDF

    	
  !end do
  
  CALL MPI_TYPE_VECTOR(nPointVDF, 1, nPosPlaneVDF, MPI_INTEGER, sendplaneVDF, info)
  CALL MPI_TYPE_COMMIT(sendplaneVDF, info)

  CALL MPI_TYPE_VECTOR(nPointVDF, 1, nPosSliceVDF, MPI_INTEGER, sendsliceVDF, info)
  CALL MPI_TYPE_COMMIT(sendsliceVDF, info)

    CALL MPI_TYPE_VECTOR(nPointVDF_NI, 1, nPosSliceVDF, MPI_INTEGER, sendsliceVDF_NI, info)
  CALL MPI_TYPE_COMMIT(sendsliceVDF_NI, info)

end subroutine Initialization


subroutine Deallocation()
	
	use Parallel
	implicit none


	deallocate( V )
	deallocate( Exm )
	deallocate( Eym )
	deallocate( Ezm )

    deallocate( ReadB )
    deallocate( Bxm )
    deallocate( Bym )
    deallocate( Bzm )
    
        deallocate(V_EG)    

 	deallocate( Ad )
 	deallocate( r )
 	deallocate( rho )
 	deallocate( d )
 	deallocate( Inzone )
 	deallocate( ZoneNeig )
 	deallocate( alphax )
 	deallocate( alphay )
 	deallocate( alphaz )
 	deallocate( C )
 	deallocate( z )
	deallocate( status )
	deallocate( i00)
	deallocate( i11)
	deallocate( j00)
	deallocate( j11)
	deallocate( k00)
	deallocate( k11)

	deallocate( P)
	deallocate( P_trajectory)
	deallocate( FLAG)
	deallocate( mass)
	deallocate( charge)
	deallocate( weight)
	deallocate( DelParticles)
	deallocate( DelParticles_sort)
	deallocate( Arr_new_sort)
	deallocate( AddParticles)
	deallocate( AddWallParticles)
 
! 	close(90)
!        close(91)
! 	close(92)
    
    deallocate(The_Object)


end subroutine Deallocation


subroutine Initialization_MPI()

  use Parallel
  implicit none
  include 'mpif.h'

  call MPI_INIT(ierr)
  if (ierr .ne. MPI_SUCCESS) then
    print *,'Error starting MPI program. Terminating!!'
    call MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
  endif

  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierr)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, numtasks, ierr)

  if ( NprocX*NprocY*NprocZ .NE. numtasks) then
    write(*,*) 'Number of Tasks is different than in File "Proces.dat"!!!!', NprocX, NprocY, NprocZ, numtasks
    call MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
  endif

end subroutine Initialization_MPI


subroutine ChargeProjection(l)
	
	use Parallel
	implicit none


	integer, intent(in):: l

	integer :: i=0,j=0,k=0
	real :: distance(0:2)
	real :: vol=0

	i=P(l)%X/delta(0)
	j=P(l)%Y/delta(1)
	k=P(l)%Z/delta(2)

	distance(0)=1.*P(l)%X/delta(0)-i
 	distance(1)=1.*P(l)%Y/delta(1)-j
 	distance(2)=1.*P(l)%Z/delta(2)-k
 	
	vol=1.0/(delta(0)*delta(1)*delta(2))





if(i+1>i1+1 .OR. j+1>j1+1 .OR. k+1>k1+1 .OR. i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then
	!if we gain particle from the other procces that is in the border of other proces - in function Particle_Loss P().X<i0 and P().X>=i1, then we need one extra plane rho(i1+2) where is not calculation

	
	if(i==i1+1 .AND. j<j1+1 .AND. k<k1+1 ) then

		rho(i,j,k)      =rho(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
		rho(i,j+1,k)    =rho(i,j+1,k)    +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
		rho(i,j,k+1)    =rho(i,j,k+1)    +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
		rho(i,j+1,k+1)  =rho(i,j+1,k+1)  +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)

	endif

	if(j==j1+1 .AND. (i<i1+1) .AND. (k<k1+1) ) then	
		
		rho(i,j,k)      =rho(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)	
		rho(i+1,j,k)    =rho(i+1,j,k)    +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
		rho(i,j,k+1)    =rho(i,j,k+1)    +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
	
	endif


	if(k==k1+1 .AND.  i<i1+1 .AND. j<j1+1) then

		rho(i,j,k)      =rho(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
		rho(i+1,j,k)    =rho(i+1,j,k)    +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
		rho(i,j+1,k)    =rho(i,j+1,k)    +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
		rho(i+1,j+1,k)  =rho(i+1,j+1,k)  +charge(P(l)%sp)*distance(0)      * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
		
	endif


	if(i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then

			!write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
			!write(2000+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l).X,P(l).Y,P(l).Z,P(l).IdentPart,P(l).X,P(l).Y,P(l).Z
			!write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

	endif



else		

	rho(i,j,k)      =rho(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
	rho(i+1,j,k)    =rho(i+1,j,k)    +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
	rho(i,j+1,k)    =rho(i,j+1,k)    +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
	rho(i+1,j+1,k)  =rho(i+1,j+1,k)  +charge(P(l)%sp)*distance(0)      * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
	rho(i,j,k+1)    =rho(i,j,k+1)    +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
	rho(i+1,j,k+1)  =rho(i+1,j,k+1)  +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
	rho(i,j+1,k+1)  =rho(i,j+1,k+1)  +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)
	rho(i+1,j+1,k+1)=rho(i+1,j+1,k+1)+charge(P(l)%sp)*(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)

endif





end subroutine  ChargeProjection


subroutine EfieldProjection(l,ex,ey,ez)
	
	use Parallel
	implicit none

	integer, intent(in):: l
	real, intent(out):: ex
	real, intent(out):: ey
	real, intent(out):: ez

	integer :: i=0,j=0,k=0,id=0,jd=0,kd=0
	real :: distance(0:2)
	real :: distanceD(0:2)

	i=P(l)%X/delta(0)
	j=P(l)%Y/delta(1)
	k=P(l)%Z/delta(2)

!   	write(6,*) iter,i,j,k
	distance(0)=1.*P(l)%X/delta(0)-i
 	distance(1)=1.*P(l)%Y/delta(1)-j
 	distance(2)=1.*P(l)%Z/delta(2)-k

	id=P(l)%X/delta(0)-0.5
	jd=P(l)%Y/delta(1)-0.5
	kd=P(l)%Z/delta(2)-0.5
!    	write(6,*) iter,id,jd,kd

	distanceD(0)=1.*P(l)%X/delta(0)-(id+0.5)
 	distanceD(1)=1.*P(l)%Y/delta(1)-(jd+0.5)
 	distanceD(2)=1.*P(l)%Z/delta(2)-(kd+0.5)


if(i+1>i1+1 .OR. j+1>j1+1 .OR. k+1>k1+1  .OR. i<i0-1 .OR. j<j0-1 .OR. k<k0-1  .OR. id<i0-1 .OR. jd<j0-1  .OR. kd<k0-1) then

!if we gain particle from the other procces that is in the border of other proces - in function Particle_Loss P().X<i0 and P().X>=i1, then we need one extra plane Exm(i1+2) where is not calculation - just skip these points (less then 0.000001% per iteration)

	if(i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then

			write(3000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
			write(3000+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart
			write(3000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

	endif
else

!if(i<i0-1 .OR. id<i0-1) write(6,*) rank,iter,id,i,P(l).X,i0,P(l).IdentPart

	ex=0
  	ex=ex +Exm(id,  j,k)      *(1.-distanceD(0)) * (1.-distance(1)) * (1.-distance(2))
	ex=ex +Exm(id+1,j,k)      *(distanceD(0))    * (1.-distance(1)) * (1.-distance(2))
	ex=ex +Exm(id,  j+1,k)    *(1.-distanceD(0)) * (distance(1))    * (1.-distance(2))
	ex=ex +Exm(id+1,j+1,k)    *distanceD(0)      * (distance(1))    * (1.-distance(2))
	ex=ex +Exm(id,  j,k+1)    *(1.-distanceD(0)) * (1.-distance(1)) * (distance(2))
	ex=ex +Exm(id+1,j,k+1)    *(distanceD(0))    * (1.-distance(1)) * (distance(2))
	ex=ex +Exm(id,  j+1,k+1)  *(1.-distanceD(0)) * (distance(1))    * (distance(2))
	ex=ex +Exm(id+1,j+1,k+1)  *(distanceD(0))    * (distance(1))    * (distance(2))



!if(j<j0-1 .OR. jd<j0-1) write(6,*) rank,iter,jd,j,P(l).Y,P(l).IdentPart

	ey=0
	ey=ey +Eym(i,jd,k)      *(1.-distance(0)) * (1.-distanceD(1)) * (1.-distance(2))
	ey=ey +Eym(i+1,jd,k)    *(distance(0))    * (1.-distanceD(1)) * (1.-distance(2))
	ey=ey +Eym(i,jd+1,k)    *(1.-distance(0)) * (distanceD(1))    * (1.-distance(2))
	ey=ey +Eym(i+1,jd+1,k)  *distance(0)      * (distanceD(1))    * (1.-distance(2))
	ey=ey +Eym(i,jd,k+1)    *(1.-distance(0)) * (1.-distanceD(1)) * (distance(2))
	ey=ey +Eym(i+1,jd,k+1)  *(distance(0))    * (1.-distanceD(1)) * (distance(2))
	ey=ey +Eym(i,jd+1,k+1)  *(1.-distance(0)) * (distanceD(1))    * (distance(2))
	ey=ey +Eym(i+1,jd+1,k+1)*(distance(0))    * (distanceD(1))    * (distance(2))
! 
! 	

!if(k<k0-1 .OR. kd<k0-1) write(6,*) rank,iter,kd,j,P(l).Z,P(l).IdentPart
	ez=0
	ez=ez +Ezm(i,j,kd)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distanceD(2))
	ez=ez +Ezm(i+1,j,kd)    *(distance(0))    * (1.-distance(1)) * (1.-distanceD(2))
	ez=ez +Ezm(i,j+1,kd)    *(1.-distance(0)) * (distance(1))    * (1.-distanceD(2))
	ez=ez +Ezm(i+1,j+1,kd)  *distance(0)      * (distance(1))    * (1.-distanceD(2))
	ez=ez +Ezm(i,j,kd+1)    *(1.-distance(0)) * (1.-distance(1)) * (distanceD(2))
	ez=ez +Ezm(i+1,j,kd+1)  *(distance(0))    * (1.-distance(1)) * (distanceD(2))
	ez=ez +Ezm(i,j+1,kd+1)  *(1.-distance(0)) * (distance(1))    * (distanceD(2))
	ez=ez +Ezm(i+1,j+1,kd+1)*(distance(0))    * (distance(1))    * (distanceD(2))



endif
end subroutine  EfieldProjection






subroutine InvErrorFunction(yy,ierf)

      implicit none

     real, intent(in)   :: yy
     real, intent(out)   :: ierf

     real  s, t, u, w, x, z,y


     real, parameter  ::    qa = 9.16461398268964e-01
     real, parameter  ::    qb = 2.31729200323405e-01
     real, parameter  ::    qc = 4.88826640273108e-01 
     real, parameter  ::    qd = 1.24610454613712e-01 
     real, parameter  ::    q0 = 4.99999303439796e-01 
     real, parameter  ::    q1 = 1.16065025341614e-01 
     real, parameter  ::    q2 = 1.50689047360223e-01 
     real, parameter  ::    q3 = 2.69999308670029e-01 
     real, parameter  ::    q4 = -7.28846765585675e-02

     real, parameter  ::    pa = 3.97886080735226000e+00 
     real, parameter  ::    pb = 1.20782237635245222e-01 
     real, parameter  ::    p0 = 2.44044510593190935e-01 
     real, parameter  ::    p1 = 4.34397492331430115e-01 
     real, parameter  ::    p2 = 6.86265948274097816e-01 
     real, parameter  ::    p3 = 9.56464974744799006e-01 
     real, parameter  ::    p4 = 1.16374581931560831e+00 
     real, parameter  ::    p5 = 1.21448730779995237e+00 
     real, parameter  ::    p6 = 1.05375024970847138e+00 
     real, parameter  ::    p7 = 7.13657635868730364e-01 
     real, parameter  ::    p8 = 3.16847638520135944e-01 
     real, parameter  ::    p9 = 1.47297938331485121e-02
     real, parameter  ::    p10 = -1.05872177941595488e-01 
     real, parameter  ::    p11 = -7.43424357241784861e-02

     real, parameter  ::    p12 = 2.20995927012179067e-02 
     real, parameter  ::    p13 = 3.46494207789099922e-02 
     real, parameter  ::    p14 = 1.42961988697898018e-02 
     real, parameter  ::    p15 = -1.18598117047771104e-02 
     real, parameter  ::    p16 = -1.12749169332504870e-02 
     real, parameter  ::    p17 = 3.39721910367775861e-03 
     real, parameter  ::    p18 = 6.85649426074558612e-03 
     real, parameter  ::    p19 = -7.71708358954120939e-04 
     real, parameter  ::    p20 = -3.51287146129100025e-03 
     real, parameter  ::    p21 = 1.05739299623423047e-04 
     real, parameter  ::    p22 = 1.12648096188977922e-03



      y=1-yy
	if(yy .LE. -0.9999999 ) y=1+0.9999998
	if(yy .GE.  0.9999999 ) y=1-0.9999998

      z = y
      if (y > 1) then 
		
		z = 2 - y
! 		write(6,*) z 
		endif

      w = qa - log(z)
      u = sqrt(w)
      s = (qc + log(u)) / w
      t = 1 / (u + qb)
      x = u * (1 - s * (0.5d0 + s * qd)) - ((((q4 * t + q3) * t + q2) * t + q1) * t + q0) * t
      t = pa / (pa + x)
      u = t - 0.5d0
      s = (((((((((p22 * u + p21) * u + p20) * u +  p19) * u + p18) * u + p17) * u + p16) * u + p15) * u + p14) * u + p13) * u + p12
      s = ((((((((((((s * u + p11) * u + p10) * u + p9) * u + p8) * u + p7) * u + p6) * u + p5) * u + p4) * u + p3) * u + p2) * u + p1) * u + p0) * t - z * exp(x * x - pb)
      x = x + s * (1 + x * s)
      if (y > 1) x = -x

      ierf = x



end subroutine InvErrorFunction


subroutine movepart(l,ex,ey,ez,bx,by,bz)
	use Parallel
	implicit none
	real :: rqm
	real, intent(in):: ex,ey,ez,bx,by,bz
	integer, intent(in):: l
	real :: tex,tey,tez
	real :: tvitx,tvity,tvitz
	real :: tvitx2,tvity2,tvitz2
	real :: rb1g,rb2g,rb3g
	real a1,a2,a3,a4,a5,a6
	real det	
        rqm = 0.5_8*dt*charge(P(l)%sp)/(mass(P(l)%sp) )

        IF(iter .EQ. 0) rqm = 0.25_8*dt*charge(P(l)%sp)/(mass(P(l)%sp))

	    tex = rqm*ex
            tey = rqm*ey
            tez = rqm*ez


! --- Action of the 1st half of the field E

          tvitx = P(l)%VX + tex
          tvity = P(l)%VY + tey
          tvitz = P(l)%VZ + tez

! --- 	Magnetic rotation
          rb1g = bx * rqm
          rb2g = by * rqm
          rb3g = bz * rqm

          a1 = rb1g * rb1g
          a2 = rb2g * rb2g
          a3 = rb3g * rb3g
          a4 = rb1g * rb2g
          a5 = rb2g * rb3g
          a6 = rb3g * rb1g

          det = 1._8 / (1._8+a1+a2+a3)

          tvitx2 = ( (1.0+a1-a2-a3)*tvitx + 2.0*( (a4+rb3g)*tvity+(a6-rb2g)*tvitz ) ) * det
          tvity2 = ( (1.0+a2-a3-a1)*tvity + 2.0*( (a5+rb1g)*tvitz+(a4-rb3g)*tvitx ) ) * det
          tvitz2 = ( (1.0+a3-a1-a2)*tvitz + 2.0*( (a6+rb2g)*tvitx+(a5-rb1g)*tvity ) ) * det 

! ---Action of the 2nd half of the field E -> speed the moment n+1/2


          tvitx = tvitx2 + tex
          tvity = tvity2 + tey
          tvitz = tvitz2 + tez


! --- New velocities
          P(l)%VX  = tvitx
          P(l)%VY  = tvity
          P(l)%VZ  = tvitz


          P(l)%X=P(l)%X + dt*tvitx
          P(l)%Y=P(l)%Y + dt*tvity
          P(l)%Z=P(l)%Z + dt*tvitz


end subroutine

subroutine movepartAll()
	use Parallel
	implicit none
	real :: rqm
	real :: tex,tey,tez
	real :: tvitx,tvity,tvitz
	real :: tvitx2,tvity2,tvitz2
	real :: rb1g,rb2g,rb3g
	real a1,a2,a3,a4,a5,a6
	real det
  integer :: l
  
  integer :: i=0,j=0,k=0,id=0,jd=0,kd=0
	real :: distance(0:2)
	real :: distanceD(0:2)
  real ex, ey, ez, bx, by, bz
  do l=1, NPART

      px_arr(l)=P(l)%X
      py_arr(l)=P(l)%Y
      pz_arr(l)=P(l)%Z
      pvx_arr(l)=P(l)%VX
      pvy_arr(l)=P(l)%VY
      pvz_arr(l)=P(l)%VZ



	i=P(l)%X/delta(0)
	j=P(l)%Y/delta(1)
	k=P(l)%Z/delta(2)

!   	write(6,*) iter,i,j,k
	distance(0)=1.*P(l)%X/delta(0)-i
 	distance(1)=1.*P(l)%Y/delta(1)-j
 	distance(2)=1.*P(l)%Z/delta(2)-k

	id=P(l)%X/delta(0)-0.5
	jd=P(l)%Y/delta(1)-0.5
	kd=P(l)%Z/delta(2)-0.5
!    	write(6,*) iter,id,jd,kd

	distanceD(0)=1.*P(l)%X/delta(0)-(id+0.5)
 	distanceD(1)=1.*P(l)%Y/delta(1)-(jd+0.5)
 	distanceD(2)=1.*P(l)%Z/delta(2)-(kd+0.5)

!  	write(6,*) iter,distanceD
! 	ex=0
!   	ex=ex +Exm(i,  j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))
! 	ex=ex +Exm(i+1,j,k)      *(distance(0))    * (1.-distance(1)) * (1.-distance(2))
! 	ex=ex +Exm(i,  j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2))
! 	ex=ex +Exm(i+1,j+1,k)    *distance(0)      * (distance(1))    * (1.-distance(2))
! 	ex=ex +Exm(i,  j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2))
! 	ex=ex +Exm(i+1,j,k+1)    *(distance(0))    * (1.-distance(1)) * (distance(2))
! 	ex=ex +Exm(i,  j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2))
! 	ex=ex +Exm(i+1,j+1,k+1)  *(distance(0))    * (distance(1))    * (distance(2))

! if(i+1>i1+1 .OR. id+1>i1+1 .OR. j+1>j1+1 .OR. jd+1>j1+1 .OR. k+1>k1+1 .OR. kd+1>k1+1) then
if(i+1>i1+1 .OR. j+1>j1+1 .OR. k+1>k1+1  .OR. i<i0-1 .OR. j<j0-1 .OR. k<k0-1  .OR. id<i0-1 .OR. jd<j0-1  .OR. kd<k0-1) then

!if we gain particle from the other procces that is in the border of other proces - in function Particle_Loss P().X<i0 and P().X>=i1, then we need one extra plane Exm(i1+2) where is not calculation - just skip these points (less then 0.000001% per iteration)

	if(i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then

			write(3000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
			write(3000+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart
			write(3000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

	endif

  ex=0
  ey=0
  ez=0
  bx=0
  by=0
  bz=0
else

!if(i<i0-1 .OR. id<i0-1) write(6,*) rank,iter,id,i,P(l).X,i0,P(l).IdentPart

  
  ex =  Exm(id,  j,k)      *(1.-distanceD(0)) * (1.-distance(1)) * (1.-distance(2)) &
              +Exm(id+1,j,k)      *(distanceD(0))    * (1.-distance(1)) * (1.-distance(2)) &
              +Exm(id,  j+1,k)    *(1.-distanceD(0)) * (distance(1))    * (1.-distance(2)) &
              +Exm(id+1,j+1,k)    *distanceD(0)      * (distance(1))    * (1.-distance(2)) &
              +Exm(id,  j,k+1)    *(1.-distanceD(0)) * (1.-distance(1)) * (distance(2)) &
              +Exm(id+1,j,k+1)    *(distanceD(0))    * (1.-distance(1)) * (distance(2)) &
              +Exm(id,  j+1,k+1)  *(1.-distanceD(0)) * (distance(1))    * (distance(2)) &
              +Exm(id+1,j+1,k+1)  *(distanceD(0))    * (distance(1))    * (distance(2))



!if(j<j0-1 .OR. jd<j0-1) write(6,*) rank,iter,jd,j,P(l).Y,P(l).IdentPart

  ey = Eym(i,jd,k)      *(1.-distance(0)) * (1.-distanceD(1)) * (1.-distance(2)) &
            + Eym(i+1,jd,k)    *(distance(0))    * (1.-distanceD(1)) * (1.-distance(2)) &
            + Eym(i,jd+1,k)    *(1.-distance(0)) * (distanceD(1))    * (1.-distance(2)) &
            + Eym(i+1,jd+1,k)  *distance(0)      * (distanceD(1))    * (1.-distance(2)) &
            + Eym(i,jd,k+1)    *(1.-distance(0)) * (1.-distanceD(1)) * (distance(2)) &
            + Eym(i+1,jd,k+1)  *(distance(0))    * (1.-distanceD(1)) * (distance(2)) &
            + Eym(i,jd+1,k+1)  *(1.-distance(0)) * (distanceD(1))    * (distance(2)) &
            + Eym(i+1,jd+1,k+1)*(distance(0))    * (distanceD(1))    * (distance(2))
! 
! 	

!if(k<k0-1 .OR. kd<k0-1) write(6,*) rank,iter,kd,j,P(l).Z,P(l).IdentPart
  ez = Ezm(i,j,kd)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distanceD(2)) &
            + Ezm(i+1,j,kd)    *(distance(0))    * (1.-distance(1)) * (1.-distanceD(2)) &
            + Ezm(i,j+1,kd)    *(1.-distance(0)) * (distance(1))    * (1.-distanceD(2)) &
            + Ezm(i+1,j+1,kd)  *distance(0)      * (distance(1))    * (1.-distanceD(2)) &
            + Ezm(i,j,kd+1)    *(1.-distance(0)) * (1.-distance(1)) * (distanceD(2)) &
            + Ezm(i+1,j,kd+1)  *(distance(0))    * (1.-distance(1)) * (distanceD(2)) &
            + Ezm(i,j+1,kd+1)  *(1.-distance(0)) * (distance(1))    * (distanceD(2)) &
            + Ezm(i+1,j+1,kd+1)*(distance(0))    * (distance(1))    * (distanceD(2))
            
  bx = Bxm(i,  j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2)) &


            + Bxm(i+1,j,k)      *(distance(0))    * (1.-distance(1)) * (1.-distance(2)) &
            + Bxm(i,  j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2)) &
            + Bxm(i+1,j+1,k)    *distance(0)      * (distance(1))    * (1.-distance(2)) &
            + Bxm(i,  j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2)) &
            + Bxm(i+1,j,k+1)    *(distance(0))    * (1.-distance(1)) * (distance(2)) &
            + Bxm(i,  j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2)) &
            + Bxm(i+1,j+1,k+1)  *(distance(0))    * (distance(1))    * (distance(2))

  by = Bym(i,j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2)) &
            + Bym(i+1,j,k)    *(distance(0))    * (1.-distance(1)) * (1.-distance(2)) &
            + Bym(i,j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2)) &
            + Bym(i+1,j+1,k)  *distance(0)      * (distance(1))    * (1.-distance(2)) &
            + Bym(i,j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2)) &
            + Bym(i+1,j,k+1)  *(distance(0))    * (1.-distance(1)) * (distance(2)) &
            + Bym(i,j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2)) &
            + Bym(i+1,j+1,k+1)*(distance(0))    * (distance(1))    * (distance(2))

  bz = Bzm(i,j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2)) & 
            + Bzm(i+1,j,k)    *(distance(0))    * (1.-distance(1)) * (1.-distance(2)) &
            + Bzm(i,j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2)) &
            + Bzm(i+1,j+1,k)  *distance(0)      * (distance(1))    * (1.-distance(2)) &
            + Bzm(i,j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2)) &
            + Bzm(i+1,j,k+1)  *(distance(0))    * (1.-distance(1)) * (distance(2)) &
            + Bzm(i,j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2)) &
            + Bzm(i+1,j+1,k+1)*(distance(0))    * (distance(1))    * (distance(2))

  ex_arr(l) = ex
  ey_arr(l) = ey
  ez_arr(l) = ez
  bx_arr(l) = bx
  by_arr(l) = by
  bz_arr(l) = bz


endif

        rqm = 0.5_8*dt*charge(P(l)%sp)/(mass(P(l)%sp) )

        IF(iter .EQ. 0) rqm = 0.25_8*dt*charge(P(l)%sp)/(mass(P(l)%sp))

	    tex = rqm*ex
            tey = rqm*ey
            tez = rqm*ez


! --- Action of the 1st half of the field E

          tvitx = P(l)%VX + tex
          tvity = P(l)%VY + tey
          tvitz = P(l)%VZ + tez

! --- 	Magnetic rotation
          rb1g = bx * rqm
          rb2g = by * rqm
          rb3g = bz * rqm

          a1 = rb1g * rb1g
          a2 = rb2g * rb2g
          a3 = rb3g * rb3g
          a4 = rb1g * rb2g
          a5 = rb2g * rb3g
          a6 = rb3g * rb1g

          det = 1._8 / (1._8+a1+a2+a3)

          tvitx2 = ( (1.0+a1-a2-a3)*tvitx + 2.0*( (a4+rb3g)*tvity+(a6-rb2g)*tvitz ) ) * det
          tvity2 = ( (1.0+a2-a3-a1)*tvity + 2.0*( (a5+rb1g)*tvitz+(a4-rb3g)*tvitx ) ) * det
          tvitz2 = ( (1.0+a3-a1-a2)*tvitz + 2.0*( (a6+rb2g)*tvitx+(a5-rb1g)*tvity ) ) * det 

! ---Action of the 2nd half of the field E -> speed the moment n+1/2


          tvitx = tvitx2 + tex
          tvity = tvity2 + tey
          tvitz = tvitz2 + tez


! --- New velocities
          P(l)%VX  = tvitx
          P(l)%VY  = tvity
          P(l)%VZ  = tvitz


          P(l)%X=P(l)%X + dt*tvitx
          P(l)%Y=P(l)%Y + dt*tvity
          P(l)%Z=P(l)%Z + dt*tvitz

          end do
          
! do l=1,Npart
!   
!   i=P(l)%X/delta(0)
!   j=P(l)%Y/delta(1)
!   k=P(l)%Z/delta(2)
!   
!   if(i<i0-1 .OR. i> i1+1 .OR. j<j0-1 .OR. j>j1+1 .OR. k<k0-1 .OR. k>k1+1) then
!      write(*,*) "/|\", P(l)%X, P(l)%Y, P(l)%Z, P(l)%VX, &
!      P(l)%VY, P(l)%VZ, P(l)%sp, i0, j0, k0, P(l)%IdentPart, &
!      px_arr(l), py_arr(l), pz_arr(l), &
!      pvx_arr(l), pvy_arr(l), pvz_arr(l), &
!      ex_arr(l), ey_arr(l), ez_arr(l), &
!      bx_arr(l), by_arr(l), bz_arr(l), l
!   end if

! end do

end subroutine


subroutine movepart_trajectories(i_traj)
	use Parallel
	implicit none
	integer,intent( in ) :: i_traj
	real :: rqm
	real :: tex,tey,tez
	real :: tvitx,tvity,tvitz
	real :: tvitx2,tvity2,tvitz2
	real :: rb1g,rb2g,rb3g
	real a1,a2,a3,a4,a5,a6
	real det
	real :: Exm_aux(1:2,1:2,1:2),Eym_aux(1:2,1:2,1:2),Ezm_aux(1:2,1:2,1:2)
	integer :: l
  
	integer :: i=0,j=0,k=0,id=0,jd=0,kd=0
	real :: distance(0:2)
	real :: distanceD(0:2)
        real ex, ey, ez, bx, by, bz
	logical :: isin
	integer :: m, id_follow_part

  do l=1, N_p_t

      px_arr(l)=P_trajectory(l)%X
      py_arr(l)=P_trajectory(l)%Y
      pz_arr(l)=P_trajectory(l)%Z
      pvx_arr(l)=P_trajectory(l)%VX
      pvy_arr(l)=P_trajectory(l)%VY
      pvz_arr(l)=P_trajectory(l)%VZ



	i=P_trajectory(l)%X/delta(0)
	j=P_trajectory(l)%Y/delta(1)
	k=P_trajectory(l)%Z/delta(2)

!   	write(6,*) iter,i,j,k
	distance(0)=1.*P_trajectory(l)%X/delta(0)-i
 	distance(1)=1.*P_trajectory(l)%Y/delta(1)-j
 	distance(2)=1.*P_trajectory(l)%Z/delta(2)-k

	id=P_trajectory(l)%X/delta(0)-0.5
	jd=P_trajectory(l)%Y/delta(1)-0.5
	kd=P_trajectory(l)%Z/delta(2)-0.5
!    	write(6,*) iter,id,jd,kd

	distanceD(0)=1.*P_trajectory(l)%X/delta(0)-(id+0.5)
 	distanceD(1)=1.*P_trajectory(l)%Y/delta(1)-(jd+0.5)
 	distanceD(2)=1.*P_trajectory(l)%Z/delta(2)-(kd+0.5)

!  	write(6,*) iter,distanceD
! 	ex=0
!   	ex=ex +Exm(i,  j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))
! 	ex=ex +Exm(i+1,j,k)      *(distance(0))    * (1.-distance(1)) * (1.-distance(2))
! 	ex=ex +Exm(i,  j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2))
! 	ex=ex +Exm(i+1,j+1,k)    *distance(0)      * (distance(1))    * (1.-distance(2))
! 	ex=ex +Exm(i,  j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2))
! 	ex=ex +Exm(i+1,j,k+1)    *(distance(0))    * (1.-distance(1)) * (distance(2))
! 	ex=ex +Exm(i,  j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2))
! 	ex=ex +Exm(i+1,j+1,k+1)  *(distance(0))    * (distance(1))    * (distance(2))

! if(i+1>i1+1 .OR. id+1>i1+1 .OR. j+1>j1+1 .OR. jd+1>j1+1 .OR. k+1>k1+1 .OR. kd+1>k1+1) then
if(i+1>i1+1 .OR. j+1>j1+1 .OR. k+1>k1+1  .OR. i<i0-1 .OR. j<j0-1 .OR. k<k0-1  .OR. id<i0-1 .OR. jd<j0-1  .OR. kd<k0-1) then

!if we gain particle from the other procces that is in the border of other proces - in function Particle_Loss P().X<i0 and P().X>=i1, then we need one extra plane Exm(i1+2) where is not calculation - just skip these points (less then 0.000001% per iteration)

	if(i<i0-1 .OR. j<j0-1 .OR. k<k0-1) then

			write(3000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
			write(3000+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P_trajectory(l)%X,P_trajectory(l)%Y,P_trajectory(l)%Z,P_trajectory(l)%IdentPart
			write(3000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

	endif

  ex=0
  ey=0
  ez=0
  bx=0
  by=0
  bz=0
else

!if(i<i0-1 .OR. id<i0-1) write(6,*) rank,iter,id,i,P(l).X,i0,P(l).IdentPart	
  !Exm_aux(1,1,1)=Exm(id,  j,k)
  !Exm_aux(2,1,1)=Exm(id+1,j,k)	  
  !Exm_aux(1,2,1)=Exm(id,  j+1,k)
  !Exm_aux(2,2,1)=Exm(id+1,j+1,k)
  !Exm_aux(1,1,2)=Exm(id,  j,k+1)
  !Exm_aux(2,1,2)=Exm(id+1,j,k+1)		
  !Exm_aux(1,2,2)=Exm(id,  j+1,k+1)
  !Exm_aux(2,2,2)=Exm(id+1,j+1,k+1)
  

  Exm_aux(1,1,1)=potential_average(1,id,  j,k)/1000
  Exm_aux(2,1,1)=potential_average(1,id+1,j,k)/1000	  
  Exm_aux(1,2,1)=potential_average(1,id,  j+1,k)/1000
  Exm_aux(2,2,1)=potential_average(1,id+1,j+1,k)/1000
  Exm_aux(1,1,2)=potential_average(1,id,  j,k+1)/1000
  Exm_aux(2,1,2)=potential_average(1,id+1,j,k+1)/1000		
  Exm_aux(1,2,2)=potential_average(1,id,  j+1,k+1)/1000
  Exm_aux(2,2,2)=potential_average(1,id+1,j+1,k+1)/1000	


  ex =  Exm_aux(1,1,1)*(1.-distanceD(0)) * (1.-distance(1)) * (1.-distance(2)) &
              +Exm_aux(2,1,1)*(distanceD(0))    * (1.-distance(1)) * (1.-distance(2)) &
              +Exm_aux(1,2,1)*(1.-distanceD(0)) * (distance(1))    * (1.-distance(2)) &
              +Exm_aux(2,2,1)*distanceD(0)      * (distance(1))    * (1.-distance(2)) &
              +Exm_aux(1,1,2)*(1.-distanceD(0)) * (1.-distance(1)) * (distance(2)) &
              +Exm_aux(2,1,2)*(distanceD(0))    * (1.-distance(1)) * (distance(2)) &
              +Exm_aux(1,2,2)*(1.-distanceD(0)) * (distance(1))    * (distance(2)) &
              +Exm_aux(2,2,2)*(distanceD(0))    * (distance(1))    * (distance(2))



!if(j<j0-1 .OR. jd<j0-1) write(6,*) rank,iter,jd,j,P(l).Y,P(l).IdentPart

  Eym_aux(1,1,1)=potential_average(2,id,  j,k)/1000
  Eym_aux(2,1,1)=potential_average(2,id+1,j,k)/1000	  
  Eym_aux(1,2,1)=potential_average(2,id,  j+1,k)/1000
  Eym_aux(2,2,1)=potential_average(2,id+1,j+1,k)/1000
  Eym_aux(1,1,2)=potential_average(2,id,  j,k+1)/1000
  Eym_aux(2,1,2)=potential_average(2,id+1,j,k+1)/1000		
  Eym_aux(1,2,2)=potential_average(2,id,  j+1,k+1)/1000
  Eym_aux(2,2,2)=potential_average(2,id+1,j+1,k+1)/1000	


  ey = Eym_aux(1,1,1)*(1.-distance(0)) * (1.-distanceD(1)) * (1.-distance(2)) &
            + Eym_aux(2,1,1)*(distance(0))    * (1.-distanceD(1)) * (1.-distance(2)) &
            + Eym_aux(1,2,1)*(1.-distance(0)) * (distanceD(1))    * (1.-distance(2)) &
            + Eym_aux(2,2,1)*distance(0)      * (distanceD(1))    * (1.-distance(2)) &
            + Eym_aux(1,1,2)*(1.-distance(0)) * (1.-distanceD(1)) * (distance(2)) &
            + Eym_aux(2,1,2)*(distance(0))    * (1.-distanceD(1)) * (distance(2)) &
            + Eym_aux(1,2,2)*(1.-distance(0)) * (distanceD(1))    * (distance(2)) &
            + Eym_aux(2,2,2)*(distance(0))    * (distanceD(1))    * (distance(2))
! 
! 	

!if(k<k0-1 .OR. kd<k0-1) write(6,*) rank,iter,kd,j,P(l).Z,P(l).IdentPart

  Ezm_aux(1,1,1)=potential_average(3,id,  j,k)/1000
  Ezm_aux(2,1,1)=potential_average(3,id+1,j,k)/1000	  
  Ezm_aux(1,2,1)=potential_average(3,id,  j+1,k)/1000
  Ezm_aux(2,2,1)=potential_average(3,id+1,j+1,k)/1000
  Ezm_aux(1,1,2)=potential_average(3,id,  j,k+1)/1000
  Ezm_aux(2,1,2)=potential_average(3,id+1,j,k+1)/1000		
  Ezm_aux(1,2,2)=potential_average(3,id,  j+1,k+1)/1000
  Ezm_aux(2,2,2)=potential_average(3,id+1,j+1,k+1)/1000



  ez = Ezm_aux(1,1,1)*(1.-distance(0)) * (1.-distance(1)) * (1.-distanceD(2)) &
            + Ezm_aux(2,1,1)*(distance(0))    * (1.-distance(1)) * (1.-distanceD(2)) &
            + Ezm_aux(1,2,1)*(1.-distance(0)) * (distance(1))    * (1.-distanceD(2)) &
            + Ezm_aux(2,2,1)*distance(0)      * (distance(1))    * (1.-distanceD(2)) &
            + Ezm_aux(1,1,2)*(1.-distance(0)) * (1.-distance(1)) * (distanceD(2)) &
            + Ezm_aux(2,1,2)*(distance(0))    * (1.-distance(1)) * (distanceD(2)) &
            + Ezm_aux(1,2,2)*(1.-distance(0)) * (distance(1))    * (distanceD(2)) &
            + Ezm_aux(2,2,2)*(distance(0))    * (distance(1))    * (distanceD(2))
            
  bx = Bxm(i,  j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2)) &


            + Bxm(i+1,j,k)      *(distance(0))    * (1.-distance(1)) * (1.-distance(2)) &
            + Bxm(i,  j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2)) &
            + Bxm(i+1,j+1,k)    *distance(0)      * (distance(1))    * (1.-distance(2)) &
            + Bxm(i,  j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2)) &
            + Bxm(i+1,j,k+1)    *(distance(0))    * (1.-distance(1)) * (distance(2)) &
            + Bxm(i,  j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2)) &
            + Bxm(i+1,j+1,k+1)  *(distance(0))    * (distance(1))    * (distance(2))

  by = Bym(i,j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2)) &
            + Bym(i+1,j,k)    *(distance(0))    * (1.-distance(1)) * (1.-distance(2)) &
            + Bym(i,j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2)) &
            + Bym(i+1,j+1,k)  *distance(0)      * (distance(1))    * (1.-distance(2)) &
            + Bym(i,j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2)) &
            + Bym(i+1,j,k+1)  *(distance(0))    * (1.-distance(1)) * (distance(2)) &
            + Bym(i,j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2)) &
            + Bym(i+1,j+1,k+1)*(distance(0))    * (distance(1))    * (distance(2))

  bz = Bzm(i,j,k)      *(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2)) & 
            + Bzm(i+1,j,k)    *(distance(0))    * (1.-distance(1)) * (1.-distance(2)) &
            + Bzm(i,j+1,k)    *(1.-distance(0)) * (distance(1))    * (1.-distance(2)) &
            + Bzm(i+1,j+1,k)  *distance(0)      * (distance(1))    * (1.-distance(2)) &
            + Bzm(i,j,k+1)    *(1.-distance(0)) * (1.-distance(1)) * (distance(2)) &
            + Bzm(i+1,j,k+1)  *(distance(0))    * (1.-distance(1)) * (distance(2)) &
            + Bzm(i,j+1,k+1)  *(1.-distance(0)) * (distance(1))    * (distance(2)) &
            + Bzm(i+1,j+1,k+1)*(distance(0))    * (distance(1))    * (distance(2))

  ex_arr(l) = ex
  ey_arr(l) = ey
  ez_arr(l) = ez
  bx_arr(l) = bx
  by_arr(l) = by
  bz_arr(l) = bz


endif

        rqm = 0.5_8*dt*charge(P_trajectory(l)%sp)/(mass(P_trajectory(l)%sp) )

        IF(iter .EQ. 0) rqm = 0.25_8*dt*charge(P_trajectory(l)%sp)/(mass(P_trajectory(l)%sp))

	    tex = rqm*ex
            tey = rqm*ey
            tez = rqm*ez


! --- Action of the 1st half of the field E

          tvitx = P_trajectory(l)%VX + tex
          tvity = P_trajectory(l)%VY + tey
          tvitz = P_trajectory(l)%VZ + tez

! --- 	Magnetic rotation
          rb1g = bx * rqm
          rb2g = by * rqm
          rb3g = bz * rqm

          a1 = rb1g * rb1g
          a2 = rb2g * rb2g
          a3 = rb3g * rb3g
          a4 = rb1g * rb2g
          a5 = rb2g * rb3g
          a6 = rb3g * rb1g

          det = 1._8 / (1._8+a1+a2+a3)

          tvitx2 = ( (1.0+a1-a2-a3)*tvitx + 2.0*( (a4+rb3g)*tvity+(a6-rb2g)*tvitz ) ) * det
          tvity2 = ( (1.0+a2-a3-a1)*tvity + 2.0*( (a5+rb1g)*tvitz+(a4-rb3g)*tvitx ) ) * det
          tvitz2 = ( (1.0+a3-a1-a2)*tvitz + 2.0*( (a6+rb2g)*tvitx+(a5-rb1g)*tvity ) ) * det 

! ---Action of the 2nd half of the field E -> speed the moment n+1/2


          tvitx = tvitx2 + tex
          tvity = tvity2 + tey
          tvitz = tvitz2 + tez


! --- New velocities
          P_trajectory(l)%VX  = tvitx
          P_trajectory(l)%VY  = tvity
          P_trajectory(l)%VZ  = tvitz


          P_trajectory(l)%X=P_trajectory(l)%X + dt*tvitx
          P_trajectory(l)%Y=P_trajectory(l)%Y + dt*tvity
          P_trajectory(l)%Z=P_trajectory(l)%Z + dt*tvitz
	  		
	     if(mod(i_traj,50)==0)then
		if(P_trajectory(l)%sp == 0 ) then
			id_follow_part = P_trajectory(l)%IdentPart
			write(str3, '(I7)') id_follow_part
			lgc = LEN_TRIM(ADJUSTL(str3))
			str = 'Pa_track_'//str3(8-lgc:7)//'.dat'          		
		    OPEN(UNIT=3333, FILE=str, status="unknown", position= "append" )		
			 write(3333,'( 1(1x,I8), 7(1x,E14.7) )') i_traj,P_trajectory(l)%X,P_trajectory(l)%Y,P_trajectory(l)%Z,P_trajectory(l)%sp,P_trajectory(l)%VX,P_trajectory(l)%VY,P_trajectory(l)%VZ
	    	    close(3333)
		endif

		if( (P_trajectory(l)%sp == 5 .OR. P_trajectory(l)%sp == 9 .OR. P_trajectory(l)%sp == 2 ) .AND. (mod(i_traj,100)==0) ) then
			id_follow_part = P_trajectory(l)%IdentPart
			write(str3, '(I7)') id_follow_part
			lgc = LEN_TRIM(ADJUSTL(str3))
			str = 'Pa_track_'//str3(8-lgc:7)//'.dat'          		
		    OPEN(UNIT=3333, FILE=str, status="unknown", position= "append" )		
			 write(3333,'( 1(1x,I8), 7(1x,E14.7) )') i_traj,P_trajectory(l)%X,P_trajectory(l)%Y,P_trajectory(l)%Z,P_trajectory(l)%sp,P_trajectory(l)%VX,P_trajectory(l)%VY,P_trajectory(l)%VZ
	    	    close(3333)
		endif
	
	     endif							            	    				
	  	
  end do
          



end subroutine movepart_trajectories



subroutine ChargeExchange(Arr)
	
	use Parallel
	implicit none
	include 'mpif.h'

        real,intent( inout ) :: Arr(i0-1:i1+1  , j0-1:j1+1, k0-1:k1+1) 


	!---------------------------X direction-------------------------------------------------------------
	if(mod(indexx,2)==0) then	

		if(neig(1)>-1) CALL MPI_SEND(Arr(i1+1,j0-1,k0-1), 1,yz0, neig(1),51, MPI_COMM_WORLD, info)
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,yz0, neig(0),51, MPI_COMM_WORLD, status, info)

	else
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,yz0, neig(0),51, MPI_COMM_WORLD, status, info)
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1+1,j0-1,k0-1), 1,yz0, neig(1),51, MPI_COMM_WORLD, info)

	endif


	!---------------------------Y direction-------------------------------------------------------------


	if(mod(indexy,2)==0) then
		
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-1,j1+1,k0-1), 1,xz0, neig(3),51, MPI_COMM_WORLD, info)
		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xz0, neig(2),51, MPI_COMM_WORLD, status, info)
	
	else
		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xz0, neig(2),51, MPI_COMM_WORLD, status, info)
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-1,j1+1,k0-1), 1,xz0, neig(3),51, MPI_COMM_WORLD, info)

	endif
		

	!---------------------------Z direction-------------------------------------------------------------
	if(mod(indexz,2)==0) then
	
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-1,j0-1,k1+1), 1,xy0, neig(5),51, MPI_COMM_WORLD, info)
		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xy0, neig(4),51, MPI_COMM_WORLD, status, info)
	else

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-1,j0-1,k0-1), 1,xy0, neig(4),51, MPI_COMM_WORLD, status, info)
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-1,j0-1,k1+1), 1,xy0, neig(5),51, MPI_COMM_WORLD, info)
	endif

	Arr(i0,:,:)=Arr(i0,:,:)+Arr(i0-1,:,:)
	Arr(:,j0,:)=Arr(:,j0,:)+Arr(:,j0-1,:)
	Arr(:,:,k0)=Arr(:,:,k0)+Arr(:,:,k0-1)

	call Send_Plane(Arr)


end subroutine  ChargeExchange




subroutine  Sort_DeleteParticle2(Arr_to_sort,length_arr)
	use Parallel
	implicit none

        integer,intent( inout ) :: Arr_to_sort(1:NPARTL) 
        integer,intent( in ) :: length_arr


	integer i_loop2,j_loop2,temp_sort
	i_loop2=0
	j_loop2=0
	temp_sort=0
	Arr_new_sort=0

	do i_loop2=OutParticles,1,-1

	Arr_new_sort(i_loop2)=Arr_to_sort(i_loop2)

		do j_loop2=i_loop2-1,1,-1
			if(Arr_new_sort(i_loop2)<Arr_to_sort(j_loop2)) then
				temp_sort=Arr_new_sort(i_loop2)
				Arr_new_sort(i_loop2)=Arr_to_sort(j_loop2)
				Arr_to_sort(j_loop2)=temp_sort
			endif
		enddo

	enddo




	Arr_to_sort=Arr_new_sort

end subroutine Sort_DeleteParticle2


subroutine  Sort_DeleteParticle()
	use Parallel
	implicit none

	integer i_loop2,j_loop2,temp_sort
	i_loop2=0
	j_loop2=0
	temp_sort=0
	DelParticles_sort=0

	do i_loop2=OutParticles,1,-1

	DelParticles_sort(i_loop2)=DelParticles(i_loop2)

		do j_loop2=i_loop2-1,1,-1
			if(DelParticles_sort(i_loop2)<DelParticles(j_loop2)) then
				temp_sort=DelParticles_sort(i_loop2)
				DelParticles_sort(i_loop2)=DelParticles(j_loop2)
				DelParticles(j_loop2)=temp_sort
			endif
		enddo

	enddo




	DelParticles=DelParticles_sort

end subroutine Sort_DeleteParticle







subroutine DeleteParticle()
	
	use Parallel
	implicit none

	integer i,countDelPart


	
	countDelPart=0
	do i=OutParticles,1,-1
		P(DelParticles(i))=P(Npart-countDelPart)
		countDelPart=countDelPart+1

	enddo

	Npart=Npart-OutParticles

end subroutine DeleteParticle


subroutine DeleteParticle_trajectory()
	
	use Parallel
	implicit none

	integer i,countDelPart


	
	countDelPart=0
	do i=OutParticles,1,-1
		P_trajectory(DelParticles(i))=P_trajectory(N_p_t-countDelPart)
		countDelPart=countDelPart+1

	enddo

	N_p_t=N_p_t-OutParticles

end subroutine DeleteParticle_trajectory





subroutine MagneticFieldProjection(l,bz,by)
	
	use Parallel
	implicit none

	integer, intent(in) :: l
	real, intent(out):: bz
	real, intent(out):: by


	real:: Bpeak
	real:: Zpeak
        real:: Ypeak
	real:: Sigma
! by=0
!-------------------------For small (normal) domain	

	Bpeak=7e-3
	Zpeak=0.0000
	Sigma=3.5e-2

      bz=Bpeak*exp(-(  (P(l)%X-Zpeak)/(2.*Sigma ))**2)


	!Bpeak=130e-3
	!Bpeak=60e-3
	Bpeak=50e-3
	Ypeak=0.0316
	Sigma=0.3e-2
	!Sigma=0.3e-2

  	by=Bpeak*exp(-(  (P(l)%X-Ypeak)/(2.*Sigma ))**2)



!----------------------------------------------------------------

!---------------------------------For large domain 	
!	Bpeak=7e-3
 !	Zpeak=0.01
 !	Sigma=3.5e-2


  !      bz=Bpeak*exp(-(  (P(l).X-Zpeak)/(2.*Sigma ))**2)


!	Bpeak=130e-3
!	Ypeak=0.04
!	Sigma=0.5e-2

	
!	by=Bpeak*exp(-(  (P(l).X-Ypeak)/(2.*Sigma ))**2)
!------------------------------------------------------------

end subroutine MagneticFieldProjection

subroutine ChargeProjection_Second_Order(l)
	
	use Parallel
	implicit none


	integer, intent(in):: l

	integer :: i=0,j=0,k=0
	real :: distance(0:2)
	real :: vol=0
	real :: vol2=0


	i=P(l)%X/delta(0)
	j=P(l)%Y/delta(1)
	k=P(l)%Z/delta(2)

	distance(0)=1.*P(l)%X/delta(0)-i
 	distance(1)=1.*P(l)%Y/delta(1)-j
 	distance(2)=1.*P(l)%Z/delta(2)-k
 	
	vol=1.0/(delta(0)*delta(1)*delta(2))
	vol2=1.0/(64.0*delta(0)*delta(1)*delta(2))

!vol2=1.0/8.0
if(i<0 .OR. j<0 .OR. k<0  .OR. i>Nxp .OR. j>Nyp .OR. k>Nzp) then

	write(2500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
	write(2500+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart
	write(2500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

endif


IF(i<i1+1 .AND. j<j1+1 .AND. k<k1+1 .AND. i>i0-1 .AND. j>j0-1 .AND. k>k0-1) THEN


      !Main charge Projection routine

      !First plane
      !======================================================================================================================================================================
      rho_Sec(i-1,j-1,k-1)  =rho_Sec(i-1,j-1,k-1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j-1,k)    =rho_Sec(i-1,j-1,k)    +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j-1,k+1)  =rho_Sec(i-1,j-1,k+1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j-1,k+2)  =rho_Sec(i-1,j-1,k+2)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i-1,j,k-1)    =rho_Sec(i-1,j,k-1)    +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j,k)      =rho_Sec(i-1,j,k)      +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j,k+1)    =rho_Sec(i-1,j,k+1)    +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j,k+2)    =rho_Sec(i-1,j,k+2)    +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i-1,j+1,k-1)  =rho_Sec(i-1,j+1,k-1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+1,k)    =rho_Sec(i-1,j+1,k)    +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+1,k+1)  =rho_Sec(i-1,j+1,k+1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+1,k+2)  =rho_Sec(i-1,j+1,k+2)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i-1,j+2,k-1)  =rho_Sec(i-1,j+2,k-1)  +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+2,k)    =rho_Sec(i-1,j+2,k)    +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+2,k+1)  =rho_Sec(i-1,j+2,k+1)  +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+2,k+2)  =rho_Sec(i-1,j+2,k+2)  +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (distance(2))        *vol2*weight(P(l)%sp)
      !======================================================================================================================================================================

      !Second plane
      !======================================================================================================================================================================

      rho_Sec(i,j-1,k-1)    =rho_Sec(i,j-1,k-1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j-1,k)      =rho_Sec(i,j-1,k)      +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j-1,k+1)    =rho_Sec(i,j-1,k+1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j-1,k+2)    =rho_Sec(i,j-1,k+2)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i,j,k-1)      =rho_Sec(i,j,k-1)      +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j,k)        =rho_Sec(i,j,k)        +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j,k+1)      =rho_Sec(i,j,k+1)      +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j,k+2)      =rho_Sec(i,j,k+2)      +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      rho_Sec(i,j+1,k-1)    =rho_Sec(i,j+1,k-1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j+1,k)      =rho_Sec(i,j+1,k)      +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)

      rho_Sec(i,j+1,k+1)    =rho_Sec(i,j+1,k+1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j+1,k+2)    =rho_Sec(i,j+1,k+2)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      rho_Sec(i,j+2,k-1)    =rho_Sec(i,j+2,k-1)    +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j+2,k)      =rho_Sec(i,j+2,k)      +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j+2,k+1)    =rho_Sec(i,j+2,k+1)    +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j+2,k+2)    =rho_Sec(i,j+2,k+2)    +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (distance(2))        *vol2*weight(P(l)%sp)
      !======================================================================================================================================================================


      !Third plane
      !======================================================================================================================================================================

      rho_Sec(i+1,j-1,k-1)  =rho_Sec(i+1,j-1,k-1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j-1,k)    =rho_Sec(i+1,j-1,k)    +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j-1,k+1)  =rho_Sec(i+1,j-1,k+1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j-1,k+2)  =rho_Sec(i+1,j-1,k+2)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+1,j,k-1)    =rho_Sec(i+1,j,k-1)    +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j,k)      =rho_Sec(i+1,j,k)      +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j,k+1)    =rho_Sec(i+1,j,k+1)    +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j,k+2)    =rho_Sec(i+1,j,k+2)    +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+1,j+1,k-1)  =rho_Sec(i+1,j+1,k-1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+1,k)    =rho_Sec(i+1,j+1,k)    +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+1,k+1)  =rho_Sec(i+1,j+1,k+1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+1,k+2)  =rho_Sec(i+1,j+1,k+2)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+1,j+2,k-1)  =rho_Sec(i+1,j+2,k-1)  +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+2,k)    =rho_Sec(i+1,j+2,k)    +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+2,k+1)  =rho_Sec(i+1,j+2,k+1)  +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+2,k+2)  =rho_Sec(i+1,j+2,k+2)  +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)
      !======================================================================================================================================================================

      !Fourth plane
      !======================================================================================================================================================================

      rho_Sec(i+2,j-1,k-1)  =rho_Sec(i+2,j-1,k-1)  +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j-1,k)    =rho_Sec(i+2,j-1,k)    +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j-1,k+1)  =rho_Sec(i+2,j-1,k+1)  +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j-1,k+2)  =rho_Sec(i+2,j-1,k+2)  +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+2,j,k-1)    =rho_Sec(i+2,j,k-1)    +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j,k)      =rho_Sec(i+2,j,k)      +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j,k+1)    =rho_Sec(i+2,j,k+1)    +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j,k+2)    =rho_Sec(i+2,j,k+2)    +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+2,j+1,k-1)  =rho_Sec(i+2,j+1,k-1)  +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+1,k)    =rho_Sec(i+2,j+1,k)    +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+1,k+1)  =rho_Sec(i+2,j+1,k+1)  +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+1,k+2)  =rho_Sec(i+2,j+1,k+2)  +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+2,j+2,k-1)  =rho_Sec(i+2,j+2,k-1)  +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+2,k)    =rho_Sec(i+2,j+2,k)    +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+2,k+1)  =rho_Sec(i+2,j+2,k+1)  +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+2,k+2)  =rho_Sec(i+2,j+2,k+2)  +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)

      !======================================================================================================================================================================


ELSE


      if(i>i1 .OR. j>j1 .OR. k>k1 .OR. i<i0 .OR. j<j0 .OR. k<k0) then


				if(i==i1+1 .AND. j<j1+1 .AND. k<k1+1 ) then

					rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					rho_Sec(i,j+1,k)    =rho_Sec(i,j+1,k)    +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
					rho_Sec(i,j,k+1)    =rho_Sec(i,j,k+1)    +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
					rho_Sec(i,j+1,k+1)  =rho_Sec(i,j+1,k+1)  +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)

				endif

				if(j==j1+1 .AND. (i<i1+1) .AND. (k<k1+1) ) then	
		
					  rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)	
					  rho_Sec(i+1,j,k)    =rho_Sec(i+1,j,k)    +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i,j,k+1)    =rho_Sec(i,j,k+1)    +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
	
				endif

				if(k==k1+1 .AND.  i<i1+1 .AND. j<j1+1) then


					  rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i+1,j,k)    =rho_Sec(i+1,j,k)    +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i,j+1,k)    =rho_Sec(i,j+1,k)    +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i+1,j+1,k)  =rho_Sec(i+1,j+1,k)  +charge(P(l)%sp)*distance(0)      * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
		
				endif


				if(i<i0 .OR. j<j0 .OR. k<k0) then

					  write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
					  write(2000+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart,P(l)%X,P(l)%Y,P(l)%Z
					  write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

				endif


	 else
					  write(2100+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
					  write(2100+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart,P(l)%X,P(l)%Y,P(l)%Z
					  write(2100+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"




				!rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l).sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j,k)    =rho_Sec(i+1,j,k)    +charge(P(l).sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i,j+1,k)    =rho_Sec(i,j+1,k)    +charge(P(l).sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j+1,k)  =rho_Sec(i+1,j+1,k)  +charge(P(l).sp)*distance(0)      * (distance(1))    * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i,j,k+1)    =rho_Sec(i,j,k+1)    +charge(P(l).sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j,k+1)  =rho_Sec(i+1,j,k+1)  +charge(P(l).sp)*(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i,j+1,k+1)  =rho_Sec(i,j+1,k+1)  +charge(P(l).sp)*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j+1,k+1)=rho_Sec(i+1,j+1,k+1)+charge(P(l).sp)*(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(l).sp)


	 endif



ENDIF




end subroutine  ChargeProjection_Second_Order

subroutine ChargeProjection_Second_Order_momentum(l)
	
	use Parallel
	implicit none


	integer, intent(in):: l

	integer :: i=0,j=0,k=0
	real :: distance(0:2)
	real :: vol=0
	real :: vol2=0




	i=P(l)%X/delta(0)
	j=P(l)%Y/delta(1)
	k=P(l)%Z/delta(2)

	distance(0)=1.*P(l)%X/delta(0)-i
 	distance(1)=1.*P(l)%Y/delta(1)-j
 	distance(2)=1.*P(l)%Z/delta(2)-k
 	
	vol=1.0/(delta(0)*delta(1)*delta(2))
	vol2=1.0/(64.0*delta(0)*delta(1)*delta(2))

!vol2=1.0/8.0
if(i<0 .OR. j<0 .OR. k<0  .OR. i>Nxp .OR. j>Nyp .OR. k>Nzp) then

	write(2500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
	write(2500+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart
	write(2500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

endif


IF(i<i1+1 .AND. j<j1+1 .AND. k<k1+1 .AND. i>i0-1 .AND. j>j0-1 .AND. k>k0-1) THEN


      !Main charge Projection routine

      !First plane
      !======================================================================================================================================================================
      
      !!x

      Mom_e_x(i-1,j-1,k-1)  =Mom_e_x(i-1,j-1,k-1)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j-1,k)    =Mom_e_x(i-1,j-1,k)    + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j-1,k+1)  =Mom_e_x(i-1,j-1,k+1)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j-1,k+2)  =Mom_e_x(i-1,j-1,k+2)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_x(i-1,j,k-1)    =Mom_e_x(i-1,j,k-1)    + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j,k)      =Mom_e_x(i-1,j,k)      + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j,k+1)    =Mom_e_x(i-1,j,k+1)    + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j,k+2)    =Mom_e_x(i-1,j,k+2)    + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (2.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_x(i-1,j+1,k-1)  =Mom_e_x(i-1,j+1,k-1)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j+1,k)    =Mom_e_x(i-1,j+1,k)    + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j+1,k+1)  =Mom_e_x(i-1,j+1,k+1)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j+1,k+2)  =Mom_e_x(i-1,j+1,k+2)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (1.0+distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_x(i-1,j+2,k-1)  =Mom_e_x(i-1,j+2,k-1)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (distance(1))     * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j+2,k)    =Mom_e_x(i-1,j+2,k)    + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (distance(1))     * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j+2,k+1)  =Mom_e_x(i-1,j+2,k+1)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (distance(1))     * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i-1,j+2,k+2)  =Mom_e_x(i-1,j+2,k+2)  + mass(P(l)%sp)*P(l)%VX * (1.0-distance(0)) * (distance(1))     * (distance(2))        *vol2*weight(P(l)%sp)

      !!y

      Mom_e_y(i-1,j-1,k-1)  =Mom_e_y(i-1,j-1,k-1)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j-1,k)    =Mom_e_y(i-1,j-1,k)    + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j-1,k+1)  =Mom_e_y(i-1,j-1,k+1)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j-1,k+2)  =Mom_e_y(i-1,j-1,k+2)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_y(i-1,j,k-1)    =Mom_e_y(i-1,j,k-1)    + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j,k)      =Mom_e_y(i-1,j,k)      + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j,k+1)    =Mom_e_y(i-1,j,k+1)    + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j,k+2)    =Mom_e_y(i-1,j,k+2)    + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (2.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_y(i-1,j+1,k-1)  =Mom_e_y(i-1,j+1,k-1)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j+1,k)    =Mom_e_y(i-1,j+1,k)    + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j+1,k+1)  =Mom_e_y(i-1,j+1,k+1)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j+1,k+2)  =Mom_e_y(i-1,j+1,k+2)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (1.0+distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_y(i-1,j+2,k-1)  =Mom_e_y(i-1,j+2,k-1)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (distance(1))     * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j+2,k)    =Mom_e_y(i-1,j+2,k)    + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (distance(1))     * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j+2,k+1)  =Mom_e_y(i-1,j+2,k+1)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (distance(1))     * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i-1,j+2,k+2)  =Mom_e_y(i-1,j+2,k+2)  + mass(P(l)%sp)*P(l)%VY * (1.0-distance(0)) * (distance(1))     * (distance(2))        *vol2*weight(P(l)%sp)	



      !!z	

      Mom_e_z(i-1,j-1,k-1)  =Mom_e_z(i-1,j-1,k-1)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j-1,k)    =Mom_e_z(i-1,j-1,k)    + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j-1,k+1)  =Mom_e_z(i-1,j-1,k+1)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j-1,k+2)  =Mom_e_z(i-1,j-1,k+2)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_z(i-1,j,k-1)    =Mom_e_z(i-1,j,k-1)    + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j,k)      =Mom_e_z(i-1,j,k)      + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j,k+1)    =Mom_e_z(i-1,j,k+1)    + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j,k+2)    =Mom_e_z(i-1,j,k+2)    + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (2.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_z(i-1,j+1,k-1)  =Mom_e_z(i-1,j+1,k-1)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j+1,k)    =Mom_e_z(i-1,j+1,k)    + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j+1,k+1)  =Mom_e_z(i-1,j+1,k+1)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j+1,k+2)  =Mom_e_z(i-1,j+1,k+2)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (1.0+distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_z(i-1,j+2,k-1)  =Mom_e_z(i-1,j+2,k-1)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (distance(1))     * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j+2,k)    =Mom_e_z(i-1,j+2,k)    + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (distance(1))     * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j+2,k+1)  =Mom_e_z(i-1,j+2,k+1)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (distance(1))     * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i-1,j+2,k+2)  =Mom_e_z(i-1,j+2,k+2)  + mass(P(l)%sp)*P(l)%VZ * (1.0-distance(0)) * (distance(1))     * (distance(2))        *vol2*weight(P(l)%sp)	



      !======================================================================================================================================================================

      !Second plane
      !======================================================================================================================================================================

      Mom_e_x(i,j-1,k-1)    =Mom_e_x(i,j-1,k-1)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i,j-1,k)      =Mom_e_x(i,j-1,k)      + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i,j-1,k+1)    =Mom_e_x(i,j-1,k+1)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i,j-1,k+2)    =Mom_e_x(i,j-1,k+2)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_x(i,j,k-1)      =Mom_e_x(i,j,k-1)      + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (2.0-distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_x(i,j,k)        =Mom_e_x(i,j,k)        + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (2.0-distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_x(i,j,k+1)      =Mom_e_x(i,j,k+1)      + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (2.0-distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_x(i,j,k+2)      =Mom_e_x(i,j,k+2)      + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (2.0-distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      Mom_e_x(i,j+1,k-1)    =Mom_e_x(i,j+1,k-1)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0+distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_x(i,j+1,k)      =Mom_e_x(i,j+1,k)      + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0+distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_x(i,j+1,k+1)    =Mom_e_x(i,j+1,k+1)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0+distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_x(i,j+1,k+2)    =Mom_e_x(i,j+1,k+2)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (1.0+distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      Mom_e_x(i,j+2,k-1)    =Mom_e_x(i,j+2,k-1)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (distance(1))      * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i,j+2,k)      =Mom_e_x(i,j+2,k)      + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (distance(1))      * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i,j+2,k+1)    =Mom_e_x(i,j+2,k+1)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (distance(1))      * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_x(i,j+2,k+2)    =Mom_e_x(i,j+2,k+2)    + mass(P(l)%sp)*P(l)%VX * (2.0-distance(0)) * (distance(1))      * (distance(2))        *vol2*weight(P(l)%sp)
      




      Mom_e_y(i,j-1,k-1)    =Mom_e_y(i,j-1,k-1)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i,j-1,k)      =Mom_e_y(i,j-1,k)      + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i,j-1,k+1)    =Mom_e_y(i,j-1,k+1)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i,j-1,k+2)    =Mom_e_y(i,j-1,k+2)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_y(i,j,k-1)      =Mom_e_y(i,j,k-1)      + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (2.0-distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_y(i,j,k)        =Mom_e_y(i,j,k)        + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (2.0-distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_y(i,j,k+1)      =Mom_e_y(i,j,k+1)      + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (2.0-distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_y(i,j,k+2)      =Mom_e_y(i,j,k+2)      + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (2.0-distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      Mom_e_y(i,j+1,k-1)    =Mom_e_y(i,j+1,k-1)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0+distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_y(i,j+1,k)      =Mom_e_y(i,j+1,k)      + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0+distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_y(i,j+1,k+1)    =Mom_e_y(i,j+1,k+1)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0+distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_y(i,j+1,k+2)    =Mom_e_y(i,j+1,k+2)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (1.0+distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      Mom_e_y(i,j+2,k-1)    =Mom_e_y(i,j+2,k-1)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (distance(1))      * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i,j+2,k)      =Mom_e_y(i,j+2,k)      + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (distance(1))      * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i,j+2,k+1)    =Mom_e_y(i,j+2,k+1)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (distance(1))      * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_y(i,j+2,k+2)    =Mom_e_y(i,j+2,k+2)    + mass(P(l)%sp)*P(l)%VY * (2.0-distance(0)) * (distance(1))      * (distance(2))        *vol2*weight(P(l)%sp)	


      Mom_e_z(i,j-1,k-1)    =Mom_e_z(i,j-1,k-1)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i,j-1,k)      =Mom_e_z(i,j-1,k)      + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i,j-1,k+1)    =Mom_e_z(i,j-1,k+1)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i,j-1,k+2)    =Mom_e_z(i,j-1,k+2)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      Mom_e_z(i,j,k-1)      =Mom_e_z(i,j,k-1)      + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (2.0-distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_z(i,j,k)        =Mom_e_z(i,j,k)        + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (2.0-distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_z(i,j,k+1)      =Mom_e_z(i,j,k+1)      + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (2.0-distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_z(i,j,k+2)      =Mom_e_z(i,j,k+2)      + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (2.0-distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      Mom_e_z(i,j+1,k-1)    =Mom_e_z(i,j+1,k-1)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0+distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_z(i,j+1,k)      =Mom_e_z(i,j+1,k)      + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0+distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_z(i,j+1,k+1)    =Mom_e_z(i,j+1,k+1)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0+distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      Mom_e_z(i,j+1,k+2)    =Mom_e_z(i,j+1,k+2)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (1.0+distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      Mom_e_z(i,j+2,k-1)    =Mom_e_z(i,j+2,k-1)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (distance(1))      * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i,j+2,k)      =Mom_e_z(i,j+2,k)      + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (distance(1))      * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i,j+2,k+1)    =Mom_e_z(i,j+2,k+1)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (distance(1))      * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      Mom_e_z(i,j+2,k+2)    =Mom_e_z(i,j+2,k+2)    + mass(P(l)%sp)*P(l)%VZ * (2.0-distance(0)) * (distance(1))      * (distance(2))        *vol2*weight(P(l)%sp)		



	!======================================================================================================================================================================


      !Third plane
      !======================================================================================================================================================================

      Mom_e_x(i+1,j-1,k-1)  =Mom_e_x(i+1,j-1,k-1)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j-1,k)    =Mom_e_x(i+1,j-1,k)    + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j-1,k+1)  =Mom_e_x(i+1,j-1,k+1)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j-1,k+2)  =Mom_e_x(i+1,j-1,k+2)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_x(i+1,j,k-1)    =Mom_e_x(i+1,j,k-1)    + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j,k)      =Mom_e_x(i+1,j,k)      + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j,k+1)    =Mom_e_x(i+1,j,k+1)    + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j,k+2)    =Mom_e_x(i+1,j,k+2)    + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)


      Mom_e_x(i+1,j+1,k-1)  =Mom_e_x(i+1,j+1,k-1)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j+1,k)    =Mom_e_x(i+1,j+1,k)    + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j+1,k+1)  =Mom_e_x(i+1,j+1,k+1)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j+1,k+2)  =Mom_e_x(i+1,j+1,k+2)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_x(i+1,j+2,k-1)  =Mom_e_x(i+1,j+2,k-1)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j+2,k)    =Mom_e_x(i+1,j+2,k)    + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j+2,k+1)  =Mom_e_x(i+1,j+2,k+1)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+1,j+2,k+2)  =Mom_e_x(i+1,j+2,k+2)  + mass(P(l)%sp)*P(l)%VX * (1.0+distance(0)) * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)





      Mom_e_y(i+1,j-1,k-1)  =Mom_e_y(i+1,j-1,k-1)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j-1,k)    =Mom_e_y(i+1,j-1,k)    + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j-1,k+1)  =Mom_e_y(i+1,j-1,k+1)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j-1,k+2)  =Mom_e_y(i+1,j-1,k+2)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_y(i+1,j,k-1)    =Mom_e_y(i+1,j,k-1)    + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j,k)      =Mom_e_y(i+1,j,k)      + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j,k+1)    =Mom_e_y(i+1,j,k+1)    + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j,k+2)    =Mom_e_y(i+1,j,k+2)    + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_y(i+1,j+1,k-1)  =Mom_e_y(i+1,j+1,k-1)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j+1,k)    =Mom_e_y(i+1,j+1,k)    + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j+1,k+1)  =Mom_e_y(i+1,j+1,k+1)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j+1,k+2)  =Mom_e_y(i+1,j+1,k+2)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_y(i+1,j+2,k-1)  =Mom_e_y(i+1,j+2,k-1)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j+2,k)    =Mom_e_y(i+1,j+2,k)    + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j+2,k+1)  =Mom_e_y(i+1,j+2,k+1)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+1,j+2,k+2)  =Mom_e_y(i+1,j+2,k+2)  + mass(P(l)%sp)*P(l)%VY * (1.0+distance(0)) * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)


      Mom_e_z(i+1,j-1,k-1)  =Mom_e_z(i+1,j-1,k-1)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j-1,k)    =Mom_e_z(i+1,j-1,k)    + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j-1,k+1)  =Mom_e_z(i+1,j-1,k+1)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j-1,k+2)  =Mom_e_z(i+1,j-1,k+2)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_z(i+1,j,k-1)    =Mom_e_z(i+1,j,k-1)    + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j,k)      =Mom_e_z(i+1,j,k)      + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j,k+1)    =Mom_e_z(i+1,j,k+1)    + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j,k+2)    =Mom_e_z(i+1,j,k+2)    + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_z(i+1,j+1,k-1)  =Mom_e_z(i+1,j+1,k-1)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j+1,k)    =Mom_e_z(i+1,j+1,k)    + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j+1,k+1)  =Mom_e_z(i+1,j+1,k+1)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j+1,k+2)  =Mom_e_z(i+1,j+1,k+2)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_z(i+1,j+2,k-1)  =Mom_e_z(i+1,j+2,k-1)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j+2,k)    =Mom_e_z(i+1,j+2,k)    + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j+2,k+1)  =Mom_e_z(i+1,j+2,k+1)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+1,j+2,k+2)  =Mom_e_z(i+1,j+2,k+2)  + mass(P(l)%sp)*P(l)%VZ * (1.0+distance(0)) * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)	

      !======================================================================================================================================================================

      !Fourth plane
      !======================================================================================================================================================================

      Mom_e_x(i+2,j-1,k-1)  =Mom_e_x(i+2,j-1,k-1)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j-1,k)    =Mom_e_x(i+2,j-1,k)    + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j-1,k+1)  =Mom_e_x(i+2,j-1,k+1)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j-1,k+2)  =Mom_e_x(i+2,j-1,k+2)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_x(i+2,j,k-1)    =Mom_e_x(i+2,j,k-1)    + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j,k)      =Mom_e_x(i+2,j,k)      + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j,k+1)    =Mom_e_x(i+2,j,k+1)    + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j,k+2)    =Mom_e_x(i+2,j,k+2)    + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_x(i+2,j+1,k-1)  =Mom_e_x(i+2,j+1,k-1)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j+1,k)    =Mom_e_x(i+2,j+1,k)    + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j+1,k+1)  =Mom_e_x(i+2,j+1,k+1)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j+1,k+2)  =Mom_e_x(i+2,j+1,k+2)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_x(i+2,j+2,k-1)  =Mom_e_x(i+2,j+2,k-1)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j+2,k)    =Mom_e_x(i+2,j+2,k)    + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j+2,k+1)  =Mom_e_x(i+2,j+2,k+1)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_x(i+2,j+2,k+2)  =Mom_e_x(i+2,j+2,k+2)  + mass(P(l)%sp)*P(l)%VX * (distance(0))     * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)





      Mom_e_y(i+2,j-1,k-1)  =Mom_e_y(i+2,j-1,k-1)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j-1,k)    =Mom_e_y(i+2,j-1,k)    + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j-1,k+1)  =Mom_e_y(i+2,j-1,k+1)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j-1,k+2)  =Mom_e_y(i+2,j-1,k+2)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_y(i+2,j,k-1)    =Mom_e_y(i+2,j,k-1)    + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j,k)      =Mom_e_y(i+2,j,k)      + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j,k+1)    =Mom_e_y(i+2,j,k+1)    + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j,k+2)    =Mom_e_y(i+2,j,k+2)    + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_y(i+2,j+1,k-1)  =Mom_e_y(i+2,j+1,k-1)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j+1,k)    =Mom_e_y(i+2,j+1,k)    + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j+1,k+1)  =Mom_e_y(i+2,j+1,k+1)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j+1,k+2)  =Mom_e_y(i+2,j+1,k+2)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_y(i+2,j+2,k-1)  =Mom_e_y(i+2,j+2,k-1)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j+2,k)    =Mom_e_y(i+2,j+2,k)    + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j+2,k+1)  =Mom_e_y(i+2,j+2,k+1)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_y(i+2,j+2,k+2)  =Mom_e_y(i+2,j+2,k+2)  + mass(P(l)%sp)*P(l)%VY * (distance(0))     * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)


      Mom_e_z(i+2,j-1,k-1)  =Mom_e_z(i+2,j-1,k-1)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j-1,k)    =Mom_e_z(i+2,j-1,k)    + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j-1,k+1)  =Mom_e_z(i+2,j-1,k+1)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j-1,k+2)  =Mom_e_z(i+2,j-1,k+2)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_z(i+2,j,k-1)    =Mom_e_z(i+2,j,k-1)    + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j,k)      =Mom_e_z(i+2,j,k)      + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j,k+1)    =Mom_e_z(i+2,j,k+1)    + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j,k+2)    =Mom_e_z(i+2,j,k+2)    + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_z(i+2,j+1,k-1)  =Mom_e_z(i+2,j+1,k-1)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j+1,k)    =Mom_e_z(i+2,j+1,k)    + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j+1,k+1)  =Mom_e_z(i+2,j+1,k+1)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j+1,k+2)  =Mom_e_z(i+2,j+1,k+2)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      Mom_e_z(i+2,j+2,k-1)  =Mom_e_z(i+2,j+2,k-1)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j+2,k)    =Mom_e_z(i+2,j+2,k)    + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j+2,k+1)  =Mom_e_z(i+2,j+2,k+1)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      Mom_e_z(i+2,j+2,k+2)  =Mom_e_z(i+2,j+2,k+2)  + mass(P(l)%sp)*P(l)%VZ * (distance(0))     * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp) 	


      !======================================================================================================================================================================


ELSE


      if(i>i1 .OR. j>j1 .OR. k>k1 .OR. i<i0 .OR. j<j0 .OR. k<k0) then


				if(i==i1+1 .AND. j<j1+1 .AND. k<k1+1 ) then

					Mom_e_x(i,j,k)  =Mom_e_x(i,j,k) + mass(P(l)%sp)*P(l)%VX*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					Mom_e_x(i,j+1,k) =Mom_e_x(i,j+1,k) + mass(P(l)%sp)*P(l)%VX*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
					Mom_e_x(i,j,k+1) =Mom_e_x(i,j,k+1) + mass(P(l)%sp)*P(l)%VX*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
					Mom_e_x(i,j+1,k+1) =Mom_e_x(i,j+1,k+1) +mass(P(l)%sp)*P(l)%VX*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)


					Mom_e_y(i,j,k)  =Mom_e_y(i,j,k) + mass(P(l)%sp)*P(l)%VY*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					Mom_e_y(i,j+1,k) =Mom_e_y(i,j+1,k) + mass(P(l)%sp)*P(l)%VY*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
					Mom_e_y(i,j,k+1) =Mom_e_y(i,j,k+1) + mass(P(l)%sp)*P(l)%VY*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
					Mom_e_y(i,j+1,k+1) =Mom_e_y(i,j+1,k+1) +mass(P(l)%sp)*P(l)%VY*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)


					Mom_e_z(i,j,k)  =Mom_e_z(i,j,k) + mass(P(l)%sp)*P(l)%VZ*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					Mom_e_z(i,j+1,k) =Mom_e_z(i,j+1,k) + mass(P(l)%sp)*P(l)%VZ*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
					Mom_e_z(i,j,k+1) =Mom_e_z(i,j,k+1) + mass(P(l)%sp)*P(l)%VZ*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
					Mom_e_z(i,j+1,k+1) =Mom_e_z(i,j+1,k+1) +mass(P(l)%sp)*P(l)%VZ*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)

				endif

				if(j==j1+1 .AND. (i<i1+1) .AND. (k<k1+1) ) then	
		
					  Mom_e_x(i,j,k) =Mom_e_x(i,j,k) +mass(P(l)%sp)*P(l)%VX*(1.-distance(0))*(1.-distance(1))*(1.-distance(2))*vol*weight(P(l)%sp)	
					  Mom_e_x(i+1,j,k) =Mom_e_x(i+1,j,k) +mass(P(l)%sp)*P(l)%VX*(distance(0))  * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_x(i,j,k+1) =Mom_e_x(i,j,k+1) +mass(P(l)%sp)*P(l)%VX*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
	
					
					  Mom_e_y(i,j,k) =Mom_e_y(i,j,k) +mass(P(l)%sp)*P(l)%VY*(1.-distance(0))*(1.-distance(1))*(1.-distance(2))*vol*weight(P(l)%sp)	
					  Mom_e_y(i+1,j,k) =Mom_e_y(i+1,j,k) +mass(P(l)%sp)*P(l)%VY*(distance(0))  * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_y(i,j,k+1) =Mom_e_y(i,j,k+1) +mass(P(l)%sp)*P(l)%VY*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp) 	

					  Mom_e_z(i,j,k) =Mom_e_z(i,j,k) +mass(P(l)%sp)*P(l)%VZ*(1.-distance(0))*(1.-distance(1))*(1.-distance(2))*vol*weight(P(l)%sp)	
					  Mom_e_z(i+1,j,k) =Mom_e_z(i+1,j,k) +mass(P(l)%sp)*P(l)%VZ*(distance(0))  * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_z(i,j,k+1) =Mom_e_z(i,j,k+1) +mass(P(l)%sp)*P(l)%VZ*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp) 	


				endif

				if(k==k1+1 .AND.  i<i1+1 .AND. j<j1+1) then

					  Mom_e_x(i,j,k) =Mom_e_x(i,j,k)  +mass(P(l)%sp)*P(l)%VX*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_x(i+1,j,k) =Mom_e_x(i+1,j,k)  +mass(P(l)%sp)*P(l)%VX*(distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_x(i,j+1,k) =Mom_e_x(i,j+1,k) +mass(P(l)%sp)*P(l)%VX*(1.-distance(0)) * (distance(1))  * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_x(i+1,j+1,k)  =Mom_e_x(i+1,j+1,k)  +mass(P(l)%sp)*P(l)%VX*distance(0) * (distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
		
					  Mom_e_y(i,j,k) =Mom_e_y(i,j,k)  +mass(P(l)%sp)*P(l)%VY*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_y(i+1,j,k) =Mom_e_y(i+1,j,k)  +mass(P(l)%sp)*P(l)%VY*(distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_y(i,j+1,k) =Mom_e_y(i,j+1,k) +mass(P(l)%sp)*P(l)%VY*(1.-distance(0)) * (distance(1))  * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_y(i+1,j+1,k)  =Mom_e_y(i+1,j+1,k)  +mass(P(l)%sp)*P(l)%VY*distance(0) * (distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)

					  Mom_e_z(i,j,k) =Mom_e_z(i,j,k)  +mass(P(l)%sp)*P(l)%VZ*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_z(i+1,j,k) =Mom_e_z(i+1,j,k)  +mass(P(l)%sp)*P(l)%VZ*(distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  Mom_e_z(i,j+1,k) =Mom_e_z(i,j+1,k) +mass(P(l)%sp)*P(l)%VZ*(1.-distance(0)) * (distance(1))  * (1.-distance(2))*vol*weight(P(l)%sp)

					  Mom_e_z(i+1,j+1,k)  =Mom_e_z(i+1,j+1,k)  +mass(P(l)%sp)*P(l)%VZ*distance(0) * (distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)	


				endif


				if(i<i0 .OR. j<j0 .OR. k<k0) then

					  write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
					  write(2000+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart,P(l)%X,P(l)%Y,P(l)%Z
					  write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

				endif


	 else
					  write(2100+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
					  write(2100+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart,P(l)%X,P(l)%Y,P(l)%Z
					  write(2100+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

	 endif



ENDIF




end subroutine  ChargeProjection_Second_Order_momentum


subroutine ChargeProjection_Second_OrderAll()
	
	use Parallel
	implicit none



	integer :: i=0,j=0,k=0,l
	real :: distance(0:2)
	real :: vol=0
	real :: vol2=0
do l=1, NPART

	i=P(l)%X/delta(0)
	j=P(l)%Y/delta(1)
	k=P(l)%Z/delta(2)

	distance(0)=1.*P(l)%X/delta(0)-i
 	distance(1)=1.*P(l)%Y/delta(1)-j
 	distance(2)=1.*P(l)%Z/delta(2)-k
 	
	vol=1.0/(delta(0)*delta(1)*delta(2))
	vol2=1.0/(64.0*delta(0)*delta(1)*delta(2))

!vol2=1.0/8.0
if(i<0 .OR. j<0 .OR. k<0  .OR. i>Nxp .OR. j>Nyp .OR. k>Nzp) then

	write(2500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
	write(2500+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart
	write(2500+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"



endif


IF(i<i1+1 .AND. j<j1+1 .AND. k<k1+1 .AND. i>i0-1 .AND. j>j0-1 .AND. k>k0-1) THEN


      !Main charge Projection routine

      !First plane
      !======================================================================================================================================================================
      rho_Sec(i-1,j-1,k-1)  =rho_Sec(i-1,j-1,k-1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j-1,k)    =rho_Sec(i-1,j-1,k)    +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j-1,k+1)  =rho_Sec(i-1,j-1,k+1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j-1,k+2)  =rho_Sec(i-1,j-1,k+2)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i-1,j,k-1)    =rho_Sec(i-1,j,k-1)    +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j,k)      =rho_Sec(i-1,j,k)      +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j,k+1)    =rho_Sec(i-1,j,k+1)    +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j,k+2)    =rho_Sec(i-1,j,k+2)    +charge(P(l)%sp) * (1.0-distance(0)) * (2.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i-1,j+1,k-1)  =rho_Sec(i-1,j+1,k-1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+1,k)    =rho_Sec(i-1,j+1,k)    +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+1,k+1)  =rho_Sec(i-1,j+1,k+1)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+1,k+2)  =rho_Sec(i-1,j+1,k+2)  +charge(P(l)%sp) * (1.0-distance(0)) * (1.0+distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i-1,j+2,k-1)  =rho_Sec(i-1,j+2,k-1)  +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+2,k)    =rho_Sec(i-1,j+2,k)    +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+2,k+1)  =rho_Sec(i-1,j+2,k+1)  +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i-1,j+2,k+2)  =rho_Sec(i-1,j+2,k+2)  +charge(P(l)%sp) * (1.0-distance(0)) * (distance(1))     * (distance(2))        *vol2*weight(P(l)%sp)
      !======================================================================================================================================================================

      !Second plane
      !======================================================================================================================================================================

      rho_Sec(i,j-1,k-1)    =rho_Sec(i,j-1,k-1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j-1,k)      =rho_Sec(i,j-1,k)      +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j-1,k+1)    =rho_Sec(i,j-1,k+1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j-1,k+2)    =rho_Sec(i,j-1,k+2)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0-distance(1)) * (distance(2))        *vol2*weight(P(l)%sp)

      rho_Sec(i,j,k-1)      =rho_Sec(i,j,k-1)      +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j,k)        =rho_Sec(i,j,k)        +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j,k+1)      =rho_Sec(i,j,k+1)      +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j,k+2)      =rho_Sec(i,j,k+2)      +charge(P(l)%sp) * (2.0-distance(0)) * (2.0-distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      rho_Sec(i,j+1,k-1)    =rho_Sec(i,j+1,k-1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (1.0-distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j+1,k)      =rho_Sec(i,j+1,k)      +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (2.0-distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j+1,k+1)    =rho_Sec(i,j+1,k+1)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (1.0+distance(2)  ) *vol2*weight(P(l)%sp)
      rho_Sec(i,j+1,k+2)    =rho_Sec(i,j+1,k+2)    +charge(P(l)%sp) * (2.0-distance(0)) * (1.0+distance(1))  * (distance(2))       *vol2*weight(P(l)%sp)

      rho_Sec(i,j+2,k-1)    =rho_Sec(i,j+2,k-1)    +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (1.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j+2,k)      =rho_Sec(i,j+2,k)      +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (2.0-distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j+2,k+1)    =rho_Sec(i,j+2,k+1)    +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (1.0+distance(2)  )  *vol2*weight(P(l)%sp)
      rho_Sec(i,j+2,k+2)    =rho_Sec(i,j+2,k+2)    +charge(P(l)%sp) * (2.0-distance(0)) * (distance(1))      * (distance(2))        *vol2*weight(P(l)%sp)
      !======================================================================================================================================================================


      !Third plane
      !======================================================================================================================================================================

      rho_Sec(i+1,j-1,k-1)  =rho_Sec(i+1,j-1,k-1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j-1,k)    =rho_Sec(i+1,j-1,k)    +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j-1,k+1)  =rho_Sec(i+1,j-1,k+1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j-1,k+2)  =rho_Sec(i+1,j-1,k+2)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+1,j,k-1)    =rho_Sec(i+1,j,k-1)    +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j,k)      =rho_Sec(i+1,j,k)      +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j,k+1)    =rho_Sec(i+1,j,k+1)    +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j,k+2)    =rho_Sec(i+1,j,k+2)    +charge(P(l)%sp) * (1.0+distance(0)) * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+1,j+1,k-1)  =rho_Sec(i+1,j+1,k-1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+1,k)    =rho_Sec(i+1,j+1,k)    +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+1,k+1)  =rho_Sec(i+1,j+1,k+1)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+1,k+2)  =rho_Sec(i+1,j+1,k+2)  +charge(P(l)%sp) * (1.0+distance(0)) * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+1,j+2,k-1)  =rho_Sec(i+1,j+2,k-1)  +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+2,k)    =rho_Sec(i+1,j+2,k)    +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+2,k+1)  =rho_Sec(i+1,j+2,k+1)  +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+1,j+2,k+2)  =rho_Sec(i+1,j+2,k+2)  +charge(P(l)%sp) * (1.0+distance(0)) * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)
      !======================================================================================================================================================================

      !Fourth plane
      !======================================================================================================================================================================

      rho_Sec(i+2,j-1,k-1)  =rho_Sec(i+2,j-1,k-1)  +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j-1,k)    =rho_Sec(i+2,j-1,k)    +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j-1,k+1)  =rho_Sec(i+2,j-1,k+1)  +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j-1,k+2)  =rho_Sec(i+2,j-1,k+2)  +charge(P(l)%sp) * (distance(0))     * (1.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+2,j,k-1)    =rho_Sec(i+2,j,k-1)    +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j,k)      =rho_Sec(i+2,j,k)      +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j,k+1)    =rho_Sec(i+2,j,k+1)    +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j,k+2)    =rho_Sec(i+2,j,k+2)    +charge(P(l)%sp) * (distance(0))     * (2.0-distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+2,j+1,k-1)  =rho_Sec(i+2,j+1,k-1)  +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+1,k)    =rho_Sec(i+2,j+1,k)    +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+1,k+1)  =rho_Sec(i+2,j+1,k+1)  +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+1,k+2)  =rho_Sec(i+2,j+1,k+2)  +charge(P(l)%sp) * (distance(0))     * (1.0+distance(1)) * (distance(2) )        *vol2*weight(P(l)%sp)

      rho_Sec(i+2,j+2,k-1)  =rho_Sec(i+2,j+2,k-1)  +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (1.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+2,k)    =rho_Sec(i+2,j+2,k)    +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (2.0-distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+2,k+1)  =rho_Sec(i+2,j+2,k+1)  +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (1.0+distance(2)  )   *vol2*weight(P(l)%sp)
      rho_Sec(i+2,j+2,k+2)  =rho_Sec(i+2,j+2,k+2)  +charge(P(l)%sp) * (distance(0))     * (distance(1))     * (distance(2) )        *vol2*weight(P(l)%sp)

      !======================================================================================================================================================================


ELSE


      if(i>i1 .OR. j>j1 .OR. k>k1 .OR. i<i0 .OR. j<j0 .OR. k<k0) then


				if(i==i1+1 .AND. j<j1+1 .AND. k<k1+1 ) then

					rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					rho_Sec(i,j+1,k)    =rho_Sec(i,j+1,k)    +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
					rho_Sec(i,j,k+1)    =rho_Sec(i,j,k+1)    +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
					rho_Sec(i,j+1,k+1)  =rho_Sec(i,j+1,k+1)  +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l)%sp)

				endif

				if(j==j1+1 .AND. (i<i1+1) .AND. (k<k1+1) ) then	
		
					  rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)	
					  rho_Sec(i+1,j,k)    =rho_Sec(i+1,j,k)    +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i,j,k+1)    =rho_Sec(i,j,k+1)    +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l)%sp)
	
				endif

				if(k==k1+1 .AND.  i<i1+1 .AND. j<j1+1) then

					  rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l)%sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i+1,j,k)    =rho_Sec(i+1,j,k)    +charge(P(l)%sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i,j+1,k)    =rho_Sec(i,j+1,k)    +charge(P(l)%sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
					  rho_Sec(i+1,j+1,k)  =rho_Sec(i+1,j+1,k)  +charge(P(l)%sp)*distance(0)      * (distance(1))    * (1.-distance(2))*vol*weight(P(l)%sp)
		
				endif


				if(i<i0 .OR. j<j0 .OR. k<k0) then

					  write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
					  write(2000+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart,P(l)%X,P(l)%Y,P(l)%Z
					  write(2000+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"

				endif


	 else
					  write(2100+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"
					  write(2100+rank,*) rank,iter,l,i,j,k,i0,j0,k0,i1,j1,k1, distance(0),distance(1),distance(2),P(l)%X,P(l)%Y,P(l)%Z,P(l)%IdentPart,P(l)%X,P(l)%Y,P(l)%Z
					  write(2100+rank,*) "++++++++++++++++++++++++++++++++++++++++++++++++"




				!rho_Sec(i,j,k)      =rho_Sec(i,j,k)      +charge(P(l).sp)*(1.-distance(0)) * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j,k)    =rho_Sec(i+1,j,k)    +charge(P(l).sp)*(distance(0))    * (1.-distance(1)) * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i,j+1,k)    =rho_Sec(i,j+1,k)    +charge(P(l).sp)*(1.-distance(0)) * (distance(1))    * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j+1,k)  =rho_Sec(i+1,j+1,k)  +charge(P(l).sp)*distance(0)      * (distance(1))    * (1.-distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i,j,k+1)    =rho_Sec(i,j,k+1)    +charge(P(l).sp)*(1.-distance(0)) * (1.-distance(1)) * (distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j,k+1)  =rho_Sec(i+1,j,k+1)  +charge(P(l).sp)*(distance(0))    * (1.-distance(1)) * (distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i,j+1,k+1)  =rho_Sec(i,j+1,k+1)  +charge(P(l).sp)*(1.-distance(0)) * (distance(1))    * (distance(2))*vol*weight(P(l).sp)
				!rho_Sec(i+1,j+1,k+1)=rho_Sec(i+1,j+1,k+1)+charge(P(l).sp)*(distance(0))    * (distance(1))    * (distance(2))*vol*weight(P(l).sp)


	 endif



ENDIF

end do


end subroutine  ChargeProjection_Second_OrderAll


subroutine ChargeExchange_3planes(Arr)
	
	use Parallel
	implicit none
	include 'mpif.h'

        real,intent( inout ) :: Arr(i0-3:i1+3  , j0-3:j1+3, k0-3:k1+3) 


	!---------------------------X direction-------------------------------------------------------------
	if(mod(indexx,2)==0) then	

		if(neig(1)>-1) CALL MPI_SEND(Arr(i1+1,j0-3,k0-3), 1,yz30, neig(1),51, MPI_COMM_WORLD, info)
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-3), 1,yz30, neig(0),51, MPI_COMM_WORLD, status, info)

		if(neig(1)>-1) CALL MPI_SEND(Arr(i1+2,j0-3,k0-3), 1,yz30, neig(1),51, MPI_COMM_WORLD, info)
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-2,j0-3,k0-3), 1,yz30, neig(0),51, MPI_COMM_WORLD, status, info)


		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+3,j0-3,k0-3), 1,yz30, neig(1),51, MPI_COMM_WORLD, status, info)
		if(neig(0)>-1) CALL MPI_SEND(Arr(i0-1,j0-3,k0-3), 1,yz30, neig(0),51, MPI_COMM_WORLD, info)



	else
		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-3), 1,yz30, neig(0),51, MPI_COMM_WORLD, status, info)
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1+1,j0-3,k0-3), 1,yz30, neig(1),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_RECV(Arr(i0-2,j0-3,k0-3), 1,yz30, neig(0),51, MPI_COMM_WORLD, status, info)
		if(neig(1)>-1) CALL MPI_SEND(Arr(i1+2,j0-3,k0-3), 1,yz30, neig(1),51, MPI_COMM_WORLD, info)

		if(neig(0)>-1) CALL MPI_SEND(Arr(i0-1,j0-3,k0-3), 1,yz30, neig(0),51, MPI_COMM_WORLD, info)
		if(neig(1)>-1) CALL MPI_RECV(Arr(i1+3,j0-3,k0-3), 1,yz30, neig(1),51, MPI_COMM_WORLD, status, info)



	endif


	!---------------------------Y direction-------------------------------------------------------------


	if(mod(indexy,2)==0) then
		
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-3,j1+1,k0-3), 1,xz30, neig(3),51, MPI_COMM_WORLD, info)
		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-3), 1,xz30, neig(2),51, MPI_COMM_WORLD, status, info)

		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-3,j1+2,k0-3), 1,xz30, neig(3),51, MPI_COMM_WORLD, info)
		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-3,j0-2,k0-3), 1,xz30, neig(2),51, MPI_COMM_WORLD, status, info)

	        if(neig(3)>-1) CALL MPI_RECV(Arr(i0-3,j1+3,k0-3), 1,xz30, neig(3),51, MPI_COMM_WORLD, status, info)
		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-3,j0-1,k0-3), 1,xz30, neig(2),51, MPI_COMM_WORLD, info)

	
	else
		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-3), 1,xz30, neig(2),51, MPI_COMM_WORLD, status, info)
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-3,j1+1,k0-3), 1,xz30, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_RECV(Arr(i0-3,j0-2,k0-3), 1,xz30, neig(2),51, MPI_COMM_WORLD, status, info)
		if(neig(3)>-1) CALL MPI_SEND(Arr(i0-3,j1+2,k0-3), 1,xz30, neig(3),51, MPI_COMM_WORLD, info)

		if(neig(2)>-1) CALL MPI_SEND(Arr(i0-3,j0-1,k0-3), 1,xz30, neig(2),51, MPI_COMM_WORLD, info)
		if(neig(3)>-1) CALL MPI_RECV(Arr(i0-3,j1+3,k0-3), 1,xz30, neig(3),51, MPI_COMM_WORLD, status, info)



	endif
		

	!---------------------------Z direction-------------------------------------------------------------
	if(mod(indexz,2)==0) then
	
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-3,j0-3,k1+1), 1,xy30, neig(5),51, MPI_COMM_WORLD, info)
		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-3), 1,xy30, neig(4),51, MPI_COMM_WORLD, status, info)

		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-3,j0-3,k1+2), 1,xy30, neig(5),51, MPI_COMM_WORLD, info)
		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-2), 1,xy30, neig(4),51, MPI_COMM_WORLD, status, info)



		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k1+3), 1,xy30, neig(5),51, MPI_COMM_WORLD, status, info)
		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-3,j0-3,k0-1), 1,xy30, neig(4),51, MPI_COMM_WORLD, info)

	else

		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-3), 1,xy30, neig(4),51, MPI_COMM_WORLD, status, info)
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-3,j0-3,k1+1), 1,xy30, neig(5),51, MPI_COMM_WORLD, info)



		if(neig(4)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k0-2), 1,xy30, neig(4),51, MPI_COMM_WORLD, status, info)
		if(neig(5)>-1) CALL MPI_SEND(Arr(i0-3,j0-3,k1+2), 1,xy30, neig(5),51, MPI_COMM_WORLD, info)


		if(neig(4)>-1) CALL MPI_SEND(Arr(i0-3,j0-3,k0-1), 1,xy30, neig(4),51, MPI_COMM_WORLD, info)
		if(neig(5)>-1) CALL MPI_RECV(Arr(i0-3,j0-3,k1+3), 1,xy30, neig(5),51, MPI_COMM_WORLD, status, info)



	endif

	Arr(i0,:,:)=Arr(i0,:,:)+Arr(i0-3,:,:)
	Arr(i0+1,:,:)=Arr(i0+1,:,:)+Arr(i0-2,:,:)
	Arr(i1,:,:)=Arr(i1,:,:)+Arr(i1+3,:,:)


	Arr(:,j0,:)=Arr(:,j0,:)+Arr(:,j0-3,:)
	Arr(:,j0+1,:)=Arr(:,j0+1,:)+Arr(:,j0-2,:)
	Arr(:,j1,:)=Arr(:,j1,:)+Arr(:,j1+3,:)


	Arr(:,:,k0)=Arr(:,:,k0)+Arr(:,:,k0-3)
	Arr(:,:,k0+1)=Arr(:,:,k0+1)+Arr(:,:,k0-2)
	Arr(:,:,k1)=Arr(:,:,k1)+Arr(:,:,k1+3)





end subroutine  ChargeExchange_3planes

subroutine E_field_calculation15points()

  use Parallel
  implicit none


  integer  i,j,k
  real tempE1, tempE2, xp, xm


    Exm=0
    Eym=0
    Ezm=0
    do i=i0, i1
      do j=j0,j1
        do k =k0, k1
        
          Exm(i,j,k)=-1.0/3.0*(V(i+1,j,k)-V(i,j,k))/(delta(0))

          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j+1,k-1)-V(i,j+1,k-1))/(delta(0)))!
          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j+1,k)-V(i,j+1,k))/(delta(0)))
          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j+1,k+1)-V(i,j+1,k+1))/(delta(0)))


          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j,k-1)-V(i,j,k-1))/(delta(0)))
          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j,k+1)-V(i,j,k+1))/(delta(0)))


          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j-1,k-1)-V(i,j-1,k-1))/(delta(0)))
          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j-1,k)-V(i,j-1,k))/(delta(0)))
          Exm(i,j,k)=Exm(i,j,k)+(-1.0/12.0*(V(i+1,j-1,k+1)-V(i,j-1,k+1))/(delta(0)))

          
          Eym(i,j,k)=-1.0/3.0*(V(i,j+1,k)-V(i,j,k))/(delta(1))

          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i+1,j+1,k)-V(i+1,j,k))/(delta(1)))
          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i+1,j+1,k-1)-V(i+1,j,k-1))/(delta(1)))
          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i+1,j+1,k+1)-V(i+1,j,k+1))/(delta(1)))

          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i,j+1,k+1)-V(i,j,k+1))/(delta(1)))
          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i,j+1,k-1)-V(i,j,k-1))/(delta(1)))

          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i-1,j+1,k)-V(i-1,j,k))/(delta(1)))
          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i-1,j+1,k-1)-V(i-1,j,k-1))/(delta(1)))
          Eym(i,j,k)=Eym(i,j,k)+(-1.0/12.0*(V(i-1,j+1,k+1)-V(i-1,j,k+1))/(delta(1)))


          Ezm(i,j,k)=-1.0/3.0*(V(i,j,k+1)-V(i,j,k))/(delta(1))

          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i+1,j,k+1)-V(i+1,j,k))/(delta(2)))
          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i+1,j-1,k+1)-V(i+1,j-1,k))/(delta(2)))
          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i+1,j+1,k+1)-V(i+1,j+1,k))/(delta(2)))

          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i,j+1,k+1)-V(i,j+1,k))/(delta(2)))
          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i,j-1,k+1)-V(i,j-1,k))/(delta(2)))

          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i-1,j,k+1)-V(i-1,j,k))/(delta(2)))
          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i-1,j+1,k+1)-V(i-1,j+1,k))/(delta(2)))
          Ezm(i,j,k)=Ezm(i,j,k)+(-1.0/12.0*(V(i-1,j-1,k+1)-V(i-1,j-1,k))/(delta(2)))

 
        !Exm=0
    !Eym=0
    !Ezm=0

    !Exm(i0:i1,:,:)=-(V(i0+1:i1+1,:,:)-V(i0:i1,:,:))/(delta(0))
    !Eym(:,j0:j1,:)=-(V(:,j0+1:j1+1,:)-V(:,j0:j1,:))/(delta(1))
    !Ezm(:,:,k0:k1)=-(V(:,:,k0+1:k1+1)-V(:,:,k0:k1))/(delta(2))
 
        enddo
      enddo
    enddo

    do i=i0+1, i1-1
      do j=j0,j1
        do k =k0, k1
          if(Inzone(i,j,k) /= 0) then
            !X
            if(Inzone(i-1, j, k) == 0) then
              if(alphax(i-1, j, k) < 0.5) then
                xp = alphax(i-1,j,k)*delta(0)
                xm = delta(0)
                tempE1 = V(i, j,k)*xm/(xp*(xm+xp)) &
                       + V(i-1,j,k)*(xp-xm)/(xm*xp) &
                       - V(i-2,j,k)*xp/(xm*(xm+xp))
                tempE2 = (V(i,j,k)-V(i-1,j,k))/xp
                tempE1 = tempE1*-1
                tempE2 = tempE2*-1
                Exm(i-1,j,k) = (tempE2-tempE1)/(alphax(i-1,j,k)*delta(0))*(delta(0)*0.5)+tempE1
              else
                xp = alphax(i-1,j,k)*delta(0)
                xm = delta(0)
                tempE1 = V(i, j,k)*xm/(xp*(xm+xp)) &
                       + V(i-1,j,k)*(xp-xm)/(xm*xp) &
                       - V(i-2,j,k)*xp/(xm*(xm+xp))
                tempE2 = (V(i,j,k)-V(i-1,j,k))/xp
                tempE1 = tempE1*-1
                tempE2 = tempE2*-1
                Exm(i-1,j,k) = (tempE2-tempE1)/(alphax(i-1,j,k)*delta(0))*(delta(0)*0.5)+tempE1
                Exm(i,j,k) = (tempE2-tempE1)/(alphax(i-1,j,k)*delta(0))*(delta(0)*1.5)+tempE1
              end if
            else 
              if(Inzone(i+1,j,k) == 0) then
                if(alphax(i+1,j,k) > -0.5) then
                  xp = delta(0)
                  xm = -alphax(i+1,j,k)*delta(0)
                  tempE1 = V(i+2, j,k)*xm/(xp*(xm+xp)) &
                         + V(i+1,j,k)*(xp-xm)/(xm*xp) &
                         - V(i,j,k)*xp/(xm*(xm+xp))
                  tempE2 = (V(i+1,j,k)-V(i,j,k))/xm
                  tempE1 = tempE1*-1
                  tempE2 = tempE2*-1
                  Exm(i,j,k) = (tempE1-tempE2)/(xm)*(delta(0)*-0.5)+tempE1
                else
                  xp = delta(0)
                  xm = -alphax(i+1,j,k)*delta(0)
                  tempE1 = V(i+2, j,k)*xm/(xp*(xm+xp)) &
                         + V(i+1,j,k)*(xp-xm)/(xm*xp) &
                         - V(i,j,k)*xp/(xm*(xm+xp))
                  tempE2 = (V(i+1,j,k)-V(i,j,k))/xm
                  tempE1 = tempE1*-1
                  tempE2 = tempE2*-1
                  Exm(i,j,k) = (tempE1-tempE2)/(xm)*(delta(0)*-0.5)+tempE1
                  Exm(i-1,j,k) = (tempE1-tempE2)/(xm)*(delta(0)*-1.5)+tempE1
                end if
              end if
            end if
            !Y
            
            !Z
          end if
        enddo
      enddo
    enddo
    if(3==3) then
    do i=i0, i1
      do j=j0+1,j1-1
        do k =k0, k1
          if(Inzone(i,j,k) /= 0) then
            if(Inzone(i, j-1, k) == 0) then
              if(alphay(i, j-1, k) < 0.5) then
                xp = alphay(i,j-1,k)*delta(1)
                xm = delta(1)
                if(xp<1e-7) continue
                tempE1 = V(i, j,k)*xm/(xp*(xm+xp)) &
                       + V(i,j-1,k)*(xp-xm)/(xm*xp) &
                       - V(i,j-2,k)*xp/(xm*(xm+xp))
                tempE2 = (V(i,j,k)-V(i,j-1,k))/xp
                tempE1 = tempE1*-1
                tempE2 = tempE2*-1
                Eym(i,j-1,k) = (tempE2-tempE1)/(alphay(i,j-1,k)*delta(1))*(delta(1)*0.5)+tempE1
              else
                xp = alphay(i,j-1,k)*delta(1)
                xm = delta(1)
                if(xp<1e-7) continue
                tempE1 = V(i,j,k)*xm/(xp*(xm+xp)) &
                       + V(i,j-1,k)*(xp-xm)/(xm*xp) &
                       - V(i,j-2,k)*xp/(xm*(xm+xp))
                tempE2 = (V(i,j,k)-V(i,j-1,k))/xp
                tempE1 = tempE1*-1
                tempE2 = tempE2*-1
                Eym(i,j-1,k) = (tempE2-tempE1)/xp*(delta(1)*0.5)+tempE1
                Eym(i,j,k) = (tempE2-tempE1)/xp*(delta(1)*1.5)+tempE1
              end if
            else 
              if(Inzone(i,j+1,k) == 0) then
                if(abs(alphay(i,j+1,k)) < 0.5) then

                  xp = delta(1)
                  xm = abs(alphay(i,j+1,k)*delta(1))
                  if(xm<1e-7) continue
                  tempE1 = V(i,j+2,k)*xm/(xp*(xm+xp)) &
                         + V(i,j+1,k)*(xp-xm)/(xm*xp) &
                         - V(i,j,k)*xp/(xm*(xm+xp))
                  tempE2 = (V(i,j+1,k)-V(i,j,k))/xm
                  tempE1 = tempE1*-1
                  tempE2 = tempE2*-1
                  Eym(i,j,k) = (tempE1-tempE2)/(xm)*(delta(1)*-0.5)+tempE1
                else
                  xp = delta(1)
                  xm = abs(alphay(i,j+1,k)*delta(1))
                  if(xm<1e-7) continue
                  tempE1 = V(i,j+2,k)*xm/(xp*(xm+xp)) &
                         + V(i,j+1,k)*(xp-xm)/(xm*xp) &
                         - V(i,j,k)*xp/(xm*(xm+xp))
                  tempE2 = (V(i,j+1,k)-V(i,j,k))/xm
                  tempE1 = tempE1*-1
                  tempE2 = tempE2*-1
                  Eym(i,j,k) = (tempE1-tempE2)/(xm)*(delta(1)*-0.5)+tempE1
                  Eym(i,j-1,k) = (tempE1-tempE2)/(xm)*(delta(1)*-1.5)+tempE1
                end if
              end if
            end if
          end if
        enddo
      enddo
    enddo
  endif

   ! write(*,*) "OK y", rank
    
    if(3==3) then
    do i=i0, i1
      do j=j0,j1
        do k =k0+1, k1-1
          if(Inzone(i,j,k) /= 0) then
            if(Inzone(i, j, k-1) == 0) then
              if(alphaz(i, j, k-1) < 0.5) then
                xp = alphaz(i,j,k-1)*delta(2)
                xm = delta(2)
                if(xp<1e-7) continue
                tempE1 = V(i, j,k)*xm/(xp*(xm+xp)) &
                       + V(i,j,k-1)*(xp-xm)/(xm*xp) &
                       - V(i,j,k-2)*xp/(xm*(xm+xp))
                tempE2 = (V(i,j,k)-V(i,j,k-1))/xp
                tempE1 = tempE1*-1
                tempE2 = tempE2*-1
                Ezm(i,j,k-1) = (tempE2-tempE1)/(alphaz(i,j,k-1)*delta(2))*(delta(2)*0.5)+tempE1
              else
                xp = alphaz(i,j,k-1)*delta(2)
                xm = delta(2)
                if(xp<1e-7) continue
                tempE1 = V(i,j,k)*xm/(xp*(xm+xp)) &
                       + V(i,j,k-1)*(xp-xm)/(xm*xp) &
                       - V(i,j,k-2)*xp/(xm*(xm+xp))
                tempE2 = (V(i,j,k)-V(i,j,k-1))/xp
                tempE1 = tempE1*-1
                tempE2 = tempE2*-1
                Ezm(i,j,k-1) = (tempE2-tempE1)/(alphaz(i,j,k-1)*delta(2))*(delta(2)*0.5)+tempE1
                Ezm(i,j,k) = (tempE2-tempE1)/(alphaz(i,j,k-1)*delta(2))*(delta(2)*1.5)+tempE1
              end if
            else 
              if(Inzone(i,j,k+1) == 0) then
                if(alphaz(i,j,k+1) > -0.5) then
                  xp = delta(2)
                  xm = -alphaz(i,j,k+1)*delta(2)
                  if(xm<1e-7) continue
                  tempE1 = V(i,j,k+2)*xm/(xp*(xm+xp)) &
                         + V(i,j,k+1)*(xp-xm)/(xm*xp) &
                         - V(i,j,k)*xp/(xm*(xm+xp))
                  tempE2 = (V(i,j,k+1)-V(i,j,k))/xm
                  tempE1 = tempE1*-1
                  tempE2 = tempE2*-1
                  Ezm(i,j,k) = (tempE1-tempE2)/(xm)*(delta(2)*-0.5)+tempE1
                else
                  xp = delta(2)
                  xm = -alphaz(i,j,k+1)*delta(2)
                  if(xm<1e-7) continue
                  tempE1 = V(i,j,k+2)*xm/(xp*(xm+xp)) &
                         + V(i,j,k+1)*(xp-xm)/(xm*xp) &
                         - V(i,j,k)*xp/(xm*(xm+xp))
                  tempE2 = (V(i,j,k+1)-V(i,j,k))/xm
                  tempE1 = tempE1*-1
                  tempE2 = tempE2*-1
                  Ezm(i,j,k) = (tempE1-tempE2)/(xm)*(delta(2)*-0.5)+tempE1
                  Ezm(i,j,k-1) = (tempE1-tempE2)/(xm)*(delta(2)*-1.5)+tempE1
                end if
              end if
            end if
          end if
        enddo
      enddo
    enddo
  end if
  
  !write(*,*) "OK z", rank



end subroutine E_field_calculation15points


subroutine NI_emission_rate()

    use Parallel
	implicit none

	real:: s_con
    real:: s_flat


        if(The_Object(4)%types==3) then
        
            s_con=3.14*sqrt (  (The_Object(4)%width(0)*2)**2 +(The_Object(4)%width(1)-The_Object(4)%width(2))**2)*(The_Object(4)%width(1)+The_Object(4)%width(2))
            NI_rate_from_cone=(  NI_emission*s_con*dt/(abs(charge(9))*weight(9)  )      )
        else
            
            write(str3,'(i7)') iter
            lgc=len_trim(adjustl(str3))
            str='ERROR_iteration_'//str3(8-lgc:7)//'.txt'
            open(unit=65, file=str, status="unknown")
            write(65,*) "ERROR, Problem with NI emmision rate calculation"
            close(65)
            Stop
                        
        endif 
       
        
        if(The_Object(1)%types==1) then
            
            s_flat=2*The_Object(1)%width(1)*2*The_Object(1)%width(2)-3.14*The_Object(4)%width(1)**2 
            NI_rate_from_flat=(  NI_emission* s_flat*dt/(abs(charge(5))*weight(5)  )      )
        
        else
            
            write(str3,'(i7)') iter
            lgc=len_trim(adjustl(str3))
            str='ERROR_iteration_'//str3(8-lgc:7)//'.txt'
            open(unit=65, file=str, status="unknown")
            write(65,*)  "ERROR, Problem with NI emmision rate calculation"
            close(65)
            Stop
            
        endif    
            
  
end subroutine NI_emission_rate

subroutine thermal_energy_slice()
  
  use Parallel
  implicit none
  integer :: l
  real :: v_module
  integer :: i_vel=0, i

  do l=1, NPART
    if(abs(P(l)%sp)<1e-5) then

      do i=1,nPosSliceVDF
        if(P(l)%X >= posSliceVDF(i) .AND. P(l)%X < (posSliceVDF(i) + delta(0))) then
          v_module= sqrt(P(l)%VX*P(l)%VX + P(l)%VY*P(l)%VY + P(l)%VZ*P(l)%VZ)
          i_vel = v_module/deltaVDF
          
          if(i_vel>=0 .AND. i_vel<=nPointVDF) then
            vel_distribution_slice(i,i_vel) = vel_distribution_slice(i,i_vel) + 1
          endif
          if(i_vel>nPointVDF) then
            vel_distribution_slice(i,nPointVDF)=vel_distribution_slice(i,nPointVDF) + 1
          endif
        endif
      end do

    endif
  end do

end subroutine


subroutine thermal_energy_slice_vx()
  
  use Parallel
  implicit none
  integer :: l
  integer :: i_vel=0, i

  do l=1, NPART

    if( (abs(P(l)%sp - 5.0)<1e-5) .OR. (abs(P(l)%sp - 9.0)<1e-5)  ) then

      do i=1,nPosSliceVDF
        if(P(l)%X >= posSliceVDF(i) .AND. P(l)%X < (posSliceVDF(i) + delta(0))) then
          i_vel = (P(l)%VX/deltaVDFNI) + (nPointVDF_NI/2)
          if(i_vel>=0 .AND. i_vel<=nPointVDF_NI) then
            vel_distribution_slice_NI(i,i_vel) = vel_distribution_slice_NI(i,i_vel) + 1
          endif
          if(i_vel>nPointVDF_NI) then
            vel_distribution_slice_NI(i,nPointVDF_NI)=vel_distribution_slice_NI(i,nPointVDF_NI) + 1
          endif
	  if(i_vel<0) then
            vel_distribution_slice_NI(i,0)=vel_distribution_slice_NI(i,0) + 1
          endif
        endif
      end do

    endif

  end do

end subroutine



subroutine thermal_energy_plane()
  
  use Parallel
  implicit none
  integer :: l
  real :: v_module
  integer :: i_vel=0, i=0
  
  do l=1, NPART
    if(abs(P(l)%sp)<1e-5) then

      do i=1, nPosPlaneVDF
        if(px_arr(l) < posPlaneVDF(i) .AND.  P(l)%X >= posPlaneVDF(i)) then
          v_module= sqrt(P(l)%VX*P(l)%VX + P(l)%VY*P(l)%VY + P(l)%VZ*P(l)%VZ)
          i_vel = v_module/deltaVDF
          !write(*,*) rank, i_vel

          if(i_vel>=0 .AND. i_vel<=nPointVDF) then
            vel_distribution_plane(i,i_vel)=vel_distribution_plane(i,i_vel) + 1
          endif
          if(i_vel>nPointVDF) then
            vel_distribution_plane(i,nPointVDF)=vel_distribution_plane(i,nPointVDF) + 1
          endif
        endif
      end do
    endif
  end do

end subroutine

subroutine write_vdf(l)

  use Parallel
  implicit none
  include 'mpif.h'
  integer :: i=0, j, k
  integer, intent(in):: l

 CALL MPI_TYPE_VECTOR(nPointVDF_NI, 1, nPosSliceVDF, MPI_INTEGER, sendsliceVDF_NI, info)
  CALL MPI_TYPE_COMMIT(sendsliceVDF_NI, info)


  if(mod(l, time_interval_measure_vdf)==0) then
    if(rank==0) then
      do j=1, numtasks-1
        do i=1, nPosPlaneVDF
          call MPI_Recv(vel_distribution_planeMPI(i,0), 1, sendplaneVDF, j, 0, MPI_COMM_WORLD, status, info)
          do k=0, nPointVDF
            vel_distribution_plane(i,k) = vel_distribution_plane(i,k) + vel_distribution_planeMPI(i,k)
          end do
        end do
       
	 do i=1, nPosSliceVDF
          call MPI_Recv(vel_distribution_sliceMPI(i,0), 1, sendsliceVDF, j, 0, MPI_COMM_WORLD, status, info)	
          do k=0, nPointVDF
            vel_distribution_slice(i,k) = vel_distribution_slice(i,k) + vel_distribution_sliceMPI(i,k) 
          end do
        end do

	do i=1, nPosSliceVDF    
	  call MPI_Recv(vel_distribution_sliceMPI_NI(i,0), 1, sendsliceVDF_NI, j, 0, MPI_COMM_WORLD, status, info) 	
          do k=0, nPointVDF_NI
            vel_distribution_slice_NI(i,k) = vel_distribution_slice_NI(i,k) + vel_distribution_sliceMPI_NI(i,k)	 
          end do
        end do

      end do
    else
      do i=1, nPosPlaneVDF
        call MPI_Send(vel_distribution_plane(i,0), 1,sendplaneVDF , 0, 0, MPI_COMM_WORLD, info)
      end do
      do i=1, nPosSliceVDF
        call MPI_Send(vel_distribution_slice(i,0), 1,sendsliceVDF , 0, 0, MPI_COMM_WORLD, info)
      end do

      do i=1, nPosSliceVDF
        call MPI_Send(vel_distribution_slice_NI(i,0), 1, sendsliceVDF_NI, 0, 0, MPI_COMM_WORLD, info)
      end do


    end if

    if(rank==0) then
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))
      str='vel_dist_pln_'//str3(8-lgc:7)//'.dat'
      OPEN(UNIT=998, FILE=str, status="unknown", position="append")
      do j=0, nPointVDF 
        write(998, '(E11.4)', advance="no") deltaVDF*j
        do i=1, nPosPlaneVDF
          write(998, '(1x, I8)', advance="no") vel_distribution_plane(i, j) 
        end do
        write(998, *)
      end do
      CLOSE(UNIT=998)

      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))
      str='vel_dist_slc_'//str3(8-lgc:7)//'.dat'
      OPEN(UNIT=998, FILE=str, status="unknown", position="append")
      do j=0, nPointVDF
        write(998, '(E11.4)', advance="no") deltaVDF*j
        do i=1, nPosPlaneVDF
          write(998, '(1x, I8)', advance="no") vel_distribution_slice(i, j)
        end do
        write(998, *)
      end do
      CLOSE(UNIT=998)

       write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))
      str='vel_dist_slc_NI_'//str3(8-lgc:7)//'.dat'
      OPEN(UNIT=998, FILE=str, status="unknown", position="append")
      do j=0, nPointVDF_NI 
	 write(998, '(E11.4)', advance="no") deltaVDFNI*(j - (nPointVDF_NI/2) ) 
        do i=0, nPosPlaneVDF
          write(998, '(2(1x, E11.4))', advance="no") vel_distribution_slice_NI(i, j)
        end do
        write(998, *)
      end do
      CLOSE(UNIT=998)



    end if
    do i=1, nPosPlaneVDF
      vel_distribution_plane(i,:)=0
    end do
    do i=1, nPosSliceVDF
      vel_distribution_slice(i,:)=0
    end do
  end if

  do i=1, nPosSliceVDF
      vel_distribution_slice_NI(i,:)=0
  end do 

end subroutine 

subroutine electron_thermalization()

  use Parallel
  implicit none
  include 'mpif.h'

  integer :: i
  

  real :: auxx(3),auxx2(3),Vt,Vt2,erfi
  real :: w_e_reset
  real :: prob_of_coll
  
  

  w_e_reset=2.0e9 !!valor utilizadopor Fubiani
  prob_of_coll=1.0-exp(-w_e_reset*dt)

  Vt=0
  Vt2=0
  erfi=0

  do i=1, NPART

    if(P(i)%sp==0) then
      if(P(i)%x>plasma_x0 .AND. P(i)%x<plasma_x1) then
        call random_number(auxx)
        if(auxx(1)<prob_of_coll) then
          Vt=sqrt(K_b*temperature(P(i)%sp)/mass(P(i)%sp))
          Vt2=sqrt(2.0)*Vt
          
          call random_number(auxx2)
          auxx2=auxx2*2.0-1.0
          
          call InvErrorFunction(auxx2(1),erfi)
          P(i)%VX=Vt2*erfi
          call InvErrorFunction(auxx2(2),erfi)
          P(i)%VY=Vt2*erfi
          call InvErrorFunction(auxx2(3),erfi)
          P(i)%VZ=Vt2*erfi
        endif
      endif
    end if

  enddo

end subroutine electron_thermalization



subroutine generate_trajectories()
	use Parallel
	implicit none
	include 'mpif.h'


	integer :: i_traj=0


	call Particle_trajectory_loading(2)
   	do i_traj=1,150000
    		call movepart_trajectories(i_traj)	
		call reflection_trajectories()		
		call Particle_Exchange_trajectories()

		call Heapsort(DelParticles, OutParticles)

		NpartBefoteDelete=Npart
	 	if(OutParticles>0)  then
			call DeleteParticle_trajectory()
		endif

		NpartBefoteDelete=0
   		Flag=-10
    		LCounter=0
    		GCounter=0
    		OutParticles=0
	   	ll=0
	enddo


end subroutine generate_trajectories

subroutine print_flux_to_objects(flux_every_it)
	use Parallel
	implicit none
	include 'mpif.h'
	integer ii
	integer,intent( in ) :: flux_every_it
	integer obj
		
	  if (mod (iter,flux_every_it) .EQ. 0) then

      call MPI_ALLREDUCE(In_flux_object,Total_In_flux_object, 52, MPI_real, MPI_SUM, MPI_COMM_WORLD,info)
      !write(*,*) In_flux_object(1, 3), Total_In_flux_object(1, 3)

      Total_In_flux_object=Total_In_flux_object/flux_every_it	

      if(rank==0) then	
      
!       str='Object_1_inFlux.dat'
!       OPEN(UNIT=97, FILE=str, status="unknown", position="append")
!       write(97,'(10(1x,E11.4) )')  iter*dt*1e6,Total_In_flux_object(1,0), Total_In_flux_object(1,1),Total_In_flux_object(1,2), Total_In_flux_object(1,3),& 
!         Total_In_flux_object(1,5), Total_In_flux_object(1,8), Total_In_flux_object(1,9),Total_In_flux_object(1,10), Total_In_flux_object(1,11)
!       close(UNIT=97)
     
        str='Object_1_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(1,ii)
        end do
        write(97,*)
        close(UNIT=97)
        
        str='Object_2_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(2,ii)
        end do
        write(97,*)
        close(UNIT=97)
        
        str='Object_3_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(3,ii)
        end do
        write(97,*)
        close(UNIT=97)
        
        str='Object_4_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(4,ii)
        end do
        write(97,*)
        close(UNIT=97)
        
        str='Object_5_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(5,ii)
        end do
        write(97,*)
        close(UNIT=97)
        
        str='Object_6_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(6,ii)
        end do
        write(97,*)
        close(UNIT=97)
		
		str='Object_7_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(7,ii)
        end do
        write(97,*)
        close(UNIT=97)
        
        str='Object_8_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(8,ii)
        end do
        write(97,*)
        close(UNIT=97)
		
		str='Object_9_inFlux.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(9,ii)
        end do
        write(97,*)
        close(UNIT=97)		   

		!!! new 

        str='Object_obj_inFlux_N.dat'
        OPEN(UNIT=97, FILE=str, status="unknown", position="append")
        write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(obj,ii)
        end do
        write(97,*)
        close(UNIT=97)
		
		 write(str3,'(i7)') iter
            lgc=len_trim(adjustl(str3))
			
            str='Obj_'//str3(8-lgc:7)//'.dat'
			OPEN(UNIT=65, FILE=str, status="unknown", position="append")
        write(65,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
        do ii=0,11
        write(65, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(obj,ii)
        end do
        write(65,*)
            close(65)
            

!      CHARACTER(LEN=7):: str3

!      write(str3,'(i7)') iter
!      lgc=len_trim(adjustl(str3))
!      str='Object_NI_'//str3(8-lgc:7)//'.dat'
!      OPEN(UNIT=97, FILE=str, status="unknown", position="append")
!	   write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
!        do ii=0,11
!          write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(Nb_Obj,ii)
!        end do
!       write(97, *)
!      CLOSE(UNIT=97)

	
	!	if(rank==0) then
    !  write(str3,'(i7)') Obj
    !  lgc=len_trim(adjustl(str3))
    !  str='Object_inFlux_'//str3(8-lgc:7)//'.dat'
    !  OPEN(UNIT=998, FILE=str, status="unknown", position="append")
	!  do ii=0,11
    !    write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(obj,ii)
    !    end do
    !    write(998, *)
    !  end do
    !  CLOSE(UNIT=998)
		
	  ! str='Object_1_inFlux.dat'
      !  OPEN(UNIT=97, FILE=str, status="unknown", position="append")
      !  write(97,'(10(1x,E11.4) )', advance="no")  iter*dt*1e6
      !  do ii=0,12
      !  write(97, '(10(1x,E11.4) )', advance="no") Total_In_flux_object(1,ii)
      !  end do
      !  write(97,*)
      !  close(UNIT=97)
	  
	  
	!  Write the integer into a string:
	!write(file_id, '(i0)') m
	!  Construct the filename:
	!file_name = 'file' // trim(adjustl(file_id)) // '.dat'
	!  Open the file with this name
	!open(file = trim(file_name), unit = fu)
		
      endif 	

      In_flux_object=0
     
    endif

end subroutine print_flux_to_objects 


subroutine reset_variables()
    use Parallel
    implicit none
    include 'mpif.h'

    NpartBefoteDelete=0   
    Flag=-10
    LCounter=0
    GCounter=0
    OutParticles=0
    count1=0
    count2=0
    count3=0
    count4=0

    ll=0
    rho=0
    rho_Sec=0
    
    Coulomb_cell_list=0	    
    Num_part_Coul_cell=0

    Mom_e_x=0
    Mom_e_y=0	
    Mom_e_z=0	


end subroutine reset_variables

