subroutine Heapsort(ArrayToSort, length)
  
  integer length
  integer, intent(inout) :: ArrayToSort(1:length)

  integer i
  real temp

  do i=length/2, 1, -1
    call Heap(ArrayToSort, i, length)
  end do
  do i=length, 2, -1
    temp = ArrayToSort(1)
    ArrayToSort(1) = ArrayToSort(i)
    ArrayToSort(i) = temp
    call Heap(ArrayToSort, 1, i-1)
  end do
end subroutine Heapsort

subroutine Heap(Array, node, n)
  
  integer node, n
  integer, intent(inout) :: Array(1:n)
  
  integer k, j
  real temp

  k = node
  j = 2*k

  do while (j <= n)
    if(j<n) then
	if( Array(j)<Array(j+1)) then
		j = j+1
	endif 
   endif

    if(Array(k) < Array(j)) then
      temp = Array(j)
      Array(j) = Array(k)
      Array(k) = temp
      k = j
      j = 2*k
    else
      exit
    end if
  end do

end subroutine Heap

SUBROUTINE init_random_seed(init)
  integer init
  INTEGER :: i, n, clock
  INTEGER, DIMENSION(:), ALLOCATABLE :: seed

  CALL RANDOM_SEED(size = n)
  ALLOCATE(seed(n))

  CALL SYSTEM_CLOCK(COUNT=clock)

  seed =clock +init +37 * (/(i - 1,i = 1,n) /)
  CALL RANDOM_SEED(PUT=seed)

  DEALLOCATE(seed)
END SUBROUTINE

subroutine Send_Plane_v2(Arr)
  
  use Parallel
  implicit none
  include 'mpif.h'
  
  real,intent( inout ) :: Arr(i0-1:i1+1 ,j0-1:j1+1, k0-1:k1+1)
  real, allocatable :: buff(:)
  
  nx2=i1+1-(i0-1)+1
  ny2=j1+1-(j0-1)+1
  nz2=k1+1-(k0-1)+1
 
  !write(*,*) rank, neig(0), neig(1), neig(2), neig(3), neig(4), neig(5),&
  !indexx, indexy, indexz
  allocate(buff(1:ny2*nz2))
  if(mod(indexx,2)==0) then
    !---------------------------X direction-------------------------------------------------------------
    if(neig(0)>-1) then
      call MeshToVect(Arr, 1, buff, ny2*nz2)
      !if(rank==0) write(*,*) "communication first ok"
      !call MPI_Barrier(MPI_COMM_WORLD)
      CALL MPI_SEND(buff(1), ny2*nz2, MPI_REAL, neig(0),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(1)>-1) then
      CALL MPI_RECV(buff(1), ny2*nz2, MPI_REAL, neig(1),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 2, buff, ny2*nz2)
    end if
    
    if(neig(1)>-1) then
      call MeshToVect(Arr, 2, buff, ny2*nz2)
      CALL MPI_SEND(buff(1), ny2*nz2, MPI_REAL, neig(1),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(0)>-1) then
      CALL MPI_RECV(buff(1), ny2*nz2, MPI_REAL, neig(0),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 1, buff, ny2*nz2)
    end if
  
  else
    if(neig(1)>-1) then
      !call MPI_Barrier(MPI_COMM_WORLD)
      CALL MPI_RECV(buff(1), ny2*nz2, MPI_REAL, neig(1),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 2, buff, ny2*nz2)
    end if
    
    if(neig(0)>-1) then
      call MeshToVect(Arr, 1, buff, ny2*nz2)
      CALL MPI_SEND(buff(1), ny2*nz2, MPI_REAL, neig(0),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(0)>-1) then
      CALL MPI_RECV(buff(1), ny2*nz2,MPI_REAL, neig(0),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 1, buff, ny2*nz2)
    end if
    
    if(neig(1)>-1) then
      call MeshToVect(Arr, 2, buff, ny2*nz2)
      CALL MPI_SEND(buff(1), ny2*nz2,MPI_REAL, neig(1),51, MPI_COMM_WORLD, info)
    endif
  end if

  !write(*,*) "success", rank
  !call MPI_Barrier(MPI_COMM_WORLD)
  !if(rank==0) write(*,*) "communication x ok"
  deallocate(buff)
  allocate(buff(1:nx2*nz2))
  if(mod(indexy,2)==0) then
    !---------------------------Y direction-------------------------------------------------------------
    if(neig(2)>-1) then
      call MeshToVect(Arr, 3, buff, nx2*nz2)
      CALL MPI_SEND(buff(1), nx2*nz2, MPI_REAL, neig(2),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(3)>-1) then
      CALL MPI_RECV(buff(1), nx2*nz2, MPI_REAL, neig(3),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 4, buff, nx2*nz2)
    end if
    
    if(neig(3)>-1) then
      call MeshToVect(Arr, 4, buff, nx2*nz2)
      CALL MPI_SEND(buff(1), nx2*nz2, MPI_REAL, neig(3),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(2)>-1) then
      CALL MPI_RECV(buff(1), nx2*nz2, MPI_REAL, neig(2),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 3, buff, nx2*nz2)
    end if
  
  else
    if(neig(3)>-1) then
      CALL MPI_RECV(buff(1), nx2*nz2, MPI_REAL, neig(3),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 4, buff, nx2*nz2)
    end if
    
    if(neig(2)>-1) then
      call MeshToVect(Arr, 3, buff, nx2*nz2)
      CALL MPI_SEND(buff(1), nx2*nz2, MPI_REAL, neig(2),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(2)>-1) then
      CALL MPI_RECV(buff(1), nx2*nz2,MPI_REAL, neig(2),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 3, buff, nx2*nz2)
    end if
    
    if(neig(3)>-1) then
      call MeshToVect(Arr, 4, buff, nx2*nz2)
      CALL MPI_SEND(buff(1), nx2*nz2,MPI_REAL, neig(3),51, MPI_COMM_WORLD, info)
    endif
  end if
  !if(rank==0) write(*,*) "communication y ok"
  
  deallocate(buff)
  allocate(buff(1:nx2*ny2))
  if(mod(indexz,2)==0) then
    !---------------------------Z direction-------------------------------------------------------------
    if(neig(4)>-1) then
      call MeshToVect(Arr, 5, buff, nx2*ny2)
      CALL MPI_SEND(buff(1), nx2*ny2, MPI_REAL, neig(4),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(5)>-1) then
      CALL MPI_RECV(buff(1), nx2*ny2, MPI_REAL, neig(5),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 6, buff, nx2*ny2)
    end if
    
    if(neig(5)>-1) then
      call MeshToVect(Arr, 6, buff, nx2*ny2)
      CALL MPI_SEND(buff(1), nx2*ny2, MPI_REAL, neig(5),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(4)>-1) then
      CALL MPI_RECV(buff(1), nx2*ny2, MPI_REAL, neig(4),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 5, buff, nx2*ny2)
    end if
  
  else
    if(neig(5)>-1) then
      CALL MPI_RECV(buff(1), nx2*ny2, MPI_REAL, neig(5),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 6, buff, nx2*ny2)
    end if
    
    if(neig(4)>-1) then
      call MeshToVect(Arr, 5, buff, nx2*ny2)
      CALL MPI_SEND(buff(1), nx2*ny2, MPI_REAL, neig(4),51, MPI_COMM_WORLD, info)
    end if
    
    if(neig(4)>-1) then
      CALL MPI_RECV(buff(1), nx2*ny2,MPI_REAL, neig(4),51, MPI_COMM_WORLD, status, info)
      call VectToMesh(Arr, 5, buff, nx2*ny2)
    end if
    
    if(neig(5)>-1) then
      call MeshToVect(Arr, 6, buff, nx2*ny2)
      CALL MPI_SEND(buff(1), nx2*ny2,MPI_REAL, neig(5),51, MPI_COMM_WORLD, info)
    endif
  end if
  !if(rank==0) write(*,*) "communication z ok"

  deallocate(buff)
end subroutine

subroutine MeshToVect(Arr, flagDirection, buff, length)
  
  use Parallel
  implicit none
 
  integer flagDirection, length
  real, intent(inout) :: Arr(i0-1:i1+1 ,j0-1:j1+1, k0-1:k1+1)
  real, intent(inout) :: buff(1:length)

  integer i, j, k

  select case (flagDirection)
    case(1)
      do k=k0-1, k1+1
        do j=j0-1, j1+1
          buff((k-k0+1)*ny2 + (j-j0+1+1)) = Arr(i0, j, k)
        end do
      end do
    case(2)
      do k=k0-1, k1+1
        do j=j0-1, j1+1
          buff((k-k0+1)*ny2 + (j-j0+1+1)) = Arr(i1, j, k)
        end do
      end do
    case(3)
      do k=k0-1, k1+1
        do i=i0-1, i1+1
          buff((k-k0+1)*nx2 + (i-i0+1+1)) = Arr(i, j0, k)
        end do
      end do
    case(4)
      do k=k0-1, k1+1
        do i=i0-1, i1+1
          buff((k-k0+1)*nx2 + (i-i0+1+1)) = Arr(i, j1, k)
        end do
      end do
    case(5)
      do j=j0-1, j1+1
        do i=i0-1, i1+1
          buff((j-j0+1)*nx2 + (i-i0+1+1)) = Arr(i, j, k0)
        end do
      end do
    case(6)
      do j=j0-1, j1+1
        do i=i0-1, i1+1
          buff((j-j0+1)*nx2 + (i-i0+1+1)) = Arr(i, j, k1)
        end do
      end do
  end select

  !write(*,*) "tot", flagDirection

end subroutine

subroutine VectToMesh(Arr, flagDirection, buff, length)
  
  use Parallel
  implicit none
 
  integer flagDirection, length
  real, intent(inout) :: Arr(i0-1:i1+1 ,j0-1:j1+1, k0-1:k1+1)
  real, intent(inout) :: buff(1:length)
  
  integer i, j, k

  select case (flagDirection)
    case(1)
      do k=k0-1, k1+1
        do j=j0-1, j1+1
          Arr(i0-1, j, k) = buff((k-k0+1)*ny2 + (j-j0+1+1))
        end do
      end do
    case(2)
      do k=k0-1, k1+1
        do j=j0-1, j1+1
          Arr(i1+1, j, k) = buff((k-k0+1)*ny2 + (j-j0+1+1))
        end do
      end do
    case(3)
      do k=k0-1, k1+1
        do i=i0-1, i1+1
          Arr(i, j0-1, k) = buff((k-k0+1)*nx2 + (i-i0+1+1))
        end do
      end do
    case(4)
      do k=k0-1, k1+1
        do i=i0-1, i1+1
          Arr(i, j1+1, k) = buff((k-k0+1)*nx2 + (i-i0+1+1))
        end do
      end do
    case(5)
      do j=j0-1, j1+1
        do i=i0-1, i1+1
          Arr(i, j, k0-1) = buff((j-j0+1)*nx2 + (i-i0+1+1))
        end do
      end do
    case(6)
      do j=j0-1, j1+1
        do i=i0-1, i1+1
          Arr(i, j, k1+1) = buff((j-j0+1)*nx2 + (i-i0+1+1))
        end do
      end do
  end select
  
  !write(*,*) "tot2", flagDirection

end subroutine
