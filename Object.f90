subroutine isinobj(posx, posy, posz, obj, isin)

      use Parallel
      implicit none
      include 'mpif.h'

      integer obj
      real posx, posy, posz, x0, x1, y0, y1, z0, z1, &
        r0, r1, rx, rp
      logical,intent(inout)::isin

      isin = .FALSE.
      select case (The_Object(obj)%types)
        case (1) !Rectangle
          x0 = The_Object(obj)%center(0)-The_Object(obj)%width(0)
          x1 = The_Object(obj)%center(0)+The_Object(obj)%width(0)
          y0 = The_Object(obj)%center(1)-The_Object(obj)%width(1)
          y1 = The_Object(obj)%center(1)+The_Object(obj)%width(1)
          z0 = The_Object(obj)%center(2)-The_Object(obj)%width(2)
          z1 = The_Object(obj)%center(2)+The_Object(obj)%width(2)
          if(posx>=x0 .AND. posx<=x1 .AND. &
             posy>=y0 .AND. posy<=y1 .AND. &
             posz>=z0 .AND. posz<=z1) then
             isin = .TRUE.
             !write(*,*) "isin", posx, x0, x1
          end if
        case (2) !Cylinder not used -> see conus (case 3)
        case (3) !Conus
          x0 = The_Object(obj)%center(0)-The_Object(obj)%width(0)
          x1 = The_Object(obj)%center(0)+The_Object(obj)%width(0)
          r0 = The_Object(obj)%width(1)
          r1 = The_Object(obj)%width(2)
          rx = (r1-r0)/(x1-x0)*(posx-x0)+r0
          rp = sqrt((posy-The_Object(obj)%center(1))**2+(posz-The_Object(obj)%center(2))**2)
          if(posx>=x0 .AND. posx<=x1 .AND. rp >= rx) then
            isin = .TRUE.
          end if
        case default
      end select

end subroutine

subroutine Reflection_Box(obj, l)

  use Parallel
  implicit none
  include 'mpif.h'

  integer obj, l
  real x0, x1

  x0 = The_Object(obj)%center(0) - The_Object(obj)%width(0)
  x1 = The_Object(obj)%center(0) + The_Object(obj)%width(0)

  if(px_arr(l) < x0) then
    P(l)%X = -P(l)%X+2.*x0
    P(l)%VX = -P(l)%VX
  else if(px_arr(l) > x1) then
    P(l)%X = -P(l)%X+2.*x1
    P(l)%VX = -P(l)%VX
  else
    write(*, *) "Subroutine Reflection_Box error", obj, l, P(l)%X
  end if

end subroutine Reflection_Box

subroutine Reflection_Box_newT(obj, l)

  use Parallel
  implicit none
  include 'mpif.h'

  integer obj, l
  real x0, x1, f, xinter, yinter, zinter, VV
  real auxx(3), erfi, Vt

  x0 = The_Object(obj)%center(0) - The_Object(obj)%width(0)
  x1 = The_Object(obj)%center(0) + The_Object(obj)%width(0)

  if(px_arr(l) < x0) then
    VV = sqrt(P(l)%VX**2+P(l)%VY**2+P(l)%VZ**2)
    xinter = x0
    f = (P(l)%X-x0)/P(l)%VX
    yinter = py_arr(l) + (dt-f)*P(l)%VY
    zinter = pz_arr(l) + (dt-f)*P(l)%VZ
    Vt = sqrt(2.*k_b*temperature(P(l)%sp)/mass(P(l)%sp))
    
    call random_number(auxx)
    auxx = 2.*auxx-1.

    call InvErrorFunction(auxx(1),erfi)
    P(l)%VX = -abs(Vt*erfi)
    call InvErrorFunction(auxx(2),erfi)
    P(l)%VY = Vt*erfi
    call InvErrorFunction(auxx(3),erfi)
    P(l)%VZ = Vt*erfi

    P(l)%X = xinter + f*P(l)%VX
    P(l)%Y = yinter + f*P(l)%VY
    P(l)%Z = zinter + f*P(l)%VZ
  else if(px_arr(l) > x1) then
    !write(*,*) P(l).X, x1, P(l).VX
    VV = sqrt(P(l)%VX**2+P(l)%VY**2+P(l)%VZ**2)
    xinter = x1
    f = (P(l)%X-x1)/P(l)%VX
    yinter = py_arr(l) + (dt-f)*P(l)%VY
    zinter = pz_arr(l) + (dt-f)*P(l)%VZ
    Vt = sqrt(2.*k_b*temperature(P(l)%sp)/mass(P(l)%sp))
    
    call random_number(auxx)
    auxx = 2.*auxx-1.

    call InvErrorFunction(auxx(1),erfi)
    P(l)%VX = abs(Vt*erfi)
    call InvErrorFunction(auxx(2),erfi)
    P(l)%VY = Vt*erfi
    call InvErrorFunction(auxx(3),erfi)
    P(l)%VZ = Vt*erfi

    P(l)%X = xinter + f*P(l)%VX
    P(l)%Y = yinter + f*P(l)%VY
    P(l)%Z = zinter + f*P(l)%VZ

    if(P(l)%X==x1) P(l)%X = P(l)%X + 1e-5

    !write(*,*) f, dt, Vt, erfi, xinter, yinter, zinter, VV, P(l).X, P(l).Y, P(l).Z, P(l).VX, P(l).VY, P(l).VZ, px_arr(l)
  else
    write(*, *) "Subroutine Reflection_Box error", obj, l, P(l)%X, px_arr(l), P(l)%VX
  end if

end subroutine Reflection_Box_newT

subroutine Reflection_Conus(obj, l)

  use Parallel
  implicit none
  include 'mpif.h'

  integer obj, l
  double precision x0, x1, k, xinter, yinter, zinter, rr2
  logical InterConusWall
  double precision ar, a, b, cc,ddd, kk
  double precision kx,ky,kz, nnx,nny,nnz, ux,uy,uz, n, vx, vy, vz, mx, my, mz, U, VV, f

  InterConusWall = .FALSE.

  x0 = The_Object(obj)%center(0) - The_Object(obj)%width(0)
  x1 = The_Object(obj)%center(0) + The_Object(obj)%width(0)

  if(px_arr(l) < x0 .AND. P(l)%X >= x0) then
    k = (x0-px_arr(l))/(P(l)%X-px_arr(l)) !intersection droite plan to determine the radius of intersection
    xinter = x0
    yinter = py_arr(l)+k*(P(l)%Y-py_arr(l))
    zinter = pz_arr(l)+k*(P(l)%Z-pz_arr(l))
    rr2 = sqrt((yinter-The_Object(obj)%center(1))**2+(zinter-The_Object(obj)%center(2))**2)
    if(rr2 >= The_Object(obj)%width(1)) then !Intersection first plane of conus
      P(l)%X = -P(l)%X+2.*x0
      P(l)%VX = -P(l)%VX
      if(P(l)%X==x0) P(l)%X = P(l)%X - 1e-5
    else !Intersection with the inside conus wall
      InterConusWall = .TRUE.
    end if
  else if(px_arr(l) > x1 .AND. P(l)%X <= x1) then
    k = (x1-px_arr(l))/(P(l)%X-px_arr(l)) !intersection droite plan to determine the radius of intersection
    xinter = x1
    yinter = py_arr(l)+k*(P(l)%Y-py_arr(l))
    zinter = pz_arr(l)+k*(P(l)%Z-pz_arr(l))
    rr2 = sqrt((yinter-The_Object(obj)%center(1))**2+(zinter-The_Object(obj)%center(2))**2)
    if(rr2 >= The_Object(obj)%width(2)) then !Intersection first plane of conus
      P(l)%X = -P(l)%X+2.*x1
      P(l)%VX = -P(l)%VX
      if(P(l)%X==x1) P(l)%X = P(l)%X + 1e-5
    else !Intersection with the inside conus wall
      InterConusWall = .TRUE.
    end if
  else
    InterConusWall = .TRUE.
  end if

  if(InterConusWall .EQV. .TRUE.) then !treatment of reflection with conus wall
    ar = (The_Object(obj)%width(2)-The_Object(obj)%width(1))/(x1-x0)
    a = ar*ar*(P(l)%X-px_arr(l))**2-(P(l)%Y-py_arr(l))**2-(P(l)%Z-pz_arr(l))**2
    b = 2.*ar*(ar*(P(l)%X-px_arr(l))*(px_arr(l)-x0)+The_Object(obj)%width(1)*(P(l)%X-px_arr(l))) &
      + 2.*((P(l)%Y-py_arr(l))*(The_Object(obj)%center(1)-py_arr(l)) + (P(l)%Z-pz_arr(l))*(The_Object(obj)%center(2)-pz_arr(l)))
    cc = ar*ar*(px_arr(l)-x0)**2+The_Object(obj)%width(1)**2+2.*ar*The_Object(obj)%width(1)*(px_arr(l)-x0) &
      - (py_arr(l)-The_Object(obj)%center(1))**2 - (pz_arr(l)-The_Object(obj)%center(2))**2
    ddd = b*b-4.*a*cc
    !if(a==0) then
    !  write(*,*) "a=", a
    !end if

    !if(ddd<=0) then
    !  write(*,*) "ddd=", ddd
    !  ddd = 0
    !end if
    kk = (-b+sqrt(ddd))/2./a
    if(kk<0 .OR. kk>1) then
      kk = (-b-sqrt(ddd))/2./a
    end if
    xinter = px_arr(l) + kk*(P(l)%X-px_arr(l))
    yinter = py_arr(l) + kk*(P(l)%Y-py_arr(l))
    zinter = pz_arr(l) + kk*(P(l)%Z-pz_arr(l))

    kx = xinter - px_arr(l)
    ky = yinter - py_arr(l)
    kz = zinter - pz_arr(l)
    
    nny = The_Object(obj)%center(1) - yinter
    nnz = The_Object(obj)%center(2) - zinter
    nnx = -(nny*nny+nnz*nnz)/(-The_Object(obj)%width(1)*(x1-x0)/(The_Object(obj)%width(2)-The_Object(obj)%width(1)) + x0 - xinter)

    n = -(nnx*(xinter-px_arr(l)) + nny*(yinter-py_arr(l)) + nnz*(zinter-pz_arr(l)))/(nnx*nnx+nny*nny+nnz*nnz)
    ux = xinter + n*nnx
    uy = yinter + n*nny
    uz = zinter + n*nnz

    vx = ux - px_arr(l)
    vy = uy - py_arr(l)
    vz = uz - pz_arr(l)

    mx = px_arr(l) + 2.*vx
    my = py_arr(l) + 2.*vy
    mz = pz_arr(l) + 2.*vz

    vx = mx - xinter
    vy = my - yinter
    vz = mz - zinter

    VV = sqrt(P(l)%VX**2 + P(l)%VY**2 + P(l)%VZ**2)
    f = sqrt((xinter-px_arr(l))**2 + (yinter-py_arr(l))**2 + (zinter-pz_arr(l))**2)/VV
    U = sqrt(vx*vx + vy*vy + vz*vz)

    P(l)%VX = vx*VV/U
    P(l)%VY = vy*VV/U
    P(l)%VZ = vz*VV/U

    P(l)%X = xinter + P(l)%VX*(dt-f+9e-13)
    P(l)%Y = yinter + P(l)%VY*(dt-f+9e-13)
    P(l)%Z = zinter + P(l)%VZ*(dt-f+9e-13)

    write(20000+rank,*) px_arr(l), py_arr(l), pz_arr(l)
    write(20000+rank,*) xinter, yinter, zinter
    write(20000+rank,*) P(l)%X, P(l)%Y, P(l)%Z
    write(20000+rank,*) " "
  end if
  
end subroutine Reflection_Conus
