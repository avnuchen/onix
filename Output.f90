subroutine Print_particle_position()
	use Parallel
	implicit none
	integer  i

  
  			write(str3,'(I7)') iter+rank
            lgc=LEN_TRIM(ADJUSTL(str3))
            str='Pa_pos_'//str3(8-lgc:7)//'.dat'           
                         
             OPEN(UNIT=65, FILE=str, status="unknown")		
                write(65,*) Npart
   				do i = 1, Npart
   						write(65,'(1(1x,I8) ,8(1x,E14.7) )') i,P(i)%X,P(i)%Y,P(i)%Z,P(i)%sp,P(i)%VX,P(i)%VY,P(i)%VZ,P(i)%IdentPart
                end do
            close(65)
            
 end subroutine Print_particle_position


subroutine Print_charge_density2()

  use Parallel
  implicit none
  include 'mpif.h'

  integer  i,j,k,l,ii,jj,ip,jp,jp2,kp,kp2, sizebuff, sizebuff2

  ip = modulo(modulo(rank,NprocX*NprocY),NprocX)
  jp = modulo(rank,NprocX*NprocY)/NprocX
  kp = rank/(NprocX*NprocY)
  
  output_plane_density=0
  output_plane_density_xz=0

  allocate( buffer(0:i1-i0, 0:j1-j0) )
  allocate( buffer2(0:i1-i0, 0:k1-k0) )
  sizebuff = (i1-i0+1)*(j1-j0+1)
  sizebuff2 = (i1-i0+1)*(k1-k0+1)

  do jj=0, 11

    rho_Sec=0
    do l=1,npart
      if( p(l)%sp == jj ) then
        call chargeprojection_Second_Order(l)
      endif
    enddo
    call ChargeExchange_3planes(rho_Sec)


    density_average(jj,:,:,:) = density_average(jj,:,:,:) + rho_Sec(:,:,:)

    if(modulo(iter, 10000) == 0) then
    
      do j=0,j1-j0
        do i=0, i1-i0
          buffer(i,j) = rho_Sec(i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_density(jj, i00(ii)+i, j00(ii)+j) = buffer(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if


      do j=0,j1-j0
        do i=0, i1-i0
          buffer(i,j) = density_average(jj, i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_density_average(jj, i00(ii)+i, j00(ii)+j) = buffer(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if
      
      do k=0,k1-k0
        do i=0, i1-i0
          buffer2(i,k) = rho_Sec(i+i0, j0,k0+k)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          jp2 = modulo(ii,NprocX*NprocY)/NprocX
          if(jp2 == NprocY/2) then
            call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
            do k=0,k1-k0
              do i=0, i1-i0
                output_plane_density_xz(jj, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
              end do
            end do
          end if
        end do
      else
        if(jp == NprocY/2) then
          call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if


      do k=0,k1-k0
        do i=0, i1-i0
          buffer2(i,k) = density_average(jj, i+i0,j0,k0+k)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          jp2 = modulo(ii,NprocX*NprocY)/NprocX
          if(jp2 == NprocY/2) then
            call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
            do k=0,k1-k0
              do i=0, i1-i0
                output_plane_density_average_xz(jj, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
              end do
            end do
          end if
        end do
      else
        if(jp == NprocY/2) then
          call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

    end if

  end do
  
  deallocate(buffer)
  deallocate(buffer2)

  if(modulo(iter, 5000) == 0) then
    if(rank==0) then

      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='xy_mid_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,11
            write(65,'(1(1x, E11.4))',advance="no") output_plane_density(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='xy_mid_A_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,11
            write(65,'(1(1x, E11.4))',advance="no") output_plane_density_average(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='xz_mid_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do k = 0, nzp-1
          j =nyp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,11
            write(65,'(1(1x, E11.4))',advance="no") output_plane_density_xz(ii,i,k)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='xz_mid_A_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do k = 0, nzp-1
          j =nyp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,11
            write(65,'(1(1x, E11.4))',advance="no") output_plane_density_average_xz(ii,i,k)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)



    end if

    density_average = 0
  end if

   rho_Sec=0

end subroutine Print_charge_density2

subroutine Print_potential2()

  use Parallel
  implicit none
  include 'mpif.h'

  integer  i,j,k,ii,jj,ip,jp,jp2, kp,kp2, sizebuff, sizebuff2
  real :: i_wall


  i_wall = (The_Object(2)%center(0) - The_Object(2)%width(0))/delta(0)

  ip = modulo(modulo(rank,NprocX*NprocY),NprocX)
  jp = modulo(rank,NprocX*NprocY)/NprocX
  kp = rank/(NprocX*NprocY)
  
  output_plane_potential=0
  output_plane_potential_xz=0
  allocate( buffer(0:i1-i0, 0:j1-j0) )
  allocate( buffer2(0:i1-i0, 0:k1-k0) )
  sizebuff = (i1-i0+1)*(j1-j0+1)
  sizebuff2 = (i1-i0+1)*(k1-k0+1)


  potential_average(0,:,:,:) = potential_average(0,:,:,:) + V(:,:,:)
  potential_average(1,:,:,:) = potential_average(1,:,:,:) + Exm(:,:,:)
  potential_average(2,:,:,:) = potential_average(2,:,:,:) + Eym(:,:,:)
  potential_average(3,:,:,:) = potential_average(3,:,:,:) + Ezm(:,:,:)


  if (mod (iter,10000)==0 .AND. iter > 100000  ) then
    if ( i_wall > i0 .and. i_wall < i1 ) then
     write(str3,'(I7)') iter + rank
      lgc=LEN_TRIM(ADJUSTL(str3))
      str='PoteSliceA_'//str3(8-lgc:7)//'.dat'
      OPEN(UNIT=65, FILE=str, status="unknown")
        do i = i0, i1
          do j = i0, i1
            do k = k0, k1
            	write(65,'(4(1x, E11.4))') i*delta(0),j*delta(1),k*delta(2),potential_average(0,i,j,k)
            enddo	  
	  end do
         end do
      close(65)
     endif	 
 endif




  if(modulo(iter, 10000) == 0) then
  
    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = V(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(0, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if
    
    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Exm(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(1, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Eym(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(2, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Ezm(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(3, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do jj=0,3

      do j=0,j1-j0
        do i=0, i1-i0
          buffer(i,j) = potential_average(jj, i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_potential_average(jj, i00(ii)+i, j00(ii)+j) = buffer(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

    end do
    
    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = V(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(0, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Exm(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(1, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Eym(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(2, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Ezm(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(3, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if
    
    do jj=0,3

      do k=0,k1-k0
        do i=0, i1-i0
          buffer2(i,k) = potential_average(jj, i+i0,j0,k0+k)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          jp2 = modulo(ii,NprocX*NprocY)/NprocX
          if(jp2 == NprocY/2) then
            call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
            do k=0,k1-k0
              do i=0, i1-i0
                output_plane_potential_average_xz(jj, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
              end do
            end do
          end if
        end do
      else
        if(jp == NprocY/2) then
          call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

    end do



  end if

  
  deallocate(buffer)
  deallocate(buffer2)

  if(modulo(iter, 10000) == 0) then
    if(rank==0) then

      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='Poten_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='PoteA_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential_average(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='Poten_xz_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do k = 0, nzp-1
          j =nyp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential_xz(ii,i,k)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='PoteA_xz_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do k = 0, nzp-1
          j =nyp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential_average_xz(ii,i,k)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)




    end if

    potential_average = 0
  end if

end subroutine Print_potential2



subroutine emittance()

  use Parallel
  implicit none
  include 'mpif.h'

  integer i,j,l, ii, sizebuff
  real theta_em, delta_theta, delta_radial
  real dx_em, ddx_em, dy_em, ddy_em
  real :: x_eg_pos


  x_eg_pos = (The_Object(4)%center(0) - The_Object(4)%width(0) ) - 2.0*delta(0)	

  

  delta_theta = acos(-1.0)/rN_emit
  delta_radial = Nyp*delta(1)/rN_emit

  do l=1,NPART
    if(px_arr(l)<x_eg_pos .AND. P(l)%x>x_eg_pos) then
      theta_em = atan(P(l)%vy/P(l)%vx)
      dx_em = (theta_em+acos(-1.0)/2.0)/delta_theta
      dy_em = P(l)%y/delta_radial
      i = floor(dx_em)
      j = floor(dy_em)
      !write(*,*) "emittance calc", i, j, P(l)%x, P(l)%y, P(l)%vy, P(l)%vx, dx_em, dy_em, P(l)%sp
      if(i>=0 .AND. i<iN_emit .AND. j>=0 .AND. j<iN_emit) then
        dx_em = dx_em - i
        dy_em = dy_em - j
        ddx_em = 1.0-dx_em
        ddy_em = 1.0-dy_em
        emittance_arr(P(l)%sp,0,i,j)     = emittance_arr(P(l)%sp,0,i,j)     + ddx_em*ddy_em
        emittance_arr(P(l)%sp,0,i,j+1)   = emittance_arr(P(l)%sp,0,i,j+1)   + ddx_em* dy_em
        emittance_arr(P(l)%sp,0,i+1,j)   = emittance_arr(P(l)%sp,0,i+1,j)   +  dx_em*ddy_em
        emittance_arr(P(l)%sp,0,i+1,j+1) = emittance_arr(P(l)%sp,0,i+1,j+1) +  dx_em* dy_em
      end if
      
      theta_em = atan(P(l)%vz/P(l)%vx)
      dx_em = (theta_em+acos(-1.0)/2.0)/delta_theta
      dy_em = P(l)%z/delta_radial
      i = floor(dx_em)
      j = floor(dy_em)
      if(i>=0 .AND. i<iN_emit .AND. j>=0 .AND. j<iN_emit) then
        dx_em = dx_em - i
        dy_em = dy_em - j
        ddx_em = 1.0-dx_em
        ddy_em = 1.0-dy_em
        emittance_arr(P(l)%sp,1,i,j)     = emittance_arr(P(l)%sp,1,i,j)     + ddx_em*ddy_em
        emittance_arr(P(l)%sp,1,i,j+1)   = emittance_arr(P(l)%sp,1,i,j+1)   + ddx_em* dy_em
        emittance_arr(P(l)%sp,1,i+1,j)   = emittance_arr(P(l)%sp,1,i+1,j)   +  dx_em*ddy_em
        emittance_arr(P(l)%sp,1,i+1,j+1) = emittance_arr(P(l)%sp,1,i+1,j+1) +  dx_em* dy_em
      end if
    end if
  end do

  sizebuff = 12*2*(iN_emit + 1 )*(iN_emit + 1 )

  if(modulo(iter,10000) == 0) then
    if(rank==0) then
      allocate( buffer3(0:11,0:1,0:iN_emit,0:iN_emit) )
      do ii=0, numtasks-1
        if(i00(ii)*delta(0) < x_eg_pos .AND. i11(ii)*delta(0) >x_eg_pos) then
          call mpi_recv(buffer3(0,0,0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          emittance_arr = emittance_arr + buffer3
        end if
      end do
      deallocate( buffer3 )

      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='emittancey_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, iN_emit
        do j = 0, iN_emit
          write(65,'(2(1x, E11.4))',advance="no") i*delta_theta,j*delta_radial
          do ii=0,11
            write(65,'(1(1x, E11.4))',advance="no") emittance_arr(ii,0,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)


      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='emittancez_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, iN_emit
        do j = 0, iN_emit
          write(65,'(2(1x, E11.4))',advance="no") i*delta_theta,j*delta_radial
          do ii=0,11
            write(65,'(1(1x, E11.4))',advance="no") emittance_arr(ii,1,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)


    else
      if(i0*delta(0) < x_eg_pos .AND. i1*delta(0) >x_eg_pos) then
        call mpi_send(emittance_arr(0,0,0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end if
    end if

    emittance_arr = 0
  end if


end subroutine emittance


subroutine Current_density()

  use Parallel
  implicit none
  include 'mpif.h'

  integer i,j,l, ii, sizebuff
  real delta_radial, delta_radial_z
  real dz_em, ddz_em, dy_em, ddy_em

  delta_radial = Nyp*delta(1)/1000.0
  delta_radial_z = Nzp*delta(1)/1000.0	

  do l=1,NPART
    if(px_arr(l)<24e-3 .AND. P(l)%x>24e-3) then
      
      dz_em = P(l)%z/delta_radial_z
      dy_em = P(l)%y/delta_radial
      i = floor(dz_em)
      j = floor(dy_em)
      if(i>=0 .AND. i<1000 .AND. j>=0 .AND. j<1000) then
        dz_em = dz_em - i
        dy_em = dy_em - j
        ddz_em = 1.0-dz_em
        ddy_em = 1.0-dy_em
        current_den(P(l)%sp, i, j)     = current_den(P(l)%sp, i, j)     + ddz_em*ddy_em
        current_den(P(l)%sp, i, j+1)   = current_den(P(l)%sp, i, j+1)   + ddz_em* dy_em
        current_den(P(l)%sp, i+1, j)   = current_den(P(l)%sp, i+1, j)   +  dz_em*ddy_em
        current_den(P(l)%sp, i+1, j+1) = current_den(P(l)%sp, i+1, j+1) +  dz_em* dy_em
      end if
      
    end if
  end do

  sizebuff = 12*1001*1001

   if(modulo(iter,10000) == 0) then
    if(rank==0) then
      allocate( buffer4(0:11,0:1000,0:1000) )
      do ii=0, numtasks-1
        if(i00(ii)*delta(0) < 24e-3 .AND. i11(ii)*delta(0) >24e-3) then
          call mpi_recv(buffer4(0,0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          current_den = current_den + buffer4
        end if
      end do
      deallocate( buffer4 )

      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='current_den_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, 1000
        do j = 0, 1000
          write(65,'(2(1x, E11.4))',advance="no") i*delta_radial_z,j*delta_radial
          do ii=0,11
            write(65,'(1(1x, E11.4))',advance="no") current_den(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)

   else
   	 if(i0*delta(0) < 24e-3 .AND. i1*delta(0) >24e-3) then
   	     call mpi_send(current_den(0,0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
   	 end if
   end if
   	 current_den = 0
  end if


end subroutine Current_density


subroutine Print_Momentum_density()
use Parallel
  implicit none
  include 'mpif.h'

  integer  i,j,k,l,ii,jj,ip,jp,kp,kp2, sizebuff
  real mom_module, mom_angle	


  ip = modulo(modulo(rank,NprocX*NprocY),NprocX)
  jp = modulo(rank,NprocX*NprocY)/NprocX
  kp = rank/(NprocX*NprocY)
  
  output_plane_momx_xy=0
  output_plane_momy_xy=0
  allocate( buffer(0:i1-i0, 0:j1-j0) )
  allocate( bufferx(0:i1-i0, 0:j1-j0) )
  allocate( buffery(0:i1-i0, 0:j1-j0) )
  sizebuff = (i1-i0+1)*(j1-j0+1)

  do jj=0, 11

    Mom_e_x=0
    Mom_e_y=0
    mom_module=0 
    mom_angle=0


    do l=1,npart
      if( p(l)%sp == jj ) then
        call ChargeProjection_Second_Order_momentum(l)
      endif
    enddo
    call ChargeExchange_3planes(Mom_e_x)
    call ChargeExchange_3planes(Mom_e_y)

    momx_xy(jj,:,:,:) = momx_xy(jj,:,:,:) + Mom_e_x(:,:,:)
    momy_xy(jj,:,:,:) = momy_xy(jj,:,:,:) + Mom_e_y(:,:,:)

    if(modulo(iter, 10000) == 0) then
    
      do j=0,j1-j0
        do i=0, i1-i0
          bufferx(i,j) = Mom_e_x(i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(bufferx(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_momx_xy(jj, i00(ii)+i, j00(ii)+j) = bufferx(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(bufferx(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if


      do j=0,j1-j0
        do i=0, i1-i0
          bufferx(i,j) = momx_xy(jj, i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(bufferx(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_momx_xy_average(jj, i00(ii)+i, j00(ii)+j) = bufferx(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(bufferx(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if
      
      
      
      do j=0,j1-j0
        do i=0, i1-i0
          buffery(i,j) = Mom_e_y(i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(buffery(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_momy_xy(jj, i00(ii)+i, j00(ii)+j) = buffery(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(buffery(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if


      do j=0,j1-j0
        do i=0, i1-i0
          buffery(i,j) = momy_xy(jj, i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(buffery(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_momy_xy_average(jj, i00(ii)+i, j00(ii)+j) = buffery(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(buffery(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

      

      

    end if

  end do
  
  deallocate(buffer)
  deallocate(bufferx)
  deallocate(buffery)

  if(modulo(iter, 10000) == 0) then
    if(rank==0) then

      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='xy_mid_mom_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,11
	    mom_module=SQRT(output_plane_momx_xy(ii,i,j)*output_plane_momx_xy(ii,i,j) + output_plane_momy_xy(ii,i,j)*output_plane_momy_xy(ii,i,j)) 
    	    mom_angle=ATAN2(output_plane_momy_xy(ii,i,j) , output_plane_momx_xy(ii,i,j) )		

            write(65,'(4(1x, E11.4))',advance="no") output_plane_momx_xy(ii,i,j), output_plane_momy_xy(ii,i,j),mom_module, mom_angle
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='xy_mid_mom_A_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,11
	    
	    mom_module=SQRT(output_plane_momx_xy_average(ii,i,j)*output_plane_momx_xy_average(ii,i,j) + output_plane_momy_xy_average(ii,i,j)*output_plane_momy_xy_average(ii,i,j)) 
    	    mom_angle=ATAN2(output_plane_momy_xy_average(ii,i,j) , output_plane_momx_xy_average(ii,i,j) )	 

            write(65,'(4(1x, E11.4))',advance="no") output_plane_momx_xy_average(ii,i,j), output_plane_momy_xy_average(ii,i,j),mom_module, mom_angle
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
     

    end if

    momx_xy = 0
    momy_xy = 0
  end if

end subroutine Print_Momentum_density


subroutine Print_particle_position_meniscus()
	use Parallel
	implicit none
	integer :: l
	integer :: i=0,j=0,k=0
	real	:: distance(0:2)

	real :: Vavg
		write(str3, '(I7)') iter + rank 
		lgc = LEN_TRIM(ADJUSTL(str3))
		str = 'Pa_pos_Men'//str3(8-lgc:7)//'.dat'

		OPEN(UNIT=65, FILE=str, status='unknown')
		   write(65,*) Npart
				do l= 1, Npart
					i=P(l)%X/delta(0)
					j=P(l)%Y/delta(1)
					k=P(l)%Z/delta(2)
					distance(0)=1.*P(l)%X/delta(0)-i		
					distance(1)=1.*P(l)%Y/delta(1)-j
					distance(2)=1.*P(l)%Z/delta(2)-k

					Vavg = V(i,j,k)*( 1.0 - distance(0))*( 1.0 - distance(1))*( 1.0 - distance(2)) + V(i + 1,j,k)*distance(0)*( 1.0 - distance(1))*( 1.0 - distance(2)) + V(i,j + 1,k)*( 1.0 - distance(0))*distance(1)*( 1.0 - distance(2)) + V(i,j,k + 1)*( 1.0 - distance(0))*( 1.0 - distance(1))*distance(2) + V(i + 1,j + 1,k)*distance(0)*distance(1)*( 1.0 - distance(2)) + V(i+1,j,k + 1)*distance(0)*( 1.0 - distance(1))*distance(2) + V(i,j + 1,k + 1)*( 1.0 - distance(0))*distance(1)*distance(2) + V(i + 1,j + 1,k + 1)*distance(0)*distance(1)*distance(2)

					if( Vavg > 3.5 .AND. Vavg < 5.0) then
						write(65, '(1(1x,I8), 9(1x, E14.7) )') l, P(l)%X, P(l)%Y, P(l)%Z, P(l)%sp, P(l)%VX, P(l)%VY, P(l)%VZ, P(l)%IdentPart, Vavg
					endif
				end do
		close(65)
end subroutine Print_particle_position_meniscus




subroutine follow_particle_position_mpi()
	use Parallel
	implicit none
	include 'mpif.h'
	integer  i,ii
	integer, dimension(:) :: Part_followed_task(0:numtasks-1)
	integer :: Num_follow, Num_follow_total
	integer partial_counter	
	
	integer id_follow_part        

	id_follow_part=0	

	Num_follow=0
	Num_follow_total=0
	partial_counter=0
  



	     do i = 1, Npart	
		if(P(i)%IdentPart > 0.0  ) then			
			Num_follow=Num_follow+1	
			PLoss_followed(Num_follow) = P(i)						            	    	
		endif
           end do 
	
	   
	   call	MPI_GATHER(Num_follow,1, MPI_INTEGER, Part_followed_task, 1 ,MPI_INTEGER,0,MPI_COMM_WORLD,info)		  
	  
	 if (rank ==0) then	
		
			do  i=0, numtasks-1
				Num_follow_total = Part_followed_task(i) + Num_follow_total
			enddo	
			 
			str = 'Part_followed_numbers.dat'            		          
	             		OPEN(UNIT=3335, FILE=str, status="unknown", position= "append" )	
					write(3335,'(1(1x,I8))') Num_follow_total
	            	        close(3335)
	   endif



	if(rank == 0) then

	    PGain_followed(1:Part_followed_task(0)) = PLoss_followed(1:Part_followed_task(0)) 
    	    partial_counter= Part_followed_task(0) + 1 	
	    do ii=1,numtasks-1
		partial_counter= partial_counter 
     	 	call mpi_recv(PGain_followed(partial_counter:Part_followed_task(numtasks)), Part_followed_task(numtasks)*8,mpi_real, ii,51, mpi_comm_world, status, info)	 
    	    	partial_counter = partial_counter +  Part_followed_task(ii)	
	    end do
        else
    		call mpi_send(PLoss_followed(1:Num_follow), Num_follow*8,mpi_real, 0,51, mpi_comm_world, info)
  	endif
	   


	    if (rank ==0) then	
	      if(Num_follow_total>0) then	

		do i = 1, Num_follow_total	
			id_follow_part = PGain_followed(i)%IdentPart

			write(str3, '(I7)') id_follow_part
			lgc = LEN_TRIM(ADJUSTL(str3))
			str = 'Pa_track_'//str3(8-lgc:7)//'.dat'          		
			      	OPEN(UNIT=3333, FILE=str, status="unknown", position= "append" )		
	   			write(3333,'(1(1x,E14.7), 1(1x,I8) ,8(1x,E14.7) )') PGain_followed(i)%IdentPart,iter,PGain_followed(i)%X,PGain_followed(i)%Y,PGain_followed(i)%Z,PGain_followed(i)%sp,PGain_followed(i)%VX,PGain_followed(i)%VY,PGain_followed(i)%VZ, PGain_followed(i)%sp
	            	        close(3333)
		enddo

	      endif		
	    endif




end subroutine follow_particle_position_mpi


subroutine follow_particle_position_mpi2()
	use Parallel
	implicit none
	include 'mpif.h'
	integer  i,ii
		
	integer id_follow_part        

	id_follow_part=0	
 
	     do i = 1, Npart	
		if(P(i)%IdentPart > 0.0  ) then			
			id_follow_part = P(i)%IdentPart

			write(str3, '(I7)') id_follow_part
			lgc = LEN_TRIM(ADJUSTL(str3))
			str = 'Pa_track_'//str3(8-lgc:7)//'.dat'          		
			      	OPEN(UNIT=3333, FILE=str, status="unknown", position= "append" )		
	   			write(3333,'(1(1x,E14.7), 1(1x,I8) ,8(1x,E14.7) )') P(i)%IdentPart,iter,P(i)%X,P(i)%Y,P(i)%Z,P(i)%sp,P(i)%VX,P(i)%VY,P(i)%VZ, P(i)%sp
	            	        close(3333)							            	    	
		endif
           end do 

end subroutine follow_particle_position_mpi2


subroutine Print_magnetic_field()

  use Parallel
  implicit none
  include 'mpif.h'

  integer  i,j,k,ii,jj,ip,jp,jp2, kp,kp2, sizebuff, sizebuff2

  ip = modulo(modulo(rank,NprocX*NprocY),NprocX)
  jp = modulo(rank,NprocX*NprocY)/NprocX
  kp = rank/(NprocX*NprocY)
  
  output_plane_potential=0
  output_plane_potential_xz=0
  allocate( buffer(0:i1-i0, 0:j1-j0) )
  allocate( buffer2(0:i1-i0, 0:k1-k0) )
  sizebuff = (i1-i0+1)*(j1-j0+1)
  sizebuff2 = (i1-i0+1)*(k1-k0+1)


  potential_average(0,:,:,:) = potential_average(0,:,:,:) + V(:,:,:)
  potential_average(1,:,:,:) = potential_average(1,:,:,:) + Bxm(:,:,:)
  potential_average(2,:,:,:) = potential_average(2,:,:,:) + Bym(:,:,:)
  potential_average(3,:,:,:) = potential_average(3,:,:,:) + Bzm(:,:,:)

  if(modulo(iter, 10000) == 0) then
  
    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = V(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(0, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if
    
    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Bxm(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(1, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Bym(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(2, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Bzm(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(3, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do jj=0,3

      do j=0,j1-j0
        do i=0, i1-i0
          buffer(i,j) = potential_average(jj, i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_potential_average(jj, i00(ii)+i, j00(ii)+j) = buffer(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

    end do
    
    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = V(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(0, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Bxm(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(1, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Bym(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(2, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Bzm(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(3, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if
    
    do jj=0,3

      do k=0,k1-k0
        do i=0, i1-i0
          buffer2(i,k) = potential_average(jj, i+i0,j0,k0+k)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          jp2 = modulo(ii,NprocX*NprocY)/NprocX
          if(jp2 == NprocY/2) then
            call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
            do k=0,k1-k0
              do i=0, i1-i0
                output_plane_potential_average_xz(jj, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
              end do
            end do
          end if
        end do
      else
        if(jp == NprocY/2) then
          call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

    end do



  end if

  
  deallocate(buffer)
  deallocate(buffer2)

  if(modulo(iter, 10000) == 0) then
    if(rank==0) then

      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))


      str='B_xy_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='B_xz_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do k = 0, nzp-1
          j =nyp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential_xz(ii,i,k)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      
    end if

    potential_average = 0
  end if

end subroutine Print_magnetic_field




subroutine Print_potential_YZ()

  use Parallel
  implicit none
  include 'mpif.h'

  integer i,j,k,l, ii, sizebuff3
  integer :: i_eg	
  real :: x_eg_pos		


  allocate( bufferyz(0:j1-j0, 0:k1-k0) )

  x_eg_pos = 0.025  !Position at which I extract the potential map in the YZ plane
  x_eg_pos = (The_Object(4)%center(0) - The_Object(4)%width(0))/delta(0)
  sizebuff3 = (j1-j0+1)*(k1-k0+1)   
  
  output_plane_potential_yz=0 

  if( modulo(iter,10000) == 0 ) then
    do k=0,k1-k0
	do j=0,j1-j0
		if( x_eg_pos > i0 .AND. x_eg_pos< i1 )then
			i_eg= int(x_eg_pos/delta(0))	
			bufferyz(j,k) = V( i_eg, j+j0, k+k0 )
		endif		
	end do
    end do	
	

	if(rank==0) then	
		do  ii =1,numtasks-1
			if( x_eg_pos > i00(ii) .AND. x_eg_pos< i11(ii) ) then
				call mpi_recv( bufferyz(0,0), sizebuff3, mpi_real, ii, 51, mpi_comm_world, status, info )
				do k = 0 , k1-k0					
					do j = j0, j1-j0
						output_plane_potential_yz( j00(ii) + j, k00(ii) + k ) = bufferyz(j,k)
					end do
				end do
			endif	
		end do
	else	
		if( x_eg_pos > i0 .AND. x_eg_pos< i1 ) then
			call mpi_send(bufferyz(0,0), sizebuff3, mpi_real,0,51,mpi_comm_world, info )
		endif
	end if
  endif


  deallocate(bufferyz)
  
  if( modulo(iter,5000)==0) then
	if( rank == 0 ) then
		write(str3, '(i7)') iter
		lgc=len_trim(adjustl(str3))
		str = 'PoteA_YZ'//str3(8-lgc:7)//'.dat'
		open(unit=65, file=str, status="unknown")
		do j=0,Nyp-1
			do k=0,Nzp-1
				i= int(x_eg_pos/delta(0))		
				write(65, '(4(1x,E11.4))', advance = "no" ) i*delta(0),j*delta(1),k*delta(2),output_plane_potential_yz(j,k)
				write(65,*)	
			end do
			write(65,*)
		enddo
		close(65)		
	endif 

  endif	



end subroutine Print_potential_YZ


subroutine Print_potential3()

  use Parallel
  implicit none
  include 'mpif.h'

  integer  i,j,k,ii,jj,ip,jp,jp2, kp,kp2, sizebuff, sizebuff2, sizebuff3
  real :: i_wall
  integer :: i_eg	
  real :: x_eg_pos	


  i_wall = (The_Object(2)%center(0) - The_Object(2)%width(0))/delta(0)

  x_eg_pos = (The_Object(4)%center(0) - The_Object(4)%width(0) ) -0.0015  !Position at which the potential map in the YZ plane is extracted
  i_eg= int(x_eg_pos/delta(0)) 

  ip = modulo(modulo(rank,NprocX*NprocY),NprocX)
  jp = modulo(rank,NprocX*NprocY)/NprocX
  kp = rank/(NprocX*NprocY)
  
  output_plane_potential=0

  output_plane_potential_xz=0

  output_plane_potential_yz=0

  allocate( buffer(0:i1-i0, 0:j1-j0) )
  allocate( buffer2(0:i1-i0, 0:k1-k0) )
  allocate( bufferyz(0:j1-j0, 0:k1-k0) )

  sizebuff = (i1-i0+1)*(j1-j0+1)
  sizebuff2 = (i1-i0+1)*(k1-k0+1)
  sizebuff3 = (j1-j0+1)*(k1-k0+1)   

  potential_average(0,:,:,:) = potential_average(0,:,:,:) + V(:,:,:)
  potential_average(1,:,:,:) = potential_average(1,:,:,:) + Exm(:,:,:)
  potential_average(2,:,:,:) = potential_average(2,:,:,:) + Eym(:,:,:)
  potential_average(3,:,:,:) = potential_average(3,:,:,:) + Ezm(:,:,:)


  if ( modulo(iter, 5000)==0 .AND. iter > 100000  ) then
    if ( i_wall > i0 .and. i_wall < i1 ) then
     write(str3,'(I7)') iter + rank
      lgc=LEN_TRIM(ADJUSTL(str3))
      str='PoteSliceA_'//str3(8-lgc:7)//'.dat'
      OPEN(UNIT=65, FILE=str, status="unknown")
        do i = i0, i1
          do j = i0, i1
            do k = k0, k1
            	write(65,'(4(1x, E11.4))') i*delta(0),j*delta(1),k*delta(2),potential_average(0,i,j,k)
            enddo	  
	  end do
         end do
      close(65)
     endif	 
 endif




  if(modulo(iter, 10000) == 0) then
  
    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = V(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(0, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if
    
    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Exm(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(1, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Eym(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(2, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do j=0,j1-j0
      do i=0, i1-i0
        buffer(i,j) = Ezm(i+i0,j+j0,k0)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        kp2 = ii/(NprocX*NprocY)
        if(kp2 == NprocZ/2) then
          call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
          do j=0,j1-j0
            do i=0, i1-i0
              output_plane_potential(3, i00(ii)+i, j00(ii)+j) = buffer(i,j)
            end do
          end do
        end if
      end do
    else
      if(kp == NprocZ/2) then
        call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do jj=0,3

      do j=0,j1-j0
        do i=0, i1-i0
          buffer(i,j) = potential_average(jj, i+i0,j+j0,k0)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          kp2 = ii/(NprocX*NprocY)
          if(kp2 == NprocZ/2) then
            call mpi_recv(buffer(0,0), sizebuff, mpi_real, ii,51, mpi_comm_world, status, info)
            do j=0,j1-j0
              do i=0, i1-i0
                output_plane_potential_average(jj, i00(ii)+i, j00(ii)+j) = buffer(i,j)
              end do
            end do
          end if
        end do
      else
        if(kp == NprocZ/2) then
          call mpi_send(buffer(0,0), sizebuff,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

    end do
    
    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = V(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(0, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Exm(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(1, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Eym(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(2, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if

    do k=0,k1-k0
      do i=0, i1-i0
        buffer2(i,k) = Ezm(i+i0,j0,k0+k)
      end do
    end do

    if(rank == 0) then
      do ii=1,numtasks-1
        jp2 = modulo(ii,NprocX*NprocY)/NprocX
        if(jp2 == NprocY/2) then
          call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
          do k=0,k1-k0
            do i=0, i1-i0
              output_plane_potential_xz(3, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
            end do
          end do
        end if
      end do
    else
      if(jp == NprocY/2) then
        call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
      end  if
    end if
    
    do jj=0,3

      do k=0,k1-k0
        do i=0, i1-i0
          buffer2(i,k) = potential_average(jj, i+i0,j0,k0+k)
        end do
      end do

      if(rank == 0) then
        do ii=1,numtasks-1
          jp2 = modulo(ii,NprocX*NprocY)/NprocX
          if(jp2 == NprocY/2) then
            call mpi_recv(buffer2(0,0), sizebuff2, mpi_real, ii,51, mpi_comm_world, status, info)
            do k=0,k1-k0
              do i=0, i1-i0
                output_plane_potential_average_xz(jj, i00(ii)+i, k00(ii)+k) = buffer2(i,k)
              end do
            end do
          end if
        end do
      else
        if(jp == NprocY/2) then
          call mpi_send(buffer2(0,0), sizebuff2,mpi_real, 0,51, mpi_comm_world, info)
        end  if
      end if

    end do
   	
    do k=0,k1-k0
	do j=0,j1-j0
		if( i_eg > i0 .AND. i_eg< i1 )then			   	
			bufferyz(j,k) = V( i_eg, j+j0, k+k0 )
		endif		
	end do
     end do	

	if(rank==0) then	
		do  ii =1,numtasks-1
			if( i_eg > i00(ii) .AND. i_eg< i11(ii) ) then
				write(*,*) "rank ", ii, "  contains the EG" 
				call mpi_recv( bufferyz(0,0), sizebuff3, mpi_real, ii, 51, mpi_comm_world, status, info )
				do k = 0 , k1-k0 					
					do j = 0, j1-j0
						output_plane_potential_yz( j00(ii) + j, k00(ii) + k ) = bufferyz(j,k)
					end do
				end do
			endif	
		end do
	else	
		if( i_eg > i0 .AND. i_eg< i1 ) then
			call mpi_send(bufferyz(0,0), sizebuff3, mpi_real,0,51,mpi_comm_world, info )
		endif
	end if
	


  end if

  
  deallocate(buffer)
  deallocate(buffer2)
  deallocate(bufferyz)
 
 
   if( modulo(iter, 10000)==0) then
   if( rank == 0 ) then
		write(str3, '(i7)') iter
		lgc=len_trim(adjustl(str3))
		str = 'PoteA_YZ_'//str3(8-lgc:7)//'.dat'
		open(unit=65, file=str, status="unknown")
		do j=0,Nyp -1
			do k=0,Nzp -1
				i= int(x_eg_pos/delta(0))		
				write(65, '(4(1x,E11.4))', advance = "no" ) i*delta(0),j*delta(1),k*delta(2),output_plane_potential_yz(j,k)
				write(65,*)	
			end do
			write(65,*)
		enddo
		close(65)		
	endif 

  endif	

  if(modulo(iter, 5000) == 0) then
    if(rank==0) then

           
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='PoteA_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential_average(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='Poten_xz_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do k = 0, nzp-1
          j =nyp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential_xz(ii,i,k)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)
      
      write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='PoteA_xz_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do k = 0, nzp-1
          j =nyp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential_average_xz(ii,i,k)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
      close(65)


    endif

    potential_average = 0
  endif
  
  
  
    if(modulo(iter, 5000) == 0) then
    if(rank==0) then
	
	 write(str3,'(i7)') iter
      lgc=len_trim(adjustl(str3))

      str='Poten_'//str3(8-lgc:7)//'.dat'
      open(unit=65, file=str, status="unknown")

      do i = 0, nxp-1
        do j = 0, nyp-1
          k =nzp/2
          write(65,'(3(1x, E11.4))',advance="no") i*delta(0),j*delta(1),k*delta(2)
          do ii=0,3
            write(65,'(1(1x, E11.4))',advance="no") output_plane_potential(ii,i,j)
          end do
          write(65,*)
        end do
        write(65,*)
      end do
      
     close(65)
	 endif




    potential_average = 0
  endif

end subroutine Print_potential3

subroutine Print_particle_position_EG()
	!This subroutine is used the obtain the input that is given to IBSimu	
	use Parallel
	implicit none
	integer :: l
	integer :: i=0,j=0,k=0
	real ::	x_eg_pos
	x_eg_pos = (The_Object(4)%center(0) - The_Object(4)%width(0)  ) - 1.0*delta(0)

	write(str3, '(I7)') rank
	lgc = LEN_TRIM(ADJUSTL(str3))
	str = 'Pa_pos_EG_'//str3(8-lgc:7)//'.dat'
 
	OPEN(UNIT=65, FILE=str, status='unknown',position="append")
		do l=1, Npart
			if(P(l)%X > x_eg_pos .AND. px_arr(l) < x_eg_pos) then
				write(65, '(7(1x, E14.7) )') P(l)%X, P(l)%Y, P(l)%Z, P(l)%VX, P(l)%VY, P(l)%VZ, P(l)%sp
			endif
		enddo
end subroutine 



