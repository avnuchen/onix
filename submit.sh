#!/usr/bin/bash
#SBATCH --time=00:01:00
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=20
#SBATCH --cpus-per-task 1
#SBATCH --partition=cpu
#SBATCH --job-name=ONIX_job
#SBATCH --error  myJob_ONIX_%j.err      # std-error file
#SBATCH --output myJob_ONIX_%j.out      # std-output file  
#SBATCH --mail-type=ALL 
#SBATCH --mail-user=anna.vnuchenko@cern.ch
#SBATCH --exclusive 
#SBATCH --hint=nomultithread  
                                                                                 
ulimit -s unlimited

module load mpi/mvapich2/2.2

FOLDER_NAME=${SLURM_JOB_NAME}_${SLURM_JOBID}
BINARY=ONIX_IPP

mkdir $FOLDER_NAME
cp {$BINARY,submit.sh} $FOLDER_NAME
cp *.dat $FOLDER_NAME
cp *.in $FOLDER_NAME
cp ELISE_Ue $FOLDER_NAME
cp IS03_field* $FOLDER_NAME 
cp *.f90 $FOLDER_NAME
#mv PaPos/* $FOLDER_NAME

cd $FOLDER_NAME
srun -n 160 ./$BINARY>myoutput

mv ../myJob_ONIX_${SLURM_JOBID}.err .
mv ../myJob_ONIX_${SLURM_JOBID}.out .
